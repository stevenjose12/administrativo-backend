<!DOCTYPE html>
<html>
    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>NOTA DE ENTREGA - {{ $file->code }}</title>
    </head>
    <body>
		<td>
			@if(!empty($enterprise->person->avatar))
				<img src="{{ public_path().'/'.$enterprise->person->avatar }}" height="100" width="120"/>
			@endif
		</td>
		<td>
			<b>{{ $enterprise->name }}</b>
		</td>
	</body>
</html>
