<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>NOTA DE ENTREGA - {{ $file->code }}</title>
	<link rel="stylesheet" href="{{ asset('stylesheets/styles.css') }}" />
</head>

<body>
	<!-- main -->
	<div class="page-break">
		<main>
			<!-- header -->
			<div class="header">
				<div class="flex">
					<div class="stylesheet-logo">
						@if(!empty($enterprise->avatar))
						<img src="{{ asset($enterprise->avatar) }}" class="img" height="100" width="150" />
						@endif
					</div>
					<div class="stylesheet-letterhead">
						<div class="headletterhead">
							<h2 class="h2-head">{{ $enterprise_name }}</h2>
							<p class="font-address">
								{{ $enterprise->direction }}
								<br>{{ $enterprise->phone }}
								<br> {{ $enterprise->email }}
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="content-header">

				<h3 class="font-title"> RIF.:{{ $enterprise->fiscal_identification ? $enterprise->fiscal_identification : $enterprise->identity_document  }} </h3>

				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr class="left">
						<th class="text-center" colSpan="8">
							NOTA DE ENTREGA
						</th>
					</tr>
					<tr class="left">
						<td colSpan="4">
							<span class="uppercase">NOMBRE: {{ $file->client->first_name }}</span>
						</td>
						<td class="text-right" colSpan="4">
							<span>N/E: {{ $file->code }}</span>
						</td>
					</tr>
					<tr class="left">
						<td colSpan="4">
							<span class="uppercase">DIRECCIÓN: {{ $file->client->direction }}</span>
						</td>
						<td class="text-right" colSpan="4">
							<span>Fecha: {{ date("d/m/Y", strtotime($file->date_emission)) }} </span>
						</td>
					</tr>
				</table>
				<table width="100%" border="1" cellspacing="0" cellpadding="0">
					<thead>
						<tr class="text-center">
							<th width="25%">EQUIPO</th>
							<th width="25%">SERIAL</th>
							<th width="10%">CANTIDAD</th>
							<th width="20%">PRECIO UND</th>
							<th width="20%">TOTAL</th>
						</tr>
					</thead>
					<tbody>
						@php
							$i = 0;
						@endphp
						@foreach($file->details as $key => $product)
						<tr class="text-center">
							<td>{{ $product->name }}</td>
							<td>
								@if($product->serialization)
									{{implode('/ ',$serials[$i])}}
								@endif
								@php
									$i++;
								@endphp
							</td><!-- <td>{{ $product->name }}</td> -->
							<td>{{ $product->pivot->quantity }}</td>
							<td>{{ number_format($product->pivot->subtotal / $product->pivot->quantity, 2, ",", ".") }}</td>
							<td>{{ number_format($product->pivot->subtotal, 2, ",", ".") }}</td>
						</tr>
						@endforeach
						<tr>
							<td rowspan="6" colspan="2">
								<span>FORMA DE PAGO:</span>
								@foreach($file->method_payment as $key => $payment)
								@if($payment->type == 1)
								EFECTIVO
								@elseif($payment->type == 2)
								CREDITO
								@elseif($payment->type == 3)
								DEBITO
								@elseif($payment->type == 4)
								TRANSFERENCIA
								@elseif($payment->type == 5)
								A CREDITO
								@endif
								@endforeach
							</td>
							<td class="text-left" colspan="2">SUBTOTAL</td>
							<td class="text-center">{{ number_format($file->subtotal, 2, ",", ".") }}</td>
						</tr>
						<tr>
							<td class="text-left" colspan="2">DESCUENTO</td>
							<td class="text-center">{{ number_format($file->discount_percentage, 2, ",", ".") }}</td>
						</tr>
						<tr>
							<td class="text-left" colspan="2">EXENTO</td>
							<td class="text-center">{{ number_format($file->exempt, 2, ",", ".") }}</td>
						</tr>
						<tr>
							<td class="text-left" colspan="2">BASE IMPONIBLE</td>
							<td class="text-center">{{ number_format($file->taxable, 2, ",", ".") }}</td>
						</tr>
						<tr>
							<td class="text-left" colspan="2">IVA 16%</td>
							<td class="text-center">{{ number_format($file->vat, 2, ",", ".") }}</td>
						</tr>
						<tr>
							<td class="text-left" colspan="2">TOTAL</td>
							<td class="text-center">{{ number_format($file->total, 2, ",", ".") }}</td>
						</tr>
					</tbody>
				</table>
				<div class="text-center">
					<table>
						<thead>
							<tr>
								<td></td>
								<td class="font-weight-light border-black">ENTREGADO POR:</td>
								<td></td>
								<td class="font-weight-light border-black">RECIBIDO POR:</td>
								<td></td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width="3.5%"></td>
								<td width="31%"></td>
								<td width="3.5%"></td>
								<td width="31%"></td>
								<td width="3.5%"></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</main>
	</div>

	@if(count($file->details) <= 5) <hr class="hr">
		@endif

		<div class="page-break">
			<main>
				<!-- header -->
				<div class="header">
					<div class="flex">
						<div class="stylesheet-logo" style="page-break-before: {{ count($file->details) > 6 ? 'always' : '' }}">
							@if(!empty($enterprise->avatar))
							<img src="{{ asset($enterprise->avatar) }}" class="img" height="100" width="150" />
							@endif
						</div>
						<div class="stylesheet-letterhead">
							<div class="headletterhead">
								<h2 class="h2-head">{{ $enterprise_name }}</h2>
								<p class="font-address">
									{{ $enterprise->direction }}
									<br>{{ $enterprise->phone }}
									<br> {{ $enterprise->email }}
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="content-header">

					<h3 class="font-title"> RIF.:{{ $enterprise->fiscal_identification ? $enterprise->fiscal_identification : $enterprise->identity_document  }} </h3>

					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr class="left">
							<th class="text-center" colSpan="8">
								NOTA DE ENTREGA
							</th>
						</tr>
						<tr class="left">
							<td colSpan="4">
								<span class="uppercase">NOMBRE: {{ $file->client->first_name }}</span>
							</td>
							<td class="text-right" colSpan="4">
								<span>N/E: {{ $file->code }}</span>
							</td>
						</tr>
						<tr class="left">
							<td colSpan="4">
								<span class="uppercase">DIRECCIÓN: {{ $file->client->direction }}</span>
							</td>
							<td class="text-right" colSpan="4">
								<span>Fecha: {{ date("d/m/Y", strtotime($file->date_emission)) }} </span>
							</td>
						</tr>
					</table>
					<table width="100%" border="1" cellspacing="0" cellpadding="0">
						<thead>
							<tr class="text-center">
								<th width="25%">EQUIPO</th>
								<th width="25%">SERIAL</th>
								<th width="10%">CANTIDAD</th>
								<th width="20%">PRECIO UND</th>
								<th width="20%">TOTAL</th>
							</tr>
						</thead>
						<tbody>
							@php
								$j = 0;
							@endphp
							@foreach($file->details as $key => $product)
							<tr class="text-center">
								<td>{{ $product->name }}</td>
								<td>
									@if($product->serialization)
										{{implode('/ ',$serials[$j])}}
									@endif
									@php
										$j++;
									@endphp
								</td><!-- <td>{{ $product->name }}</td> -->
								<td>{{ $product->pivot->quantity }}</td>
								<td>{{ number_format($product->pivot->subtotal / $product->pivot->quantity, 2, ",", ".") }}</td>
								<td>{{ number_format($product->pivot->subtotal, 2, ",", ".") }}</td>
							</tr>
							@endforeach
							<tr>
								<td rowspan="6" colspan="2">
									<span>FORMA DE PAGO:</span>
									@foreach($file->method_payment as $key => $payment)
									@if($payment->type == 1)
									EFECTIVO
									@elseif($payment->type == 2)
									CREDITO
									@elseif($payment->type == 3)
									DEBITO
									@elseif($payment->type == 4)
									TRANSFERENCIA
									@elseif($payment->type == 5)
									A CREDITO
									@endif
									@endforeach
								</td>
								<td class="text-left" colspan="2">TOTAL</td>
								<td class="text-center">{{ number_format($file->subtotal, 2, ",", ".") }}</td>
							</tr>
							<tr>
								<td class="text-left" colspan="2">DESCUENTO</td>
								<td class="text-center">{{ number_format($file->discount_percentage, 2, ",", ".") }}</td>
							</tr>
							<tr>
								<td class="text-left" colspan="2">EXENTO</td>
								<td class="text-center">{{ number_format($file->exempt, 2, ",", ".") }}</td>
							</tr>
							<tr>
								<td class="text-left" colspan="2">BASE IMPONIBLE</td>
								<td class="text-center">{{ number_format($file->taxable, 2, ",", ".") }}</td>
							</tr>
							<tr>
								<td class="text-left" colspan="2">IVA 16%</td>
								<td class="text-center">{{ number_format($file->vat, 2, ",", ".") }}</td>
							</tr>
							<tr>
								<td class="text-left" colspan="2">TOTAL</td>
								<td class="text-center">{{ number_format($file->total, 2, ",", ".") }}</td>
							</tr>
						</tbody>
					</table>
					<div class="text-center">
						<table>
							<thead>
								<tr>
									<td></td>
									<td class="font-weight-light border-black">ENTREGADO POR:</td>
									<td></td>
									<td class="font-weight-light border-black">RECIBIDO POR:</td>
									<td></td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td width="3.5%"></td>
									<td width="31%"></td>
									<td width="3.5%"></td>
									<td width="31%"></td>
									<td width="3.5%"></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</main>
		</div>
		<script type="text/php">
			if (isset($pdf)) {
			$text = "Page {PAGE_NUM} / {PAGE_COUNT}";
			$size = 10;
			$font = $fontMetrics->getFont("Calibri");
			$width = $fontMetrics->get_text_width($text, $font, $size) / 2;
			$x = ($pdf->get_width() - $width) / 2;
			$y = $pdf->get_height() - 35;
			$pdf->page_text($x, $y, $text, $font, $size);
		}
	</script>
</body>

</html>