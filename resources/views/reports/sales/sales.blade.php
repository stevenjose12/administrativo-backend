<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
		<title>REPORTE DE VENTAS</title>
    </head>
    <body>
    <!-- Headings -->
    <td><h1>LISTADO DE VENTAS</h1></td>
    <!--  Bold -->
    <table>
        <thead>
            <tr>
                <th>CLIENTE</th>
                <th>VENDEDOR</th>
                <th>Nº</th>
                <th>FECHA DE EMISIÓN</th>
                <th>HORA</th>
                <th>TIPO</th>
                <th>EFECTIVO</th>
                <th>DEBITO</th>
                <th>T.CREDITO</th>
                <th>TRANSFERENCIA</th>
                <th>A CREDITO</th>
                <th>MONTO</th>
            </tr>
        </thead>
        <tbody>
            @foreach($reports as $key => $item)
                <tr>
                    <td>{{ $item->client->person->first_name }}</td>
                    <td>{{ $item->seller->person->first_name }}</td>
                    <td>{{ $item->code }}</td>
                    <td>{{ $item->date_emission }}</td>
                    <td>{{ $item->time_emission }}</td>
                    <td>{{ $item->type }}</td>
                    <td>{{ $item->cash }}</td>
                    <td>{{ $item->debit }}</td>
                    <td>{{ $item->credit }}</td>
                    <td>{{ $item->transfer }}</td>
                    <td>{{ $item->deadlines }}
                    <td>{{ $item->total }}</td>
                </tr>
            @endforeach
            <tr>
                <th colSpan="6">MONTOS TOTALES DE REPORTE DE VENTAS</th>
                <td>{{ $amounts['amount_cash'] }}</td>
                <td>{{ $amounts['amount_debit'] }}</td>
                <td>{{ $amounts['amount_credit'] }}</td>
                <td>{{ $amounts['amount_transfer'] }}</td>
                <td>{{ $amounts['amount_deadlines'] }}</td>
                <td>{{ $amounts['amount_total'] }}</td>
            </tr>
        </tbody>
    </table>
</html>
