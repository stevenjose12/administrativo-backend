<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
            <title>Productos</title>
    </head>
    <body>
        <h1>Lista de Productos</h1>
        <table>
            <thead>
                <tr>
                    <th>Código</th>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Estatus</th>
                    <th>Porcentaje de ganancia</th>
                    <th>Porcentaje de comisión</th>
                    <th>Exento</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($products as $product)
                    <tr>
                        <td>{{ $product->code }}</td>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->description }}</td>
                        <td>{{ $product->status == 0 ? 'Desactivado' : 'Activado' }}</td>
                        <td>{{ $product->percentage_earning }}</td>
                        <td>{{ $product->percentage_commission }}</td>
                        <td>{{ $product->exempt }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </body>
</html>
