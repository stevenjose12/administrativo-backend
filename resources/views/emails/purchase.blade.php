<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<title>Nuevo Pedido</title>
	<style type="text/css">
		.container {
			text-align: center;
			font-family: Calibri;
			padding: 40px;
		}
		img {
			width: 350px;
		}
		.title {
			text-transform: uppercase;
			font-weight: 300;
			font-size: 30px;
			margin-top: 40px;
		}
		button {
			margin-top: 20px;
			text-transform: uppercase;
			font-weight: 300;
			width: 250px;
			border-radius: 3px;
			padding: 15px;
			text-align: center;
			outline: 0px !important;
			font-size: 16px !important;
			border: 0px !important;
			background-color: #3281e2 !important;
			color: #fff !important;
			cursor: pointer;
		}
		.codigo {
			font-size: 30px;
			text-transform: uppercase;
			font-weight: bold;
		}
		table.table {
			width: 100%;
			text-align: center;
		}
		table.table {
		  border-collapse: collapse;
		  border-bottom: 1px solid black !important;
		}
		table.table, table.table th, table.table td {
		  border: 1px solid black;
		}
		table.table th {
			font-weight: 600;
		}
	</style>
</head>
<body>
	<div class="container">
		<img src="{{ URL('img/logo.png') }}" />
		<h4 class="title">Nuevo Pedido</h4>
		<p><strong>Código:</strong> {{ $purchase->code }}</p>
		<p><strong>Método de Pago:</strong> {{ $purchase->method->name }}</p>
		<p><strong>Vendedor:</strong> {{ $purchase->user->person->name.' '.$purchase->user->person->lastname }}</p>
		<p><strong>Cliente:</strong> {{ $purchase->client->name }}</p>
		<p><strong>Total:</strong> {{ Money::get($purchase->total) }}</p>
		<p><strong>Fecha:</strong> {{ \Carbon\Carbon::parse($purchase->created_at)->format('d/m/Y H:i') }}</p>

		<table class="table" style="border-bottom: 1px solid black">
			<thead>
				<tr>
					<th>Producto</th>
					<th>Cantidad</th>
					<th>Precio Unitario</th>
					<th>Total</th>
				</tr>
			</thead>
			<tbody>
				@foreach($purchase->details as $product)
					<tr>
						<td>{{ $product->product->name }}</td>
						<td>{{ $product->quantity }}</td>
						<td>{{ Money::get($product->price) }}</td>
						<td>{{ Money::get($product->total) }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</body>
</html>