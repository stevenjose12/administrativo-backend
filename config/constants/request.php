<?php
    return [
        'status'  => [
            'REQUEST_NOT_PROCESSED' => 0,
            'REQUEST_PROCESSED' => 1,
            'REQUEST_CANCELED' => 2,
        ]
    ];
