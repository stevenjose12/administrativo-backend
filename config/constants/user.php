<?php
    return [
        'levels' => [
            'SUPERADMINISTRATOR' => 1,
            'ADMINISTRATOR'      => 2,
            'USER_ENTERPRISE'    => 3,
            'SUB_USER'           => 4,
            'CLIENT'             => 5,
            'PROVIDER'           => 6
        ],
        'roles' => [
            'SUPERADMINISTRATOR' => 1,
            'ADMINISTRATOR'      => 2,
            'ENTERPRISE'         => 3,
            'SUB_USER'           => 4,
            'SELLER'             => 5,
        ]
    ];