<?php
    return [
        'payment_types'  => [
            'CASH' => 1,
            'CREDIT' => 2,
        ],
        'method_types'  => [
            'CASH' => 1,
            'CREDIT' => 2,
            'DEBIT' => 3,
            'TRANSFER' => 4
        ],
        'status' => [
            'PENDING' => 0,
            'PROCESSED' => 1
        ]
    ];