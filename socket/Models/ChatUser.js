const sequelize = require('../conexion')
const Seq = require('sequelize')

const ChatUser = sequelize.define('ChatUser',{
	id: {
		type: Seq.INTEGER,
		primaryKey: true,
		autoIncrement: true
	},
	user_id: Seq.INTEGER,
	chat_id: Seq.INTEGER
},{
	createdAt: 'created_at',
	updatedAt: 'updated_at',
	deletedAt: 'deleted_at',
	tableName: 'chats_users'
})

module.exports = ChatUser