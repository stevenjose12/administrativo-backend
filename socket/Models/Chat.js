const sequelize = require('../conexion')
const Seq = require('sequelize')

const Chat = sequelize.define('Chat',{
	id: {
		type: Seq.INTEGER,
		primaryKey: true,
		autoIncrement: true
	},
},{
	createdAt: 'created_at',
	updatedAt: 'updated_at',
	deletedAt: 'deleted_at',
	tableName: 'chats'
})

module.exports = Chat