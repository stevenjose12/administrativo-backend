module.exports = Models => {

	Models.ChatUser.belongsTo(Models.User,{
	    as: 'user',
	    foreignKey: 'user_id'
	})

	Models.ChatUser.belongsTo(Models.Chat,{
	    as: 'chat',
	    foreignKey: 'chat_id'
	})

}