module.exports = Models => {

	Models.Message.belongsTo(Models.User,{
	    as: 'user',
	    foreignKey: 'user_id'
	})

	Models.Message.belongsTo(Models.Chat,{
	    as: 'chat',
	    foreignKey: 'chat_id'
	})

}