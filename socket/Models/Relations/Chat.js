module.exports = Models => {

	Models.Chat.belongsToMany(Models.User,{
	    as: 'users',
	    foreignKey: 'chat_id',
	    through: Models.ChatUser,
	    otherKey: 'user_id'
	})

	Models.Chat.hasMany(Models.Message,{
	    as: 'messages',
	    foreignKey: 'chat_id'
	})

}