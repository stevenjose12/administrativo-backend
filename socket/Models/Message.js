const sequelize = require('../conexion')
const Seq = require('sequelize')

const Message = sequelize.define('Message',{
	id: {
		type: Seq.INTEGER,
		primaryKey: true,
		autoIncrement: true
	},
	user_id: Seq.INTEGER,
	file: Seq.STRING,
	text: Seq.TEXT,
	view: Seq.INTEGER 
},{
	createdAt: 'created_at',
	updatedAt: 'updated_at',
	deletedAt: 'deleted_at',
	tableName: 'messages'
})

module.exports = Message