const Models = {
    Chat: require('./Chat'),
    ChatUser: require('./ChatUser'),
    Message: require('./Message'),
    Person: require('./Person'),
    User: require('./User'),
}

require('./Relations/Chat')(Models)
require('./Relations/ChatUser')(Models)
require('./Relations/Message')(Models)
require('./Relations/Person')(Models)
require('./Relations/User')(Models)

module.exports = Models