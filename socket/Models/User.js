const sequelize = require('../conexion')
const Seq = require('sequelize')

const User = sequelize.define('User',{
	id: {
		type: Seq.INTEGER,
		primaryKey: true,
		autoIncrement: true
	},
	email: Seq.STRING,
	password: Seq.STRING,
	gmail: Seq.INTEGER,
	facebook: Seq.INTEGER,
	level: Seq.INTEGER,
	status: Seq.INTEGER,
	remember_token: Seq.STRING
},{
	createdAt: 'created_at',
	updatedAt: 'updated_at',
	deletedAt: 'deleted_at',
	tableName: 'users'
})

module.exports = User