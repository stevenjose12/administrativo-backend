const sequelize = require('../conexion')
const Seq = require('sequelize')

const Person = sequelize.define('Person',{
	id: {
		type: Seq.INTEGER,
		primaryKey: true,
		autoIncrement: true
	},
	name: Seq.STRING,
	lastname: Seq.STRING,
	phone: Seq.STRING,
	document: Seq.STRING,
	image: Seq.STRING,
	user_id: Seq.INTEGER
},{
	createdAt: 'created_at',
	updatedAt: 'updated_at',
	deletedAt: 'deleted_at',
	tableName: 'persons'
})

module.exports = Person