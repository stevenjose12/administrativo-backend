const Models = require('../Models')

const create = async (io, data) => {
	const message = await Models.Message.create(data);
	const _message = await Models.Message.findOne({
		where: {
			id: message.id
		},
		include: ['chat',{
			as: 'user',
			model: Models.User,
			include: ['person']
		}]
	})
	io.emit('message',_message);
}

module.exports = {
	create
}