const express = require('express')
const app = express()
const server = require('http').Server(app)
const io = require('socket.io')(server)
const ChatController = require('./Controllers/ChatController')

io.on('connection',socket => {
	
	socket.on('activate-user',data => {
		io.emit('activate-user',data)
	})

	socket.on('disable-user',data => {
		io.emit('disable-user',data)
	})

	socket.on('message',data => {
		ChatController.create(io,data)
	})
})

app.get('/',(req,res) => {
	res.status(403).send("403 Acceso Denegado")
})

server.listen(11032)