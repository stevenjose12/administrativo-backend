<?php

Route::group(['middleware' => 'api', 'namespace' => 'Api'], function () {
    Route::post('login', 'Auth\AuthController@login')->name('login');
    Route::group(['middleware' => 'api', 'namespace' => 'Admin'], function () {
        /**
         * Menu by user's
         */
        Route::post('admin/modules/get-module', 'Menu\ModuleController@getModules');
        Route::post('admin/modules/enterprise', 'Menu\ModuleController@getModuleByEnterprise');
        Route::post('admin/modules/get-module-user', 'Menu\ModuleController@getModuleByUser');
        Route::post('admin/modules/seller', 'Menu\ModuleController@getModuleBySeller');
        /**
         * Warehouses
         */
        Route::get('admin/warehouses/enterprises', 'Warehouse\WarehouseController@warehouseEnterprises')->name('adminWarehouseEnterprises');
        Route::post('admin/warehouses/branch', 'Warehouse\WarehouseController@getWarehouseByBranch')->name('adminWarehouseByBranch');
        Route::post('admin/warehouses/get', 'Warehouse\WarehouseController@warehouses');
        Route::post('admin/warehouses/create', 'Warehouse\WarehouseController@warehouseCreate')->name('adminWarehouseCreate');
        Route::post('admin/warehouses/edit', 'Warehouse\WarehouseController@warehouseEdit')->name('adminWarehouseEdit');
        Route::post('admin/warehouses/delete', 'Warehouse\WarehouseController@warehouseDelete')->name('adminWarehouseDelete');
        Route::post('admin/warehouses/suspend', 'Warehouse\WarehouseController@warehouseSuspend')->name('adminWarehouseSuspend');
        Route::post('admin/warehouse/enterprise', 'Warehouse\WarehouseController@getWarehouseEnterprises')->name('adminWarehouseEnterprise');

        /**
         * Categories
         */
        Route::post('admin/categories/get', 'Category\CategoryController@categories')->name('adminCategoriesGet');
        Route::post('admin/categories/create', 'Category\CategoryController@categoryCreate')->name('adminCategoryCreate');
        Route::post('admin/categories/edit', 'Category\CategoryController@categoryEdit')->name('adminCategoryEdit');
        Route::post('admin/categories/delete', 'Category\CategoryController@categoryDelete')->name('adminCategoryDelete');
        Route::post('admin/categories/suspend', 'Category\CategoryController@categorySuspend')->name('adminCategorySuspend');
        Route::post('admin/categories/category', 'Category\CategoryController@categoryCreateFromCompound');

        /**
         * Subcategories
         */
        Route::post('admin/subcategories/get', 'Subcategory\SubcategoryController@subcategories')->name('adminSubcategoriesGet');
        Route::post('admin/subcategories/create', 'Subcategory\SubcategoryController@subcategoryCreate')->name('adminSubcategoryCreate');
        Route::post('admin/subcategories/edit', 'Subcategory\SubcategoryController@subcategoryEdit')->name('adminSubcategoryEdit');
        Route::post('admin/subcategories/delete', 'Subcategory\SubcategoryController@subcategoryDelete')->name('adminSubcategoryDelete');
        Route::post('admin/subcategories/suspend', 'Subcategory\SubcategoryController@subcategorySuspend')->name('adminSubcategorySuspend');
        Route::post('admin/subcategories/subcategory', 'Subcategory\SubcategoryController@subcategoryCreateFromCompound');

        /**
         * Productos
         */
        Route::post('admin/products/search-code', 'Product\ProductController@searchProductByCode')->name('adminProductsSearchCode');
        Route::post('admin/products/search-name', 'Product\ProductController@searchProductByName')->name('adminProductsSearchName');
        Route::post('admin/products/get', 'Product\ProductController@products')->name('adminProductsGet');
        Route::post('admin/products/create', 'Product\ProductController@productCreate')->name('adminProductCreate');
        Route::post('admin/products/edit', 'Product\ProductController@productEdit')->name('adminProductEdit');
        Route::post('admin/products/delete', 'Product\ProductController@productDelete')->name('adminProductDelete');
        Route::post('admin/products/categories', 'Product\ProductController@productCategories')->name('adminProductCategories');
        Route::post('admin/products/brands', 'Product\ProductController@productBrands')->name('adminProductBrands');
        Route::post('admin/products/companies', 'Product\ProductController@companiesGet')->name('adminCompaniesGet');
        Route::post('admin/products/warehouses', 'Product\ProductController@warehousesGet');
        Route::post('admin/products/details', 'Product\ProductController@productDetails')->name('adminProductDetails');
        Route::post('admin/products/export', 'Product\ProductController@productExport')->name('adminProductExport');
        Route::get('admin/products/export/{user_id}', 'Product\ProductController@productExportDetail')->name('adminProductExportDetail');
        Route::post('admin/products/enterprises', 'Product\ProductController@getAllProductsByEnterprise')->name('adminAllProductsEnterprise');
        // Route::post('admin/products/prices/enterprises', 'Product\ProductController@getAllProductsWithPriceByEnterprise')->name('admingetAllProductsWithPriceByEnterprise');
        Route::post('admin/products/code', 'Product\ProductController@lastCode')->name('adminProductLastCode');
        Route::post('admin/products/getbycode', 'Product\ProductController@getProductByCode')->name('adminProductByCode');
        /**
         * Product Compound
         */
        Route::post('admin/products-compound/get', 'ProductCompound\ProductCompoundController@products')->name('adminProductCompoundsGet');
        Route::post('admin/products-compound/create', 'ProductCompound\ProductCompoundController@productCreate')->name('adminProductCompoundCreate');
        Route::post('admin/products-compound/edit', 'ProductCompound\ProductCompoundController@productEdit')->name('adminProductCompoundEdit');
        Route::post('admin/products-compound/delete', 'ProductCompound\ProductCompoundController@productDelete')->name('adminProductCompoundDelete');
        Route::post('admin/products-compound/categories', 'ProductCompound\ProductCompoundController@productCategories')->name('adminProductCompoundCategories');
        Route::post('admin/products-compound/brands', 'ProductCompound\ProductCompoundController@productBrands')->name('adminProductCompoundBrands');
        Route::post('admin/products-compound/companies', 'ProductCompound\ProductCompoundController@companiesGet')->name('adminCompaniesGet');
        Route::post('admin/products-compound/warehouses', 'ProductCompound\ProductCompoundController@warehousesGet');
        Route::post('admin/products-compound/details', 'ProductCompound\ProductCompoundController@productDetails')->name('adminProductCompoundDetails');
        Route::post('admin/products-compound/export', 'ProductCompound\ProductCompoundController@productExport')->name('adminProductCompoundExport');
        Route::post('admin/products-compound/search-products', 'ProductCompound\ProductCompoundController@productSearch')->name('adminProductCompoundSearchProduct');
        Route::post('admin/products/search-products', 'ProductCompound\ProductCompoundController@getProductsFromPurchase')->name('adminProductsFromPurchase');
        Route::get('admin/products-compound/export/{user_id}', 'ProductCompound\ProductCompoundController@productExportDetail')->name('adminProductCompoundExportDetail');

        /**
         * Reports
         */
        Route::post('admin/report/sales', 'Request\ReportController@export_report');

        /**
         * Product Providers
         */
        Route::post('admin/products/providers', 'ProductProvider\ProductProviderController@productProviders')->name('adminProductProviders');
        Route::post('admin/products/providers/delete', 'ProductProvider\ProductProviderController@productProvidersDelete')->name('adminProductProvidersDelete');
        Route::post('admin/products/providers/assign', 'ProductProvider\ProductProviderController@productProvidersAssign')->name('adminProductProvidersAssign');
        Route::post('admin/products/providers/change', 'ProductProvider\ProductProviderController@productProvidersChange')->name('adminProductProvidersChange');
        Route::post('admin/products/providers/include', 'ProductProvider\ProductProviderController@productProvidersInclude')->name('adminProductProvidersInclude');
        Route::post('admin/products/providers/get', 'ProductProvider\ProductProviderController@productProvidersGet')->name('adminProductProvidersGet');

        /**
         * Clients
         */
        Route::post('admin/clients/get', 'Client\ClientController@clients')->name('adminClientGet');
        Route::post('admin/clients/create', 'Client\ClientController@clientCreate')->name('adminClientCreate');
        Route::post('admin/clients/edit', 'Client\ClientController@clientEdit')->name('adminClientEdit');
        Route::post('admin/clients/delete', 'Client\ClientController@clientDelete')->name('adminClientDelete');
        Route::get('admin/clients/zones', 'Client\ClientController@getZones')->name('adminClientZones');
        Route::post('admin/clients/suspend', 'Client\ClientController@clientSuspend')->name('adminClientSuspend');
        Route::post('admin/clients/verify', 'Client\ClientController@clientVerify')->name('adminClientVerify');
        Route::get('admin/clients/retentions', 'Client\ClientController@getRetentions')->name('adminClientRetentions');
        Route::post('admin/clients/enterprise', 'Client\ClientController@getCustomersEnterprise')->name('adminCustomerEnterprise');
        Route::post('admin/clients/seller', 'Client\ClientController@getCustomersSeller');
        Route::post('admin/clients/create/request', 'Client\ClientController@clientCreateFromRequest')->name('clientCreateFromRequest');

        /**
         * Enterprises
         */
        Route::post('admin/enterprises/get', 'Enterprise\EnterpriseController@enterprises')->name('adminEnterpriseGet');
        Route::post('admin/enterprises/create', 'Enterprise\EnterpriseController@enterpriseCreate')->name('adminEnterpriseCreate');
        Route::post('admin/enterprises/edit', 'Enterprise\EnterpriseController@enterpriseEdit')->name('adminEnterpriseEdit');
        Route::post('admin/enterprises/delete', 'Enterprise\EnterpriseController@enterpriseDelete')->name('adminEnterpriseDelete');
        Route::post('admin/enterprises/suspend', 'Enterprise\EnterpriseController@enterpriseSuspend')->name('adminEnterpriseSuspend');
        Route::post('admin/enterprises/verify', 'Enterprise\EnterpriseController@enterpriseVerify')->name('adminEnterpriseVerify');
        Route::post('admin/enterprises/get-enterprise-data', 'Enterprise\EnterpriseController@getEnterpriseData')->name('adminEnterpriseGetData');
        Route::get('admin/enterprises/administrators', 'Enterprise\EnterpriseController@getAdministrators')->name('adminEnterpriseAdministrators');

        /**
         * Providers
         */
        Route::post('admin/providers/get', 'Provider\ProviderController@providers')->name('adminProviderGet');
        Route::post('admin/providers/create', 'Provider\ProviderController@providerCreate')->name('adminProviderCreate');
        Route::post('admin/providers/from-order/create', 'Provider\ProviderController@providerCreateFromOrder')->name('adminProviderCreateFromOrder');
        Route::post('admin/providers/edit', 'Provider\ProviderController@providerEdit')->name('adminProviderEdit');
        Route::post('admin/providers/delete', 'Provider\ProviderController@providerDelete')->name('adminProviderDelete');
        Route::post('admin/providers/suspend', 'Provider\ProviderController@providerSuspend')->name('adminProviderSuspend');
        Route::post('admin/providers/active', 'Provider\ProviderController@providerActive')->name('adminProviderSuspend');
        Route::post('admin/providers/enterprise', 'Provider\ProviderController@getProvidersByEnterprise')->name('adminProvidersByEnterprise');

        /**
         * Usuarios
         */
        Route::post('admin/users/get', 'User\UserController@users')->name('adminUserGet');
        Route::post('admin/users/create', 'User\UserController@userCreate')->name('adminUserCreate');
        Route::post('admin/users/edit', 'User\UserController@userEdit')->name('adminUserEdit');
        Route::post('admin/users/suspend', 'User\UserController@userSuspend')->name('adminUserSuspend');
        Route::post('admin/users/active', 'User\UserController@userActive')->name('adminUserActive');
        Route::post('admin/users/delete', 'User\UserController@userDelete')->name('adminUserDelete');
        Route::post('admin/users/verify', 'User\UserController@userVerify')->name('adminUserVerify');
        Route::post('admin/users/get-roles-enterprises', 'User\UserController@userRoleEnterprise')->name('adminUserRoleEnterprise');
        Route::post('admin/users/get-user-data', 'User\UserController@getUserData')->name('adminUserGetData');
        Route::post('admin/users/get-admins', 'User\UserController@getAdmins')->name('adminUserGetAdmins');

        /**
         * Brands
         */
        Route::post('admin/brands/get', 'Brand\BrandController@brands')->name('adminBrandGet');
        Route::post('admin/brands/create', 'Brand\BrandController@brandCreate')->name('adminBrandCreate');
        Route::post('admin/brands/edit', 'Brand\BrandController@brandEdit')->name('adminBrandEdit');
        Route::post('admin/brands/suspend', 'Brand\BrandController@brandSuspend')->name('adminBrandSuspend');
        Route::post('admin/brands/active', 'Brand\BrandController@brandActive')->name('adminBrandActive');
        Route::post('admin/brands/delete', 'Brand\BrandController@brandDelete')->name('adminBrandDelete');
        Route::post('admin/brands/brand', 'Brand\BrandController@brandCreateFromCompound');

        /**
         * Models
         */
        Route::post('admin/models/get', 'Model\ModelController@models')->name('adminModelGet');
        Route::post('admin/models/create', 'Model\ModelController@modelCreate')->name('adminModelCreate');
        Route::post('admin/models/edit', 'Model\ModelController@modelEdit')->name('adminModelEdit');
        Route::post('admin/models/suspend', 'Model\ModelController@modelSuspend')->name('adminModelSuspend');
        Route::post('admin/models/active', 'Model\ModelController@modelActive')->name('adminModelActive');
        Route::post('admin/models/delete', 'Model\ModelController@modelDelete')->name('adminModelDelete');
        Route::post('admin/models/model', 'Model\ModelController@modelCreateFromCompound');

        /**
         * Currencies
         */
        Route::post('admin/currencies/purchase', 'Currency\CurrencyController@getCurrenciesFromPurchase')->name('adminGetCurrencyFromPurchase');
        Route::post('admin/currencies/get', 'Currency\CurrencyController@currencies')->name('adminCurrencyGet');
        Route::post('admin/currencies/create', 'Currency\CurrencyController@currencyCreate')->name('adminCurrencyCreate');
        Route::post('admin/currencies/edit', 'Currency\CurrencyController@currencyEdit')->name('adminCurrencyEdit');
        Route::post('admin/currencies/suspend', 'Currency\CurrencyController@currencySuspend')->name('adminCurrencySuspend');

        /**
         * Orders
         */
        Route::post('admin/orders/get', 'Order\OrderController@orders')->name('adminOrders');
        Route::post('admin/orders/generateId', 'Order\OrderController@getId')->name('adminGetOrderId');
        Route::post('admin/orders/provider-products', 'Order\OrderController@productsByProvider')->name('adminProductsByProvider');
        Route::post('admin/orders/create', 'Order\OrderController@createOrder')->name('adminOrdersCreate');
        Route::post('admin/orders/update', 'Order\OrderController@updateOrder')->name('adminOrdersUpdate');

        /**
         * Requests
         */
        Route::post('admin/requests/get', 'Request\RequestController@requests')->name('adminGetRequests');
        Route::post('admin/requests/getAll', 'Request\RequestController@processedAllStatus')->name('adminProcessedAllStatus');
        Route::post('admin/requests/generateId', 'Request\RequestController@getId')->name('adminGetRequestId');
        Route::post('admin/requests/getId', 'Request\RequestController@generateId')->name('adminGenerateRequestId');
        Route::post('admin/requests/create', 'Request\RequestController@createRequest')->name('adminCreateRequest');
        Route::post('admin/requests/reject', 'Request\RequestController@rejectRequest')->name('adminRejectRequest');
        Route::post('admin/requests/update', 'Request\RequestController@updateRequest')->name('adminRequestsUpdate');

        /**
         * DeliveryNotes
         */
        Route::post('admin/deliver-notes/get', 'Delivery\DeliveryNotesController@deliveryNotes')->name('adminGetDeliveryNotes');
        Route::post('admin/deliver-notes/generateId', 'Delivery\DeliveryNotesController@getId')->name('adminGetDeliveryNoteId');
        Route::post('admin/deliver-notes/getId', 'Delivery\DeliveryNotesController@generateId')->name('adminGenerateDeliveryNoteId');
        Route::post('admin/deliver-notes/create', 'Delivery\DeliveryNotesController@createDeliveryNote')->name('adminCreateDeliveryNote');

        /**
         * Sales
         */
        Route::post('admin/sales/generateId', 'Sale\SaleController@generateId')->name('adminGetGenerateId');
        Route::post('admin/sales/create', 'Sale\SaleController@createSale')->name('adminSaleCreate');
        Route::post('admin/sales/get-sale-type', 'Sale\SaleController@getSaleType')->name('adminGetSaleType');

        /**
         * Modules
         */
        Route::post('admin/modules/get', 'Module\ModuleController@modules')->name('adminModuleGet');
        Route::post('admin/modules/get-enterprises', 'Module\ModuleController@enterpriseModules')->name('adminModuleGet');
        Route::post('admin/modules/edit', 'Module\ModuleController@enterpriseModulesEdit')->name('adminEnterpriseModulesEdit');

        /**
         * Roles Enterprise
         */
        Route::post('admin/roles_enterprises/get', 'Role\RoleEnterpriseController@roles')->name('adminRolesGet');
        Route::post('admin/roles_enterprises/enterprises', 'Role\RoleEnterpriseController@enterprises')->name('adminRolesEnterprises');
        Route::post('admin/roles_enterprises', 'Role\RoleEnterpriseController@rolesByEnterprise')->name('adminRolesByEnterprise');
        Route::post('admin/roles_enterprises/create', 'Role\RoleEnterpriseController@rolesCreate')->name('adminRolesCreate');
        Route::post('admin/roles_enterprises/edit', 'Role\RoleEnterpriseController@rolesEdit')->name('adminRolesEdit');
        Route::post('admin/roles_enterprises/suspend', 'Role\RoleEnterpriseController@rolesSuspend')->name('adminRolesSuspend');
        Route::post('admin/roles_enterprises/active', 'Role\RoleEnterpriseController@rolesActive')->name('adminRolesActive');
        Route::post('admin/roles_enterprises/delete', 'Role\RoleEnterpriseController@rolesDelete')->name('adminRolesDelete');

        /**
         * Roles Subuser
         */
        Route::post('admin/roles_sub/get', 'Role\RoleSubuserController@roles')->name('adminRolesGet');
        Route::post('admin/roles_sub/create', 'Role\RoleSubuserController@rolesCreate')->name('adminRolesCreate');
        Route::post('admin/roles_sub/edit', 'Role\RoleSubuserController@rolesEdit')->name('adminRolesEdit');

        /**
         * Branches
         */
        Route::post('admin/branches/get', 'Branch\BranchController@branches')->name('adminBranchGet');
        Route::post('admin/branches/enterprise', 'Branch\BranchController@getBranchesByEnterprise')->name('adminBranchByEnterprise');
        Route::post('admin/branches/create', 'Branch\BranchController@branchCreate')->name('adminBranchCreate');
        Route::post('admin/branches/edit', 'Branch\BranchController@branchEdit')->name('adminBranchEdit');
        Route::post('admin/branches/suspend', 'Branch\BranchController@branchSuspend')->name('adminBranchSuspend');
        Route::post('admin/branches/active', 'Branch\BranchController@branchActive')->name('adminBranchActive');
        Route::post('admin/branches/delete', 'Branch\BranchController@branchDelete')->name('adminBranchDelete');

        /**
         * Prices
         */
        Route::post('admin/prices/get', 'Price\PriceController@prices')->name('adminPriceGet');
        Route::post('admin/prices/create', 'Price\PriceController@priceCreate')->name('adminPriceCreate');
        Route::post('admin/prices/edit', 'Price\PriceController@priceEdit')->name('adminPriceEdit');
        Route::post('admin/prices/suspend', 'Price\PriceController@priceSuspend')->name('adminPriceSuspend');
        Route::post('admin/prices/active', 'Price\PriceController@priceActive')->name('adminPriceActive');

        /**
         * Inventory
         */
        Route::post('admin/inventory/get', 'Inventory\InventoryController@inventory')->name('adminInventoryGet');
        Route::post('admin/inventory/create', 'Inventory\InventoryController@inventoryCreate')->name('adminInventoryCreate');
        Route::post('admin/inventory/translate', 'Inventory\InventoryController@inventoryTranslate')->name('adminInventoryTranslate');
        Route::post('admin/inventory/check-existence', 'Inventory\InventoryController@checkExistence')->name('adminInventoryCheckExistence');

        /**
         * Report Inventory
         */
        Route::post('admin/report-inventory/get', 'Report\ReportInventoryController@reportInventory')->name('adminReportInventoryGet');

        /**
         * Transfer Warehouse
         */
        Route::post('admin/transfer-warehouses/get', 'TransferWarehouse\TransferWarehouseController@transfer')->name('adminTransferWarehouseGet');
        Route::post('admin/transfer-warehouses/abort', 'TransferWarehouse\TransferWarehouseController@abortTransfer')->name('adminTransferWarehouseAbort');
        Route::post('admin/transfer-warehouses/process', 'TransferWarehouse\TransferWarehouseController@processTransfer')->name('adminTransferWarehouseProcess');

        /**
         * Report Movement
         */
        Route::post('admin/movements/get', 'Movement\MovementController@movements')->name('adminReportMovementGet');

        /**
         * Payments
         */
        Route::post('admin/payments/customers', 'Payment\PaymentController@getCustomersWithDebts')->name('adminGetCustomersWithDebts');
        Route::post('admin/payments/get', 'Payment\PaymentController@get')->name('adminGetPayments');
        Route::post('admin/payments/create', 'Payment\PaymentController@createPayment');
        Route::post('admin/payments/processing', 'Payment\PaymentController@processing');
        Route::post('admin/payments/report', 'Payment\PaymentController@report');

        /**
         * Sellers
         */
        Route::post('admin/sellers/get', 'Seller\SellerController@sellers');
        Route::post('admin/sellers/enterprise', 'Seller\SellerController@sellers_enterprise');
        Route::post('admin/sellers/delivery-notes', 'Seller\SellerController@sellersDeliveryNote');
        Route::post('admin/sellers/requests', 'Seller\SellerController@sellersRequests');
        Route::post('admin/sellers/create', 'Seller\SellerController@create');
        Route::post('admin/sellers/update', 'Seller\SellerController@update');

        /**
         * Zones
         */
        Route::post('admin/zones/get', 'Zone\ZoneController@zones');

        /**
         * Banks
         */
        Route::post('admin/banks/index', 'Bank\BankController@index'); //PAGINADO
        Route::post('admin/banks/get', 'Bank\BankController@get'); //SIN PAGINADO
        Route::post('admin/banks/activate/{id}', 'Bank\BankController@activate');
        Route::post('admin/banks/show','Bank\BankController@show');
        Route::resource('admin/banks', 'Bank\BankController');

        /**
         * Banks Accounts
         */
        Route::post('admin/bank-accounts/index', 'Bank\BankAccountController@index'); //PAGINADO
        Route::post('admin/bank-accounts/get', 'Bank\BankAccountController@get'); //SIN PAGINADO
        Route::post('admin/bank-accounts/activate/{id}', 'Bank\BankAccountController@activate');
        Route::resource('admin/bank-accounts', 'Bank\BankAccountController');

        /**
         * Expenses Ingress
         */
        Route::post('admin/expenses-ingress/generate-id', 'ExpensesIngress\ExpensesIngressController@generateId');
        Route::post('admin/expenses-ingress/get', 'ExpensesIngress\ExpensesIngressController@index'); //PAGINADO
        Route::post('admin/expenses-ingress/create', 'ExpensesIngress\ExpensesIngressController@store');
        Route::post('admin/expenses-ingress/payment-report', 'ExpensesIngress\ExpensesIngressController@paymentReport');

        /**
         * Debts to Pay
         */
        Route::post('admin/debts-to-pay/get', 'DebtsToPay\DebtsToPayController@index');
        Route::post('admin/debts-to-pay/pay', 'DebtsToPay\DebtsToPayController@pay');

        /**
         * Banking Transactions
         */
        Route::post('admin/banking-transactions/get','Bank\BankingTransactionController@index');

        /**
         * Cash Count
         */
        Route::post('admin/cash-count/get', 'CashCount\CashCountController@index');
        Route::post('admin/cash-count/process', 'CashCount\CashCountController@process');
    });
});
