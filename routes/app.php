<?php

	Route::group(['middleware' => 'api','namespace' => 'Api\App', 'prefix' => 'app'],function() {
		
		// Auth
		Route::post('auth/login','AuthController@login');
		Route::post('auth/register','AuthController@register');
		Route::post('auth/recover','AuthController@recover');
		Route::post('auth/code','AuthController@code');
		Route::post('auth/reset','AuthController@reset');
		Route::post('auth/check-user','AuthController@checkUser');

		// Productos
		Route::post('products/all','ProductController@all');

		// Profile
		Route::post('profile/get','ProfileController@get');
		Route::post('profile/set','ProfileController@set');
		Route::post('profile/password','ProfileController@setPassword');
		Route::post('profile/photo','ProfileController@photo');

		// Clients
		Route::post('clients/all','ClientController@all');

		// Shipping 
		Route::post('shipping/get','ShippingController@get');
		Route::post('shipping/set','ShippingController@set');
		Route::post('shipping/check','ShippingController@check');

		// Commissions
		Route::post('commissions/get','CommissionController@get');

		// Repots
		Route::post('reports/get','ReportController@get');

		// Chat
		Route::post('chat/get','ChatController@get');
		Route::post('chat/view','ChatController@view');
		Route::post('chat/file','ChatController@file');
		Route::post('chat/all','ChatController@getAll');
		Route::post('chat/viewed','ChatController@view');
		Route::post('chat/count','ChatController@count');

		Route::group(['prefix' => 'admin'],function() {

			Route::post('permissions','ProfileController@permissions');

			// Sellers
			Route::post('sellers/get','SellerController@get');
			Route::post('sellers/create','SellerController@set');
			Route::post('sellers/edit','SellerController@edit');
			Route::post('sellers/delete','SellerController@delete');

			// Clients
			Route::post('clients/delete','ClientController@delete');
			Route::post('clients/create','ClientController@create');
			Route::post('clients/edit','ClientController@edit');

			// Chat
			Route::post('chat/all','ChatController@all');
			Route::post('chat/sellers','ChatController@getSellers');
			Route::post('chat/create','ChatController@create');
			Route::post('chat/get-admin','ChatController@getAdmin');
			Route::post('chat/count-admin','ChatController@countAdmin');

			// Moderators
			Route::post('moderators/get','ModeratorController@get');
			Route::post('moderators/delete','ModeratorController@delete');
			Route::post('moderators/create','ModeratorController@set');
			Route::post('moderators/edit','ModeratorController@edit');
			Route::post('moderators/modules','ModeratorController@modules');

			// Products
			Route::post('products/categories','ProductController@categories');
			Route::post('products/delete','ProductController@delete');
			Route::post('products/create','ProductController@set');
			Route::post('products/edit','ProductController@edit');

			// Sales
			Route::post('sales/get','SaleController@get');

			// Records
			Route::post('records/get','RecordController@get');
			Route::post('records/set','RecordController@set');

			// Commissions
			Route::post('commissions/get','CommissionController@getEdit');
			Route::post('commissions/set','CommissionController@setEdit');
		});
		
	});