<?php

Route::group(['namespace' => 'Api'], function () {
    Route::group(['namespace' => 'Admin'], function () {
        Route::get('sales/print-sale/{enterprise_id}/{process_id}', 'Sale\SaleController@printSale');
    });
});

// Route::get('file/{id}', 'HomeController@file');
// EXAMPLE DE EXCEL
// Route::post('report/sales', 'HomeController@export_report');

Route::get('/', function () {
    abort(403);
});
