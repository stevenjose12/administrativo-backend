<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserBalanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_balance', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('action_id')->unsigned()->comment('ID de la accion')->nullable();
            $table->index('action_id');
            $table->integer('action_class')->unsigned()->comment('1: Ingreso por pago, 2: Egreso por modulo NE, 3: Ajuste manual')->nullable();
            $table->index('action_class');
            $table->string('action_type')->nullable()->comment('Columna de relacion polimorfica');
            $table->integer('user_id')->unsigned()->comment('ID del usuario');
            $table->index('user_id');
            $table->integer('type')->comment('1: Ingreso, 2: Egreso');
            $table->double('current_amount', 20, 3)->comment('cantidad al momento que se genero el movimiento');
            $table->double('amount', 20, 3)->comment('Monto del ingreso o egreso');
            $table->double('total', 20, 3)->comment('Monto total de la operacion');
            $table->integer('creator_id')->unsigned()->comment('ID del creador');
            $table->index('creator_id');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('creator_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_balance');
    }
}
