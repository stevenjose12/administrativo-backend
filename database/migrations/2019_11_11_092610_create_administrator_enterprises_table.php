<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdministratorEnterprisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('administrator_enterprises', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('administrator_id')->comment('ID del usuario tipo administrador')->unsigned();
            $table->index('administrator_id');
            $table->integer('enterprise_id')->comment('ID del usuario tipo empresa')->unsigned();
            $table->index('enterprise_id');
            $table->integer('creator_id')->unsigned()->comment('ID del Usuario creador');
            $table->index('creator_id');
            $table->timestamps();

            $table->foreign('administrator_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('enterprise_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('creator_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('administrator_enterprises');
    }
}
