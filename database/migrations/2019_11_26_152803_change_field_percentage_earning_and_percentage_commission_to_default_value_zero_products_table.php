<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class ChangeFieldPercentageEarningAndPercentageCommissionToDefaultValueZeroProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            DB::statement("ALTER TABLE products CHANGE percentage_earning percentage_earning DOUBLE NULL DEFAULT '0' COMMENT 'Porcentaje de Ganancia'");
            DB::statement("ALTER TABLE products CHANGE percentage_commission percentage_commission DOUBLE NULL DEFAULT '0' COMMENT 'Porcentaje de Comision'");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            DB::statement("ALTER TABLE products CHANGE percentage_commission percentage_commission DOUBLE NULL COMMENT 'Porcentaje de Comision'");
            DB::statement("ALTER TABLE products CHANGE percentage_earning percentage_earning DOUBLE NULL COMMENT 'Porcentaje de Ganancia'");
        });
    }
}
