<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_clients', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')->unsigned()->comment('ID del cliente');
            $table->index('client_id');
            $table->integer('days_deadline')->default(0)->comment('Plazo que obtienen los clientes para límite de pagos.');
            $table->integer('creator_id')->comment('ID del usuario que crea la configuración');

            $table->foreign('client_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config_clients');
    }
}
