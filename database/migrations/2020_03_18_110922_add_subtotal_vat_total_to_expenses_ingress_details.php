<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubtotalVatTotalToExpensesIngressDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expenses_ingress_details', function (Blueprint $table) {
            $table->integer('exempt')->comment('Exento de impuesto, 0: No Exento, 1: Si Exento')->change();
            $table->double('subtotal', 10, 2)->comment('Subtotal')->after('exempt');
            $table->double('vat', 10, 2)->comment('IVA')->after('subtotal');
            $table->double('total', 10, 2)->comment('Total')->after('vat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expenses_ingress_details', function (Blueprint $table) {
            $table->dropColumn(['subtotal', 'vat', 'total']);
        });
    }
}
