<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnActionTypeToProductWarehouseMovement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_warehouse_movement', function (Blueprint $table) {
            $table->string('action_type')->nullable()->after('action_class');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_warehouse_movement', function (Blueprint $table) {
            $table->dropColumn('action_type');
        });
    }
}
