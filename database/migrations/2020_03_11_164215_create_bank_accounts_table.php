<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bank_id')->unsigned()->comment('ID del banco asociado a la cuenta');
            $table->index('bank_id');
            $table->string('account_number')->comment('Numero de cuenta');
            $table->integer('type')->comment('1: Corriente, 2: Ahorro');
            $table->integer('status')->default(1)->comment('1: Activo, 2: Desactivado');
            $table->integer('creator_id')->unsigned()->comment('ID del usuario que creo el registro');
            $table->index('creator_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('bank_id')->references('id')->on('banks')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('creator_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_accounts');
    }
}
