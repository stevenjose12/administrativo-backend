<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_payments', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('user_id')->unsigned()->comment('ID del usuario a quien se le genera el pago');
            $table->index('user_id');
            $table->integer('type')->comment('1: Efectivo, 2: Debito, 3: Credito, 4: Transferencia');
            $table->double('amount', 20, 3)->comment('Cantidad a pagar');
            $table->string('image')->comment('Imagen del pago')->nullable();
            $table->integer('creator_id')->unsigned()->comment('ID del usuario que creo el pago');
            $table->index('creator_id');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('creator_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_payments');
    }
}
