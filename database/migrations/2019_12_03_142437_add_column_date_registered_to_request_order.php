<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDateRegisteredToRequestOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('request_order', function (Blueprint $table) {
            $table->dateTime('date_registered')->after('currency_id')->comment('Fecha de registro')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('request_order', function (Blueprint $table) {
            $table->dropColumn('date_registered');
        });
    }
}
