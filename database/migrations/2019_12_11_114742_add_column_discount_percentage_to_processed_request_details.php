<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDiscountPercentageToProcessedRequestDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('processed_request_details', function (Blueprint $table) {
            $table->double('discount_percentage', 2)->comment('Porcentaje de descuento')->nullable()->after('quantity');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('processed_request_details', function (Blueprint $table) {
            $table->dropColumn('discount_percentage');
        });
    }
}
