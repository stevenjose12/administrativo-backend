<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEnterpriseOnlyColumnModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('modules', function (Blueprint $table) {
            $table->integer('enterprise_only')->comment('Solamente los vera el usuario tipo empresa')->default(0)->after('admin_only');
            $table->integer('user_only')->comment('Solamente los vera que tengan el role sub_usuario')->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('modules', function (Blueprint $table) {
            $table->dropColumn(['enterprise_only']);
            $table->integer('user_only')->comment('Solamente los vera el usuario tipo empresa')->default(0)->change();
        });
    }
}
