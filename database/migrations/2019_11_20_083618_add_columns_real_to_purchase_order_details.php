<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsRealToPurchaseOrderDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_order_details', function (Blueprint $table) {
            $table->integer('quantity_real')->comment('Cantidad real')->nullable()->after('quantity');
            $table->double('subtotal_real', 2)->comment('Subtotal real')->nullable()->after('subtotal');
            $table->double('vat_real', 2)->comment('IVA real')->nullable()->after('vat');
            $table->double('total_real', 2)->comment('Total real')->nullable()->after('total');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_order_details', function (Blueprint $table) {
            $table->dropColumn('quantity_real');
            $table->dropColumn('subtotal_real');
            $table->dropColumn('vat_real');
            $table->dropColumn('total_real');
        });
    }
}
