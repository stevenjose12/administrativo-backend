<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeOperationNumberNullableBankBalance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bank_balance', function (Blueprint $table) {
            $table->integer('action_class')->comment('Tipo de Movimiento: 1) Ingreso por CXC, 2) Egreso por CXP')->change();
            $table->string('operation_number')->nullable()->comment('Numero de operacion')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bank_balance', function (Blueprint $table) {
            $table->integer('action_class')->comment('Tipo de Movimiento')->change();
            $table->string('operation_number')->comment('Numero de operacion')->change();
        });
    }
}
