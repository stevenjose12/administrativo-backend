<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPersonTypeFiscalIdentificationCodeColumnsToPersons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('persons', function (Blueprint $table) {
            $table->string('code')->after('last_name')->comment('Codigo del usuario')->nullable();
            $table->text('direction')->after('phone')->nullable();
            $table->string('fiscal_identification')->after('direction')->comment('RIF')->nullable();
            $table->integer('person_type')->after('avatar')->comment('1: Natural, 2: Juridica')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('persons', function (Blueprint $table) {
            $table->dropColumn(['code', 'direction', 'fiscal_identification', 'person_type']);
        });
    }
}
