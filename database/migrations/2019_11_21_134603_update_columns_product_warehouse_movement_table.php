<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColumnsProductWarehouseMovementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_warehouse_movement', function (Blueprint $table) {
            $table->integer('action_id')->comment('ID de la accion asociada al movieminto')->change();
            $table->integer('type')->comment('1: Entrada al inventario, 2: Salida del inventario')->change();
            $table->integer('action_class')->comment('Tipo de moviento, 1: Ajuste de Inventario, 2: Orden de compra, 3: Nota de entrega, 4: Traslado entre almacenes')->change();
            DB::statement("ALTER TABLE product_warehouse_movement MODIFY COLUMN action_class DATE AFTER product_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_warehouse_movement', function (Blueprint $table) {
            //
        });
    }
}
