<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsUserPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests_user_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('processed_request_id')->unsigned()->comment('ID de la factura');
            $table->index('processed_request_id');
            $table->integer('user_payment_id')->unsigned()->comment('ID del pago');
            $table->index('user_payment_id');
            $table->integer('creator_id')->unsigned()->comment('ID del usuario creador')->nullable();
            $table->index('creator_id');
            $table->timestamps();

            $table->foreign('processed_request_id')->references('id')->on('processed_request')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_payment_id')->references('id')->on('user_payments')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('creator_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests_user_payments');
    }
}
