<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesIngressPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses_ingress_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('expenses_ingress_id')->unsigned()->comment('ID de la ingreos de gasto');
            $table->index('expenses_ingress_id');
            $table->integer('payment_type')->comment('1: Efectivo, 2: Credito, 3: Debito, 4: Transferencia');
            $table->double('amount', 10, 2)->comment('Monto');
            $table->dateTime('date')->comment('Fecha del pago')->nullable();
            $table->integer('status')->default(0)->comment('0: Sin Usar, 1: Usado');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('expenses_ingress_id')->references('id')->on('expenses_ingress')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses_ingress_payments');
    }
}
