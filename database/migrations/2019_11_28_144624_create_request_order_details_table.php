<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_order_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('request_order_id')->comment('ID de la orden de compra')->unsigned();
            $table->index('request_order_id');
            $table->integer('product_id')->comment('ID del producto')->unsigned();
            $table->index('product_id');
            $table->double('subtotal', 2)->comment('Subtotal');
            $table->double('vat', 2)->comment('IVA');
            $table->double('total', 2)->comment('Total');
            $table->timestamps();

            $table->foreign('request_order_id')->references('id')->on('request_order')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_order_details');
    }
}
