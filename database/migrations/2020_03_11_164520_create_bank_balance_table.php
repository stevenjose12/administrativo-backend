<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankBalanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_balance', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bank_id')->unsigned()->comment('ID del banco asociado al movimiento');
            $table->index('bank_id');
            $table->integer('bank_account_id')->unsigned()->comment('ID de la cuenta de banco asociada al movimiento');
            $table->index('bank_account_id');
            $table->integer('action_id')->comment('ID de la operacion asociada al movimiento');
            $table->integer('action_class')->comment('Tipo de Movimiento');
            $table->string('action_type')->comment('Modelo de la operacion asociada');
            $table->string('operation_number')->comment('Numero de operacion');
            $table->integer('type')->comment('1: Ingreso (Suma), 2: Egreso (Resta)');
            $table->double('amount', 10, 2)->comment('Cantidad que ingresa');
            $table->double('current_amount', 10, 2)->comment('Cantidad al momento de realizar la operacion');
            $table->double('total', 10, 2)->comment('Total de la operacion aritmetica entre amount y current_amount');
            $table->text('description')->comment('Descripcion/Observaciones');
            $table->integer('creator_id')->unsigned()->comment('ID del usuario que creo el movimiento');
            $table->index('creator_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('bank_id')->references('id')->on('banks')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('bank_account_id')->references('id')->on('bank_accounts')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('creator_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_balance');
    }
}
