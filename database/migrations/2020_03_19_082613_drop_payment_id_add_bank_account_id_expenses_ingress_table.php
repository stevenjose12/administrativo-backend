<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropPaymentIdAddBankAccountIdExpensesIngressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expenses_ingress', function (Blueprint $table) {
            $table->dropIndex(['payment_id']);
            $table->dropColumn(['payment_id']);
            $table->integer('bank_account_id')->after('provider_id')->nullable()->comment('ID de la cuenta de banco asociada');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expenses_ingress', function (Blueprint $table) {
            $table->dropColumn(['bank_account_id']);
            $table->integer('payment_id')->nullable()->comment('ID del pago asociado');
            $table->index('payment_id');
        });
    }
}
