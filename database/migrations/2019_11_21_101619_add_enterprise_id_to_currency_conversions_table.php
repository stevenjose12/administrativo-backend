<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEnterpriseIdToCurrencyConversionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('currency_conversions', function (Blueprint $table) {

            $table->integer('enterprise_id')->unsigned()->comment('ID de la empresa')->after('conversion_rate');
            $table->index('enterprise_id');

            $table->foreign('enterprise_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('currency_conversions', function (Blueprint $table) {
            //
        });
    }
}
