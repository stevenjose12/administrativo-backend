<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPercentageEarningCommissionAndCostToProductWarehouseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_warehouse', function (Blueprint $table) {
            $table->dropColumn(['price']);
        });

        Schema::table('product_warehouse', function (Blueprint $table) {
            $table->double('price', 20, 2)->comment('Precio del producto con el porcentaje de ganancia incluido')->default(0)->after('stock_max');
            $table->double('cost', 20, 2)->comment('Ultimo costo del producto')->default(0)->after('price');
            $table->double('percentage_earning', 2)->comment('Porcentaje de Ganancia')->default(0)->nullable()->after('status');
            $table->double('percentage_commission', 2)->comment('Porcentaje de Comision')->default(0)->nullable()->after('percentage_earning');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_warehouse', function (Blueprint $table) {
            $table->dropColumn(['percentage_earning', 'percentage_commission', 'cost']);
        });
    }
}
