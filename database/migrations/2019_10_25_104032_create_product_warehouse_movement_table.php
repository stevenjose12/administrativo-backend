<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductWarehouseMovementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_warehouse_movement', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_warehouse_id')->unsigned();
            $table->index('product_warehouse_id');
            $table->integer('product_id')->unsigned();
            $table->index('product_id');
            $table->integer('type');
            $table->integer('current_stock')->comment('Stock al momento que se genero el movimiento');
            $table->integer('amount')->comment('Cantidad que entra o sale del producto en el almacen');
            $table->integer('creator_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('product_warehouse_id')->references('id')->on('product_warehouse')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('creator_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_warehouse_movement');
    }
}
