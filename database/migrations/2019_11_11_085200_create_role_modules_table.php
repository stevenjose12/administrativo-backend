<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoleModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_modules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('role_enterprise_sub_id')->comment('ID del rol dentro de la empresa')->unsigned();
            $table->index('role_enterprise_sub_id');
            $table->integer('module_id')->comment('ID del modulo')->unsigned();
            $table->index('module_id');
            $table->timestamps();

            $table->foreign('role_enterprise_sub_id')->references('id')->on('role_enterprise_subs')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('module_id')->references('id')->on('modules')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_modules');
    }
}
