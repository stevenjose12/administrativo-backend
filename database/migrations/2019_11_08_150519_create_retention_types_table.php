<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRetentionTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rentention_types', function (Blueprint $table) {
            $table->increments('id');
            $table->double('percentage')->comment('Porcentaje del tipo de retencion');
            $table->integer('status')->comment('1: Activo, 2: Suspendido');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rentention_types');
    }
}
