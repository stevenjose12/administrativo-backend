<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBillNumberControlNumberAndTypeColumnsToPurchaseOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_order', function (Blueprint $table) {
            $table->string('bill_number')->comment('Numero de Factura')->nullable()->after('code');
            $table->string('control_number')->comment('Numero de Control de la Factura')->nullable()->after('bill_number');
            $table->string('type')->default(1)->comment('Tipo de movimiento de Compra, 1: Orden de Compra, 2: Factura de compra')->nullable()->after('control_number');
            $table->dateTime('bill_date_reception')->comment('Fecha de recepcion de la factura')->nullable()->after('date_delivery_real');
            $table->dateTime('bill_date_emission')->comment('Fecha de emision de la factura')->nullable()->after('bill_date_reception');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_order', function (Blueprint $table) {
            $table->dropColumn(['bill_number', 'control_number', 'type', 'bill_date_reception', 'bill_date_emission']);
        });
    }
}
