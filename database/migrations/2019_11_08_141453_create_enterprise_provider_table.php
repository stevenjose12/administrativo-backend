<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnterpriseProviderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enterprise_providers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('enterprise_id')->unsigned()->nullable()->comment('ID del la empresa');
            $table->index('enterprise_id');
            $table->integer('provider_id')->unsigned()->nullable()->comment('ID del proveedor');
            $table->integer('creator_id')->unsigned()->nullable();

            $table->foreign('enterprise_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enterprise_providers');
    }
}
