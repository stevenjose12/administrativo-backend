<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigSellersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_sellers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('seller_id')->unsigned()->comment('ID del vendedor');
            $table->index('seller_id');
            $table->integer('price_offer')->default(0)->comment('Precio 2: Oferta');
            $table->integer('price_max')->default(0)->comment('Precio 3: Máximo');
            $table->integer('percentage')->nullable()->comment('Porcentaje de descuento');
            $table->timestamps();

            $table->foreign('seller_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config_sellers');
    }
}
