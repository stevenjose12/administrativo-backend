<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrencyConversionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currency_conversions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('master_id')->unsigned()->comment('ID de la moneda maestra (Dolar)');
            $table->index('master_id');
            $table->integer('slave_id')->unsigned()->comment('ID de la moneda esclava (Resto de las monedas)');
            $table->index('slave_id');
            $table->double('conversion_rate', 2)->comment('Taza de conversion');
            $table->timestamps();

            $table->foreign('master_id')->references('id')->on('currencies')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('slave_id')->references('id')->on('currencies')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currency_conversions');
    }
}
