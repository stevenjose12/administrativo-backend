<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameExpenseIngressClassAndExpenseIngressTypeToExpensesIngressTypeAndExpensesIngressClass extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::table('expenses_ingress_payments', function (Blueprint $table) {
        //     $table->renameColumn('expense_ingress_class','expenses_ingress_class');
        //     $table->renameColumn('expense_ingress_class','expenses_ingress_type');
        // });
        Schema::table('expenses_ingress_payments', function (Blueprint $table) {
            $table->renameColumn('expense_ingress_class','expenses_ingress_class');
            $table->renameColumn('expense_ingress_type','expenses_ingress_type');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses_ingress_payments');
    }
}
