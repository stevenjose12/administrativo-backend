<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRetentionTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_retention_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('retention_type_id')->unsigned()->comment('ID del tipo de retencion');
            $table->integer('user_id')->unsigned()->comment('ID del usuario');
            $table->timestamps();

            $table->foreign('retention_type_id')->references('id')->on('rentention_types')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_retention_types');
    }
}
