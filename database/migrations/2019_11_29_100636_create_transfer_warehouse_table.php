<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransferWarehouseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfer_warehouse', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('origin_warehouse_id')->unsigned()->comment('Almacen origen');
            $table->index('origin_warehouse_id');
            $table->integer('destiny_warehouse_id')->unsigned()->comment('Almacen destino');
            $table->index('destiny_warehouse_id');
            $table->dateTime('date_sent')->comment('Fecha de envio')->nullable();
            $table->dateTime('date_recieved')->comment('Fecha de recepcion')->nullable();
            $table->integer('status')->default(1)->comment('1: Enviado, 2: Recibido, 3: Anulado');
            $table->integer('creator_id')->unsigned()->comment('ID del usuario creador')->nullable();
            $table->timestamps();

            $table->foreign('origin_warehouse_id')->references('id')->on('warehouses')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('destiny_warehouse_id')->references('id')->on('warehouses')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('creator_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfer_warehouse');
    }
}
