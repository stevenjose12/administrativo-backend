<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPercentageEarningsPercentageCommissionAndExemptColumnsToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->double('percentage_earning', 2)->comment('Porcentaje de Ganancia')->nullable()->after('status');
            $table->double('percentage_commission', 2)->comment('Porcentaje de Comision')->nullable()->after('percentage_earning');
            $table->integer('exempt')->default(0)->comment('0: No esta exento de impuestos, 1: Esta exento de impuestos')->after('percentage_commission');
            $table->integer('serialization')->default(0)->comment('0: No posee serializacion, 1: Posee serializacion')->after('exempt');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn(['percentage_earning', 'percentage_commission', 'exempt', 'serialization']);
        });
    }
}
