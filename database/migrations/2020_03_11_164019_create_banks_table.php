<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('enterprise_id')->unsigned()->comment('ID de la empresa asociado a la cuenta');
            $table->index('enterprise_id');
            $table->string('name');
            $table->integer('status')->default(1)->comment('1: Activo, 2: Desactivado');
            $table->integer('creator_id')->unsigned()->comment('ID del usuario que creo el registro');
            $table->index('creator_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('enterprise_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('creator_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banks');
    }
}
