<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStatusToRequestOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('request_order', function (Blueprint $table) {
            $table->integer('status')->comment('0: Sin procesar, 1: Procesado, 2: Cancelado')->after('total');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('request_order', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
