<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeletePurchaseOrderIdColumnProductSerials extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_serials', function (Blueprint $table) {
            \DB::statement('SET FOREIGN_KEY_CHECKS = 0');

                $table->dropForeign('product_serials_purchase_order_id_foreign'); 
                $table->dropColumn('purchase_order_id');

            \DB::statement('SET FOREIGN_KEY_CHECKS = 1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_serials', function (Blueprint $table) {
            $table->integer('purchase_order_id')->unsigned()->after('product_warehouse_id')->nullable();
            $table->foreign('purchase_order_id')->references('id')->on('purchase_order')->onUpdate('cascade')->onDelete('cascade');
        });
    }
}
