<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameRoleEnterpriseSubIdRoleModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('role_modules', function (Blueprint $table) {
            $table->dropForeign('role_modules_role_enterprise_sub_id_foreign');
            $table->dropIndex('role_modules_role_enterprise_sub_id_index');
            $table->renameColumn('role_enterprise_sub_id', 'role_enterprise_id');
            $table->index('role_enterprise_id');
            $table->foreign('role_enterprise_id')->references('id')->on('role_enterprises')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
