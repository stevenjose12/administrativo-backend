<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusColumnProductWarehouseMovementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_warehouse_movement', function (Blueprint $table) {
            $table->integer('status')->comment('0: Compremetido, 1: Procesado')->default(1)->after('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_warehouse_movement', function (Blueprint $table) {
            $table->dropColumn(['status']);
        });
    }
}
