<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExpensesIngressClassAndExpressIngressTypeToExpressIngressPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expenses_ingress_payments', function (Blueprint $table) {
            $table->dropForeign(['expenses_ingress_id']);
            $table->dropIndex(['expenses_ingress_id']);
            $table->integer('expense_ingress_class')->after('amount')->comment('Tipo de Movimiento, 1: Compra de Mercancia, 2: Ingreso de Factura');
            $table->string('expense_ingress_type')->after('amount')->comment('Modelo de la operacion asociada');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses_ingress_payments');
    }
}
