<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesIngressDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses_ingress_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('expenses_ingress_id')->unsigned()->comment('ID de la ingreos de gasto');
            $table->index('expenses_ingress_id');
            $table->string('description')->comment('Nombre/Descripcion del producto');
            $table->double('price', 10, 2)->comment('Precio del producto');
            $table->integer('amount')->comment('Cantidad del producto');
            $table->integer('exempt')->comment('Extento de impuesto');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('expenses_ingress_id')->references('id')->on('expenses_ingress')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses_ingress_details');
    }
}
