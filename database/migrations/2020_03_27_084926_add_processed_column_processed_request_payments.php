<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProcessedColumnProcessedRequestPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('processed_request_payments', function (Blueprint $table) {
            $table->integer('processed')->default(0)->after('amount')->comment('0: Sin Procesar, 1: Procesado. Dependiente cierre de caja');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('processed_request_payments', function (Blueprint $table) {
            $table->dropColumn(['processed']);
        });
    }
}
