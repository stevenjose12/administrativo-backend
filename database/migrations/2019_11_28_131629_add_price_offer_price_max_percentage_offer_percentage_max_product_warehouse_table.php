<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPriceOfferPriceMaxPercentageOfferPercentageMaxProductWarehouseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_warehouse', function (Blueprint $table) {
            $table->double('price_offer', 20, 2)->comment('Precio de Oferta')->default(0)->after('price');
            $table->double('price_max', 20, 2)->comment('Precio de Maxio')->default(0)->after('price_offer');
            $table->double('percentage_offer', 2)->comment('Porcentaje de ganancia al precio de oferta')->default(0)->nullable()->after('status');
            $table->double('percentage_max', 2)->comment('Porcentaje de ganancia al precio de maximo')->default(0)->nullable()->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_warehouse', function (Blueprint $table) {
            $table->dropColumn(['price_offer', 'price_max', 'percentage_offer', 'percentage_max']);
        });
    }
}
