<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransferWarehouseDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfer_warehouse_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transfer_warehouse_id')->comment('ID de la nota de entrega/ID de la factura')->unsigned();
            $table->index('transfer_warehouse_id');
            $table->integer('product_id')->comment('ID del producto')->unsigned();
            $table->index('product_id');
            $table->integer('amount')->comment('Cantidad');
            $table->double('cost', 2)->comment('Costo del producto en el almacen origen');
            $table->timestamps();

            $table->foreign('transfer_warehouse_id')->references('id')->on('transfer_warehouse')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfer_warehouse_details');
    }
}
