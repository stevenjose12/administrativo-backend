<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bank_account_id')->unsigned()->comment('ID del banco asociado a la cuenta');
            $table->index('bank_account_id');
            $table->double('amount', 20,3)->comment('Monto de la operacion');
            $table->integer('type')->default(1)->comment('1: Ingreso, 2: Egreso');
            $table->integer('status')->default(1)->comment('1: Activo, 2: Desactivado');
            $table->integer('creator_id')->unsigned()->comment('ID del usuario que creo el registro');
            $table->index('creator_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('bank_account_id')->references('id')->on('bank_accounts')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('creator_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_payments');
    }
}
