<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_order', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->comment('Numero de orden de compra');
            $table->integer('currency_id')->unsigned()->comment('A que divisa le pertenece');
            $table->dateTime('date_purchase')->comment('Fecha de compra')->nullable();
            $table->dateTime('date_delivery')->comment('Fecha de entrega (Teorica)')->nullable();
            $table->dateTime('date_delivery_real')->comment('Fecha de entrega (Real)')->nullable();
            $table->text('observations')->comment('Observaciones')->nullable();
            $table->double('subtotal', 2)->comment('Subtotal')->nullable();
            $table->double('vat', 2)->comment('IVA')->nullable();
            $table->double('total', 2)->comment('Total')->nullable();
            $table->integer('creator_id')->unsigned()->comment('ID del usuario creador')->nullable();
            $table->timestamps();

            $table->foreign('currency_id')->references('id')->on('currencies')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('creator_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_order');
    }
}
