<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcessedRequestDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('processed_request_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('processed_request_id')->comment('ID de la nota de entrega/ID de la factura')->unsigned();
            $table->index('processed_request_id');
            $table->integer('product_id')->comment('ID del producto')->unsigned();
            $table->index('product_id');
            $table->double('subtotal', 2)->comment('Subtotal');
            $table->double('vat', 2)->comment('IVA');
            $table->double('total', 2)->comment('Total');
            $table->timestamps();

            $table->foreign('processed_request_id')->references('id')->on('processed_request')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('processed_request_details');
    }
}
