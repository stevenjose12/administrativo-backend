<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('compound_id')->unsigned()->nullable()->comment('ID del producto maestro');
            $table->integer('product_id')->unsigned()->nullable()->comment('ID del producto esclavo');
            $table->double('amount', 15, 2)->nullable()->default(0);
            $table->timestamps();

            $table->index('compound_id');
            $table->index('product_id');
            $table->index('amount');

            $table->foreign('compound_id')->references('id')->on('products')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_details');
    }
}
