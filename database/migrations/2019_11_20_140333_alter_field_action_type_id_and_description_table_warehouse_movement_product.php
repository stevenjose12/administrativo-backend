<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFieldActionTypeIdAndDescriptionTableWarehouseMovementProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_warehouse_movement', function (Blueprint $table) {
            $table->integer('action_id')->nullable()->change();
            $table->integer('action_class')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_warehouse_movement', function (Blueprint $table) {
            $table->integer('action_id');
            $table->dropColumn('action_class');
        });
    }
}
