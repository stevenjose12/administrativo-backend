<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterColumnsToProcessedRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('processed_request', function (Blueprint $table) {
            $table->string('code')->nullable()->change();
            $table->string('bill_number')->nullable()->change();
            $table->string('control_number')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('processed_request', function (Blueprint $table) {
            $table->string('code')->comment('Numero de nota de entrega');
            $table->string('bill_number')->comment('Numero de Factura de venta');
            $table->string('control_number')->comment('Numero de Control de la Factura');
        });
    }
}
