<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusColumnProcessedRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('processed_request', function (Blueprint $table) {
            $table->integer('status')->default(1)->comment('0: Sin pagar, 1: Pagada')->after('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('processed_request', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
