<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoleEnterpriseSubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_enterprise_subs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('role_enterprise_id')->comment('ID del rol dentro de la empresa (Usuario maestro)')->unsigned();
            $table->integer('enterprise_id')->comment('ID de la empresa (Usuario maestro)')->unsigned();
            $table->integer('user_id')->comment('ID del usuario')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('role_enterprise_id')->references('id')->on('role_enterprises')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('enterprise_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_enterprise_subs');
    }
}
