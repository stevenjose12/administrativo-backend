<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEnterpriseIdToProcessedRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('processed_request', function (Blueprint $table) {
            $table->integer('enterprise_id')->unsigned()->nullable()->after('total');
            $table->index('enterprise_id');
            $table->foreign('enterprise_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('processed_request', function (Blueprint $table) {
            $table->dropForeign(['enterprise_id']);
            $table->dropIndex(['enterprise_id']);
            $table->dropColumn('enterprise_id');
        });
    }
}
