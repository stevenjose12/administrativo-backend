<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCodeColumnTransferWarehouseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transfer_warehouse', function (Blueprint $table) {
            $table->string('code')->comment('Numero de traslado entre almacenes')->after('id')->nullable();
            $table->integer('enterprise_id')->comment('ID de la empresa que le pertenece este traslado')->after('id')->nullable();
            $table->index('enterprise_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transfer_warehouse', function (Blueprint $table) {
            $table->dropColumn(['code', 'enterprise_id']);
        });
    }
}
