<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnTypePercentageToConfigSellersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('config_sellers', function (Blueprint $table) {
            $table->integer('type_percentage')->default(1)->after('price_max')->comment('Tipo de porcentaje: 1: Productos, 2: Ventas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('config_sellers', function (Blueprint $table) {
            $table->dropColumn('type_percentage');
        });
    }
}
