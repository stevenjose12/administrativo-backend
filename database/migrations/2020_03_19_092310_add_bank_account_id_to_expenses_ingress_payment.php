<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBankAccountIdToExpensesIngressPayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expenses_ingress_payments', function (Blueprint $table) {
            $table->integer('bank_account_id')->after('expenses_ingress_id')->nullable()->comment('ID de la cuenta de banco asociada');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expenses_ingress_payments', function (Blueprint $table) {
            $table->dropColumn(['bank_account_id']);
        });
    }
}
