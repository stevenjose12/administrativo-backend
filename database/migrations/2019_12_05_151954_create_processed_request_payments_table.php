<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcessedRequestPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('processed_request_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('processed_request_id')->unsigned()->comment('ID de la Nota de Entrega/Factura');
            $table->index('processed_request_id');
            $table->integer('type')->comment('1: Efectivo, 2: Credito, 3: Debito, 4: Transferencia');
            $table->double('amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('processed_request_payments');
    }
}
