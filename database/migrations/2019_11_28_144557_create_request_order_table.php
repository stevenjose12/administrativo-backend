<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_order', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('warehouse_id')->unsigned()->comment('Almacen de donde se origina');
            $table->index('warehouse_id');
            $table->integer('client_id')->unsigned()->comment('ID del cliente');
            $table->index('client_id');
            $table->string('code')->comment('Numero de pedido');
            $table->integer('currency_id')->unsigned()->comment('A que divisa le pertenece');
            $table->dateTime('date_emission')->comment('Fecha de emision')->nullable();
            $table->text('observations')->comment('Observaciones')->nullable();
            $table->double('subtotal', 2)->comment('Subtotal')->nullable();
            $table->double('vat', 2)->comment('IVA')->nullable();
            $table->double('total', 2)->comment('Total')->nullable();
            $table->integer('creator_id')->unsigned()->comment('ID del usuario creador')->nullable();
            $table->timestamps();

            $table->foreign('warehouse_id')->references('id')->on('warehouses')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('client_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('currency_id')->references('id')->on('currencies')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('creator_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_order');
    }
}
