<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branches', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('enterprise_id')->unsigned()->comment('ID del usuario tipo empresa');
            $table->index('enterprise_id');
            $table->string('name')->nullable()->comment('Nombre');
            $table->string('code')->nullable()->comment('Codigo');
            $table->integer('status')->default(1)->comment('0: Nueva, 1: Activo, 2: Suspendida/Bloqueada');
            $table->integer('creator_id')->unsigned()->comment('ID del usuario creador');
            $table->index('creator_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('enterprise_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('creator_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branches');
    }
}
