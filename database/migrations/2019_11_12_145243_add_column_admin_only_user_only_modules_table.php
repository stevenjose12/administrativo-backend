<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnAdminOnlyUserOnlyModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('modules', function (Blueprint $table) {
            $table->integer('admin_only')->comment('Solamente los vera el usuario tipo superadministrador o administrador')->default(0)->after('path');
            $table->integer('user_only')->comment('Solamente los vera el usuario tipo empresa')->default(0)->after('admin_only');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('modules', function (Blueprint $table) {
            $table->dropColumn(['admin_only', 'user_only']);
        });
    }
}
