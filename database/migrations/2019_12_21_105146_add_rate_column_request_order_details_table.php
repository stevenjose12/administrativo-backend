<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRateColumnRequestOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('request_order_details', function (Blueprint $table) {
            $table->integer('rate')->comment('0: Precio 1, 1: Precio 2, 2: Precio 3')->after('quantity')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('request_order_details', function (Blueprint $table) {
            $table->dropColumn(['rate']);
        });
    }
}
