<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class MakeNullableRequestOrderIdColumnProcessedRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('processed_request', function (Blueprint $table) {
            $table->dropForeign('processed_request_request_order_id_foreign');
            $table->integer('request_order_id')->unsigned()->nullable()->comment('ID del pedido origen')->change();
            $table->foreign('request_order_id')->references('id')->on('request_order')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('processed_request', function (Blueprint $table) {
            $table->integer('request_order_id')->unsigned()->change();
            $table->foreign('request_order_id')->references('id')->on('request_order')->onUpdate('cascade')->onDelete('cascade');
        });
    }
}
