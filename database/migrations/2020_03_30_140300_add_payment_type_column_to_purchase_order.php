<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

    // /**
    //  * Run the migrations.
    //  *
    //  * @return void
    //  */
    // public function up()
    // {
    //     Schema::table('processed_request_payments', function (Blueprint $table) {
    //         $table->integer('processed')->default(0)->after('amount')->comment('0: Sin Procesar, 1: Procesado. Dependiente cierre de caja');
    //     });
    // }

    // /**
    //  * Reverse the migrations.
    //  *
    //  * @return void
    //  */
    // public function down()
    // {
    //     Schema::table('processed_request_payments', function (Blueprint $table) {
    //         $table->dropColumn(['processed']);
    //     });
    // }

class AddPaymentTypeColumnToPurchaseOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_order', function(Blueprint $table){
            $table->integer('payment_type')->nullable()->comment('1: Contado, 2: Credito');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_order', function (Blueprint $table) {
            $table->dropColumn(['payment_type']);
        });
    }
}
