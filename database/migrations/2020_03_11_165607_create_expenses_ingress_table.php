<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesIngressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses_ingress', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('enterprise_id')->unsigned()->comment('ID de la empresa');
            $table->index('enterprise_id');
            $table->integer('provider_id')->unsigned()->comment('ID del proveedor');
            $table->index('provider_id');
            $table->integer('payment_id')->nullable()->comment('ID del pago asociado');
            $table->index('payment_id');
            $table->dateTime('date_emission')->comment('Fecha de emision')->nullable();
            $table->dateTime('date_received')->comment('Fecha de recepcion')->nullable();
            $table->dateTime('date_expired')->comment('Fecha de expiracion')->nullable();
            $table->string('code')->comment('Correlativo de la factura ingresada')->nullable();
            $table->string('bill_number')->comment('Numero de Factura de venta')->nullable();
            $table->string('control_number')->comment('Numero de Control de la Factura')->nullable();
            $table->integer('payment_type')->comment('1: Contado, 2: Credito');
            $table->text('description')->comment('Descripcion/Observacion');
            $table->double('subtotal', 10, 2)->comment('Subtotal');
            $table->double('iva', 10, 2)->comment('IVA');
            $table->double('total', 10, 2)->comment('Total');
            $table->integer('status')->default(0)->comment('0: Sin pagar, 1: Pagado');
            $table->integer('creator_id')->unsigned()->comment('ID del usuario creador')->nullable();
            $table->index('creator_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('enterprise_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('provider_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('creator_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses_ingress');
    }
}
