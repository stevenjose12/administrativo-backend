<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashCountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cash_count', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bank_id')->unsigned()->comment('ID del banco asociado al movimiento');
            $table->index('bank_id');
            $table->integer('bank_account_id')->unsigned()->comment('ID de la cuenta de banco asociada al movimiento');
            $table->index('bank_account_id');
            $table->double('amount', 10, 2)->comment('Cantidad que ingresa');
            $table->integer('creator_id')->unsigned()->comment('ID del usuario que creo el movimiento');
            $table->index('creator_id');
            $table->timestamps();

            $table->foreign('bank_id')->references('id')->on('banks')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('bank_account_id')->references('id')->on('bank_accounts')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('creator_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cash_count');
    }
}
