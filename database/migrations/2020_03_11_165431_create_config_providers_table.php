<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_providers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('provider_id')->unsigned()->comment('ID del proveedor');
            $table->index('provider_id');
            $table->integer('days_deadline')->comment('Plazo de vencimiento');
            $table->integer('creator_id')->unsigned()->comment('ID del creador');
            $table->index('creator_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('provider_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('creator_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config_providers');
    }
}
