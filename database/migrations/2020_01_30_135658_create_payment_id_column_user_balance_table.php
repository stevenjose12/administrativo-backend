<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentIdColumnUserBalanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_balance', function (Blueprint $table) {
            $table->integer('payment_id')->unsigned()->nullable()->after('action_type')
            ->comment('ID del pago que se esta utilizando para saldar al deuda, Nulo si es un pago o ingreso');
            $table->foreign('payment_id')->references('id')->on('user_payments')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_balance', function (Blueprint $table) {
            $table->dropForeign(['payment_id']);
            $table->dropColumn(['payment_id']);
        });
    }
}
