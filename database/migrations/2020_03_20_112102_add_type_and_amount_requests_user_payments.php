<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeAndAmountRequestsUserPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('requests_user_payments', function (Blueprint $table) {
            $table->integer('type')->after('creator_id')->comment('1: Abono, 2: Pago completo');
            $table->double('amount', 20, 3)->after('type')->comment('Monto cruzado');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('requests_user_payments', function (Blueprint $table) {
            $table->dropColumn(['type', 'amount']);
        });
    }
}
