<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        App\Models\RetentionType::truncate();
        App\Models\Menu::truncate();
        App\Models\Module::truncate();
        App\Models\Permission::truncate();
        App\Models\Currency::truncate();
        App\Role::truncate();
        // App\Models\Product::truncate();
        // App\Models\ProductWarehouse::truncate();
        // App\Models\ProductModel::truncate();
        // App\Models\ProductSubCategory::truncate();
        // App\Models\ProductProvider::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $this->call(RolesTableSeeder::class);
        $this->call(MenusTableSeeder::class);
        $this->call(ModulesTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(RetentionTypesTableSeeder::class);
        $this->call(CurrenciesTableSeeder::class);
        // $this->call(ProductsTableSeeder::class);
        // $this->call(ProductsWarehouseTableSeeder::class);
        // $this->call(ProductModelSeeder::class);
        // $this->call(ProductSubcategorySeeder::class);
        // $this->call(ProductProviderSeeder::class);
    }
}
