<?php

use Illuminate\Database\Seeder;

use App\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::insert([
            // Users
            ['id' => 1, 'module_id' => 1, 'name' => 'users-create', 'display_name' => 'Create Users', 'description' => 'Create Users', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 2, 'module_id' => 1, 'name' => 'users-read', 'display_name' => 'Read Users', 'description' => 'Read Users', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 3, 'module_id' => 1, 'name' => 'users-update', 'display_name' => 'Update Users', 'description' => 'Update Users', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 4, 'module_id' => 1, 'name' => 'users-delete', 'display_name' => 'Delete Users', 'description' => 'Delete Users', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            // Roles
            ['id' => 5, 'module_id' => 2, 'name' => 'roles-create', 'display_name' => 'Create Roles', 'description' => 'Create Roles', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 6, 'module_id' => 2, 'name' => 'roles-read', 'display_name' => 'Reead Roles', 'description' => 'Read Roles', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 7, 'module_id' => 2, 'name' => 'roles-update', 'display_name' => 'Update Roles', 'description' => 'Update Roles', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 8, 'module_id' => 2, 'name' => 'roles-delete', 'display_name' => 'Delete Roles', 'description' => 'Delete Roles', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            // Brands
            ['id' => 9,  'module_id' => 3, 'name' => 'brands-create', 'display_name' => 'Create Brands', 'description' => 'Create Brands', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 10, 'module_id' => 3, 'name' => 'brands-read', 'display_name' => 'Read Brands', 'description' => 'Read Brands', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 11, 'module_id' => 3, 'name' => 'brands-update', 'display_name' => 'Update Brands', 'description' => 'Update Brands', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 12, 'module_id' => 3, 'name' => 'brands-delete', 'display_name' => 'Delete Brands', 'description' => 'Delete Brands', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            // Categories
            ['id' => 13, 'module_id' => 4, 'name' => 'categories-create', 'display_name' => 'Create Categories', 'description' => 'Create Categories', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 14, 'module_id' => 4, 'name' => 'categories-read', 'display_name' => 'Read Categories', 'description' => 'Read Categories', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 15, 'module_id' => 4, 'name' => 'categories-update', 'display_name' => 'Update Categories', 'description' => 'Update Categories', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 16, 'module_id' => 4, 'name' => 'categories-delete', 'display_name' => 'Delete Categories', 'description' => 'Delete Categories', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            // Warehouses
            ['id' => 17, 'module_id' => 5, 'name' => 'warehouses-create', 'display_name' => 'Create Warehouses', 'description' => 'Create Warehouses', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 18, 'module_id' => 5, 'name' => 'warehouses-read', 'display_name' => 'Read Warehouses', 'description' => 'Read Warehouses', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 19, 'module_id' => 5, 'name' => 'warehouses-update', 'display_name' => 'Update Warehouses', 'description' => 'Update Warehouses', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 20, 'module_id' => 5, 'name' => 'warehouses-delete', 'display_name' => 'Delete Warehouses', 'description' => 'Delete Warehouses', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            // Providers
            ['id' => 21, 'module_id' => 6, 'name' => 'providers-create', 'display_name' => 'Create Providers', 'description' => 'Create Providers', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 22, 'module_id' => 6, 'name' => 'providers-read', 'display_name' => 'Read Providers', 'description' => 'Read Providers', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 23, 'module_id' => 6, 'name' => 'providers-update', 'display_name' => 'Update Providers', 'description' => 'Update Providers', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 24, 'module_id' => 6, 'name' => 'providers-delete', 'display_name' => 'Delete Providers', 'description' => 'Delete Providers', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            // Clients
            ['id' => 25, 'module_id' => 7, 'name' => 'clients-create', 'display_name' => 'Create Clients', 'description' => 'Create Clients', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 26, 'module_id' => 7, 'name' => 'clients-read', 'display_name' => 'Read Clients', 'description' => 'Read Clients', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 27, 'module_id' => 7, 'name' => 'clients-update', 'display_name' => 'Update Clients', 'description' => 'Update Clients', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 28, 'module_id' => 7, 'name' => 'clients-delete', 'display_name' => 'Delete Clients', 'description' => 'Delete Clients', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            // Enterprises
            ['id' => 29, 'module_id' => 8, 'name' => 'enterprises-create', 'display_name' => 'Create Enterprises', 'description' => 'Create Enterprises', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 30, 'module_id' => 8, 'name' => 'enterprises-read', 'display_name' => 'Read Enterprises', 'description' => 'Read Enterprises', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 31, 'module_id' => 8, 'name' => 'enterprises-update', 'display_name' => 'Update Enterprises', 'description' => 'Update Enterprises', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 32, 'module_id' => 8, 'name' => 'enterprises-delete', 'display_name' => 'Delete Enterprises', 'description' => 'Delete Enterprises', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            // Modules
            ['id' => 33, 'module_id' => 9, 'name' => 'modules-create', 'display_name' => 'Create Modules', 'description' => 'Create Modules', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 34, 'module_id' => 9, 'name' => 'modules-read', 'display_name' => 'Read Modules', 'description' => 'Read Modules', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 35, 'module_id' => 9, 'name' => 'modules-update', 'display_name' => 'Update Modules', 'description' => 'Update Modules', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 36, 'module_id' => 9, 'name' => 'modules-delete', 'display_name' => 'Delete Modules', 'description' => 'Delete Modules', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            // Products
            ['id' => 37, 'module_id' => 10, 'name' => 'products-create', 'display_name' => 'Create Products', 'description' => 'Create Products', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 38, 'module_id' => 10, 'name' => 'products-read', 'display_name' => 'Read Products', 'description' => 'Read Products', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 39, 'module_id' => 10, 'name' => 'products-update', 'display_name' => 'Update Products', 'description' => 'Update Products', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 40, 'module_id' => 10, 'name' => 'products-delete', 'display_name' => 'Delete Products', 'description' => 'Delete Products', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
        ]);
    }
}
