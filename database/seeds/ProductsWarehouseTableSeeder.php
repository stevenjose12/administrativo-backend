<?php

use Illuminate\Database\Seeder;
use App\Models\ProductWarehouse;

class ProductsWarehouseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductWarehouse::insert([
            ['id' => 1, 'warehouse_id' => 5, 'product_id' => 1, 'status' => 1, 'stock_min' => 50, 'stock_max' => 75, 'price' => 500, 'location' => '1A', 'creator_id' => 24, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 2, 'warehouse_id' => 5, 'product_id' => 2, 'status' => 1, 'stock_min' => 75, 'stock_max' => 100, 'price' => 750, 'location' => '2A', 'creator_id' => 24, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 3, 'warehouse_id' => 6, 'product_id' => 3, 'status' => 1, 'stock_min' => 100, 'stock_max' => 125, 'price' => 1000, 'location' => '3B', 'creator_id' => 24, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
        ]);
    }
}
