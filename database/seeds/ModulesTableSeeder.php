<?php

use App\Models\Module;
use Illuminate\Database\Seeder;

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Module::insert([
            ['id' => 1, 'menu_id' => 4, 'name' => 'Usuarios', 'path' => '/users', 'admin_only' => 0, 'enterprise_only' => 0, 'user_only' => 0, 'seller_only' => 0, 'required' => 1, 'status' => 1, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 2, 'menu_id' => 4, 'name' => 'Configuración de Roles', 'path' => '/roles_enterprises', 'admin_only' => 0, 'enterprise_only' => 1, 'user_only' => 1, 'seller_only' => 0, 'required' => 1, 'status' => 1, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 3, 'menu_id' => 1, 'name' => 'Marcas', 'path' => '/brands', 'admin_only' => 0, 'enterprise_only' => 0, 'user_only' => 1, 'seller_only' => 0, 'required' => 1, 'status' => 1, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 4, 'menu_id' => 1, 'name' => 'Categorías', 'path' => '/categories', 'admin_only' => 0, 'enterprise_only' => 0, 'user_only' => 1, 'seller_only' => 0, 'required' => 1, 'status' => 1, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 5, 'menu_id' => 1, 'name' => 'Almacenes', 'path' => '/warehouses', 'admin_only' => 0, 'enterprise_only' => 1, 'user_only' => 1, 'seller_only' => 0, 'required' => 0, 'status' => 1, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 6, 'menu_id' => 2, 'name' => 'Clientes', 'path' => '/clients', 'admin_only' => 0, 'enterprise_only' => 0, 'user_only' => 1, 'seller_only' => 0, 'required' => 0, 'status' => 1, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 7, 'menu_id' => 3, 'name' => 'Proveedores', 'path' => '/providers', 'admin_only' => 0, 'enterprise_only' => 0, 'user_only' => 1, 'seller_only' => 0, 'required' => 1, 'status' => 1, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 8, 'menu_id' => 4, 'name' => 'Empresas', 'path' => '/enterprises', 'admin_only' => 1, 'enterprise_only' => 0, 'user_only' => 0, 'seller_only' => 0, 'required' => 0, 'status' => 1, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 9, 'menu_id' => 4, 'name' => 'Módulos', 'path' => '/modules', 'status' => 1, 'admin_only' => 1, 'enterprise_only' => 0, 'user_only' => 0, 'seller_only' => 0, 'required' => 0, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 10, 'menu_id' => 1, 'name' => 'Productos', 'path' => '/products', 'status' => 1, 'admin_only' => 0, 'enterprise_only' => 0, 'user_only' => 1, 'seller_only' => 0, 'required' => 1, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 11, 'menu_id' => 3, 'name' => 'Ordenes', 'path' => '/orders', 'status' => 1, 'admin_only' => 0, 'enterprise_only' => 0, 'user_only' => 1, 'seller_only' => 0, 'required' => 0, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 12, 'menu_id' => 4, 'name' => 'Divisas', 'path' => '/currencies', 'status' => 0, 'admin_only' => 1, 'enterprise_only' => 0, 'user_only' => 0, 'seller_only' => 0, 'required' => 0, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 13, 'menu_id' => 1, 'name' => 'Sucursales', 'path' => '/branches', 'status' => 1, 'admin_only' => 0, 'enterprise_only' => 1, 'user_only' => 1, 'seller_only' => 0, 'required' => 0, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 14, 'menu_id' => 1, 'name' => 'Productos Compuestos', 'path' => '/products-compounds', 'status' => 1, 'admin_only' => 0, 'enterprise_only' => 0, 'seller_only' => 0, 'required' => 0, 'user_only' => 1, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 15, 'menu_id' => 1, 'name' => 'Operaciónes de Inventario', 'path' => '/inventory-adjustment', 'status' => 1, 'admin_only' => 0, 'enterprise_only' => 0, 'seller_only' => 0, 'required' => 0, 'user_only' => 1, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 16, 'menu_id' => 1, 'name' => 'Precios', 'path' => '/prices', 'status' => 1, 'admin_only' => 0, 'enterprise_only' => 0, 'user_only' => 1, 'seller_only' => 0, 'required' => 0, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 17, 'menu_id' => 1, 'name' => 'Conversión', 'path' => '/conversions', 'status' => 0, 'admin_only' => 0, 'enterprise_only' => 0, 'user_only' => 1, 'seller_only' => 0, 'required' => 0, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 18, 'menu_id' => 2, 'name' => 'Pedidos', 'path' => '/requests', 'status' => 1, 'admin_only' => 0, 'enterprise_only' => 0, 'user_only' => 1, 'seller_only' => 1, 'required' => 0, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 19, 'menu_id' => 5, 'name' => 'Reporte de Inventario', 'path' => '/report-inventory', 'status' => 1, 'admin_only' => 0, 'enterprise_only' => 0, 'user_only' => 1, 'seller_only' => 1, 'required' => 0, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 20, 'menu_id' => 5, 'name' => 'Reporte de Traslados', 'path' => '/transfer-warehouses', 'status' => 1, 'admin_only' => 0, 'enterprise_only' => 0, 'user_only' => 1, 'seller_only' => 0, 'required' => 0, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 21, 'menu_id' => 2, 'name' => 'Notas de Entrega', 'path' => '/delivery-notes', 'status' => 1, 'admin_only' => 0, 'enterprise_only' => 0, 'user_only' => 1, 'seller_only' => 1, 'required' => 0, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 22, 'menu_id' => 2, 'name' => 'Punto de Venta', 'path' => '/sales', 'status' => 1, 'admin_only' => 0, 'enterprise_only' => 0, 'user_only' => 1, 'seller_only' => 1, 'required' => 0, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 23, 'menu_id' => 5, 'name' => 'Reporte de Ventas', 'path' => '/report-sale', 'status' => 1, 'admin_only' => 0, 'enterprise_only' => 0, 'user_only' => 1, 'seller_only' => 1, 'required' => 0, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 24, 'menu_id' => 5, 'name' => 'Reporte de Movimientos', 'path' => '/report-movement', 'status' => 1, 'admin_only' => 0, 'enterprise_only' => 0, 'user_only' => 1, 'seller_only' => 0, 'required' => 0, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 25, 'menu_id' => 6, 'name' => 'Cuentas por Cobrar', 'path' => '/payments', 'status' => 1, 'admin_only' => 0, 'enterprise_only' => 0, 'user_only' => 1, 'seller_only' => 1, 'required' => 0, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 26, 'menu_id' => 2, 'name' => 'Vendedores', 'path' => '/sellers', 'status' => 1, 'admin_only' => 0, 'enterprise_only' => 0, 'user_only' => 1, 'seller_only' => 0, 'required' => 0, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 27, 'menu_id' => 6, 'name' => 'Reporte de Cobros', 'path' => '/report-payment', 'status' => 1, 'admin_only' => 0, 'enterprise_only' => 0, 'user_only' => 1, 'seller_only' => 1, 'required' => 0, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 28, 'menu_id' => 8, 'name' => 'Ingreso de Fact de Gastos', 'path' => '/expenses-ingress', 'status' => 1, 'admin_only' => 0, 'enterprise_only' => 0, 'seller_only' => 0, 'required' => 0, 'user_only' => 1, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 29, 'menu_id' => 8, 'name' => 'Listado de Pagos a Facturas', 'path' => '/expenses-payments', 'status' => 0, 'admin_only' => 0, 'enterprise_only' => 0, 'seller_only' => 0, 'required' => 0, 'user_only' => 1, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 30, 'menu_id' => 9, 'name' => 'Listado de Bancos', 'path' => '/banks', 'status' => 1, 'admin_only' => 0, 'enterprise_only' => 0, 'seller_only' => 0, 'required' => 0, 'user_only' => 1, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 31, 'menu_id' => 8, 'name' => 'Reporte de Pagos', 'path' => '/expenses-payments-report', 'status' => 1, 'admin_only' => 0, 'enterprise_only' => 0, 'seller_only' => 0, 'required' => 0, 'user_only' => 1, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 32, 'menu_id' => 8, 'name' => 'Cuentas por Pagar', 'path' => '/debts-to-pay', 'status' => 1, 'admin_only' => 0, 'enterprise_only' => 0, 'seller_only' => 0, 'required' => 0, 'user_only' => 1, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 33, 'menu_id' => 9, 'name' => 'Reporte de Movimientos Bancarios', 'path' => '/banking-transactions', 'status' => 1, 'admin_only' => 0, 'enterprise_only' => 0, 'seller_only' => 0, 'required' => 0, 'user_only' => 1, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 34, 'menu_id' => 8, 'name' => 'Cierre de Caja', 'path' => '/cash-count', 'status' => 1, 'admin_only' => 0, 'enterprise_only' => 0, 'seller_only' => 0, 'required' => 0, 'user_only' => 1, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()]
        ]);
    }
}
