<?php

use Illuminate\Database\Seeder;

use App\Models\Currency;

class CurrenciesTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Currency::insert([
			['name' => 'Dolares', 'code' => 'USD', 'status' => 1, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
			['name' => 'Euros', 'code' => 'EUR', 'status' => 1, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
			['name' => 'Bolivares', 'code' => 'VES', 'status' => 1, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()]
		]);
	}
}
