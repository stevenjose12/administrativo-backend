<?php

use Illuminate\Database\Seeder;

use App\Models\ProductModel;

class ProductModelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductModel::insert([
            ['id' => 1, 'model_id' => 16, 'product_id' => 1, 'creator_id' => 24, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 2, 'model_id' => 20, 'product_id' => 2, 'creator_id' => 24, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 3, 'model_id' => 25, 'product_id' => 3, 'creator_id' => 24, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
        ]);
    }
}
