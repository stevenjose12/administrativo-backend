<?php

use Illuminate\Database\Seeder;

use App\Models\ProductProvider;

class ProductProviderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductProvider::insert([
            ['id' => 1, 'provider_id' => 1, 'product_id' => 1, 'creator_id' => 24, 'price' => 1000, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 2, 'provider_id' => 1, 'product_id' => 2, 'creator_id' => 24, 'price' => 800, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 3, 'provider_id' => 2, 'product_id' => 3, 'creator_id' => 24, 'price' => 300, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
        ]);
    }
}
