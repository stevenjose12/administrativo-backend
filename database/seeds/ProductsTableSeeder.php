<?php

use Illuminate\Database\Seeder;

use App\Models\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::insert([
            ['id' => 1, 'user_id' => 24, 'name' => 'Producto prueba 1', 'description' => 'Descripcion produto prueba 1', 'avatar' => NULL, 'status' => 1, 'percentage_earning' => 10, 'percentage_commission' => 20, 'exempt' => 0, 'serialization' => 0, 'creator_id' => 24, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 2, 'user_id' => 24, 'name' => 'Producto prueba 2', 'description' => 'Descripcion produto prueba 2', 'avatar' => NULL, 'status' => 1, 'percentage_earning' => 15, 'percentage_commission' => 25, 'exempt' => 1, 'serialization' => 0, 'creator_id' => 24, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 3, 'user_id' => 24, 'name' => 'Producto prueba 3', 'description' => 'Descripcion produto prueba 3', 'avatar' => NULL, 'status' => 1, 'percentage_earning' => 20, 'percentage_commission' => 30, 'exempt' => 1, 'serialization' => 0, 'creator_id' => 24, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
        ]);
    }
}
