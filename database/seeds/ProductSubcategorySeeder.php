<?php

use Illuminate\Database\Seeder;

use App\Models\ProductSubCategory;

class ProductSubcategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductSubCategory::insert([
            ['id' => 1, 'subcategory_id' => 20, 'product_id' => 1, 'creator_id' => 24, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 2, 'subcategory_id' => 24, 'product_id' => 2, 'creator_id' => 24, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
            ['id' => 3, 'subcategory_id' => 30, 'product_id' => 3, 'creator_id' => 24, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
        ]);
    }
}
