<?php

namespace App;

use Hash;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laratrust\Traits\LaratrustUserTrait;

class User extends Authenticatable
{
    use LaratrustUserTrait;
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = ['assigned_warehouses'];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function setPasswordAttribute($pass)
    {
        return $this->attributes['password'] = Hash::make($pass);
    }

    public function person()
    {
        return $this->hasOne('App\Models\Person', 'user_id')->withTrashed();
    }

    public function permissions()
    {
        return $this->belongsToMany('App\Models\Module', 'permissions', 'user_id', 'module_id')->whereNull('permissions.deleted_at');
    }

    public function enterprise_modules()
    {
        return $this->belongsToMany('App\Models\Module', 'enterprise_modules', 'enterprise_id', 'module_id')->withTimestamps();
    }

    public function permissions_by_enterprise()
    {
        return $this->belongsToMany('App\Models\Permission', 'permission_user', 'user_id', 'permission_id');
    }

    public function enterprise_users()
    {
        return $this->hasOne('App\Models\EnterpriseUser', 'user_id', 'id');
    }

    public function role_enterprise_subuser()
    {
        return $this->hasOne('App\Models\RoleSubuser', 'user_id', 'id');
    }

    public function role_enterprise()
    {
        return $this->hasMany('App\Models\RoleEnterprise', 'enterprise_id', 'id');
    }

    public function zone_user()
    {
        return $this->hasOne('App\Models\ZoneUser', 'user_id', 'id');
    }

    public function client_enterprise()
    {
        return $this->hasOne('App\Models\EnterpriseClient', 'client_id', 'id');
    }

    public function enterprise_providers()
    {
        return $this->hasOne('App\Models\EnterpriseProvider', 'provider_id', 'id');
    }

    public function user_retention_types()
    {
        return $this->hasOne('App\Models\UserRetentionType', 'user_id', 'id');
    }

    public function enterprise_administrator()
    {
        return $this->hasOne('App\Models\AdministratorEnterprise', 'enterprise_id', 'id');
    }

    public function product_providers()
    {
        return $this->hasMany('App\Models\ProductProvider', 'provider_id', 'id');
    }

    public function product_provider()
    {
        return $this->hasOne('App\Models\ProductProvider', 'provider_id', 'id');
    }

    public function enterprise_warehouse()
    {
        return $this->hasMany('App\Models\Warehouse', 'user_id', 'id');
    }

    public function warehouses()
    {
        return $this->belongsToMany('App\Models\Warehouse', 'user_warehouses', 'user_id')->withPivot([
            'id', 'branch_id', 'warehouse_id', 'user_id',
        ])->withTimestamps();
    }

    public function getAssignedWarehousesAttribute($value)
    {
        return $this->assigned_warehouses();
    }

    public function assigned_warehouses()
    {
        return $this->warehouses()->select('warehouses.id as warehouse_id', 'warehouses.branch_id')->get();
    }

    public function balance()
    {
        return $this->hasMany('App\Models\UserBalance', 'user_id', 'id');
    }

    public function accumulated_amount()
    {
        return $this->hasOne('App\Models\UserBalance', 'user_id', 'id')->latest('id');
    }

    public function amount_owed()
    {
        return $this->hasOne('App\Models\UserBalance', 'user_id', 'id')->latest();
    }

    public function request()
    {
        return $this->hasMany('App\Models\ProcessedOrder', 'client_id', 'id');
    }

    public function seller()
    {
        return $this->hasMany('App\Models\ProcessedOrder', 'creator_id', 'id');
    }

    public function requestSeller()
    {
        return $this->hasMany('App\Models\Request', 'creator_id', 'id');
    }

    public function configuration_enterprise()
    {
        return $this->hasOne('App\Models\ConfigEnterprise', 'enterprise_id', 'id');
    }

    public function modules_seller()
    {
        return $this->belongsToMany('App\Models\Module', 'sellers_modules', 'user_id', 'module_id')->withTimestamps();
    }

    public function zones()
    {
        return $this->belongsToMany('App\Models\Zone', 'zone_users', 'user_id', 'zone_id')->withPivot('creator_id')->withTimestamps();
    }

    public function configuration_seller()
    {
        return $this->hasOne('App\Models\ConfigSeller', 'seller_id', 'id');
    }

    public function configuration_client()
    {
        return $this->hasOne('App\Models\ConfigClient', 'client_id', 'id');
    }

    public function configuration_provider()
    {
        return $this->hasOne('App\Models\ConfigProvider', 'provider_id', 'id');
    }

    public function user_payments()
    {
        return $this->hasMany('App\Models\UserPayment', 'user_id', 'id')->orderBy('id', 'asc');
    }
}
