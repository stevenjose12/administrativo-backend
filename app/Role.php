<?php

namespace App;

use Laratrust\Models\LaratrustRole;

class Role extends LaratrustRole
{
    public function modules()
    {
        return $this->belongsToMany('App\Models\Module', 'role_modules', 'role_enterprise_sub_id', 'module_id')->withTimestamps();
    }
}
