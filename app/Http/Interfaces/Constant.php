<?php

namespace App\Http\Interfaces;

interface Constant
{
    const ENTRY = 1;
    const OUTPUT = 0;

    const ROLE_SUPER = 1;
    const ROLE_ADMIN = 2;
    const ROLE_ENTERPRISE = 3;
    const ROLE_SUB_USER = 4;
    const ROLE_SELLER = 5;
}
