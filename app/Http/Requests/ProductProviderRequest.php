<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Rule;

class ProductProviderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => ['required'],
            'provider_id' => ['required'],
            'price' => ['required'],
        ];
    }

    public function attributes()
    {
        return [
            'product_id' => 'Producto',
            'provider_id' => 'Proveedor',
            'price' => 'Precio',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'El campo :attribute es necesario para completar el registro.',
            'unique' => 'Ya existe un registro igual al del campo :attribute, Por favor intente con otro valor.'
        ];
    }

    public function formatErrors(Validator $validator)
    {
        return [
            'msg' => $validator->errors()->first()
        ];
    }
}
