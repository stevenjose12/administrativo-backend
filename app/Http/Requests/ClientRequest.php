<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => [
                'required',
                'string',
                'max:50',
            ],
            // 'last_name' => [
            //     'required',
            //     'string',
            //     'max:50'
            // ],
            'identity_document' => [
                // 'sometimes',
                'required_without:fiscal_identification',
                'nullable',
                'between:6,9',
            ],
            'fiscal_identification' => [
                // 'sometimes',
                'required_without:identity_document',
                'nullable',
                'between:6,9',
            ],
            'direction' => ['required'],
            'phone' => ['required', 'between:7,11'],
            'zone_id' => ['required'],
            'person_type' => ['required'],
            'retention_type_id' => ['sometimes'],
            'email' => ['required', 'email'],
            'days_deadline' => ['required', 'numeric', 'digits_between:1,3'],
        ];
    }

    public function attributes()
    {
        return [
            'first_name' => 'Nombre',
            'last_name' => 'Apellido',
            'identity_document' => 'Documento de identidad',
            'fiscal_identification' => 'Identificación Fiscal (RIF)',
            'direction' => 'Dirección',
            'phone' => 'Teléfono',
            'zone_id' => 'Zona',
            'retention_type_id' => 'Tipo de retencion',
            'email' => 'Correo',
            'person_type' => 'Personalidad Jurídica',
            'days_deadline' => 'Plazo de crédito',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'El campo :attribute es requerido para completar el registro.',
            'unique' => 'Ya se encuentran registros del campo :attribute.',
            'max' => 'El campo :attribute no puede tener mas de :max caracteres.',
            'min' => 'El campo :attribute no puede tener menos de :min caracteres.',
            'email' => 'El campo :attribute debe tener un formato de correo valido.',
            'between' => 'El campo :attribute debe tener entre :min y :max caracteres',
            'digits_between' => 'El campo :attribute debe tener entre :min y :max caracteres',
            'numeric' => ':attribute debe ser numérico.',
            "required_without" => 'Debe ingresar una cédula o RIF'
        ];
    }

    public function formatErrors(Validator $validator)
    {
        return [
            'msg' => $validator->errors()->first(),
            'result' => false,
        ];
    }
}
