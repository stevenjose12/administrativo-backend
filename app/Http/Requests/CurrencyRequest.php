<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Rule;

class CurrencyRequest extends FormRequest
{
/**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                Rule::unique('currencies')->ignore($this->id)->where(function ($query) {
                    return $query->whereNull('deleted_at');
                }),
                'between:1,50'
            ],
            'code' => [
                'required',
                Rule::unique('currencies')->ignore($this->id)->where(function ($query) {
                    return $query->whereNull('deleted_at');
                }),
                'between:1,3'
            ]
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Nombre',
            'code' => 'Código ISO'
        ];
    }

    public function messages()
    {
        return [
            'required' => 'El campo :attribute es obligatorio para completar el registro.',
            'unique' => 'Ya existe un registro con el valor que intenta ingresar en el campo :attribute',
            'max' => 'El campo :attribute debe tener un máximo de :max carácteres'
        ];
    }

    public function formatErrors(Validator $validator)
    {
        return [ 
            'msg' => $validator->errors()->first(),
            'result' => false,
        ];
    }
}
