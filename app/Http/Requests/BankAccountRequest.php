<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BankAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // \Log::info($this);
        if($this->created_at == $this->updated_at && $this->type == 3) {
            return [
                'account_number' => [
                    'required',
                ],
                'type' => [
                    'required'
                ],
                'bank_id' => [
                    'required'
                ],
                'amount' => [
                    'required_without:id'
                ],
                'user_id' => [
                    'required'
                ]
            ];
        }
        return [
            'account_number' => [
                'required',
                'max:50',
                'min:8'
            ],
            'type' => [
                'required'
            ],
            'bank_id' => [
                'required'
            ],
            'amount' => [
                'required_without:id'
            ],
            'user_id' => [
                'required'
            ]
        ];
    }

    public function attributes()
    {
        return [
            'account_number' => 'Nombre/Nº de Cuenta',
            'type' => 'Tipo de Cuenta',
            'amount' => 'Capital Inicial',
            'bank_id' => 'Banco',
            'user_id' => 'Creador'
        ];
    }

    public function messages()
    {
        return [
            'required' => 'El campo :attribute es requerido para completar el registro.',
            'required_without' => 'El campo :attribute es requerido para completar el registro.',
            'max' => 'El campo :attribute no puede tener mas de :max caracteres.',
            'min' => 'El campo :attribute debe tener por lo menos :min, caracteres.'
        ];
    }
}
