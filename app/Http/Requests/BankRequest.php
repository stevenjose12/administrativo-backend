<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BankRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'max:50',
            ],
            'type' => [
                'required',
            ],
            'enterprise_id' => [
                'required'
            ],
            'user_id' => [
                'required'
            ]
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Nombre',
            'type' => 'Tipo de Entidad',
            'enterprise_id' => 'Empresa',
            'user_id' => 'Creador'
        ];
    }

    public function messages()
    {
        return [
            'required' => 'El campo :attribute es requerido para completar el registro.',
            'max' => 'El campo :attribute no puede tener mas de :max caracteres.'
        ];
    }
}
