<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DeliveryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_id' => 'required|numeric|exists:users,id',
            'enterprise_id' => 'required|exists:users,id',
            'warehouse_id' => 'required|numeric|exists:warehouses,id',
            'code' => 'required|numeric',
            'currency_id' => 'required|numeric',
            'observations' => 'required',
            'subtotal' => 'required',
            'vat' => 'required',
            'total' => 'required',
            'creator_id' => 'required|exists:users,id',
        ];
    }
    function messages()
    {
        return[
            'required' => 'El campo :attribute es requerido',
            'exists' => 'El :attribute no se encuentra',
            'numeric' => 'El :attribute debe ser numérico',
        ];
    }
}
