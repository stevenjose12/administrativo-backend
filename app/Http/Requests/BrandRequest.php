<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Rule;

class BrandRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                Rule::unique('brands')->ignore($this->id)->where(function ($query) {
                    return $query->whereNull('deleted_at')->where('user_id', $this->user_id);
                }),
                'max:255'
            ],
            'code' => [
                'required',
                Rule::unique('brands')->ignore($this->id)->where(function ($query) {
                    return $query->whereNull('deleted_at')->where('user_id', $this->user_id);
                }),
                'between:1,3'
            ],
            'user_id' => [
                'required'
            ]
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Nombre de la marca',
            'code' => 'Código de la marca',
            'user_id' => 'Empresa'
        ];
    }

    public function messages()
    {
        return [
            'required' => 'El campo :attribute es obligatorio para completar el registro.',
            'unique' => 'Ya existe un registro con el valor que intenta ingresar en el campo :attribute',
            'max' => 'El campo :attribute debe tener un máximo de :max carácteres'
        ];
    }

    public function formatErrors(Validator $validator)
    {
        return [ 
            'msg' => $validator->errors()->first(),
            'result' => false,
        ];
    }
}
