<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Rule;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => [
                'required',
            ],
            'role'    => ['required'],
            'company' => ['required_if:role,=,1','required_if:role,=,2'],
            'name' => [
                'required',
                'string',
                'max:50',
                'min:3',
                Rule::unique('products')->ignore($this->id)->where(function ($query) {
                    return $query->whereNull('deleted_at')->where('user_id', $this->user_id);
                }),
            ],
            // 'percentage_earning' => ['nullable', 'numeric'],
            'percentage_commission' => ['nullable', 'numeric'],
            'description' => [
                'nullable',
                'string',
                'max:191'
            ],
            'exempt' => ['nullable'],
            'serialization' => ['nullable'],
            'image' => ['required_if:id, =, null'],
            'category' => ['nullable'],
            'subcategory' => ['nullable'],
            'brand' => ['nullable'],
            'model' => ['nullable', 'integer'],
        ];
    }

    public function attributes()
    {
        return [
            'code'          => 'Código',
            'role'          => 'rol',
            'company'       => 'empresa',
            'name'          => 'nombre',
            'percentage_earning' => 'Porcentaje de Ganancia',
            'percentage_commission' => 'Porcentaje de Comisión',
            'description'   => 'descripción',
            'exempt'        => 'producto exento',
            'serialization' => 'serialización',
            'image'         => 'imagen',
            'category'      => 'categoría',
            'subcategory'   => 'subcategoría',
            'brand'         => 'marca',
            'model'         => 'modelo',
        ];
    }

    public function messages()
    {
        return [
            "required_unless" => "El campo :attribute es obligatorio.",
            "required_if"     => "El campo :attribute es obligatorio.",
            "confirmed"       => "Las contraseñas no coinciden"
        ];
    }

    public function formatErrors(Validator $validator)
    {
        return [
            'result' => true,
            'msg' => $validator->errors()->first(),
        ];
    }
}
