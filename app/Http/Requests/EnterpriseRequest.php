<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Rule;

class EnterpriseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'string',
                'between:3,50',
                Rule::unique('users')->ignore($this->id, 'id')->where(function ($query){
                    $query->where('level', 3)->whereNull('deleted_at');
                })
            ],
            'first_name' => [
                'required',
                'string',
                'max:50'
            ],
            'identity_document' => [
                'nullable',
                'between:6,9'
            ],
            'fiscal_identification' => [
                'required',
                'between:6,9',
            ],
            'email' => [
                'required', 
                'email',
                Rule::unique('users')->ignore($this->id, 'id')->where(function ($query){
                    $query->where('level', 3)->whereNull('deleted_at');
                })
            ],
            'phone'            => ['required', 'between:7,11'],
            'direction'        => ['required'],
            'administrator_id' => ['required'],
            'sale_type'        => ['required'],
            'password'         => ['required_without:id','confirmed', 'min:6'],
        ];
    }

    public function attributes()
    {
        return [
            'name'                  => 'Nombre de usuario',
            'first_name'            => 'Nombre de empresa',
            'identity_document'     => 'Cédula',
            'fiscal_identification' => 'RIF',
            'direction'             => 'Dirección',
            'phone'                 => 'Teléfono',
            'administrator_id'      => 'Administrador',
            'sale_type'             => 'Tipo de Venta',
            'email'                 => 'Correo'
        ];
    }

    public function messages()
    {
        return [
            'required' => 'El campo :attribute es requerido para completar el registro.',
            'unique'   => 'Ya se encuentran registros del campo :attribute.',
            'max'      => 'El campo :attribute no puede tener mas de :max caracteres.',
            'min'      => 'El campo :attribute no puede tener menos de :min caracteres.',
            'string'   => 'El campo :attribute debe ser una cadena de caracteres.',
            'email'    => 'El campo :attribute debe tener un formato de correo valido.',
            'between' => 'El campo :attribute debe tener entre :min y :max caracteres',
            'required_without' => 'El campo :attribute es obligatorio al crear un registro por primera vez'
        ];
    }

    public function formatErrors(Validator $validator)
    {
        return [
            'msg' => $validator->errors()->first(),
            'result' => false
        ];
    }
}
