<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExpensesIngressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'enterprise_id' => [
                'required'
            ],
            'provider_id' => [
                'required'
            ],
            'creator_id' => [
                'required'
            ],
            'date_emission' => [
                'required'
            ],
            'date_received' => [
                'required'
            ],
            'date_expired' => [
                'required'
            ],
            'bill_number' => [
                'required',
                'max:50',
            ],
            'control_number' => [
                'required',
                'max:50',
            ],
            'description' => [
                'required'
            ],
            'payment_type' => [
                'required'
            ],
            'method_type' => [
                'required_if:payment_type,1'
            ],
            'bank_id' => [
                'required_if:method_type,2',
                'required_if:method_type,3',
                'required_if:method_type,4'
            ],
            'bank_account_id' => [
                'required_with:bank_id'
            ],
            'balance' => [
                'required_if:payment_type,1',
                'required',
                'numeric',
                'min:0'
            ]
        ];
    }

    public function attributes()
    {
        return [
            'enterprise_id' => 'Empresa',
            'creator_id' => 'Creador',
            'date_emission' => 'Fecha de emisión',
            'date_received' => 'Fecha de recepción',
            'date_expired' => 'Fecha de expiración',
            'bill_number' => 'Número de factura',
            'control_number' => 'Número de control',
            'description' => 'Observaciones',
            'payment_type' => 'Tipo de Pago',
            'method_type' => 'Metodo de Pago',
            'bank_id' => 'Banco',
            'bank_account_id' => 'Cuenta de Banco'
        ];
    }

    public function messages()
    {
        return [
            'required' => 'El campo :attribute es requerido para completar el registro.',
            'required_if' => 'El campo :attribute es requerido para completa el registro',
            'numeric' => 'El campo :attribute debe ser un numero.',
            'max' => 'El campo :attribute no puede tener mas de :max caracteres.',
            'min' => 'El campo :attribute debe ser mayor de :min'
        ];
    }
}
