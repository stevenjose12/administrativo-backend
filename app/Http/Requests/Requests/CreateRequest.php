<?php

namespace App\Http\Requests\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'forthright' => 'required',
            'warehouse_id' => 'required',
            'branch_id' => 'required',
            'client_id' => 'required|exists:users,id',
            'code' => 'required',
            'currency_id' => 'required|exists:currencies,id',
            'date_emission' => 'required',
            'observations' => 'required',
            'subtotal' => 'required',
            'taxable' => 'required',
            'exempt' => 'required',
            'vat' => 'required',
            'total' => 'required',
            'creator_id' => 'required|exists:users,id',
            'enterprise_id' => 'required|exists:users,id',
            'products' => 'array|required'
        ];
    }

    public function messages()
    {
        return [
            'required' => 'El campo :attribute es obligatorio para completar el registro.',
            'exists' => 'El campo :attribute debe existir',
        ];
    }
}
