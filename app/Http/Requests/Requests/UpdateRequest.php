<?php

namespace App\Http\Requests\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required',
            'forthright' => 'required',
            'warehouse_id' => 'required',
            /**
             * Inicio refactorizar
             * Se debe validar que client_id sea cliente
             */
            'client_id' => 'required|exists:users,id',
            /**
             * Fin refactorizar
             */
            'code' => 'required',
            'currency_id' => 'required|exists:currencies,id',
            'date_emission' => 'required',
            'observations' => 'required',
            'subtotal' => 'required',
            'taxable' => 'required',
            'exempt' => 'required',
            'vat' => 'required',
            'total' => 'required',
            'creator_id' => 'required|exists:users,id',
            'enterprise_id' => 'required|exists:users,id',
            'products' => 'array|required'
        ];
    }
    public function messages()
    {
        return [
            'required' => 'El campo :attribute es obligatorio para completar el registro.',
            'exists' => 'El campo :attribute debe existir',
        ];
    }
    
}
