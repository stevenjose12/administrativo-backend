<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Rule;

class InventoryRequest extends FormRequest
{
/**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
			return [
				'id' => [
					'required'
				],
				// 'last_name' => [
				//     'required',
				//     'string',
				//     'max:50'
				// ],
				'product_id' => [
					'required'
				],
				'branch_id' => [
					'required'
				],
				'warehouse_id' => [
					'required'
				],
				'type' => [
					'required'
				],
				'current_stock' => ['required'],
				'amount' => ['required', 'integer', 'min:1'],
				'description' => ['required', 'string', 'between:5,255'],
				'creator_id' => ['required']
			];
    }

    public function attributes()
    {
			return [
				'product_id' => 'Producto',
				'branch_id' => 'Sucursal',
				'warehouse_id' => 'Almacen',
				'type' => 'Tipo',
				'current_stock' => 'Stock Acutal',
				'amount' => 'Cantidad a modificar',
				'description' => 'Descripcion',
				'creator_id' => 'ID del usuario'
			];
    }

    public function messages()
    {
			return [
				'required' => 'El campo :attribute es requerido para completar el registro.',
				'unique' => 'Ya se encuentran registros del campo :attribute.',
				'max' => 'El campo :attribute no puede tener mas de :max caracteres.',
				'min' => 'El campo :attribute no puede tener menos de :min caracteres.',
				'email' => 'El campo :attribute debe tener un formato de correo valido.',
				'between' => 'El campo :attribute debe tener entre :min y :max caracteres',
				'digits_between' => 'El campo :attribute debe tener entre :min y :max caracteres'
			];
    }

    public function formatErrors(Validator $validator)
    {
			return [
				'msg' => $validator->errors()->first(),
				'result' => false
			];
    }
}
