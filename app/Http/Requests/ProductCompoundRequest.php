<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductCompoundRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company' => ['required_if:role,=,1', 'required_if:role,=,2'],
            'name' => [
                'required',
                'string',
                'max:50',
                'min:3',
                Rule::unique('products')->ignore($this->id)->where(function ($query) {
                    return $query->whereNull('deleted_at');
                }),
            ],
            'percentage_earning' => ['nullable', 'numeric'],
            'percentage_commission' => ['nullable', 'numeric'],
            'description' => ['nullable', 'string', 'max:191'],
            'exempt' => ['nullable'],
            'serialization' => ['nullable'],
            'category' => ['nullable'],
            'subcategory' => ['nullable'],
            'brand' => ['nullable'],
            'model' => ['nullable', 'integer'],
            'image' => ['required_if:id, =, null'],
            'prods' => ['required'],
        ];
    }

    public function attributes()
    {
        return [
            'role' => 'Rol',
            'company' => 'Empresa',
            'name' => 'Nombre',
            'percentage_earning' => 'Porcentaje de Ganancia',
            'percentage_commission' => 'Porcentaje de Comisión',
            'description' => 'Descripcion',
            'exempt' => 'Producto Exento',
            'serialization' => 'Serialización',
            'brand' => 'Marca',
            'model' => 'Modelo',
            'category' => 'Categoría',
            'subcategory' => 'Subcategoría',
            'image' => 'Imagen',
        ];
    }

    public function messages()
    {
        return [
            "required_unless" => "El campo :attribute es obligatorio.",
            "required_if" => "El campo :attribute es obligatorio.",
            "confirmed" => 'Las contaseñas no coinciden',
        ];
    }

    public function formatErrors(Validator $validator)
    {
        return [
            'result' => true,
            'msg' => $validator->errors()->first(),
        ];
    }
}
