<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Rule;

class ProviderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required'],
            'code' => ['required', 'between:1,3'],
            'fiscal_identification' => ['required', 'between:6,9'],
            'direction' => ['max:255'],
            'phone' => ['required', 'numeric', 'digits_between:7,11'],
            'email' => ['required', 'email'],
            'enterprise_id' => ['required'],
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Nombre',
            'code' => 'Código',
            'fiscal_identification' => 'RIF',
            'person_type' => 'Tipo de personalidad',
            'retention_type_id' => 'Tipo de Retención IVA',
            'direction' => 'Dirección',
            'phone' => 'Teléfono',
            'email' => 'E-Mail',
            'enterprise_id' => 'Empresa',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'El campo :attribute es necesario para completar el registro.',
            'unique' => 'Campo :attribute. No pueden haber dos proveedores con igual :attribute. Por favor cambielo.',
            'max' => 'El campo :attribute no puede tener más de :max caracteres.',
            'email' => 'El campo :attribute debe tener un formáto de correo válido.',
            'numeric' => 'El campo :attribute debe ser solamente numérico.',
            'between' => 'El campo :attribute debe tener entre :min y :max caracteres',
            'digits_between' => 'El campo :attribute debe tener entre :min y :max caracteres'
        ];  
    }

    public function formatErrors(Validator $validator)
    {
        return [
            'result' => true, 
            'msg' => $validator->errors()->first()
        ];
    }
}