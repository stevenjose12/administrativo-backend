<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DebtsToPayRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {


        return [
           
            'creator_id' => [
                'required'
            ],
            'date' => [
                'required'
            ],
            'method_type' => [
                'required_if:payment_type,1'
            ],
            'bank_account_id' => [
                'required_with:bank_id'
            ],
            'amount' => [
                'required'
            ],
            'balance' => [
                'required',
                'numeric',
                'min:0'
            ]
        ];
    }

    public function attributes()
    {
        return [
            'creator_id' => 'Creador',
            'date' => 'Fecha de emisión',
            'method_type' => 'Metodo de Pago',
            'bank_account_id' => 'Cuenta de Banco',
            'amount' => 'Monto',
            'balance' => 'Saldo'
        ];
    }

    public function messages()
    {
        return [
            'required' => 'El campo :attribute es requerido para completar el registro.',
            'required_if' => 'El campo :attribute es requerido para completa el registro',
            'required_with' => 'El campo :attribute es requerido para completa el registro',
            'numeric' => 'El campo :attribute debe ser un numero.',
            'min' => 'El campo :attribute debe ser mayor de :min'
        ];
    }
}
