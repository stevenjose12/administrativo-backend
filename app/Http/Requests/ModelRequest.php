<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Rule;

class ModelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                Rule::unique('models')->ignore($this->id)->where(function ($query) {
                    return $query->where('status', 1)->where('brand_id', $this->brand_id);
                }),
                'max:255'
            ],
            'code' => [
                'required',
                Rule::unique('models')->ignore($this->id)->where(function ($query) {
                    return $query->where('status', 1)->where('brand_id', $this->brand_id);
                }),
                'between:1,3'
            ],
            'brand_id' => ['required']
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Nombre del modelo',
            'code' => 'Código del modelo',
            'brand_id' => 'Marca'
        ];
    }

    public function messages()
    {
        return [
            'required' => 'El campo :attribute es obligatorio para completar el registro.',
            'unique' => 'Ya existe un registro con el valor que intenta ingresar en el campo :attribute',
            'max' => 'El campo :attribute debe tener un máximo de :max carácteres'
        ];
    }

    public function formatErrors(Validator $validator)
    {
        return [
            'msg' => $validator->errors()->first(),
            'result' => false,
        ];
    }
}
