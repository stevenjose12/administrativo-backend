<?php 

namespace App\Http\Traits;

use Carbon\Carbon;

trait Format
{
  public function parseFormat($date)
	{
    return Carbon::parse($date)->format('Y-m-d H:i:s');
  }

  public function parseFormatDateTime($date, $time = null)
  {
    return empty($time) 
      ? Carbon::parse($date)->format('Y-m-d h:i:s') : 
        $time == 'start' ? Carbon::parse($date)->startOfDay()->format('Y-m-d H:i:s') :
        Carbon::parse($date)->endOfDay()->format('Y-m-d H:i:s');

  }
}