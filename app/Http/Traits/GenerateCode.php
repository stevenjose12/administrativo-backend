<?php

	namespace App\Http\Traits;

	trait GenerateCode {

		public function generateEnterpriseCode($name) {
			$code = '';
	        $name = preg_replace('/\s+/', '', $name);
	        $randomNum=substr(str_shuffle("0123456789"), 0, 2);
	        $random_position = rand(0,strlen($randomNum)-1);
	        $random_char = $name[0];
	        $code = strtoupper(substr($randomNum,0,$random_position).$random_char.substr($randomNum,$random_position));

	        return $code;
		}
	}