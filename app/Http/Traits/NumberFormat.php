<?php

namespace App\Http\Traits;

use Carbon\Carbon;

trait NumberFormat
{
    public function numberFormat($amount, $currency_id = 1)
    {
        switch ($currency_id) {
            default:
                return number_format($amount, 2, '.', ',');
        }
    }
}
