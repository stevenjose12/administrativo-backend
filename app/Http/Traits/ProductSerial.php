<?php 

	namespace App\Http\Traits;
	use App\Models\ProductSerial as Serial;
	use App\Models\ProductWarehouse;
	use App\Models\Warehouse;

	define('_ENTRY',1);
	define('_OUTPUT',2);
	define('_COMMITTED',3);

	trait ProductSerial {

		/**
		*
		* @param $product Producto a serializar
		* @param $warehouse Almacen del producto
		* @param $operation ID de la operación
		* @param $operation_type Tipo de operación (Modelo)
		* @param $type Tipo de operación, Entrada, Salida y Traslado
		*/
		public function createProductSerials($product,$product_warehouse_id,$operation = null,$operation_type = null,$type = _ENTRY) {
			if (isset($product['serials']) && $product['serials']) {
				foreach($product['serials'] as $item) {
					$serial = new Serial;
						$serial->product_warehouse_id = $product_warehouse_id;
						$serial->serial = $item;
						if ($operation) {
							$serial->operation_id = $operation;
						}
						if ($operation_type) {
							$serial->operation_type = $operation_type;
						}
						$serial->type = $type;
					$serial->save();
				}
			}
		}

		/**
		*
		* Verifica si los seriales enviados estan almacenados en la BD
		* @param $products Array de productos
		* @param $warehouse_id ID del almacen en que se deben buscar los seriales
		*/
		public function checkExistSerial($products,$warehouse_id) {
			foreach ($products as $product) {
				if (isset($product['serials'])) {
					$check_pw = ProductWarehouse::where([
		                ['warehouse_id', $warehouse_id],
		                ['product_id', $product['id']],
		            ])->first();

		            $serials = Serial::where('product_warehouse_id',$check_pw->id)->where('type',ENTRY)->get(['serial'])->pluck('serial');

		            foreach($product['serials'] as $serial) {
		            	if (!in_array($serial,$serials->toArray())) {
		            		return response()->json([
		                        'msg' => "El serial *".$serial."* no se encuentra almacenado",
		                    ], 422);
		            	}
		            }
				}	            
	        }

	        return true;
		}

		/**
		*	Elimina los seriales de la tabla product_serials
		*	@param $product Objecto del producto con los seriales a eliminar
		*   @param $warehouse_id Almacen del que se van a eliminar los seriales
		*/
		public function deleteSerials($product,$warehouse_id) {
			if (isset($product['serials'])) {
				Serial::where('product_warehouse_id',$warehouse_id)->whereIn('serial',$product['serials'])->delete();
			}			
		}

		/**
		*	Elimina la serialización de productos comprometidos y agrega en nuevo almacen
		*	@param $id ID de la operación
		*   @param $product Producto a serializar
		*   @param $warehouse ID del almacen
		*   @param $product_warehouse_id ID del nuevo almacen
		*   @param $model Modelo de la operación
		*/
		public function deleteTransferSerials($id,$product,$warehouse,$product_warehouse_id,$model) {
			$serials = Serial::where([
				'operation_id' => $id,
				'operation_type' => $model
			])->whereHas('product_warehouse',function($q) use ($product,$warehouse) {
				$q->where('product_id',$product['id'])
					->where('warehouse_id',$warehouse);
			})->get();

			$product['serials'] = $serials->pluck('serial');

			$this->createProductSerials($product,$product_warehouse_id);

			Serial::whereIn('id',$serials->pluck('id'))->delete();
		}

		/**
		*	Agregar nuevamente la serialización a productos que estaban comprometidos
		*	@param $id ID de la operacióin abortada
		*	@param $model Modelo de la operación
		*   @param $output Por defecto en false, determina si es una salida o no
		*   @param $operation Parametro opcional para guardar la relación de la compra/venta/pedido
		*   @param $operation_type Parametro opcional para guardar el modelo de la compra/venta/pedido
		*/
		public function changeSerials($id,$model,$output = false,$operation = null, $operation_type = null) {
			$serials = Serial::where([
				'operation_id' => $id,
				'operation_type' => $model
			])->where('type',_COMMITTED)->get();

			foreach($serials as $_serial) {
				$serial = new Serial;
					$serial->product_warehouse_id = $_serial->product_warehouse_id;
					$serial->serial = $_serial->serial;
					if ($operation) {
						$serial->operation_id = $operation;
					}
					else if ($_serial->operation_id) {
						$serial->operation_id = $_serial->operation_id;
					}
					if ($operation_type) {
						$serial->operation_type = $operation_type;
					}
					else if ($_serial->operation_type) {
						$serial->operation_type = $_serial->operation_type;
					}
					$serial->type = $output ? _OUTPUT : _ENTRY;
				$serial->save();
			}

			Serial::whereIn('id',$serials->pluck('id'))->delete();
		}

		/**
		 * Verifica si un arreglo de seriales existen en la BD y son unicos en el almacen
		 * @param Array $products
		 * @param Int $warehouse
		 * @return Array $serial
		*/
		public function getIfSerialsExistsInWarehouse($products, $warehouse)
		{	
			foreach($products as $product) 
			{
				$serials = Serial::whereIn('serial',$product['serials'])
					->whereHas('product_warehouse', function($query) use ($product,$warehouse){
						$query->where('product_id',$product['id'])
							->where('warehouse_id',$warehouse);
					})->whereIn('type',[_ENTRY,_COMMITTED])->first();
				if($serials)
				{
					return $serials;
				}
			}

		}

		/**
		 * Verifica si en un arreglo de productos hay al menos uno serializado
		 * @param Array $products
		 * @return Bool
		 */
		public function checkIfAtLeastAProductIsSerialized($products)
		{
			return in_array(true,array_map(function($product) {
				return array_key_exists('serials',$product);
			},$products));
			// return array_key_exists('serials',$products);
		}

		/**
		 * @param Array products
		 * @return Boolean
		 */
		public function checkEntrySerials($products,$warehouse_id) {
			$warehouse = Warehouse::find($warehouse_id);

			foreach ($products as $product) {
				if (isset($product['serials'])) {
		            $serials = Serial::whereHas('product_warehouse',function($q) use ($product,$warehouse) {
		            	$q->where('product_id',$product['id'])->whereHas('warehouse',function($q) use ($warehouse) {
		            		$q->where('user_id',$warehouse->user_id);
		            	});
		            })->whereIn('type',[_ENTRY,_COMMITTED])->get(['serial'])->pluck('serial');

		            foreach($product['serials'] as $serial) {
		            	if (in_array($serial,$serials->toArray())) {
		            		return response()->json([
		                        'msg' => "El serial *".$serial."* ya se encuentra almacenado",
		                    ], 422);
		            	}
		            }
				}	            
	        }

	        return true;
		}

	}