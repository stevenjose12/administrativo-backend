<?php

namespace App\Http\Traits;

use App\Models\Warehouse;
use App\User;

trait WarehouseFilter
{
  public function warehouseFilter($enterpriseId, $userId, $roleUser, $branchId = [], $warehouseId = [])
  {
    $user = User::where('id', $userId)->first();

    // Si el primer valor del arreglo de las sucursales y almacenes es 0, entonces se debe tomar como un array vacio
    if (count($branchId) > 0) {
      $branchId = $branchId[0] == 0 ? [] : $branchId;
    }

    if (count($warehouseId) > 0) {
      $warehouseId = $warehouseId[0] == 0 ? [] : $warehouseId;
    }

    $warehouse = Warehouse::when($roleUser == 3, function ($query) use ($userId) {
      $query->where('user_id', $userId);
    }, function ($query) use ($enterpriseId) {
      $query->where('user_id', $enterpriseId);
    });

    switch ($roleUser) {
      case 3:
        $response = $warehouse->when((count($branchId) > 0), function ($query) use ($branchId, $warehouseId) {
          $query->whereIn('branch_id', $branchId)->when((count($warehouseId) > 0), function ($query2) use ($warehouseId) {
            $query2->whereIn('id', $warehouseId);
          });
        });
        return $response->get()->pluck('id');
        break;
      case 4:
        $response = $warehouse->when((count($branchId) > 0), function ($query) use ($branchId, $warehouseId, $user) {
          $query->whereIn('branch_id', $branchId)->when((count($warehouseId) > 0), function ($query2) use ($warehouseId) {
            $query2->whereIn('id', $warehouseId);
          }, function ($query2) use ($user) {
            // Al no enviar un arreglo de id de almacenes, entonces se filtran por lo que el usuario tenga asignado
            $query2->whereIn('id', $user->warehouses->pluck('id'));
          });
        }, function ($query) use ($warehouseId) {
          $query->whereIn('id', $warehouseId);
        });
        return $response->get()->pluck('id');
        break;
      case 5:
        $response = $warehouse->when(count($branchId) > 0, function ($query) use ($branchId, $warehouseId, $user) {
          $query->whereIn('branch_id', $branchId)->when((count($warehouseId) > 0), function ($query2) use ($warehouseId) {
            $query2->whereIn('id', $warehouseId);
          }, function ($query2) use ($user) {
            // Al no enviar un arreglo de id de almacenes, entonces se filtran por lo que el usuario tenga asignado
            $query2->whereIn('id', $user->warehouses->pluck('id'));
          });
        }, function ($query) use ($warehouseId) {
          $query->whereIn('id', $warehouseId);
        });
        return $response->get()->pluck('id');
        break;
      default:
        # code...
        break;
    }

    return $warehouse->get()->pluck('id');
  }
}
