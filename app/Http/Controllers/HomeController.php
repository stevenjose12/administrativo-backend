<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Traits\Format;
use App\Models\ProcessedRequest as Report;
use Excel;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    use Format;

    public function export_report(Request $request)
    {
        $reports = Report::with(['client' => function ($query) {
            $query->with('person')->withTrashed();
        },
            'details' => function ($query) {
                $query->with([
                    'product_warehouse.warehouse' => function ($query) {
                        $query->select('id', 'branch_id', 'name', 'code', 'deleted_at');
                    },
                    'product_warehouse.last_movement',
                ])->withTrashed();
            },
            'currency',
            'method_payment',
            'seller' => function ($query) {
                $query->with('person');
            },
        ])
            ->when($request->has('seller') && $request->seller > 0, function ($query) use ($request) {
                $query->where('creator_id', $request->seller);
            })
            ->when($request->has('client') && $request->client > 0, function ($query) use ($request) {
                $query->where('client_id', $request->client);
            })
            ->when($request->has('since') && $request->has('until'), function ($query) use ($request) {
                $query->whereBetween('date_emission', [$this->parseFormatDateTime($request->since, 'start'), $this->parseFormatDateTime($request->until, 'end')]);
            })
            ->when($request->has('since') && !$request->has('until'), function ($query) use ($request) {
                $query->whereDate('date_emission', '>=', $this->parseFormat($request->since));
            })
            ->when(!$request->has('since') && $request->has('until'), function ($query) use ($request) {
                $query->whereDate('date_emission', '<=', $this->parseFormat($request->until));
            })
            ->when($request->has('code'), function ($query) use ($request) {
                $query->where('code', 'LIKE', '%' . $request->code . '%');
            })
            ->when(!$request->has('since') && !$request->has('until'), function ($query) {
                $query->whereDate('date_emission', \Carbon\Carbon::now()->format('yy-m-d'));
            })
            ->when($request->role != 3, function ($query) use ($request) {
                $query->whereIn('warehouse_id', $request->warehouse_id);
            }, function ($query) use ($request) {
                $query->whereHas('warehouse', function ($query2) use ($request) {
                    $query2->where('user_id', $request->Id);
                });
            })->get();

        return $reports;

        Excel::create('REPORTE-' . \Carbon\Carbon::now(), function ($excel) use ($reports) {
            $excel->setCreator('LimonByte')->setCompany('Adminstrativo');
            $excel->setDescription('Reporte de Ventas');
            $excel->sheet('Listado', function ($sheet) use ($reports) {
                $sheet->loadView('reports.sales.sales')->with('reports', $reports);
            });
        })->download('xlsx');

    }
}
