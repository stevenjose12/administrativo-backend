<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Hash;
use Illuminate\Http\Request;
use Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

define('STATUS_INACTIVE', 0);
define('STATUS_UNKNOWN', 2);
class AuthController extends Controller
{
    public function login(Request $request)
    {
        $rules = [
            'name' => 'required',
            'password' => 'required',
        ];

        $messages = [
            "required_unless" => "El campo :attribute es obligatorio.",
            "required_if" => "El campo :attribute es obligatorio.",
            "confirmed" => 'Las contaseñas no coinciden',
        ];

        $attributes = [
            'name' => 'Código y Nombre de Usuario',
            'password' => 'Contraseña',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $validator->setAttributeNames($attributes);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->errors()->first(),
            ], 422);
        }

        $user = User::with([
            'zones',
            'roles',
            'person',
            'modules_seller',
            'enterprise_users',
            'enterprise_modules',
            'role_enterprise_subuser' => function ($query) {
                $query->with('role_modules');
            },
            'configuration_seller',
        ])->where('name', $request->name)->first();

        if (!isset($user)) {
            return response()->json([
                'message' => 'Las credenciales ingresadas son incorrectas, intente nuevamente',
            ], 422);
        }

        if ($user->status == STATUS_INACTIVE || $user->status == STATUS_UNKNOWN) {
            return response()->json([
                'message' => 'Disculpe, su usuario ha sido inhabilitado por el administrador',
            ], 422);
        }

        if (count($user->roles) <= 0) {
            return response()->json([
                'message' => 'Disculpe, su usuario no posee un rol asignado',
            ], 422);
        } else {
            $user->role = $user->roles[0]['id'];
        }

        if ($user->level == 5 || $user->level == 6) {
            return response()->json([
                'message' => 'Las credenciales ingresadas son incorrectas, intente nuevamente',
            ], 422);
        }

        if (Hash::check($request->password, $user->password)) {
            
            $token = JWTAuth::fromUser($user);

            return response()->json([
                'user' => $user,
                'token' => $token
            ]);
        }

        return response()->json([
            'message' => 'Las credenciales ingresadas son incorrectas, intente nuevamente',
        ], 422);
    }
}
