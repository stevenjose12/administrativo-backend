<?php

namespace App\Http\Controllers\Api\Admin\Price;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Warehouse;
use App\Models\ProductWarehouse;

class PriceController extends Controller
{
    public function prices(Request $request)
    {
        $warehouses_ids = [];

        if (!$request->has('branch_id') && $request->role == 3) {
            $warehouses_ids = Warehouse::where('user_id', $request->user_id)->withTrashed()->pluck('id');
        }

        if ($request->has('branch_id')) {
            $warehouses_ids = Warehouse::where('branch_id', $request->branch_id)->withTrashed()->pluck('id');
            $prices = ProductWarehouse::whereIn('warehouse_id', $warehouses_ids);
        }

        if ($request->has('warehouse_id') && count($request->warehouse_id) > 0) {
            $warehouses_ids = $request->warehouse_id;
        }

        $prices = ProductWarehouse::whereIn('warehouse_id', $warehouses_ids);

        $warehouses = Warehouse::whereIn('id', $warehouses_ids)->withTrashed()->get();

        if ($request->has('search')) {
            $prices->whereHas('product', function ($query) use ($request) {
                $query->where('name', 'like', '%' . $request->search . '%');
            });
        }

        if ($request->has('status')) {
            $prices->where('status', $request->status);
        }

        $response = $prices->whereHas('product')->with([
            'product',
            'warehouse' => function ($query) {
                $query->with([
                    'branch' => function ($query) {
                        $query->withTrashed();
                    }
                ])->withTrashed();
            }
        ])->orderBy('id', 'desc')->paginate(10);

        return response()->json([
            'prices' => $response,
            'warehouses' => $warehouses,
            'result' => true
        ]);
    }

    public function priceEdit(Request $request)
    {

        $product = ProductWarehouse::find($request->id);
        $product->cost = $request->cost;
        $product->price = $request->price;
        $product->price_max = $request->price_max;
        $product->price_offer = $request->price_offer;
        $product->percentage_earning = $request->percentage_earning;
        $product->percentage_max = $request->percentage_max;
        $product->percentage_offer = $request->percentage_offer;
        $product->save();

        return response()->json([
            'msg' => '¡Precio actualizado exitosamente!'
        ]);
    }
}
