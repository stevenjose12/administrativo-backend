<?php

namespace App\Http\Controllers\Api\Admin\Order;

use App\Http\Controllers\Controller;
use App\Http\Traits\Format;
use App\Models\EnterpriseUser;
use App\Models\ProductProvider;
use App\Models\ProductWarehouse;
use App\Models\ProductWarehouseMovement;
use App\Models\PurchaseDetail as Article;
use App\Models\PurchaseOrder as Order;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;
use App\Http\Traits\ProductSerial;
use Carbon\Carbon;
use DB;
use App\Models\BankBalance;
use App\Models\ExpensesIngressPayment;

define('PURCHASE', 1);
define('BILL', 2);
define('APP', 'App\Models\PurchaseOrder');

class OrderController extends Controller
{
    use Format;
    use ProductSerial;

    public function getId(Request $request)
    {
        $users_ids = EnterpriseUser::where('enterprise_id', $request->Id)->select('user_id')->get()->pluck('user_id')->toArray();
        array_push($users_ids, $request->Id);

        $count = Order::where('type', PURCHASE)->whereIn('creator_id', $users_ids)->count();

        $Id = str_pad(1, 9, 0, STR_PAD_LEFT);

        if ($count > 0) {
            $Id = str_pad($count + 1, 9, 0, STR_PAD_LEFT);
        }

        return $Id;
    }

    public function productsByProvider(Request $request)
    {
        $products = ProductProvider::with([
            'product' => function ($query) {
                $query->where('status', 1)->with('product_warehouse', function ($query) {
                    $query->select('product_id', 'percentage_earning', 'percentage_max', 'percentage_offer');
                });
            },
        ])->where('provider_id', $request->Id)->get();

        return $products;
    }

    public function orders(Request $request)
    {
        $orders = Order::with([
            'operation' => function ($query) {
                $query->select('operation_id', 'operation_type', 'product_warehouse_id', 'serial')
                      ->with(['product_warehouse' => function ($query) {
                          $query->select('id', 'product_id');
                      }]);
            },
            'warehouse' => function ($query) {
                $query->with('branch');
            },
            'currency',
            'provider' => function ($query) {
                $query->withTrashed();
            },
            'details' => function ($query) {
                $query->with([
                    'product_warehouse' => function ($query) {
                        $query->select('product_id', 'warehouse_id', 'percentage_earning', 'percentage_max', 'percentage_offer');
                    },
                ]);
            },
        ])
            ->when($request->has('provider') && $request->provider > 0, function ($query) use ($request) {
                $query->where('provider_id', $request->provider);
            })
            ->when($request->has('since') && $request->has('until'), function ($query) use ($request) {
                $query->whereBetween('created_at', [$this->parseFormatDateTime($request->since, 'start'), $this->parseFormatDateTime($request->until, 'end')]);
            })
            ->when($request->has('since') && !$request->has('until'), function ($query) use ($request) {
                $query->whereDate('created_at', '>=', $this->parseFormat($request->since));
            })
            ->when(!$request->has('since') && $request->has('until'), function ($query) use ($request) {
                $query->whereDate('created_at', '<=', $this->parseFormat($request->until));
            })
            ->when($request->has('type') && $request->type > 0, function ($query) use ($request) {
                $query->where('type', $request->type);
            })
            ->when($request->role != 3, function ($query) use ($request) {
                $query->whereIn('warehouse_id', $request->warehouse_id);
            }, function ($query) use ($request) {
                $query->whereHas('warehouse', function ($query2) use ($request) {
                    $query2->where('user_id', $request->Id);
                });
            });

        return $orders->orderBy('id', 'desc')->paginate(10);
    }

    private function ValidPurchase(array $data)
    {
        $attributes = [
            'provider_id' => 'proveedor',
            'warehouse_id' => 'almacen',
            'code' => 'código',
            'currency_id' => 'divisa',
            'date_purchase' => 'fecha de compra',
            'date_delivery' => 'fecha de entrega',
            'observations' => 'observaciones',
            'subtotal' => 'subtotal',
            'vat' => 'impuesto al valor agregado',
            'total' => 'total',
        ];

        $validator = Validator::make($data, [
            'provider_id' => ['required', 'numeric'],
            'warehouse_id' => ['required', 'numeric'],
            'code' => ['required', 'string'],
            'currency_id' => ['required', 'numeric'],
            'date_purchase' => ['required'],
            'date_delivery' => ['required'],
            'observations' => ['required'],
            'subtotal' => ['required'],
            'vat' => ['required'],
            'total' => ['required'],
        ], [
            "required" => "El campo :attribute es obligatorio.",
            "string" => "El campo :attribute debe ser una cadena de caracteres.",
            "numeric" => ":attribute debe ser numérico.",
        ]);

        $validator->setAttributeNames($attributes);

        return $validator;
    }

    private function ValidDates(array $data, $Id)
    {
        $attributes = [
            'bill_number' => 'número de factura',
            'control_number' => 'número de control',
            'bill_date_reception' => 'fecha de recepción',
            'bill_date_emission' => 'fecha de emisión',
        ];

        $validator = Validator::make($data, [
            'bill_number' =>
            [
                'required',
                'string',
                'different:control_number',
                Rule::unique('purchase_order')->where(function ($query) use ($Id) {
                    $query->where('provider_id', $Id);
                }),
            ],
            'control_number' =>
            [
                'required',
                'string',
                Rule::unique('purchase_order')->where(function ($query) use ($Id) {
                    $query->where('provider_id', $Id);
                }),
            ],
            'bill_date_reception' => ['required'],
            'bill_date_emission' => ['required'],
        ], [
            "required" => "El campo :attribute es obligatorio.",
            "string" => "El campo :attribute debe ser una cadena de caracteres.",
            "different" => ":attribute y :other deben ser diferentes.",
            "unique" => "El campo :attribute ya ha sido registrado.",
        ]);

        $validator->setAttributeNames($attributes);

        return $validator;
    }

    public function createOrder(Request $request)
    {
        $this->ValidPurchase($request->all())->validate();


        if ($request->forthright) {
            $this->ValidDates($request->all(), $Id = $request->provider_id)->validate();
            
            if ($request->payment_type == '') {
                return response()->json(['msg' => 'Debe seleccionar un tipo de pago'], 422);
            }
  
            if ((int)$request->payment_type == 1) {
                if ($request->method_type == '') {
                    return response()->json(['msg' => 'Debe seleccionar un metodo de pago'], 422);
                }

                if ($request->bank_id == '') {
                    return response()->json(['msg' => 'Debe seleccionar un banco'], 422);
                }

                if ($request->bank_account_id == '') {
                    return response()->json(['msg' => 'Debe seleccionar una cuenta bancaria'], 422);
                }

                if ($request->bank_account_id == '') {
                    return response()->json(['msg' => 'Debe seleccionar una cuenta bancaria'], 422);
                }

                $current_bank_balance = BankBalance::where('bank_account_id', $request->bank_account_id)->orderBy('id', 'desc');
                if ($current_bank_balance->exists()) {
                    $current_bank_balance = $current_bank_balance->first()->total;
                } else {
                    $current_bank_balance = 0;
                }
        
                if ($request->payment_type == config('constants.expenses_ingress.payment_types.CASH')) {
                    if ($request->total_real > $current_bank_balance) {
                        return response()->json([
                            'msg' => 'El monto de la factura no puede sobrepasar el saldo de la cuenta!',
                        ], 422);
                    }
                }
            }

            // if($this->checkIfAtLeastAProductIsSerialized($request->products)) {
            //     $serialChecked = $this->getIfSerialsExistsInWarehouse($request->products, $request->warehouse_id);
        
            //     if($serialChecked)
            //     {
            //         return response()
            //                 ->json(['msg' => 'El serial '.$serialChecked->serial.' ya está asignado para este producto'],422);
            //     }
            // }

            $response = $this->checkEntrySerials($request->products, $request->warehouse_id);

            if (!is_bool($response)) {
                return $response;
            }
        }

        $provider = User::find($request->provider_id, ['id']);
        if (is_null($provider)) {
            return response()->json([ 'msg' => 'Este proveedor ha sido eliminado!'], 422);
        }

        $order = new Order();

        if ($request->forthright) {
            $order->date_delivery = null;
            $order->date_purchase = null;
            $order->bill_number = $request->bill_number;
            $order->control_number = $request->control_number;
            $order->bill_date_reception = $this->parseFormat($request->bill_date_reception);
            $order->bill_date_emission = $this->parseFormat($request->bill_date_emission);
            $order->type = BILL;
        } else {
            $order->date_delivery = $this->parseFormat($request->date_delivery);
            $order->date_purchase = $this->parseFormat($request->date_purchase);
            $order->bill_number = null;
            $order->control_number = null;
            $order->bill_date_reception = null;
            $order->bill_date_emission = null;
            $order->type = PURCHASE;
        }

        $order->fill($request->except([
            'bill_number',
            'control_number',
            'date_delivery',
            'date_purchase',
            'bill_date_reception',
            'bill_date_emission',
            'type',
        ]))->save();

        if ($request->forthright) {
            $order->status = 1;
            $order->subtotal_real = $order->subtotal;
            $order->vat_real = $order->vat;
            $order->total_real = $order->total;
            $order->payment_type = $request->payment_type;
            $order->save();
            if ($request->payment_type == 1) {
                $payment = new ExpensesIngressPayment();
                $payment->expenses_ingress_id = $order->id;
                $payment->bank_account_id = $request->bank_account_id;
                $payment->payment_type = $request->method_type;
                $payment->amount = $order->total_real;
                $payment->expenses_ingress_type = 'App\Models\PurchaseOrder';
                $payment->expenses_ingress_class = 1;
                $payment->date = Carbon::now();
                $payment->status = 1;
                $payment->creator_id = $request->creator_id;
                $payment->save();

                $bank_balance = new BankBalance;
                $bank_balance->bank_id = $request->bank_id;
                $bank_balance->bank_account_id = $request->bank_account_id;
                $bank_balance->action_id = $payment->id;
                $bank_balance->action_class = config()->get('constants.bank.type.EXPENSES_PAYMENT_EGRESSS');
                $bank_balance->action_type = config()->get('constants.bank.type.APP_EXPENSE_PAYMENT');
                $bank_balance->type = config()->get('constants.bank.type.OUTPUT');
                $bank_balance->amount = $order->total_real;
                $bank_balance->current_amount = $current_bank_balance;
                $bank_balance->total = $current_bank_balance - $order->total_real;
                $bank_balance->description = "Egreso por pago de factura";
                $bank_balance->creator_id = $request->creator_id;
                $bank_balance->operation_number = $request->reference_number;
                $bank_balance->save();
            }
        }

        if ($request->has('products')) {
            $products = $request->products;

            foreach ($products as $key => $product) {
                $query = ProductProvider::where([
                    ['provider_id', $provider->id],
                    ['product_id', $product['id']],
                ]);

                if ($query->exists()) {
                    // Actualizo el precio del proveedor y producto
                    $product_query = $query->get()->last();
                    $product_query->price = $product['unit_price'];
                    $product_query->save();
                    // Actualizo el precio del último producto
                    $product_provider = ProductProvider::where('product_id', $product['id'])->get()->last();
                    $product_provider->price = $product['unit_price'];
                    $product_provider->save();
                } else {
                    $product_provider = new ProductProvider;
                    $product_provider->provider_id = $provider->id;
                    $product_provider->product_id = $product['id'];
                    $product_provider->creator_id = $request->creator_id;
                    $product_provider->price = $product['unit_price'];
                    $product_provider->save();
                }

                if ($request->forthright) {
                    $this->entryWareHouse($product, $order, $creatorId = $request->creator_id, $typeId = 1, $actionId = 2);
                }

                $order->details()->attach($product['id'], [
                    'quantity' => $product['quantity'],
                    'quantity_real' => $request->forthright ? $product['quantity'] : 0,
                    'subtotal' => $product['subtotal'],
                    'subtotal_real' => $request->forthright ? $product['subtotal'] : 0,
                    'vat' => $product['vat'],
                    'vat_real' => $request->forthright ? $product['vat'] : 0,
                    'total' => $product['total'],
                    'total_real' => $request->forthright ? $product['total'] : 0,
                ]);
            };
        }
        
        

        return $order;
    }

    private function lastStock($warehouseId, $productId, $typeId)
    {
        $stock = ProductWarehouseMovement::where([
            ['product_warehouse_id', $warehouseId],
            ['product_id', $productId],
        ])->get()->last();

        $quantity = 0;

        if (!empty($stock)) {
            $quantity = $stock->type == 1 ? $stock->current_stock + $stock->amount : $stock->current_stock - $stock->amount;
        }

        return $quantity;
    }

    public function updateOrder(Request $request)
    {
        $this->ValidPurchase($request->all())->validate();


        if(!$request->bill_number)
        {
            return response()->json(['msg' => 'Numero de factura requerido'],422);
        }

        if(!$request->control_number)
        {
            return response()->json(['msg' => 'Numero de control requerido'],422);
        }

        if ($request->payment_type == '') {
            return response()->json(['msg' => 'Debe seleccionar un tipo de pago'], 422);
        }

        if ((int)$request->payment_type == 1) {
            if ($request->method_type == '') {
                return response()->json(['msg' => 'Debe seleccionar un metodo de pago'], 422);
            }

            if ($request->bank_id == '') {
                return response()->json(['msg' => 'Debe seleccionar un banco'], 422);
            }

            if ($request->bank_account_id == '') {
                return response()->json(['msg' => 'Debe seleccionar una cuenta bancaria'], 422);
            }

            if ($request->bank_account_id == '') {
                return response()->json(['msg' => 'Debe seleccionar una cuenta bancaria'], 422);
            }

            $current_bank_balance = BankBalance::where('bank_account_id', $request->bank_account_id)->orderBy('id', 'desc');
            if ($current_bank_balance->exists()) {
                $current_bank_balance = $current_bank_balance->first()->total;
            } else {
                $current_bank_balance = 0;
            }
    
            if ($request->payment_type == config('constants.expenses_ingress.payment_types.CASH')) {
                if ($request->total_real > $current_bank_balance) {
                    return response()->json([
                        'msg' => 'El monto de la factura no puede sobrepasar el saldo de la cuenta!',
                    ], 422);
                }
            }
        }

        // $serialChecked = $this->getIfSerialsExistsInWarehouse($request->products, $request->warehouse_id);

        // if($serialChecked)
        // {
        //     return response()
        //             ->json(['msg' => 'El serial '.$serialChecked->serial.' ya está asignado para este producto'],422);
        // }

        $response = $this->checkEntrySerials($request->products, $request->warehouse_id);

        if (!is_bool($response)) {
            return $response;
        }
        
        $order = Order::findOrFail($request->id);
        $order->observations = $request->observations;
        $order->date_delivery_real = $this->parseFormat($request->date_delivery_real);
        $order->subtotal_real = $request->subtotal_real;
        $order->taxable = $request->taxable;
        $order->exempt = $request->exempt;
        $order->bill_number = $request->bill_number;
        $order->control_number = $request->control_number;
        $order->vat_real = $request->vat;
        $order->total_real = $request->total;
        $order->status = 1;
        $order->payment_type = $request->payment_type;
        $order->save();
        if ($request->payment_type == 1) {
            $current_bank_balance = BankBalance::where('bank_account_id', $request->bank_account_id)->orderBy('id', 'desc');
            if ($current_bank_balance->exists()) {
                $current_bank_balance = $current_bank_balance->first()->total;
            } else {
                $current_bank_balance = 0;
            }
    
            if ($request->payment_type == config('constants.expenses_ingress.payment_types.CASH')) {
                if ($request->total_real > $current_bank_balance) {
                    return response()->json([
                        'msg' => 'El monto de la factura no puede sobrepasar el saldo de la cuenta!',
                    ], 422);
                }
            }

            $payment = new ExpensesIngressPayment();
            $payment->expenses_ingress_id = $order->id;
            $payment->bank_account_id = $request->bank_account_id;
            $payment->payment_type = $request->method_type;
            $payment->amount = $order->total_real;
            $payment->expenses_ingress_type = 'App\Models\PurchaseOrder';
            $payment->expenses_ingress_class = 1;
            $payment->date = Carbon::now();
            $payment->status = 1;
            $payment->creator_id = $request->creator_id;
            $payment->save();

            $bank_balance = new BankBalance;
            $bank_balance->bank_id = $request->bank_id;
            $bank_balance->bank_account_id = $request->bank_account_id;
            $bank_balance->action_id = $payment->id;
            $bank_balance->action_class = config()->get('constants.bank.type.EXPENSES_PAYMENT_EGRESSS');
            $bank_balance->action_type = config()->get('constants.bank.type.APP_EXPENSE_PAYMENT');
            $bank_balance->type = config()->get('constants.bank.type.OUTPUT');
            $bank_balance->amount = $order->total_real;
            $bank_balance->current_amount = $current_bank_balance;
            $bank_balance->total = $current_bank_balance - $order->total_real;
            $bank_balance->description = "Egreso por pago de factura";
            $bank_balance->creator_id = $request->creator_id;
            $bank_balance->operation_number = $request->reference_number;
            $bank_balance->save();
        }
        if ($request->has('products')) {
            $products = $request->products;

            foreach ($products as $key => $product) {
                $product_provider = ProductProvider::where('product_id', $product['product_id'])->get()->last();
                $product_provider->price = $product['unit_price'];
                $product_provider->save();

                $this->entryWareHouse($product, $order, $creatorId = $request->creator_id, $typeId = 1, $actionId = 2);

                $product_detail = Article::findOrFail($product['Id']);
                $product_detail->quantity_real = $product['quantity'];
                $product_detail->subtotal_real = $product['subtotal'];
                $product_detail->vat_real = $product['vat'];
                $product_detail->total_real = $product['total'];
                $product_detail->save();
            }
        }

        return $order;
    }

    private function entryWareHouse($product, $order, $creatorId, $typeId, $actionId)
    {
        $query = ProductWarehouse::where([
            ['warehouse_id', $order->warehouse_id],
            ['product_id', $product['product_id']],
        ]);

        if ($query->exists()) {
            $product_warehouse = $query->get()->last();

            $percentage = $product['earning'] > 0 ? number_format((float) (($product['earning'] / $product['unit_price']) - 1) * 100) : 0;
            $percentage_max = $product['earning_max'] > 0 ? number_format((float) (($product['earning_max'] / $product['unit_price']) - 1) * 100) : 0;
            $percentage_offer = $product['earning_offer'] > 0 ? number_format((float) (($product['earning_offer'] / $product['unit_price']) - 1) * 100) : 0;

            $product_warehouse->cost = $product['unit_price'];
            $product_warehouse->percentage_earning = $percentage;
            $product_warehouse->percentage_max = $percentage_max;
            $product_warehouse->percentage_offer = $percentage_offer;
            $product_warehouse->price = $product['earning'] > 0 ? $product['earning'] : $product['unit_price'];
            $product_warehouse->price_max = $product['earning_max'] > 0 ? $product['earning_max'] : $product['unit_price'];
            $product_warehouse->price_offer = $product['earning_offer'] > 0 ? $product['earning_offer'] : $product['unit_price'];
            $product_warehouse->save();

            $stock = $this->lastStock($product_warehouse->id, $product['product_id'], $typeId);

            $product_warehouse_movement = new ProductWarehouseMovement();
            $product_warehouse_movement->product_warehouse_id = $product_warehouse->id;
            $product_warehouse_movement->product_id = $product['product_id'];
            $product_warehouse_movement->action_id = $order->id;
            $product_warehouse_movement->action_class = $actionId;
            $product_warehouse_movement->action_type = APP;
            $product_warehouse_movement->type = $typeId;
            $product_warehouse_movement->current_stock = $stock;
            $product_warehouse_movement->amount = $product['quantity'];
            $product_warehouse_movement->creator_id = $creatorId;
            $product_warehouse_movement->save();
        } else {
            $percentage = $product['earning'] > 0 ? number_format((float) (($product['earning'] / $product['unit_price']) - 1) * 100) : 0;
            $percentage_max = $product['earning_max'] > 0 ? number_format((float) (($product['earning_max'] / $product['unit_price']) - 1) * 100) : 0;
            $percentage_offer = $product['earning_offer'] > 0 ? number_format((float) (($product['earning_offer'] / $product['unit_price']) - 1) * 100) : 0;

            $product_warehouse = new ProductWarehouse;
            $product_warehouse->warehouse_id = $order->warehouse_id;
            $product_warehouse->product_id = $product['product_id'];
            $product_warehouse->status = 1;
            $product_warehouse->cost = $product['unit_price'];
            $product_warehouse->percentage_earning = $percentage;
            $product_warehouse->percentage_max = $percentage_max;
            $product_warehouse->percentage_offer = $percentage_offer;
            $product_warehouse->price = $product['earning'] > 0 ? $product['earning'] : $product['unit_price'];
            $product_warehouse->price_offer = $product['earning_offer'] > 0 ? $product['earning_offer'] : $product['unit_price'];
            $product_warehouse->price_max = $product['earning_max'] > 0 ? $product['earning_max'] : $product['unit_price'];
            $product_warehouse->creator_id = $creatorId;
            $product_warehouse->save();

            $stock = $this->lastStock($product_warehouse->id, $product['product_id'], $typeId);

            $product_warehouse_movement = new ProductWarehouseMovement();
            $product_warehouse_movement->product_warehouse_id = $product_warehouse->id;
            $product_warehouse_movement->product_id = $product['product_id'];
            $product_warehouse_movement->action_id = $order->id;
            $product_warehouse_movement->action_class = $actionId;
            $product_warehouse_movement->action_type = APP;
            $product_warehouse_movement->type = $typeId;
            $product_warehouse_movement->current_stock = $stock;
            $product_warehouse_movement->amount = $product['quantity'];
            $product_warehouse_movement->creator_id = $creatorId;
            $product_warehouse_movement->save();
        }

        // Serializando los productos
        $this->createProductSerials($product, $product_warehouse->id, $order->id, APP);
    }
}
