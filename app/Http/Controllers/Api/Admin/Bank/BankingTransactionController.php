<?php

namespace App\Http\Controllers\Api\Admin\Bank;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Models\BankBalance;
use App\Models\Bank;
use App\Models\BankAccount;
use App\Http\Traits\Format;

class BankingTransactionController extends Controller
{
    use Format;
    public function index(Request $request)
    {

        $bank_ids = Bank::where('enterprise_id',$request->enterprise_id)->pluck('id')->toArray();
        $bankingTransactions = BankBalance::with(['action','bank_account','bank'])->whereIn('bank_id',$bank_ids)
            ->when($request->has('provider') && $request->provider > 0, function ($query) use ($request) {
                $query->where('provider_id', $request->provider);
            })
            ->when($request->has('since') && $request->has('until'), function ($query) use ($request) {
                $query->whereBetween('created_at', [$this->parseFormatDateTime($request->since, 'start'), $this->parseFormatDateTime($request->until, 'end')]);
            })
            ->when($request->has('movement_type') && $request->movement_type > 0, function ($query) use ($request) {
                    $query->where('type', $request->movement_type);
                })            
            ->when($request->has('bank_id') && $request->bank_id > 0, function ($query) use ($request) {
                    $query->where('bank_id', $request->bank_id);
                })
            ->when($request->has('bank_id') && $request->bank_id > 0, function ($query) use ($request) {
                $query->where('bank_id', $request->bank_id);
            })
            ->when($request->has('bank_account_id') && $request->bank_account_id > 0, function ($query) use ($request) {
                $query->where('bank_account_id', $request->bank_account_id);
            })
            ->when($request->has('bank_account_id') && $request->bank_account_id > 0, function ($query) use ($request) {
                $query->where('bank_account_id', $request->bank_account_id);
            })->orderBy('id', 'desc')->paginate(10);

        return  $bankingTransactions;
    }
}
