<?php

namespace App\Http\Controllers\Api\Admin\Bank;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\BankAccountRequest;
use App\Models\BankAccount;
use App\Models\BankPayment;
use App\Models\BankBalance;
use Carbon\Carbon;

define('ENTRY', 1);

define('APP_BANK_PAYMENT', 'App\Models\BankPayment');

class BankAccountController extends Controller
{
    /**
     * Display a listing of the resource paginated.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $bank_accounts = BankAccount::with('bank')->where('bank_id', $request->bank_id)
        ->when($request->has('search'), function ($query) use ($request) {
            $query->where('account_number', 'like', '%'.$request->search.'%');
        })
        ->when($request->has('status'), function ($query) use ($request) {
            $query->where('status', $request->status);
        })
        ->orderBy('id', 'desc');

        return $bank_accounts->paginate(10);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function get(Request $request)
    {
        $bank_accounts = BankAccount::with(['bank','last_balance'])->where('bank_id', $request->bank_id)
        ->where('status', 1)
        ->orderBy('id', 'desc');

        return $bank_accounts->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BankAccountRequest $request)
    {
        $check = BankAccount::where('bank_id', $request->bank_id)->where('account_number', $request->account_number)->exists();

        if ($check) {
            return response()->json([
                'msg' => 'Ya existe una cuenta bancaria con ese numero!',
            ], 422);
        }
        
        $bank_account = new BankAccount;
        $bank_account->bank_id = $request->bank_id;
        $bank_account->account_number = $request->account_number;
        $bank_account->type = $request->type;
        $bank_account->creator_id = $request->user_id;
        $bank_account->save();

        $bank_payment = new BankPayment;
        $bank_payment->bank_account_id = $bank_account->id;
        $bank_payment->amount = $request->amount;
        $bank_payment->creator_id = $request->user_id;
        $bank_payment->save();

        $bank_balance = new BankBalance;
        $bank_balance->bank_id = $request->bank_id;
        $bank_balance->bank_account_id = $bank_account->id;
        $bank_balance->action_id = $bank_payment->id;
        $bank_balance->action_class = 1;
        $bank_balance->action_type = APP_BANK_PAYMENT;
        $bank_balance->type = ENTRY;
        $bank_balance->amount = $request->amount;
        $bank_balance->current_amount = 0;
        $bank_balance->total = $request->amount;
        $bank_balance->description = "Ingreso por capital inicial";
        $bank_balance->creator_id = $request->user_id;
        $bank_balance->save();

        return $bank_account;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BankAccountRequest $request, $id)
    {
        if ($request->type === 3 && $request->created_at == $request->updated_at) {
            $bank_account = BankAccount::find($id);
            $bank_account->updated_at = Carbon::now();
            $bank_account->save();


            $bank_payment = new BankPayment;
            $bank_payment->bank_account_id = $bank_account->id;
            $bank_payment->amount = $request->amount;
            $bank_payment->creator_id = $request->user_id;
            $bank_payment->save();
    
            $bank_balance = new BankBalance;
            $bank_balance->bank_id = $request->bank_id;
            $bank_balance->bank_account_id = $bank_account->id;
            $bank_balance->action_id = $bank_payment->id;
            $bank_balance->action_class = 1;
            $bank_balance->action_type = APP_BANK_PAYMENT;
            $bank_balance->type = ENTRY;
            $bank_balance->amount = $request->amount;
            $bank_balance->current_amount = 0;
            $bank_balance->total = $request->amount;
            $bank_balance->description = "Ingreso por capital inicial";
            $bank_balance->creator_id = $request->user_id;
            $bank_balance->save();

            return $bank_account;
        }
        $check = BankAccount::where('id', '!=', $id)
        ->where('bank_id', $request->bank_id)
        ->where('account_number', $request->account_number)->exists();

        if ($check) {
            return response()->json([
                'msg' => 'Ya existe una cuenta bancaria con ese numero!',
            ], 422);
        }

        $bank_account = BankAccount::find($id);
        $bank_account->account_number = $request->account_number;
        $bank_account->type = $request->type;
        $bank_account->save();

        return $bank_account;
    }

    /**
     * Activate the specified resource in storage.
     * @param int $id
     */

    public function activate($id)
    {
        $bank_account = BankAccount::find($id);
        $bank_account->status = $bank_account->status == 1 ? 2 : 1;
        $bank_account->save();

        return $bank_account;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bankAccount = BankAccount::with('last_balance')->find($id);
        
        $balance = 0;
        
        if ($bankAccount->last_balance->type == 1) {
            $balance = $bankAccount->last_balance->current_amount + $bankAccount->last_balance->amount;
        } else {
            $balance = $bankAccount->last_balance->current_amount - $bankAccount->last_balance->amount;
        }
        
        if($balance != 0) {
            return response()->json(['msg' => 'No puede eliminar esta cuenta'],422);
        }

        $bankAccount->delete();
        return;
    }
}
