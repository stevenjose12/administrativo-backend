<?php

namespace App\Http\Controllers\Api\Admin\Bank;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\BankRequest;
use App\Models\Bank;
use App\Models\BankAccount;

class BankController extends Controller
{
    /**
     * Display a listing of the resource paginated.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $banks = Bank::where('enterprise_id', $request->enterprise_id)
        ->when($request->has('search'), function ($query) use ($request) {
            $query->where('name', 'like', '%' . $request->search . '%');
        })
        ->when($request->has('status'), function ($query) use ($request) {
            $query->where('status', $request->status);
        })
        ->orderBy('id', 'desc');

        return $banks->paginate(10);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request)
    {
        $banks = Bank::where('enterprise_id', $request->enterprise_id)
        ->where('status', 1)
        ->orderBy('id', 'desc');

        return $banks->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BankRequest $request)
    {
        $check = Bank::where('enterprise_id', $request->enterprise_id)
        ->where('name', $request->name)->exists();

        if ($check) {
            return response()->json([
                'msg' => 'Ya existe un banco con ese nombre!',
            ], 422);
        }

        $bank = new Bank;
        $bank->enterprise_id = $request->enterprise_id;
        $bank->name = $request->name;
        $bank->type = $request->type;
        $bank->creator_id = $request->user_id;
        $bank->save();

        return $bank;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        return Bank::where('id', $request->bankId)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BankRequest $request, $id)
    {
        $check = Bank::where('id', '!=', $id)
        ->where('enterprise_id', $request->enterprise_id)
        ->where('name', $request->name)->exists();

        if ($check) {
            return response()->json([
                'msg' => 'Ya existe un banco con ese nombre!',
            ], 422);
        }

        $bank = Bank::find($id);
        $bank->name = $request->name;
        $bank->type = $request->type;
        $bank->save();

        return $bank;
    }

    /**
     * @param int $id
     */

    public function activate($id)
    {
        $bank = Bank::find($id);
        $bank->status = $bank->status == 1 ? 2 : 1;
        $bank->save();

        return $bank;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bank = Bank::withCount('accounts')->find($id);
        $bankAccounts = BankAccount::with('last_balance')->where('bank_id', $id)->get();

        $balance = 0;
        foreach ($bankAccounts as $bankAccount) {
            if ($bankAccount->last_balance->type == 1) {
                $balance += $bankAccount->last_balance->current_amount + $bankAccount->last_balance->amount;
            } else {
                $balance += $bankAccount->last_balance->current_amount - $bankAccount->last_balance->amount;
            }
        }
        

        if ($balance != 0 || $bank->accounts_count != 0) {
            return response()->json(['msg' => 'No puede eliminar este banco'], 422);
        } else {
            $bank->delete();
            return;
        }
    
    }
}
