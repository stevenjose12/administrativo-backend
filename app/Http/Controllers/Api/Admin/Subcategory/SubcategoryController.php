<?php

namespace App\Http\Controllers\Api\Admin\Subcategory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Subcategory;
use Validator;

class SubcategoryController extends Controller
{
    public function subcategories(Request $request) {
        $subcategories = Subcategory::where('category_id', $request->category_id)->with('category');

        if ($request->has('search')) {
			$subcategories->where(function($q) use ($request) {
                $q->where('name','like','%'.$request->search.'%')
                    ->orWhere('code','like','%'.$request->search.'%');
			});
        }
        if ($request->has('status')) {
            $subcategories->where('status', $request->status);
        }

        $response = $subcategories->paginate(10);
        
        if($request->has('select')) {
            $response = $subcategories->get();
        }        

        return response()->json([
            'subcategories' => $response,
            'result' => true
        ]);
    }

    private function ValidSubCategory(array $data)
    {
        $attributes = [
            'name' => 'nombre',
            'code' => 'código'
        ];

        $validator = Validator::make($data, [
            'name' => ['required', 'between:3,50'],
            'code' => ['required', 'between:1,3']
        ], [
            'required'    => 'El campo :attribute es obligatorio.',
            'between'     => ':attribute tiene que estar entre :min - :max.'
        ]);
        
        $validator->setAttributeNames($attributes);

		return $validator;
    }

    public function subcategoryCreateFromCompound(Request $request)
    {
        $this->ValidSubCategory($request->all())->validate();

        $check_code = Subcategory::where('category_id', $request->category_id)->where('code', $request->code)->count();
        $check_name = Subcategory::where('category_id', $request->category_id)->where('name', $request->name)->count();

        if($check_code > 0) {
            return response()->json([
                'msg'=> '¡El código de la subcategoría ya se encuentra registrado!'
            ], 422);
        }

        if($check_name > 0) {
            return response()->json([
                'msg'=> '¡El nombre de la subcategoría ya se encuentra registrado!'
            ], 422);
        }

        $subcategory = new Subcategory();
        $subcategory->fill($request->all())->save();

        return $subcategory;
    }

    public function subcategoryCreate(Request $request){
        $rules = [
            'name' => 'required|max:50|min:3',
            'code' => 'required|max:255'
		];
        $messages = [
            "required_unless" => "El campo :attribute es obligatorio.",
            "required_if" => "El campo :attribute es obligatorio.",
            "confirmed" => 'Las contaseñas no coinciden',
        ];
        $attributes = [
            'name' => 'Nombre',
            'code' => 'Código'
        ];
		$validator = Validator::make($request->all(), $rules, $messages);
        $validator->setAttributeNames($attributes);
        if ($validator->fails()) {
            return response()->json([
                'msg'=>$validator->errors()->first(),
                'result' => false,
            ]);
        }
        $check_code = Subcategory::where('category_id', $request->category_id)->where('code', $request->code)->count();
        if($check_code > 0) {
            return response()->json([
                'msg'=> '¡El código de la subcategoría ya se encuentra registrado!',
                'result' => false,
            ]);
        }

        $check_name = Subcategory::where('category_id', $request->category_id)->where('name', $request->name)->count();

        if($check_name > 0) {
            return response()->json([
                'msg'=> '¡El nombre de la subcategoría ya se encuentra registrado!',
                'result' => false,
            ]);
        }

        /**
         * Subcategory
         */
        $subcategory = new Subcategory();
            $subcategory->name = $request->name;
            $subcategory->code = $request->code;
            $subcategory->category_id = $request->category_id;
            $subcategory->creator_id = $request->creator_id;
        $subcategory->save();

        return response()->json([
            'msg'=> 'Subcategoría creada exitosamente',
            'result' => true,
        ]);
    }

    public function subcategoryEdit(Request $request){
        $rules = [
            'name' => 'required|max:50|min:3',
            'code' => 'required|max:255'
		];
        $messages = [
            "required_unless" => "El campo :attribute es obligatorio.",
            "required_if" => "El campo :attribute es obligatorio.",
            "confirmed" => 'Las contaseñas no coinciden',
        ];
        $attributes = [
            'name' => 'Nombre',
            'code' => 'Código'
        ];
		$validator = Validator::make($request->all(), $rules, $messages);
        $validator->setAttributeNames($attributes);
        if ($validator->fails()) {
            return response()->json([
                'msg'=>$validator->errors()->first(),
                'result' => false,
            ]);
        }

        $check_code = Subcategory::where('id', '!=', $request->id)->where('category_id', $request->category_id)->where('code', $request->code)->count();
        if($check_code > 0) {
            return response()->json([
                'msg'=> 'El Código ya se encuentra registrado',
                'result' => false,
            ]);
        }

        $subcategory = Subcategory::where('id', $request->id)->first();
            $subcategory->name = $request->name;
            $subcategory->code = $request->code;
            $subcategory->category_id = $request->category_id;
            $subcategory->creator_id = $request->creator_id;
        $subcategory->save();
        return response()->json([
            'msg'=> 'Subcategoría editada exitosamente',
            'result' => true,
        ]);
    }

    public function subcategoryDelete(Request $request){
        if(isset($request->id)){
            $subcategory = Subcategory::where('id', $request->id);
            if($subcategory->whereDoesntHave('products')->exists()){
                $subcategory->delete();
                return response()->json([
                    'msg'=>'Subcategoría eliminada exitosamente'
                ]);
            } else {
                return response()->json([
                    'msg'=> 'No puede eliminar una subcategoría que tenga productos asignados'
                ], 422);
            }
        }
        return response()->json([
            'msg'=> 'Ha ocurrido un error'
        ], 422);
    }

    public function subcategorySuspend(Request $request) {
        if(isset($request->id)){
            $subcategory = Subcategory::where('id', $request->id)->first();
            $subcategory->status = $subcategory->status == 1 ? 0 : 1;
            $subcategory->save();
            return response()->json([
                'msg'=> $subcategory->status == 0 ? 'Subcategoría suspendida exitosamente' : 'Subcategoría activada exitosamente',
                'result' => true,
            ]);
        }else{
            return response()->json([
                'msg'=> 'Ha ocurrido un error',
                'result' => false,
            ], 422);
        }
    }
}
