<?php

namespace App\Http\Controllers\Api\Admin\Warehouse;

use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\EnterpriseUser;
use App\Models\ProductWarehouseMovement;
use App\Models\Warehouse;
use App\User;
use DB;
use Illuminate\Http\Request;
use Validator;

class WarehouseController extends Controller
{
    public function warehouses(Request $request)
    {
        $branches = [];

        $branches = Branch::where('enterprise_id', $request->enterprise_id)->where('status', 1)->get();

        if ($request->role == 3) {

            $users_ids = EnterpriseUser::where('enterprise_id', $request->enterprise_id)->select('user_id')->get()->pluck('user_id')->toArray();

            array_push($users_ids, $request->user_id);

            $warehouses = Warehouse::whereIn('user_id', $users_ids)
                ->when($request->has('branch_id'), function ($query) use ($request) {
                    $query->where('branch_id', $request->branch_id);
                });

        } else {

            $warehouses = Warehouse::when(is_array($request->branch_id), function ($query) use ($request) {
                $query->whereIn('branch_id', $request->branch_id);
            }, function ($query) use ($request) {
                $query->where('branch_id', $request->branch_id);
            });
        }

        if ($request->has('search')) {
            $warehouses->where(function ($q) use ($request) {
                $q->where('name', 'like', '%' . $request->search . '%')
                    ->orWhere('code', 'like', '%' . $request->search . '%')
                    ->orWhere(function ($q2) use ($request) {
                        $q2->whereHas('branch', function ($q3) use ($request) {
                            $q3->where('name', 'like', '%' . $request->search . '%');
                        });
                    });
            });
        }

        if ($request->has('status')) {
            if ($request->status != 3) {
                $warehouses->where('status', $request->status)->whereNull('deleted_at');
            } else {
                $warehouses->whereNotNull('deleted_at');
            }
        }

        $response = $warehouses->with(['branch' => function ($q) {
            $q->withTrashed();
        }])->orderBy('id', 'desc')->withTrashed()->paginate(10);

        return response()->json([
            'warehouses' => $response,
            'branches' => $branches,
            'arr' => is_array($request->branch_id),
            'result' => true,
        ]);
    }

    public function getWarehouseByBranch(Request $request)
    {
        $warehouses = Warehouse::where([
            ['user_id', $request->user_id],
            ['branch_id', $request->branch_id],
        ])->get();

        return $warehouses;
    }

    public function warehouseCreate(Request $request)
    {
        $rules = [
            'name' => 'required|max:50|min:3',
            'description' => 'required|between:5,255',
            'code' => 'required',
            'branch_id' => 'required',
        ];
        $messages = [
            "required_unless" => "El campo :attribute es obligatorio.",
            "required_if" => "El campo :attribute es obligatorio.",
            "confirmed" => 'Las contaseñas no coinciden',
        ];
        $attributes = [
            'name' => 'Nombre',
            'description' => 'Descripcion',
            'code' => 'Código',
            'branch_id' => 'Sucursal',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        $validator->setAttributeNames($attributes);
        if ($validator->fails()) {
            return response()->json([
                'msg' => $validator->errors()->first(),
                'result' => false,
            ]);
        }

        $check_code = Warehouse::where('user_id', $request->user_id)->where('code', $request->code)->count();
        if ($check_code > 0) {
            return response()->json([
                'msg' => 'El Código ya se encuentra registrado',
                'result' => false,
            ]);
        }

        /**
         * Warehouse
         */
        $warehouse = new Warehouse();
        $warehouse->name = $request->name;
        $warehouse->description = $request->description;
        $warehouse->code = $request->code;
        $warehouse->location = $request->location ? $request->location : null;
        $warehouse->user_id = $request->user_id;
        $warehouse->creator_id = $request->creator_id;
        $warehouse->branch_id = $request->branch_id;
        $warehouse->save();

        return response()->json([
            'msg' => 'Almacen creado exitosamente',
            'result' => true,
        ]);
    }

    public function warehouseEdit(Request $request)
    {
        $rules = [
            'name' => 'required|max:50|min:3',
            'description' => 'required|min:7',
            'code' => 'required',
            'branch_id' => 'required',
        ];
        $messages = [
            "required_unless" => "El campo :attribute es obligatorio.",
            "required_if" => "El campo :attribute es obligatorio.",
            "confirmed" => 'Las contaseñas no coinciden',
        ];
        $attributes = [
            'name' => 'Nombre',
            'description' => 'Descripcion',
            'code' => 'Código',
            'branch_id' => 'Sucursal',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        $validator->setAttributeNames($attributes);
        if ($validator->fails()) {
            return response()->json([
                'msg' => $validator->errors()->first(),
                'result' => false,
            ]);
        }

        $check_code = Warehouse::where('id', '!=', $request->id)->where('user_id', $request->user_id)->where('code', $request->code)->count();
        if ($check_code > 0) {
            return response()->json([
                'msg' => 'El Código ya se encuentra registrado',
                'result' => false,
            ]);
        }

        $warehouse = Warehouse::where('id', $request->id)->first();
        $warehouse->name = $request->name;
        $warehouse->description = $request->description;
        $warehouse->code = $request->code;
        $warehouse->location = $request->location ? $request->location : null;
        $warehouse->branch_id = $request->branch_id;
        $warehouse->save();
        return response()->json([
            'msg' => 'Almacen editado exitosamente',
            'result' => true,
        ]);
    }

    public function warehouseDelete(Request $request)
    {
        if (!isset($request->id)) {
            return response()->json([
                'msg' => 'Ha ocurrido un error',
            ], 422);
        }

        $warehouse = Warehouse::where('id', $request->id);

        if (!$warehouse->firstOrFail()) {
            return response()->json([
                'msg' => 'El almacen ya fue eliminado!',
            ], 422);
        }

        // Se evalua si todos los productos del almacen poseen un stock actual igual a 0, se utilizan los metodos whereHas,
        // para no usar un foreach y evaluar cada uno de los productos del almacen, ya que estos metodos nos traen todos
        // los productos asociados al almacen y el ultimo movimiento de los mismos. Se usa el MAX para poder agrupar por el ultimo registro.

        $pwm = ProductWarehouseMovement::select(DB::raw('MAX(id) as id'))->whereHas('product_warehouse', function ($query) use ($request) {
            $query->where('warehouse_id', $request->id);
        })->orderBy('id', 'desc')->groupBy('product_id')->get();

        // Al obtener los ids de los productos, con SQL se determina el stock actual del producto y si cumple con la condicion
        // de que sea mayor a 0

        $check_pwm = ProductWarehouseMovement::whereIn('id', $pwm)
            ->whereRaw(DB::raw('IF(`type` = 1, `current_stock` + `amount`, `current_stock` - `amount`) > 0'))
            ->count();

        if ($check_pwm == 0) {
            $warehouse->delete();
            return response()->json([
                'msg' => 'Almacen eliminado exitosamente!',
            ]);
        }

        return response()->json([
            'msg' => 'El almacen posee productos en su stock! No se puede eliminar.',
        ], 422);
    }

    public function warehouseSuspend(Request $request)
    {
        if (isset($request->id)) {
            $warehouse = Warehouse::where('id', $request->id)->first();
            $warehouse->status = $warehouse->status == 1 ? 2 : 1;
            $warehouse->save();
            return response()->json([
                'msg' => $warehouse->status == 1 ? 'Almacen activado exitosamente' : 'Almacen suspendido exitosamente',
                'result' => true,
            ]);
        } else {
            return response()->json([
                'msg' => 'Ha ocurrido un error',
                'result' => false,
            ], 422);
        }
    }

    public function getWarehouseEnterprises(Request $request)
    {
        $user = User::with([
            'enterprise_warehouse' => function ($query) {
                $query->where('status', 1);
            },
        ])->findOrFail($request->Id);

        return $user->enterprise_warehouse;
    }

    public function warehouseEnterprises()
    {

        $users = User::where('status', 1)->whereHas('roles', function ($q) {
            $q->where('name', 'enterprise');
        })->get();

        return response()->json([
            'users' => $users,
            'result' => true,
        ]);
    }
}
