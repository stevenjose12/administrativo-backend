<?php

namespace App\Http\Controllers\Api\Admin\Model;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ModelRequest;

use App\Models\Modelo;
use App\Models\Brand;

use Validator;

class ModelController extends Controller
{
    public function models(Request $request)
    {
        $models = Modelo::where('brand_id', $request->brand_id)->with('brand');
        
        if ($request->has('search')) {
            $models->where(function($q) use ($request) {
				$q->where('name','like','%'.$request->search.'%')
					->orWhere('code','like','%'.$request->search.'%');
			});
        }

        if ($request->has('status')) {
            $models->where('status', intval($request->status));
        }


        if($request->has('select')) {
            $models = $models->get();
        } else {
            $models = $models->paginate(10);
        }

        return response()->json([
            'models' => $models,
            'result' => true
        ]);
    }

    protected function ValidModel(array $data)
    {
        $attributes = [
            'name' => 'nombre',
            'code' => 'código'
        ];

        $validator = Validator::make($data, [
            'name' => ['required', 'between:3,50'],
            'code' => ['required', 'between:1,3']
        ], [
            'required'    => 'El campo :attribute es obligatorio.',
            'between'     => ':attribute tiene que estar entre :min - :max.'
        ]);
        
        $validator->setAttributeNames($attributes);

		return $validator;
    }

    public function modelCreateFromCompound(Request $request)
    {
        $this->ValidModel($request->all())->validate();

        $check_code = Modelo::where('brand_id', $request->brand_id)->where('code', $request->code)->count();

        if($check_code > 0) {
            return response()->json([
                'message' => '¡Ya existe un modelo con el código ingresado!'
            ], 422);
        }

        $model = new Modelo();
        $model->fill($request->all())->save();

        return $model;
    }

    public function modelCreate(ModelRequest $request)
    {

        $check_code = Modelo::where('brand_id', $request->brand_id)->where('code', $request->code)->count();
        if($check_code > 0) {
            return response()->json([
                'msg'=> 'Ya existe un modelo con el código ingresado',
                'result' => false,
            ]);
        }

        $model = new Modelo;
        $model->brand_id = $request->brand_id;
        $model->name = $request->name;
        $model->code = $request->code;
        $model->status = 1;
        $model->creator_id = $request->creator_id;
        $model->save();

        return response()->json([
            'msg'=>'Modelo Creado exitosamente',
            'result' => true,
        ]);
    }

    public function modelEdit(ModelRequest $request)
    {

        $check_code = Modelo::where('id','!=',$request->id)->where('brand_id', $request->brand_id)->where('code', $request->code)->count();
        if($check_code > 0) {
            return response()->json([
                'msg'=> 'Ya existe un modelo con el código ingresado',
                'result' => false,
            ]);
        }

        $model = Modelo::find($request->id);
        $model->brand_id = $request->brand_id;
        $model->name = $request->name;
        $model->code = $request->code;
        $model->save();

        return response()->json([
            'msg'=>'Modelo Editado exitosamente',
            'result' => true,
        ]);
    }

    public function modelSuspend(Request $request)
    {
        $model = Modelo::find($request->id);
        $model->status = 0;
        $model->save();

        return response()->json([
            'msg'=>'Modelo Suspendido exitosamente',
            'result' => true,
        ]);
    }

    public function modelActive(Request $request)
    {
        $model = Modelo::find($request->id);
        $model->status = 1;
        $model->save();

        return response()->json([
            'msg'=>'Modelo Activado exitosamente',
            'result' => true,
        ]);
    }

    public function modelDelete(Request $request)
    {
        if(isset($request->id)){
            $model = Modelo::where('id', $request->id);
            if($model->whereDoesntHave('products')->exists()){
                $model->delete();
                return response()->json([
                    'msg'=>'Modelo eliminada exitosamente'
                ]);
            } else {
                return response()->json([
                    'msg'=> 'No puede eliminar un modelo que tenga productos asignados'
                ], 422);
            }
        }
        return response()->json([
            'msg'=> 'Ha ocurrido un error'
        ], 422);
    }
}
