<?php

namespace App\Http\Controllers\Api\Admin\Currency;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CurrencyRequest;
use App\Models\Currency;

class CurrencyController extends Controller
{
	public function currencies(Request $request)
	{
		if($request->role == 1){
		$currencies = Currency::orderBy('id', 'ASC');
		} else {
		$currencies = Currency::where('status', 1)->orderBy('id', 'ASC');
		}
		
		if ($request->has('search')) {
		$currencies->where(function($q) use ($request) {
				$q->where('name','like','%'.$request->search.'%')
					->orWhere('code','like','%'.$request->search.'%');
			});
		}

		if ($request->has('status')) {
			$currencies->where('status', $request->status);
		}

		$response = $currencies->paginate(10);
		
		return response()->json([
			'currencies' => $response,
			'result' => true
		]);
    }
    
    public function getCurrenciesFromPurchase(Request $request)
    {
        $currencies = Currency::where('status', 1)->orderBy('id', 'ASC')->get();

        return $currencies;
    }

	public function currencyCreate(CurrencyRequest $request){
        
        $currency = new Currency();
            $currency->name = $request->name;
            $currency->code = $request->code;
        $currency->save();

        return response()->json([
            'msg'=> 'Divisa creada exitosamente',
            'result' => true,
        ]);
    }

    public function currencyEdit(CurrencyRequest $request){
        
        $currency = Currency::where('id', $request->id)->first();
            $currency->name = $request->name;
            $currency->code = $request->code;
        $currency->save();
        return response()->json([
            'msg'=> 'Divisa editada exitosamente',
            'result' => true,
        ]);
	}
	
	public function currencySuspend(Request $request) {
        if(isset($request->id)){
            $currency = Currency::where('id', $request->id)->first();
				$currency->status = $currency->status == 1 ? 2 : 1;
            $currency->save();
            return response()->json([
                'msg'=> $currency->status == 2 ? 'Divisa suspendido exitosamente' : 'Divisa activado exitosamente'
            ]);
        }else{
            return response()->json([
                'msg'=> 'Ha ocurrido un error'
            ], 422);
        }
    }
}