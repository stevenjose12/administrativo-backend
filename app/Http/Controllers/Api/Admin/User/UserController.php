<?php

namespace App\Http\Controllers\Api\Admin\User;

use App\Http\Controllers\Controller;
use App\Http\Traits\GenerateCode;
use App\Models\AdministratorEnterprise;
use App\Models\Branch;
use App\Models\EnterpriseUser;
use App\Models\Person;
use App\Models\RoleEnterprise;
use App\Models\RoleSubuser;
use App\User;
use DB;
use Illuminate\Http\Request;
use Image;
use Validator;

class UserController extends Controller
{
    use GenerateCode;

    private function ValidatorRole(array $data)
    {
        $attributes = [
            'role' => 'rol',
        ];

        $validator = Validator::make($data, [
            'role' => ['required'],
        ], [
            "required_unless" => "El campo :attribute es obligatorio.",
            "required_if" => "El campo :attribute es obligatorio.",
        ]);

        $validator->setAttributeNames($attributes);

        return $validator;
    }

    private function ValidatorPassword(array $data)
    {
        $attributes = [
            'password' => 'contraseña',
            'password_confirmation' => "confirmar contraseña",
        ];

        $validator = Validator::make($data, [
            'password' => ['string', 'min:6', 'required_with:password_confirmation', 'same:password_confirmation'],
            'password_confirmation' => ['required', 'string', 'min:6'],
        ], [
            "required_unless" => "El campo :attribute es obligatorio.",
            "required_if" => "El campo :attribute es obligatorio.",
            "confirmed" => 'Las contaseñas no coinciden',
        ]);

        $validator->setAttributeNames($attributes);

        return $validator;
    }

    public function users(Request $request)
    {
        if ($request->role == 1 || $request->role == 2) {
            $users = User::whereHas('roles', function ($query) {
                $query->where('name', 'administrator');
            });
        } else if ($request->role == 3) {
            $users = User::whereHas('enterprise_users', function ($q) use ($request) {
                $q->where('enterprise_id', $request->user_id);
            })
                ->whereHas('roles', function ($query) {
                    $query->where('name', 'sub_user');
                })
                ->with(['role_enterprise_subuser' => function ($q) {
                    $q->select('role_enterprise_id', 'user_id')->with(['role_enterprise']);
                }]);
        } else if ($request->role == 4) {
            $users = User::whereHas('enterprise_users', function ($q) use ($request) {
                $q->where('enterprise_id', $request->user_id);
            })
                ->whereHas('roles', function ($query) {
                    $query->where('name', 'sub_user');
                })
                ->with(['role_enterprise_subuser' => function ($q) {
                    $q->select('role_enterprise_id', 'user_id')->with(['role_enterprise']);
                }]);
        }

        $users->select(
            'users.id',
            'users.name',
            'users.email',
            'users.status'
        )->join('persons', 'persons.user_id', '=', 'users.id')
            ->with(['person' => function ($query) {
                $query->select(
                    'persons.first_name',
                    'persons.last_name',
                    'persons.phone',
                    'persons.identity_document as document',
                    'persons.user_id',
                    'persons.avatar'
                );
            }, 'roles']);

        if ($request->has('search')) {
            $users->where(function ($q) use ($request) {
                $q->where('persons.first_name', 'like', '%' . $request->search . '%')
                    ->orWhere('persons.last_name', 'like', '%' . $request->search . '%')
                    ->orWhere(DB::raw("CONCAT(persons.first_name, ' ', persons.last_name)"), 'LIKE', "%" . $request->search . "%")
                    ->orWhere('users.email', 'like', '%' . $request->search . '%')
                    ->orWhere('persons.identity_document', 'like', '%' . $request->search . '%')
                    ->orWhere('users.name', 'like', '%' . $request->search . '%');
            });
        }

        if ($request->has('status')) {
            $users->where('users.status', intval($request->status));
        }

        $response = $users->paginate(10);

        return $response;
    }

    public function userCreate(Request $request)
    {

        $rules = [
            'name' => ['required'],
            'document' => ['required', 'string', 'between:7,11'],
            'email' => ['required', 'email', 'unique:users,email,NULL,id,deleted_at,null'],
            // 'last_name'             => ['required', 'string', 'max:50'],
            'first_name' => ['required', 'string', 'max:50'],
            'password' => ['string', 'min:6', 'required_with:password_confirmation', 'same:password_confirmation'],
            'password_confirmation' => ['required', 'string', 'min:6'],
            'phone' => ['required', 'string', 'between:7,11'],
            'level' => ['required', 'sometimes'],
        ];

        $messages = [
            "required_unless" => "El campo :attribute es obligatorio.",
            "required_if" => "El campo :attribute es obligatorio.",
            "confirmed" => 'Las contaseñas no coinciden',
        ];

        $attributes = [
            'name' => 'nombre de usuario',
            'first_name' => 'nombre',
            'last_name' => 'apellido',
            'company' => 'empresa',
            'email' => 'correo electrónico',
            'phone' => 'teléfono',
            'document' => 'cédula',
            'password' => 'contraseña',
            'password_confirmation' => "confirmar contraseña",
            'image' => 'imagen de perfil',
            'level' => 'tipo de usuario',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $validator->setAttributeNames($attributes)->validate();

        if ($request->level === '4') {
            $this->ValidatorRole($request->all())->validate();
        }

        $me = User::findOrFail($request->Id);
        $me->role = $me->roles[0]['id'];

        if ($me->role == 3) {
            $users_ids = EnterpriseUser::where('enterprise_id', $request->Id)->get()->pluck('user_id');
        }

        $documentValidate = Person::where('identity_document', $request->document_type . '-' . $request->document);
        if ($me->role == 3) {
            $documentValidate = $documentValidate->whereIn('user_id', $users_ids);
        } else {
            $documentValidate = $documentValidate->whereHas('user', function ($query) {
                $query->where('level', 2);
            });
        }
        $documentValidate = $documentValidate->whereNull('deleted_at')->count();

        if ($documentValidate > 0) {
            return response()->json([
                'msg' => '¡La cédula ya esta siendo usada por otra persona!',
                'result' => false,
            ]);
        }

        $emailValidate = User::where('email', $request->email);
        if ($me->role == 3) {
            $emailValidate = $emailValidate->whereIn('id', $users_ids);
        } else {
            $emailValidate = $emailValidate->where('level', 2);
        }
        $emailValidate = $emailValidate->whereNull('deleted_at')->count();

        if ($emailValidate > 0) {
            return response()->json([
                'msg' => '¡La dirección de correo electrónico ya esta siendo usado por otra persona!',
                'result' => false,
            ]);
        }

        $levels_code = [2, 3, 4];

        $code = $me->person->code;

        if (in_array($request->level, $levels_code)) {
            $code = $this->generateEnterpriseCode($request->name);
        }

        $_is_admin = in_array($me->level, $levels_code);

        $name = studly_case($request->name);

        if($_is_admin) {
            $name = $code . '-' . studly_case($request->name);
        }

        if($me->role == 3) {
            $name = $me->person->code . '-' . studly_case($request->name);
        }

        $nameValidate = User::where('name', $name);
        if ($me->role == 3) {
            $nameValidate = $nameValidate->whereIn('id', $users_ids);
        } else {
            $nameValidate = $nameValidate->where('level', 2);
        }
        $nameValidate = $nameValidate->whereNull('deleted_at')->count();

        if ($nameValidate > 0) {
            return response()->json([
                'msg' => '¡El nombre de usuario ya esta siendo usado por otra persona!',
                'result' => false,
            ]);
        }

        $user = new User;
        $user->name = $name;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->status = 1;
        $user->level = $request->level;
        $user->save();

        if ($me->role == 3) {

            $roleEnterprise = new RoleSubuser;
            $roleEnterprise->role_enterprise_id = $request->role;
            $roleEnterprise->enterprise_id = $me->id;
            $roleEnterprise->user_id = $user->id;
            $roleEnterprise->save();

            $enterpriseUser = new EnterpriseUser;
            $enterpriseUser->enterprise_id = $me->id;
            $enterpriseUser->user_id = $user->id;
            $enterpriseUser->save();

            $user->attachRole(4);

        } else {

            $user->attachRole($request->level);
        }

        /**
         * Image
         */
        $person = new Person;
        $person->first_name = $request->first_name;
        // $person->last_name = $request->last_name;
        $person->phone = $request->phone;
        $person->identity_document = $request->document_type . '-' . $request->document;

        if ($request->level == 2 || $request->level == 3) {
            $person->code = $code;
        } else if ($request->level == 4 || $request->level == 5) {
            $person->code = $me->person->code;
        }

        if ($request->file('image')) {
            $name = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 40);
            Image::make($request->image)->save('img/users/' . $name . '.jpg');
            $person->avatar = 'img/users/' . $name . '.jpg';
        }

        $person->user_id = $user->id;
        $person->save();

        if ($request->has('branch_id')) {

            $branches = Branch::when($request->branch_id == 0, function ($query) use ($request) {
                $query->where('enterprise_id', $request->Id);
            }, function ($query) use ($request) {
                $query->where('id', $request->branch_id);
            })
                ->where('status', 1)
                ->with(['warehouses' => function ($query) use ($request) {
                    $query->when($request->has('warehouse_id'), function ($query) use ($request) {
                        $query->where('id', $request->warehouse_id)->where('status', 1)->select('id', 'branch_id');
                    }, function ($query) use ($request) {
                        $query->where('status', 1)->select('id', 'branch_id');
                    });
                }])
                ->select('id')
                ->get();

            foreach ($branches as $branch) {
                foreach ($branch->warehouses as $warehouse) {
                    $user->warehouses()->attach($user->id, [
                        'branch_id' => $branch->id,
                        'warehouse_id' => $warehouse['id'],
                    ]);
                }
            }
        }

        return response()->json([
            'result' => true,
        ]);
    }

    public function userEdit(Request $request)
    {
        $rules = [
            'name' => ['required', 'max:255', 'string'],
            'first_name' => ['required', 'string', 'max:50'],
            // 'last_name'  => ['required', 'string', 'max:50'],
            'document' => ['required', 'string'],
            'email' => [
                'required',
                'email',
            ],
            'phone' => ['required', 'string', 'max:11'],
            'level' => ['sometimes'],
        ];

        $messages = [
            "required_unless" => "El campo :attribute es obligatorio.",
            "required_if" => "El campo :attribute es obligatorio.",
            "confirmed" => 'Las contraseñas no coinciden',
            "min" => 'El campo :attribute debe tener mínimo :min caracteres.',
        ];

        $attributes = [
            'name' => 'nombre de usuario',
            'first_name' => 'nombre',
            'last_name' => 'apellido',
            'company' => 'empresa',
            'email' => 'correo electrónico',
            'phone' => 'teléfono',
            'document' => 'cédula',
            'password' => 'contraseña',
            'password_confirmation' => "confirmar contraseña",
            'level' => 'tipo de usuario',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $validator->setAttributeNames($attributes)->validate();

        // Validación de contraseña si alguna de las 2 no es vacía
        if (!empty($request->password) || !empty($request->password_confirmation)) {
            $this->ValidatorPassword($request->all())->validate();
        }
        // Validación si el usuario es de tipo subuser
        if ($request->level === '4') {
            $this->ValidatorRole($request->all())->validate();
        }

        $me = User::findOrFail($request->Id);
        $me->role = $me->roles[0]['id'];

        if ($me->role == 3) {
            $users_ids = EnterpriseUser::where('enterprise_id', $request->Id)->where('user_id', '!=', $request->id)->get()->pluck('user_id');
        }

        $code = $me->person->code;

        $levels_code = [2, 3, 4];

        $_is_admin = in_array($me->level, $levels_code);

        if (!$code && $_is_admin) {
            $code = $this->generateEnterpriseCode($request->name);
        }

        $name = studly_case($request->name);

        if($_is_admin) {
            $name = $code . '-' . studly_case($request->name);
        }

        if($me->role == 3) {
            $name = $me->person->code . '-' . studly_case($request->name);
        }

        $nameValidate = User::where([
            ['id', '!=', $request->id],
            ['name', $name],
        ]);

        if ($me->role == 3) {
            $nameValidate = $nameValidate->whereIn('id', $users_ids);
        } else {
            $nameValidate = $nameValidate->where('level', 2);
        }

        $nameValidate = $nameValidate->whereNull('deleted_at')->count();

        if ($nameValidate > 0) {
            return response()->json([
                'msg' => '¡El nombre de usuario ya esta siendo usado por otra persona!',
                'result' => false,
            ]);
        }

        $documentValidate = Person::where([
            ['user_id', '!=', $request->id],
            ['identity_document', $request->document_type . '-' . $request->document],
        ]);

        if ($me->role == 3) {
            $documentValidate = $documentValidate->whereIn('user_id', $users_ids);
        } else {
            $documentValidate = $documentValidate->whereHas('user', function ($query) {
                $query->where('level', 2);
            });
        }

        $documentValidate = $documentValidate->whereNull('deleted_at')->count();

        if ($documentValidate > 0) {
            return response()->json([
                'msg' => '¡La cédula ya esta siendo usado por otra persona!',
                'result' => false,
            ]);
        }

        $emailValidate = User::where([
            ['id', '!=', $request->id],
            ['email', $request->email],
        ]);

        if ($me->role == 3) {
            $emailValidate = $emailValidate->whereIn('id', $users_ids);
        } else {
            $emailValidate = $emailValidate->where('level', 2);
        }

        $emailValidate = $emailValidate->whereNull('deleted_at')->count();

        if ($emailValidate > 0) {
            return response()->json([
                'msg' => '¡La dirección de correo electrónico ya esta siendo usado por otra persona!',
                'result' => false,
            ]);
        }

        $user = User::findOrFail($request->id);

        $user->name = $name;
        $user->email = $request->email;
        $user->level = $request->level;

        if (!empty($request->password)) {
            $user->password = $request->password;
        }

        $user->save();

        $user->detachRole($user->roles[0]['id']);
        $user->attachRole($request->level);

        if ($me->role == 3) {
            $roleEnterprise = RoleSubuser::where('user_id', $user->id);
            if ($roleEnterprise->exists()) {
                $roleEnterprise = $roleEnterprise->first();
                $roleEnterprise->role_enterprise_id = $request->role;
                $roleEnterprise->enterprise_id = $me->id;
                $roleEnterprise->user_id = $user->id;
                $roleEnterprise->save();
            } else {
                $roleEnterprise = new RoleSubuser;
                $roleEnterprise->role_enterprise_id = $request->role;
                $roleEnterprise->enterprise_id = $me->id;
                $roleEnterprise->user_id = $user->id;
                $roleEnterprise->save();
            }

        }

        $person = Person::where('user_id', $request->id)->first();

        if (isset($request->image)) {
            if ($request->hasFile('image')) {
                $name = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 40);
                Image::make($request->image)->save('img/users/' . $name . '.jpg');
                $person->avatar = 'img/users/' . $name . '.jpg';
            }
        }

        $person->first_name = $request->first_name;
        // $person->last_name = $request->last_name;
        $person->identity_document = $request->document_type . '-' . $request->document;
        $person->phone = $request->phone;
        if (!$person->code && $_is_admin) {
            $person->code = $code;
        }
        $person->save();

        if ($request->has('branch_id')) {

            $user->warehouses()->detach();

            $branches = Branch::when($request->branch_id == 0, function ($query) use ($request) {
                $query->where('enterprise_id', $request->Id);
            }, function ($query) use ($request) {
                $query->where('id', $request->branch_id);
            })
                ->where('status', 1)
                ->with(['warehouses' => function ($query) use ($request) {
                    $query->when($request->has('warehouse_id'), function ($query) use ($request) {
                        $query->where('id', $request->warehouse_id)->where('status', 1)->select('id', 'branch_id');
                    }, function ($query) use ($request) {
                        $query->where('status', 1)->select('id', 'branch_id');
                    });
                }])
                ->select('id')
                ->get();

            foreach ($branches as $branch) {
                foreach ($branch->warehouses as $warehouse) {
                    $user->warehouses()->attach($user->id, [
                        'branch_id' => $branch->id,
                        'warehouse_id' => $warehouse['id'],
                    ]);
                }
            }
        }

        return response()->json([
            'msg' => 'Usuario editado exitosamente',
            'result' => true,
        ]);
    }

    public function userSuspend(Request $request)
    {
        if($request->Id === $request->authenticateUser)
        {
            return response()->json(['No puedes suspender este usuario'],422);
        }
        $user = User::findOrFail($request->Id);
        $user->status = 2;
        $user->save();

        return $user;
    }

    public function userActive(Request $request)
    {
        $user = User::findOrFail($request->Id);
        $user->status = 1;
        $user->save();

        return $user;
    }

    public function userDelete(Request $request)
    {
        if($request->Id === $request->authenticateUser)
        {
            return response()->json(['No puede eliminar este usuario'],422);
        }

        $checkEnterprises = AdministratorEnterprise::where('administrator_id', $request->Id)->count();

        if($checkEnterprises > 0) {
            return response()->json(['No puede eliminar a un usuario con empresas asignadas'],422);
        }
        
        User::findOrFail($request->Id)->delete();
        Person::where('user_id', $request->Id)->delete();

        return $request->Id;
    }

    public function userVerify(Request $request)
    {
        $user = User::findOrFail($request->Id);
        $user->status = 1;
        $user->save();

        return $user;
    }

    public function userRoleEnterprise(Request $request)
    {
        $roles = RoleEnterprise::where('enterprise_id', $request->Id)->get();

        return $roles;
    }

    public function getUserData(Request $request)
    {
        if (isset($request->id)) {
            $user = User::where('id', $request->id)->with(['person', 'roles'])->first();
            $user->role = $user->roles[0]['id'];

            return response()->json([
                'user' => $user,
            ]);
        } else {
            return response()->json([
                'msg' => 'Ha ocurrido un error',
            ], 422);
        }
    }

    public function getAdmins(Request $request)
    {

        $users = User::whereIn('level', [1, 2])->with(['person', 'roles'])->get();

        if (empty($users)) {
            return response()->json([
                'msg' => 'Ha ocurrido un error',
            ], 422);
        }

        return $users;
    }
}
