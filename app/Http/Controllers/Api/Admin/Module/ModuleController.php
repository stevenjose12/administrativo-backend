<?php

namespace App\Http\Controllers\Api\Admin\Module;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Module;
use Validator;

class ModuleController extends Controller
{
  public function modules() 
  {

    $modules = Module::where(function ($query) {
      $query->where('admin_only', 0)->where('required', 0)->where('status', 1);
    })
    ->orWhere(function ($query) {
      $query->where('user_only', 1)->where('required', 0)->where('status', 1);
    })->with('permissions')->orderBy('name', 'ASC')->get();

    return response()->json([
      'modules' => $modules
    ]);
  }

  public function enterpriseModules(Request $request)
  {
    if($request->role == 1) {
      $enterprises = User::whereHas('roles', function($query) {
        $query->where('name', 'enterprise');
      });
    } else if ($request->role == 2) {
      $enterprises = User::whereHas('enterprise_administrator', function($q) use($request) {
        $q->where('administrator_id', $request->user_id);
      });
    }

    $enterprises->select(
        'users.id',
        'users.status',
        'persons.first_name',
        'persons.deleted_at'
    )->join('persons', 'persons.user_id', '=', 'users.id')
    ->with(['roles', 'enterprise_modules']);

    if ($request->has('search')) {
      $enterprises->where(function($q) use ($request) {
        $q->where('persons.first_name','like','%'.$request->search.'%');
      });
    }
    
    if ($request->has('status')) {
        $enterprises->where('users.status', intval($request->status));
    }

    $response = $enterprises->orderBy('id', 'desc')->paginate(10);
    
    return response()->json([
        'enterprises' => $response,
        'result' => true,
    ]);
  }
  
  public function enterpriseModulesEdit(Request $request)
  {
    $rules = [
      'modules'       => ['required', 'array', 'min:1']
    ];
    $messages = [
      "required" => "El campo :attribute es obligatorio.",
    ];

    $attributes = [
      'modules'       => 'modulos'
    ];
    
    $validator = Validator::make($request->all(), $rules, $messages);
    $validator->setAttributeNames($attributes)->validate();
    
    $user = User::with([
      'enterprise_modules',
      'permissions_by_enterprise'
    ])->find($request->id);
    
    $user->enterprise_modules()->detach();
    $user->permissions_by_enterprise()->detach();
    
    $modulesId = $request->modules;
    
    $this->attachmentModules($modulesId, $user);

    return response()->json([
        'msg'=>'Modules asignados exitosamente',
        'result' => true,
    ]);
  }

    private function attachmentModules($modules, $user)
    {
        foreach ($modules as $moduleId) {
            
            $user->enterprise_modules()->attach($moduleId);

            $module = Module::with([
                'permissions'
            ])->find($moduleId);

            $permissionsId = $module->permissions->pluck('id');
            
            $permissionsId->each(function ($permissionId, $key) use ($user) {
                $user->permissions_by_enterprise()->attach($permissionId, ['user_type' => 'App\User']);
            });
        }
    }
}