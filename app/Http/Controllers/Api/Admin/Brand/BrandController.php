<?php

namespace App\Http\Controllers\Api\Admin\Brand;

use App\Http\Controllers\Controller;
use App\Http\Requests\BrandRequest;
use App\Models\Brand;
use Illuminate\Http\Request;
use Validator;

class BrandController extends Controller
{
    public function brands(Request $request)
    {
        if ($request->role == 1) {

            $brands = Brand::whereNull('deleted_at')->with(['owner' => function ($query) {
                $query->with(['person' => function ($query) {
                    $query->withTrashed();
                }])->withTrashed();
            }]);

        } else if ($request->role == 2) {

            $brands = Brand::where('creator_id', $request->user_id)->with(['owner' => function ($query) {
                $query->with(['person' => function ($query) {
                    $query->withTrashed();
                }])->withTrashed();
            }]);

        } else {

            $brands = Brand::where('user_id', $request->user_id)->with(['owner' => function ($query) {
                $query->with(['person' => function ($query) {
                    $query->withTrashed();
                }])->withTrashed();
            }]);
        }

        if ($request->has('search')) {
            $brands->where(function ($q) use ($request) {
                $q->where('name', 'like', '%' . $request->search . '%')
                    ->orWhere('code', 'like', '%' . $request->search . '%');
            });
        }

        if ($request->has('status')) {
            $brands->where('status', intval($request->status));
        }

        if ($request->has('select')) {
            $brands = $brands->get();
        } else {
            $brands = $brands->paginate(10);
        }

        return response()->json([
            'brands' => $brands,
            'result' => true,
        ]);
    }

    private function ValidBrand(array $data)
    {
        $attributes = [
            'name' => 'nombre',
            'code' => 'código',
        ];

        $validator = Validator::make($data, [
            'name' => ['required', 'between:3,50'],
            'code' => ['required', 'between:1,3'],
        ], [
            'required' => 'El campo :attribute es obligatorio.',
            'between' => ':attribute tiene que estar entre :min - :max.',
        ]);

        $validator->setAttributeNames($attributes);

        return $validator;
    }

    public function brandCreateFromCompound(Request $request)
    {
        $this->ValidBrand($request->all())->validate();

        $check_code = Brand::where('user_id', $request->user_id)->where('code', $request->code)->count();

        if ($check_code > 0) {
            return response()->json([
                'message' => '¡Ya existe una marca con el código ingresado!',
            ], 422);
        }

        $brand = new Brand();
            $brand->user_id = $request->creator_id;
            $brand->code = $request->code;
            $brand->name = $request->name;
            $brand->creator_id = $request->creator_id;
        $brand->save();

        return $brand->load('models');
    }

    public function brandCreate(BrandRequest $request)
    {

        $check_code = Brand::where('user_id', $request->user_id)->where('code', $request->code)->count();
        if ($check_code > 0) {
            return response()->json([
                'msg' => 'Ya existe una marca con el código ingresado',
            ], 422);
        }

        $brand = new Brand;
        $brand->user_id = $request->user_id;
        $brand->name = $request->name;
        $brand->code = $request->code;
        $brand->creator_id = $request->creator_id;
        $brand->save();

        return response()->json([
            'msg' => 'Marca Creada exitosamente',
        ]);
    }

    public function brandEdit(BrandRequest $request)
    {

        $check_code = Brand::where('id', '!=', $request->id)->where('user_id', $request->user_id)->where('code', $request->code)->count();
        if ($check_code > 0) {
            return response()->json([
                'msg' => 'Ya existe una marca con el código ingresado',
            ], 422);
        }

        $brand = Brand::find($request->id);
        $brand->name = $request->name;
        $brand->code = $request->code;
        $brand->user_id = $request->user_id;
        $brand->save();

        return response()->json([
            'msg' => 'Marca Editada exitosamente',
        ]);
    }

    public function brandSuspend(Request $request)
    {
        $brand = Brand::find($request->id);
        $brand->status = 0;
        $brand->save();

        return response()->json([
            'msg' => 'Marca Suspendida exitosamente',
            'result' => true,
        ]);
    }

    public function brandActive(Request $request)
    {
        $brand = Brand::find($request->id);
        $brand->status = 1;
        $brand->save();

        return response()->json([
            'msg' => 'Marca Activada exitosamente',
            'result' => true,
        ]);
    }

    public function brandDelete(Request $request)
    {
        if (isset($request->id)) {
            $brand = Brand::where('id', $request->id);
            if ($brand->whereDoesntHave('products')->exists()) {
                $brand->delete();
                return response()->json([
                    'msg' => 'Marca eliminada exitosamente',
                ]);
            } else {
                return response()->json([
                    'msg' => 'No puede eliminar una marca que tenga productos asignados',
                ], 422);
            }
        }
        return response()->json([
            'msg' => 'Ha ocurrido un error',
        ], 422);
    }
}
