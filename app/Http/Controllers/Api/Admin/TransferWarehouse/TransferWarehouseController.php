<?php

namespace App\Http\Controllers\Api\Admin\TransferWarehouse;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Warehouse;
use App\Models\EnterpriseUser;
use App\Models\TransferWarehouse;
use App\Models\TransferWarehouseDetails;
use App\Models\ProductWarehouse;
use App\Models\ProductWarehouseMovement;
use App\Http\Traits\ProductSerial;

define('ENTRY', 1);
define('OUTPUT', 2);

use Carbon\Carbon;

class TransferWarehouseController extends Controller
{
    use ProductSerial;

    public function transfer(Request $request)
    {

        $transfers = TransferWarehouse::with([
            'origin_warehouse' => function ($query) {
                $query->with(['branch' => function ($query2) {
                    $query2->withTrashed();
                }])->withTrashed();
            },
            'destiny_warehouse' => function ($query) {
                $query->with(['branch' => function ($query2) {
                    $query2->withTrashed();
                }])->withTrashed();
            },
            'creator.person',
            'details' => function ($query) {
                $query->withTrashed();
            },
            'serial' => function ($query) {
                $query->with(['product_warehouse' => function ($query2) {
                    $query2->withTrashed();
                }])->withTrashed();
            },
        ])->when($request->role != 3, function ($query) use ($request) {
            $query->whereIn('origin_warehouse_id', $request->warehouse_id)
                ->orWhere(function($query) use ($request) {
                    $query->whereIn('destiny_warehouse_id', $request->warehouse_id);
                });
        }, function($query) use ($request) {
            $query->where(function($query) use ($request) {
                $query->whereHas('origin_warehouse', function($query2) use ($request) {
                    $query2->where('user_id', $request->user_id);
                });
            })->where(function($query) use ($request) {
                $query->whereHas('destiny_warehouse', function($query2) use ($request) {
                    $query2->where('user_id', $request->user_id);
                });
            });
        })->orderBy('id', 'desc');

        if ($request->has('origin_branch_id') && !$request->has('origin_warehouse_id')) {
            $warehouses_origin_ids = Warehouse::where('branch_id', $request->origin_branch_id)->get()->pluck('id');
            $transfers->whereIn('origin_warehouse_id', $warehouses_origin_ids);
        } 
        
        if ($request->has('origin_branch_id') && $request->has('origin_warehouse_id')) {
            $transfers->where('origin_warehouse_id', $request->origin_warehouse_id);
        } 

        if ($request->has('destiny_branch_id') && !$request->has('destiny_warehouse_id')) {
            $warehouses_destiny_ids = Warehouse::where('branch_id', $request->destiny_branch_id)->get()->pluck('id');
            $transfers->whereIn('destiny_warehouse_id', $warehouses_destiny_ids);
        } 

        if ($request->has('destiny_branch_id') && $request->has('destiny_warehouse_id')) {
            $transfers->where('destiny_warehouse_id', $request->destiny_warehouse_id);
        } 

        if ($request->has('status')) {
            $transfers->where('status', $request->status);
        }

        return $transfers->paginate(10);
    }

    public function processTransfer(Request $request)
    {
        $transfer = TransferWarehouse::where('id', $request->id)->first();
        if($transfer->status != 1) {
            return response()->json([
                'msg'=> "No se puede procesar este traslado"
            ], 422);
        }

        $transfer_movements = ProductWarehouseMovement::where('action_id', $request->id)->where('action_class', 4)->where('status', 0);
        $transfer_movements->update(['status' => 1]);

        foreach ($request->details as $product) {
            $check_pw = ProductWarehouse::where([
				['warehouse_id', $transfer->destiny_warehouse_id],
				['product_id', $product['id']]
            ])->first();
            
            if (!$check_pw) {

                $pwd = new ProductWarehouse;
                    $pwd->warehouse_id = $transfer->destiny_warehouse_id;
                    $pwd->product_id = $product['id'];
                    $pwd->status = 1;
                    $pwd->percentage_earning = 0;
                    $pwd->percentage_max = 0;
                    $pwd->percentage_offer = 0;
                    $pwd->cost = $product['cost'];
                    $pwd->price = $product['cost'];
                    $pwd->price_max = $product['cost'];
                    $pwd->price_offer = $product['cost'];
                    $pwd->creator_id = $request->creator_id;
                $pwd->save();

                $movement_destiny = new ProductWarehouseMovement;
                    $movement_destiny->product_warehouse_id = $pwd->id;
                    $movement_destiny->product_id = $product['id'];
                    $movement_destiny->action_id = $transfer->id;
                    $movement_destiny->action_class = 4;
                    $movement_destiny->action_type = 'App\Models\TransferWarehouse';
                    $movement_destiny->type = 1;
                    $movement_destiny->current_stock = 0;
                    $movement_destiny->status = 1;
                    $movement_destiny->amount = $product['amount'];
                    $movement_destiny->description = 'Ingreso por traslado entre almacenes procesado';
                    $movement_destiny->creator_id = $request->creator_id;
                $movement_destiny->save();

                $check_pw = $pwd;

            } else {

                $check_pw->cost = $product['cost'];
                if($product['change_price'] == true) 
                {
                    $check_pw->price = ($product['cost'] * ($check_pw->percentage_earning/100)) + $product['cost'];
                    $check_pw->price_offer = ($product['cost'] * ($check_pw->percentage_offer/100)) + $product['cost'];
                    $check_pw->price_max = ($product['cost'] * ($check_pw->percentage_max/100)) + $product['cost'];
                }
                $check_pw->save();

                $check_pwm = ProductWarehouseMovement::where('product_warehouse_id', $check_pw->id)
                ->orderBy('id', 'desc');
                
                if ($check_pwm->exists()) {
                    $current = $check_pwm->first();

                    // SETEANDO EL STOCK ACTUAL DEL PRODUCTO DE ACUERDO AL ULTIMO MOVIMIENTO DEL MISMO EN EL ALMACEN
                    $stockCurrent = $current->type == ENTRY ? $current->current_stock + $current->amount : $current->current_stock - $current->amount;
                    
                    $movement = new ProductWarehouseMovement;
                        $movement->product_warehouse_id = $check_pw->id;
                        $movement->action_id = $transfer->id;
                        $movement->product_id = $product['id'];
                        $movement->action_class = 4;
                        $movement->action_type = 'App\Models\TransferWarehouse';
                        $movement->type = 1;
                        $movement->current_stock = $stockCurrent;
                        $movement->amount = $product['amount'];
                        $movement->status = 1;
                        $movement->description = 'Ingreso por traslado entre almacenes procesado';
                        $movement->creator_id = $request->creator_id;
                    $movement->save();
                } else {

                    $movement = new ProductWarehouseMovement;
                        $movement->product_warehouse_id = $check_pw->id;
                        $movement->action_id = $transfer->id;
                        $movement->product_id = $product['id'];
                        $movement->action_class = 4;
                        $movement->action_type = 'App\Models\TransferWarehouse';
                        $movement->type = 1;
                        $movement->current_stock = 0;
                        $movement->amount = $product['amount'];
                        $movement->status = 1;
                        $movement->description = 'Ingreso por traslado entre almacenes procesado';
                        $movement->creator_id = $request->creator_id;
                    $movement->save();

                }
            }

            // Eliminando serialización comprometida
            $this->deleteTransferSerials($request->id,$product,$transfer->origin_warehouse_id,$check_pw->id,'App\Models\TransferWarehouse');

        }
            $transfer->date_recieved = Carbon::now();
            $transfer->status = 2;
        $transfer->save();

        return $transfer;
    }

    public function abortTransfer(Request $request)
    {

        $transfer = TransferWarehouse::where('id', $request->id)->first();
        if($transfer->status == 3) {
            return response()->json([
                'msg'=> "Esta transferencia ya fue anulada previamente"
            ], 422);
        } else {
                $transfer->status = 3;
            $transfer->save();
        }

        ProductWarehouseMovement::where('action_id', $request->id)->where('action_class', 4)->where('status', 0)->update(['status' => 1]);
        $transfer_movements = ProductWarehouseMovement::where('action_id', $request->id)->where('action_class', 4)->get();

        foreach($transfer_movements as $movement) {

            $current = ProductWarehouseMovement::where('product_warehouse_id', $movement->product_warehouse_id)->orderBy('id', 'desc')->first();
            $stockCurrent = $current->type == ENTRY ? $current->current_stock + $current->amount : $current->current_stock - $current->amount;

            $new_movement = new ProductWarehouseMovement;
                $new_movement->action_id = $movement->action_id;
                $new_movement->product_warehouse_id = $movement->product_warehouse_id;
                $new_movement->product_id = $movement->product_id;
                $new_movement->action_class = $movement->action_class;
                $new_movement->action_type = 'App\Models\TransferWarehouse';
                $new_movement->type = 1;
                $new_movement->current_stock = $stockCurrent;
                $new_movement->amount = $movement->amount;
                $new_movement->description = $movement->description;
                $new_movement->status = 1;
                $new_movement->creator_id = $request->user_id;
            $new_movement->save();
        }

        // Eliminando serialización comprometida
        $this->changeSerials($request->id,'App\Models\TransferWarehouse');

        return response()->json([
            'msg'=> 'Transferencia anulada con exito!'
        ]);
    }
}
