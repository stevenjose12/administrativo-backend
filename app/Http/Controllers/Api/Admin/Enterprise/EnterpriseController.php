<?php

namespace App\Http\Controllers\Api\Admin\Enterprise;

use App\Http\Controllers\Controller;
use App\Http\Requests\EnterpriseRequest;
use App\Http\Traits\GenerateCode;
use App\Models\AdministratorEnterprise;
use App\Models\Bank;
use App\Models\BankAccount;
use App\Models\Branch;
use App\Models\ConfigEnterprise;
use App\Models\CurrencyConversion;
use App\Models\Module;
use App\Models\Person;
use App\Models\Warehouse;
use Carbon\Carbon;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

define('URL', 'storage/enterprises/');
define('EXT', 'jpg');

class EnterpriseController extends Controller
{

    use GenerateCode;

    public function enterprises(Request $request)
    {

        if ($request->role == 1) {
            $enterprises = User::with(['roles', 'enterprise_modules'])->whereHas('roles', function ($query) {
                $query->where('name', 'enterprise');
            });
        } else {
            $enterprises = User::with(['roles', 'enterprise_modules'])->whereHas('enterprise_administrator', function ($q) use ($request) {
                $q->where('administrator_id', $request->user_id);
            });
        }

        $enterprises->select(
            'users.id',
            'users.name',
            'users.email',
            'users.status',
            'persons.first_name',
            'persons.phone',
            'persons.code',
            'persons.direction',
            'persons.fiscal_identification',
            'persons.identity_document',
            'persons.avatar',
            'persons.person_type',
            'persons.deleted_at'
        )->join('persons', 'persons.user_id', '=', 'users.id')
            ->with(['roles', 'configuration_enterprise', 'enterprise_administrator.administrator' => function ($query) {
                $query->with(['person' => function ($query) {
                    $query->withTrashed();
                }])->withTrashed();
            }]);

        if ($request->has('search')) {
            $enterprises->where(function ($q) use ($request) {
                $q->where('users.email', 'like', '%' . $request->search . '%')
                    ->orWhere('persons.first_name', 'like', '%' . $request->search . '%')
                    ->orWhere('persons.code', 'like', '%' . $request->search . '%')
                    ->orWhere('persons.phone', 'like', '%' . $request->search . '%')
                    ->orWhere('persons.fiscal_identification', 'like', '%' . $request->search . '%')
                    ->orWhere('persons.identity_document', 'like', '%' . $request->search . '%');
            });
        }

        if ($request->has('status')) {
            $enterprises->where('users.status', intval($request->status));
        }

        if ($request->has('administrator_id')) {
            $enterprises->whereHas('enterprise_administrator', function ($query) use ($request) {
                $query->where('administrator_id', $request->administrator_id);
            });
        }

        $response = $enterprises->orderBy('id', 'desc')->paginate(10);

        return $response;
    }

    public function enterpriseCreate(EnterpriseRequest $request)
    {
        $administrator_enterprises_ids = AdministratorEnterprise::where('administrator_id', $request->administrator_id)->select('enterprise_id')->get();

        if ($request->has('identity_document')) {
            $check_dni = Person::whereIn('user_id', $administrator_enterprises_ids)
                ->where('identity_document', $request->document_type . '-' . $request->identity_document)
                ->count();

            if ($check_dni > 0) {
                return response()->json([
                    'msg' => 'Ya esta registrada esta cedula',
                ], 422);
            }
        }

        $check_fiscal = Person::whereIn('user_id', $administrator_enterprises_ids)
            ->where('fiscal_identification', $request->fiscal_document_type . '-' . $request->fiscal_identification)
            ->count();

        if ($check_fiscal > 0) {
            return response()->json([
                'msg' => 'Ya esta registrado este RIF',
            ], 422);
        }

        $code = $this->generateEnterpriseCode($request->first_name);

        $enterprise = new User;
        $enterprise->name = $code . '-' . str_slug($request->name);
        $enterprise->email = $request->email;
        $enterprise->password = $request->password;
        $enterprise->status = 1;
        $enterprise->level = 3;
        $enterprise->save();

        $enterprise_person = new Person;
        $enterprise_person->user_id = $enterprise->id;
        $enterprise_person->first_name = $request->first_name;
        $enterprise_person->code = $code;
        $enterprise_person->phone = $request->phone;
        $enterprise_person->direction = $request->direction;
        $enterprise_person->fiscal_identification = $request->fiscal_document_type . '-' . $request->fiscal_identification;

        if ($request->has('identity_document')) {
            $enterprise_person->identity_document = $request->document_type . '-' . $request->identity_document;
        }

        if ($request->hasFile('image')) {
            $url = URL;
            $url .= $request->file('image')->storeAs('avatars', $this->quickRandom() . '.' . EXT, 'enterprises');
            $enterprise_person->avatar = $url;
        }

        $enterprise_person->save();

        $enterprise->attachRole(3);

        if ($request->has('administrator_id')) {
            $enterprise_administrator = new AdministratorEnterprise;
            $enterprise_administrator->administrator_id = $request->administrator_id;
            $enterprise_administrator->enterprise_id = $enterprise->id;
            $enterprise_administrator->creator_id = $request->creator_id;
            $enterprise_administrator->save();
        }

        $enterprise_branch = new Branch;
        $enterprise_branch->enterprise_id = $enterprise->id;
        $enterprise_branch->name = 'Sede principal';
        $enterprise_branch->code = $enterprise_person->code;
        $enterprise_branch->status = 1;
        $enterprise_branch->main = 1;
        $enterprise_branch->creator_id = $enterprise->id;
        $enterprise_branch->save();

        $enterprise_warehouse = new Warehouse;
        $enterprise_warehouse->user_id = $enterprise->id;
        $enterprise_warehouse->branch_id = $enterprise_branch->id;
        $enterprise_warehouse->name = 'Almacen principal';
        $enterprise_warehouse->code = $enterprise_person->code;
        $enterprise_warehouse->status = 1;
        $enterprise_warehouse->main = 1;
        $enterprise_warehouse->creator_id = $enterprise->id;
        $enterprise_warehouse->save();

        $enterprise_conversions = new CurrencyConversion;
        $enterprise_conversions->master_id = 1;
        $enterprise_conversions->slave_id = 1;
        $enterprise_conversions->conversion_rate = 1;
        $enterprise_conversions->enterprise_id = $enterprise->id;
        $enterprise_conversions->save();

        $enterprise_config = new ConfigEnterprise;
        $enterprise_config->enterprise_id = $enterprise->id;
        $enterprise_config->sale_type = $request->sale_type;
        $enterprise_config->creator_id = $request->creator_id;
        $enterprise_config->save();

        $enterprise_bank = new Bank;
            $enterprise_bank->enterprise_id = $enterprise->id;
            $enterprise_bank->name = 'Caja';
            $enterprise_bank->type = 2;
            $enterprise_bank->creator_id = $enterprise->id;
        $enterprise_bank->save();

        $enterprise_bank_account = new BankAccount;
            $enterprise_bank_account->bank_id = $enterprise_bank->id;
            $enterprise_bank_account->account_number = "Caja";
            $enterprise_bank_account->type = 3;
            $enterprise_bank_account->creator_id = $enterprise->id;
            $enterprise_bank_account->created_at = Carbon::now();
            $enterprise_bank_account->updated_at = Carbon::now();
        $enterprise_bank_account->save();

        $modulesId = Module::where(function ($query) {
            $query->where('admin_only', 0)->where('required', 1)->where('status', 1);
        })
            ->orWhere(function ($query) {
                $query->where('user_only', 1)->where('required', 1)->where('status', 1);
            })->with('permissions')->orderBy('name', 'ASC')->get()->pluck('id');

        $this->attachmentModules($modulesId, $enterprise);

        return response()->json([
            'msg' => '¡Empresa creada exitosamente!',
            'result' => true,
        ]);
    }

    public function enterpriseEdit(EnterpriseRequest $request)
    {

        $administrator_enterprises_ids = AdministratorEnterprise::where('administrator_id', $request->administrator_id)
            ->where('enterprise_id', '!=', $request->id)->select('enterprise_id')->get();

        if ($request->has('identity_document')) {
            $check_dni = Person::whereIn('user_id', $administrator_enterprises_ids)
                ->where('identity_document', $request->document_type . '-' . $request->identity_document)
                ->count();

            if ($check_dni > 0) {
                return response()->json([
                    'msg' => 'Ya esta registrada esta cedula',
                ], 422);
            }
        }

        $check_fiscal = Person::whereIn('user_id', $administrator_enterprises_ids)
            ->where('fiscal_identification', $request->fiscal_document_type . '-' . $request->fiscal_identification)
            ->count();

        if ($check_fiscal > 0) {
            return response()->json([
                'msg' => 'Ya esta registrado este RIF',
            ], 422);
        }

        $enterprise = User::where('id', $request->id)->first();
        $enterprise_person = Person::where('user_id', $request->id)->first();

        if (!$enterprise_person->code) {
            $enterprise_person->code = $this->generateEnterpriseCode($request->first_name);
        }

        $enterprise->name = $enterprise_person->code . '-' . str_slug($request->name);
        $enterprise->email = $request->email;

        if ($request->has('password')) {
            $enterprise->password = $request->password;
        }

        $enterprise->save();

        $enterprise_person->first_name = $request->first_name;
        $enterprise_person->phone = $request->phone;
        $enterprise_person->direction = $request->direction;

        // if($enterprise_person->code == null){
        //     $enterprise_person->code = $this->generateEnterpriseCode($request->first_name);
        // }

        $enterprise_person->fiscal_identification = $request->fiscal_document_type . '-' . $request->fiscal_identification;

        if ($request->has('identity_document')) {
            $enterprise_person->identity_document = $request->document_type . '-' . $request->identity_document;
        }

        if ($request->hasFile('image')) {
            File::delete($enterprise_person->avatar);
            $url = URL;
            $url .= $request->file('image')->storeAs('avatars', $this->quickRandom() . '.' . EXT, 'enterprises');
            $enterprise_person->avatar = $url;
        }

        $enterprise_person->save();

        if ($request->has('administrator_id')) {
            $enterprise_enterprise = AdministratorEnterprise::where('enterprise_id', $request->id)->first();
            $enterprise_enterprise->administrator_id = $request->administrator_id;
            $enterprise_enterprise->save();
        }

        $enterprise_config = ConfigEnterprise::where('enterprise_id', $request->id)->first();
        if ($enterprise_config != null) {
            $enterprise_config->sale_type = $request->sale_type;
            $enterprise_config->save();
        } else {
            $enterprise_config = new ConfigEnterprise;
            $enterprise_config->enterprise_id = $request->id;
            $enterprise_config->sale_type = $request->sale_type;
            $enterprise_config->creator_id = $request->creator_id;
            $enterprise_config->save();
        }

        return response()->json([
            'msg' => '¡Empresa editada exitosamente!',
            'result' => true,
        ]);
    }

    public function enterpriseDelete(Request $request)
    {
        if (isset($request->id)) {
            AdministratorEnterprise::where('enterprise_id', $request->id)->delete();
            Person::where('user_id', $request->id)->delete();
            User::where('id', $request->id)->delete();

            return response()->json([
                'msg' => '¡Empresa eliminada exitosamente!',
            ]);
        }

        return response()->json([
            'msg' => 'Ha ocurrido un error',
        ], 422);
    }

    public function enterpriseSuspend(Request $request)
    {
        if (isset($request->id)) {
            $enterprise = User::where('id', $request->id)->first();
            $enterprise->status = $enterprise->status == 1 ? 2 : 1;
            $enterprise->save();

            return response()->json([
                'msg' => $enterprise->status == 2
                ? '¡Empresa suspendida exitosamente!' : '¡Empresa activada exitosamente!',
            ]);

        } else {
            return response()->json([
                'msg' => 'Ha ocurrido un error',
            ], 422);
        }
    }

    public function enterpriseVerify(Request $request)
    {
        if (isset($request->id)) {
            $enterprise = User::find($request->id);
            $enterprise->status = 1;
            $enterprise->save();
            return response()->json([
                'msg' => 'Empresa verificada exitosamente',
            ]);
        } else {
            return response()->json([
                'msg' => 'Ha ocurrido un error',
            ], 422);
        }
    }

    public function getEnterpriseData(Request $request)
    {
        if (isset($request->id)) {
            $enterprise = User::where('id', $request->id)->with(['person', 'roles'])->first();
            $enterprise->role = $enterprise->roles[0]['id'];

            $modules = Module::all();
            return response()->json([
                'enterprise' => $enterprise,
                'modules' => $modules,
            ]);
        } else {
            return response()->json([
                'msg' => 'Ha ocurrido un error',
            ], 422);
        }
    }

    public function getAdministrators()
    {
        $administrators = User::where('status', 1)->whereHas('roles', function ($query) {
            $query->where('name', 'administrator')->orWhere('name', 'superadministrator');
        })->get();
        return $administrators;
    }

    private function attachmentModules($modules, $user)
    {
        foreach ($modules as $moduleId) {

            $user->enterprise_modules()->attach($moduleId);

            $module = Module::with([
                'permissions',
            ])->find($moduleId);

            $permissionsId = $module->permissions->pluck('id');

            $permissionsId->each(function ($permissionId, $key) use ($user) {
                $user->permissions_by_enterprise()->attach($permissionId, ['user_type' => 'App\User']);
            });
        }
    }

    private function quickRandom($length = 32)
    {
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
    }
}
