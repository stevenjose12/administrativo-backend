<?php

namespace App\Http\Controllers\Api\Admin\Zone;

use App\Http\Controllers\Controller;
use App\Models\Zone;
use Illuminate\Http\Request;

class ZoneController extends Controller
{
    public function zones(Request $request)
    {
        $zones = Zone::orderBy('name', 'ASC')->get();

        return $zones;
    }
}
