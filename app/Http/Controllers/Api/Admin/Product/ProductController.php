<?php

namespace App\Http\Controllers\Api\Admin\Product;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductBrand;
use App\Models\ProductCategory;
use App\Models\ProductModel;
use App\Models\ProductProvider;
use App\Models\ProductSubCategory;
use App\Models\ProductWarehouse;
use App\Models\Warehouse;
use App\Models\ProductWarehouseMovement;
use App\Models\ProductSerial as Serial;
use App\User;
use Excel;
use Illuminate\Http\Request;
use Image;
use DB;

define('SUPERADMINISTRATOR', 1);
define('ADMINISTRATOR', 2);
define('ENTERPRISE', 3);
define('SUBUSER', 4);

class ProductController extends Controller
{
    public function getAllProductsByEnterprise(Request $request)
    {
        $products_ids = Product::where([
            ['user_id', $request->user_id],
            ['status', 1],
        ])->get()->pluck('id');

        $products_warehouse = ProductWarehouse::whereIn('product_id', $products_ids);

        $products_warehouse->select('id', 'product_id', 'cost', 'warehouse_id', 'status', 'price', 'price_offer', 'price_max', 'stock_min', 'stock_max')
            ->with([
                'product' => function ($query) {
                    $query->select('id', 'name', 'code', 'type', 'exempt', 'serialization')->with(['product_details.product']);
                }, 'warehouse' => function ($query) {
                    $query->select('id', 'branch_id', 'name', 'code', 'deleted_at');
                },
                'last_movement'
            ]);

        $products = $products_warehouse->get();

        return $products;
    }

    public function searchProductByCode(Request $request)
    {
        $products = Product::with([
            'product_provider',
            'product_warehouse' => function ($query) use ($request) {
                $query->with('last_movement')->select('id', 'product_id', 'stock_min', 'warehouse_id', 'percentage_earning', 'percentage_max', 'percentage_offer', 'price', 'price_offer', 'price_max')
                    ->when($request->has('warehouse_id'), function ($query) use ($request) {
                        $query->where('warehouse_id', $request->warehouse_id);
                    });
            },
        ])
            ->whereHas('product_warehouse', function ($query) use ($request) {
                $query->where('warehouse_id', $request->warehouse_id)
                    ->where(function ($query2) {
                        $query2->whereNotNull('cost')->where('cost', '>', 0);
                    })
                    ->orWhere(function ($query2) {
                        $query2->whereNotNull('price')->where('price', '>', 0);
                    });
            })
            ->where([
                ['code', 'like', '%' . $request->search . '%'],
                ['user_id', $request->user_id],
            ])
            ->get();

        return $products;
    }

    public function searchProductByName(Request $request)
    {
        $products = Product::with([
            'product_provider',
            'product_warehouse' => function ($query) use ($request) {
                $query->with('last_movement')->select('id', 'product_id', 'stock_min', 'warehouse_id', 'percentage_earning', 'percentage_max', 'percentage_offer', 'price', 'price_offer', 'price_max')
                    ->when($request->has('warehouse_id'), function ($query) use ($request) {
                        $query->where('warehouse_id', $request->warehouse_id);
                    });
            },
        ])
            ->whereHas('product_warehouse', function ($query) use ($request) {
                $query->where('warehouse_id', $request->warehouse_id)
                    ->where(function ($query2) {
                        $query2->whereNotNull('cost')->where('cost', '>', 0);
                    })
                    ->orWhere(function ($query2) {
                        $query2->whereNotNull('price')->where('price', '>', 0);
                    });
            })
            ->where([
                ['name', 'like', '%' . $request->search . '%'],
                ['user_id', $request->user_id],
            ])
            ->get();

        return $products;
    }

    public function products(Request $request)
    {
        $products = Product::select(
            'id',
            'name',
            'code',
            'description',
            'percentage_commission',
            'exempt',
            'serialization',
            'avatar'
        )
            ->with([
                'product_details',
                'product_warehouse' => function ($query) {
                    $query->with(['warehouse', 'last_movement'])->select('id', 'warehouse_id', 'product_id');
                },
                'product_subcategory' => function ($query) {
                    $query->select('id', 'subcategory_id', 'product_id')->with(['subcategory' => function ($q) {
                        $q->select('id', 'category_id', 'name')->with(['category' => function ($categ) {
                            $categ->select('id', 'name');
                        }]);
                    }]);
                },
            ])
            ->with([
                'product_brand' => function ($product_brand) {
                    $product_brand->with('brand');
                }, 'product_model' => function ($product_model) {
                    $product_model->with('model');
                }, 'product_category' => function ($product_category) {
                    $product_category->with('category');
                },
            ])
            ->whereNull('products.deleted_at')
            ->when(count($request->warehouse_id) > 1, function ($query) use ($request) {
                $query->whereHas('product_warehouse', function ($query2) use ($request) {
                    $query2->whereIn('warehouse_id', $request->warehouse_id);
                });
            }, function ($query) use ($request) {
                $query->where('user_id', $request->user_id);
            })
            ->where('type', 1);

        if ($request->has('search')) {
            $products->where(function ($q) use ($request) {
                $q->where('products.name', 'like', '%' . $request->search . '%')
                    ->orWhere('products.code', 'like', '%' . $request->search . '%')
                    ->orWhere('products.description', 'like', '%' . $request->search . '%')
                    ->orWhereHas('product_category.category', function ($query) use ($request) {
                        $query->where('name', 'like', '%' . $request->search . '%');
                    })
                    ->orWhereHas('product_model.model.brand', function ($query) use ($request) {
                        $query->where('name', 'like', '%' . $request->search . '%');
                    });
            });
        }

        $response = $products->orderBy('id', 'desc')->paginate(10);

        return response()->json([
            'products' => $response,
            'result' => true,
        ]);
    }

    public function productDetails(Request $request)
    {
        $productWare = ProductWarehouse::select('id', 'warehouse_id', 'stock_min', 'stock_max', 'price', 'location', 'percentage_earning', 'percentage_max', 'percentage_offer')
            ->where('product_id', $request->id)
            ->get();

        $productProvider = ProductProvider::select('id', 'provider_id', 'product_id', 'price')->with(['provide' => function ($q) {
            $q->select('id', 'name')->withTrashed();
        }])
            ->where('product_id', $request->id)
            ->get();

        return response()->json([
            'product_warehouse' => $productWare,
            'product_provider' => $productProvider,
            'result' => true,
        ]);
    }

    public function productCategories(Request $request)
    {
        if ($request->role == 1) {
            $categories = Category::select('id', 'name', 'code', 'status');
        } else if ($request->role == 2) {
            $categories = Category::select('id', 'name', 'code', 'status')->where('creator_id', $request->user_id);
        } else {
            $categories = Category::select('id', 'name', 'code', 'status')->where('user_id', $request->user_id);
        }

        $response = $categories->with(['subcategories' => function ($query) {
            $query->where('status', 1);
        }])->where('status', 1)->get();

        return response()->json([
            'categories' => $response,
            'result' => true,
        ]);
    }

    public function warehousesGet(Request $request)
    {
        $warehouses = Warehouse::select('id', 'name')
            ->where('user_id', $request->user_id)
            ->where('status', 1)
            ->get();

        return response()->json([
            'result' => true,
            'warehouses' => $warehouses,
        ]);
    }

    public function companiesGet(Request $request)
    {
        return response()->json([
            'companies' => User::where('level', 3)->where('status', 1)->get(),
            'result' => true,
        ]);
    }

    public function productBrands(Request $request)
    {
        if ($request->role == 1) {
            $brands = Brand::select('id', 'name', 'code', 'status');
        } else if ($request->role == 2) {
            $brands = Brand::select('id', 'name', 'code', 'status')->where('creator_id', $request->user_id);
        } else {
            $brands = Brand::select('id', 'name', 'code', 'status')->where('user_id', $request->user_id);
        }

        $response = $brands->with(['models' => function ($query) {
            $query->where('status', 1);
        }])->where('status', 1)->get();

        return response()->json([
            'brands' => $response,
            'result' => true,
        ]);
    }

    public function lastCode(Request $request)
    {
        $producto = Product::select('id')->where('user_id', $request->user_id)
            ->orderBy('id', 'DESC');
        $prod = $producto->first();

        if ($producto->count() == 0) {
            $add = "0001";
        } else {
            $id = ++$prod->id;
            $quantyDigit = strlen($prod->id);

            for ($i = $quantyDigit; $i < 4; $i++) {
                $id = "0" . $id;
            }
            $add = $id;
        }

        return response()->json(['last' => $add]);
    }

    public function productCreate(ProductRequest $request)
    {
        /**
         * Product
         */
        $ware = json_decode($request->ware);
        $providers = json_decode($request->prov);
        $code = "";

        $code .= $request->code;
        $exist = Product::where('code', $code)->where('user_id', $request->user_id)->count();

        if ($exist > 0) {
            return response()->json([
                'msg' => 'Ya existe un producto con igual código',
                'result' => false,
            ]);
        }

        foreach ($providers as $prov) {
            if ($prov->provider_id != "" && $prov->price != "") {
                if (!is_numeric($prov->price)) {
                    return response()->json([
                        'msg' => 'El campo Costo del proveedor debe ser numérico',
                        'result' => false,
                    ]);
                }
            }
        }

        foreach ($ware as $w) {
            if ($w->stock_min != "" || $w->stock_min != null) {
                if (!is_numeric($w->stock_min)) {
                    return response()->json([
                        'msg' => 'El campo Stock mínimo de ' . $w->name . ' debe ser numérico',
                        'result' => false,
                    ]);
                }
                if (!is_numeric($w->stock_max)) {
                    return response()->json([
                        'msg' => 'El campo Stock máximo de ' . $w->name . ' debe ser numérico',
                        'result' => false,
                    ]);
                }
            }
        }

        $product = new Product;
        $product->user_id = $request->user_id;
        $product->code = $code;
        $product->name = $request->name;
        $product->description = $request->description;

        if ($request->hasFile('image')) {
            $name = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 40);
            Image::make($request->image)->save('img/products/' . $name . '.jpg');
            $product->avatar = 'img/products/' . $name . '.jpg';
        }

        // $product->percentage_earning = $request->percentage_earning && $request->percentage_earning != "null" ? $request->percentage_earning : 0;
        $product->percentage_commission = $request->percentage_commission && $request->percentage_commission != "null" ? $request->percentage_commission : 0;

        $product->exempt = $request->exempt == false ? 0 : 1;
        $product->serialization = $request->serialization == false ? 0 : 1;
        $product->creator_id = $request->user_id;
        $product->type = 1;
        $product->save();

        if ($request->has('subcategory')) {
            $productCategory = new ProductSubCategory;
            $productCategory->subcategory_id = $request->subcategory;
            $productCategory->product_id = $product->id;
            $productCategory->creator_id = $request->user_id;
            $productCategory->save();
        }

        if ($request->has('category')) {
            $productCategory = new ProductCategory;
            $productCategory->category_id = $request->category;
            $productCategory->product_id = $product->id;
            $productCategory->creator_id = $request->user_id;
            $productCategory->save();
        }

        if ($request->has('brand')) {
            $productBrand = new ProductBrand;
            $productBrand->brand_id = $request->brand;
            $productBrand->product_id = $product->id;
            $productBrand->creator_id = $request->user_id;
            $productBrand->save();
        }

        if ($request->has('model')) {
            $productCategory = new ProductModel;
            $productCategory->model_id = $request->model;
            $productCategory->product_id = $product->id;
            $productCategory->creator_id = $request->user_id;
            $productCategory->save();
        }

        foreach ($ware as $w) {
            if (
                $w->stock_min != "" || $w->stock_max != "" || $w->percentage_earning != ""
                || intval($w->percentage_max) > 0 || intval($w->percentage_offer) > 0 || intval($w->location) > 0
            ) {
                $productWare = new ProductWarehouse;
                $productWare->warehouse_id = $w->id;
                $productWare->product_id = $product->id;
                $productWare->stock_min = $w->stock_min ? $w->stock_min : null;
                $productWare->stock_max = $w->stock_max ? $w->stock_max : null;
                $productWare->percentage_earning = $w->percentage_earning ? $w->percentage_earning : 0;
                $productWare->percentage_max = $w->percentage_max ? $w->percentage_max : 0;
                $productWare->percentage_offer = $w->percentage_offer ? $w->percentage_offer : 0;
                $productWare->location = $w->location ? $w->location : null;
                $productWare->creator_id = $request->user_id;
                $productWare->save();
            }
        }

        foreach ($providers as $prov) {
            if ($prov->provider_id != "" && $prov->price != "") {
                $productProv = new ProductProvider;
                $productProv->provider_id = $prov->provider_id;
                $productProv->product_id = $product->id;
                $productProv->creator_id = $request->user_id;
                $productProv->price = $prov->price;
                $productProv->save();
            }
        }

        return response()->json([
            'msg' => 'Producto creado exitosamente',
            'result' => true,
        ]);
    }

    public function productEdit(ProductRequest $request)
    {
        $warehouse_id = json_decode($request->ware)[0]->id;
        
        $productWarehouse = ProductWarehouse::where('product_id',$request->id)
             ->where('warehouse_id', $warehouse_id)
             ->with(['last_movement', 'product'])
             ->first();
        
        $stock = 0;
        
        if($productWarehouse->product->serialization != $request->serialization) {
            if($productWarehouse->last_movement)
            {
                if($productWarehouse->last_movement->type == 1)
                {
                    $stock = $productWarehouse->last_movement->current_stock + $productWarehouse->last_movement->amount;
                }
                else
                {
                    $stock = $productWarehouse->last_movement->current_stock - $productWarehouse->last_movement->amount;
                }
            }

            if($stock > 0)
            {
                return response()->json(['msg' => 'Este producto tiene stock, para cambiar su estado de serialización debe descargar los productos'],422);
            }
        }


        $ware = json_decode($request->ware);
        $providers = json_decode($request->prov);

        foreach ($providers as $prov) {
            if ($prov->provider_id != "" && $prov->price != "") {
                if (!is_numeric($prov->price)) {
                    return response()->json([
                        'msg' => 'El campo Costo del proveedor debe ser numérico',
                        'result' => false,
                    ]);
                }
            }
        }


        foreach ($ware as $w) {
            if ($w->stock_min != "" || $w->stock_min != null) {
                if (!is_numeric($w->stock_min)) {
                    return response()->json([
                        'msg' => 'El campo Stock mínimo de ' . $w->name . ' debe ser numérico',
                        'result' => false,
                    ]);
                }
                if (!is_numeric($w->stock_max)) {
                    return response()->json([
                        'msg' => 'El campo Stock máximo de ' . $w->name . ' debe ser numérico',
                        'result' => false,
                    ]);
                }
            }
        }

        $product = Product::find($request->id);

        if ($request->edit_code) {
            $product->code = $request->code;
        }
        if ($request->edit_name) {
            $product->name = $request->name;
        }

        $product->description = !$request->description ? null : $request->description;

        if ($request->hasFile('image')) {
            $name = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 40);
            Image::make($request->image)->save('img/products/' . $name . '.jpg');
            $product->avatar = 'img/products/' . $name . '.jpg';
        }

        // $product->percentage_earning = $request->percentage_earning && $request->percentage_earning != "null" ? $request->percentage_earning : 0;
        $product->percentage_commission = $request->percentage_commission && $request->percentage_commission != "null" ? $request->percentage_commission : 0;

        $product->exempt = $request->exempt ? 1 : 0;
        $product->serialization = $request->serialization ? 1 : 0;
        $product->save();

        if ($request->has('subcategory')) {
            $productCateg = ProductSubCategory::where('product_id', $request->id);
            $productCategory = $productCateg->first();
            if ($productCateg->count() == 0) {
                $productCategory = new ProductSubCategory;
                $productCategory->product_id = $request->id;
                $productCategory->creator_id = $request->user_id;
            }
            $productCategory->subcategory_id = $request->subcategory;
            $productCategory->save();
        }

        if ($request->has('category')) {
            $productCateg = ProductCategory::where('product_id', $request->id);
            $productCategory = $productCateg->first();
            if ($productCateg->count() == 0) {
                $productCategory = new ProductCategory;
                $productCategory->product_id = $product->id;
                $productCategory->creator_id = $request->user_id;
            }
            $productCategory->category_id = $request->category;
            $productCategory->save();
        }

        if ($request->has('brand')) {
            $productBran = ProductBrand::where('product_id', $request->id);
            $productBrand = $productBran->first();
            if ($productBran->count() == 0) {
                $productBrand = new ProductBrand;
                $productBrand->product_id = $product->id;
                $productBrand->creator_id = $request->user_id;
            }
            $productBrand->brand_id = $request->brand;
            $productBrand->save();
        }

        if ($request->has('model')) {
            $productCateg = ProductModel::where('product_id', $request->id);
            $productCategory = $productCateg->first();
            if ($productCateg->count() == 0) {
                $productCategory = new ProductModel;
                $productCategory->product_id = $request->id;
                $productCategory->creator_id = $request->user_id;
            }
            $productCategory->model_id = $request->model;
            $productCategory->save();
        }

        foreach ($ware as $w) {
            if (
                $w->stock_min != null || $w->stock_max != null || $w->percentage_earning != null
                || intval($w->percentage_max) > 0 || intval($w->percentage_offer) > 0 || $w->location != null
            ) {
                $productQuery = ProductWarehouse::where('product_id', $request->id)
                    ->where('warehouse_id', $w->id);
                $productWare = $productQuery->first();
                if ($productQuery->count() == 0) {
                    $productWare = new ProductWarehouse;
                    $productWare->warehouse_id = $w->id;
                    $productWare->product_id = $request->id;
                    $productWare->creator_id = $request->user_id;
                    $productWare->price = 0;
                }
                $productWare->stock_min = $w->stock_min ? $w->stock_min : null;
                $productWare->stock_max = $w->stock_max ? $w->stock_max : null;
                $productWare->location = $w->location ? $w->location : null;
                $productWare->percentage_earning = $w->percentage_earning ? $w->percentage_earning : 0;
                $productWare->percentage_max = $w->percentage_max ? $w->percentage_max : 0;
                $productWare->percentage_offer = $w->percentage_offer ? $w->percentage_offer : 0;
                $productWare->price = ($productWare->cost * ($productWare->percentage_earning / 100)) + $productWare->cost;
                $productWare->price_offer = ($productWare->cost * ($productWare->percentage_offer / 100)) + $productWare->cost;
                $productWare->price_max = ($productWare->cost * ($productWare->percentage_max / 100)) + $productWare->cost;
                $productWare->save();
            }
        }

        ProductProvider::where('product_id', $request->id)->delete();

        foreach ($providers as $prov) {
            if ($prov->provider_id != "" && $prov->price != "") {
                $productProv = new ProductProvider;
                $productProv->provider_id = $prov->provider_id;
                $productProv->product_id = $product->id;
                $productProv->creator_id = $request->user_id;
                $productProv->price = $prov->price;
                $productProv->save();
            }
        }

        return response()->json([
            'msg' => 'Producto editado exitosamente',
            'result' => true,
        ]);
    }

    public function productDelete(Request $request)
    {
        /**
         * Verifica si el producto posee stock en cualquiera de sus almacenes
         */

        $productsWarehouse = ProductWarehouse::with('last_movement')
            ->where('product_id',$request->product_id)
            ->whereIn('warehouse_id',$request->warehouses)
            ->get()->toArray();

        $arrayStock = array_map(function($product) {
            $current = $product['last_movement'] ? $product['last_movement'] : 0;
            if($current){
                $current = $current['type'] == 1 ? $current['current_stock'] + $current['amount'] : $current['current_stock'] - $current['amount'];
            }
            if(!$current){
                return $product['id'];
            }
        },$productsWarehouse);
        
        if(count($arrayStock) == 0)
        {
            return response()->json(['msg' => 'El producto compuesto posee stock, no puede eliminarlo'],422);
        }
        
        /**
         * Verifica si un producto singular está asociado a un producto compuesto
         */
        $hasMaster = Product::withCount(['details_product'])->findOrFail($request->product_id, ['id'])->details_product_count;
        if($hasMaster > 0)
        {
            return response()->json([
                'msg' => '¡El producto esta asociado a productos compuestos en el almacén, no se puede eliminar!',
            ], 422);
        }

        ProductWarehouse::where('product_id', $request->product_id)->whereIn('warehouse_id', $request->warehouses)->delete();
        
        $product = Product::with(['product_warehouse'])->findOrFail($request->product_id, ['id']);


        if ($product->product_warehouse->count() == 0) {
            $product->delete();
        }

        return $product;

    }

    public function productExport($user_id)
    {
        $products = Product::where('user_id', $user_id)->get();
        $user = User::where('id', $user_id)->first();

        /*Excel::create('Productos-' . $user->name, function($excel) use($products, $request) {
        $excel->sheet('Listado', function($sheet) use($products, $request) {
        $sheet->loadView('reports.products.products')->with('products', $products);
        //    $sheet->setWidth('A', 60);
        //    $sheet->setWidth('B', 20);
        });
        })->store('xlsx', 'products')->export('xlsx');*/

        Excel::create('Productos-' . $user->name, function ($excel) use ($products) {
            $excel->setCreator('LimonByte')->setCompany('Adminstrativo');
            $excel->setDescription('Listado de productos');
            $excel->sheet('Listado', function ($sheet) use ($products) {
                $sheet->loadView('reports.products.products')->with('products', $products);
            });
        })->download('xlsx');
    }

    public function productExportDetail(Request $request)
    {
        $this->productExport($request->user_id);
    }

    public function getProductByCode(Request $request)
    {

        $product = Product::with([
            'product_provider',
            'product_warehouse' => function ($query) use ($request) {
                $query->with('last_movement')->select('id', 'product_id', 'stock_min', 'warehouse_id', 'percentage_earning', 'percentage_max', 'percentage_offer', 'price', 'price_offer', 'price_max')
                    ->when($request->has('warehouse_id'), function ($query) use ($request) {
                        $query->where('warehouse_id', $request->warehouse_id);
                    });
            },
        ])
            ->whereHas('product_warehouse', function ($query) use ($request) {
                $query->where('warehouse_id', $request->warehouse_id)
                    ->where(function ($query2) {
                        $query2->whereNotNull('cost')->where('cost', '>', 0);
                    })
                    ->orWhere(function ($query2) {
                        $query2->whereNotNull('price')->where('price', '>', 0);
                    });
            })
            ->where([
                ['code', $request->code],
                ['user_id', $request->user_id],
            ])
            ->first();

        if (!$product) {
            return response()->json([
                'msg' => 'Lo sentimos, no se ha encontrado un producto con este código',
            ], 422);
        }

        return $product;
    }
}
