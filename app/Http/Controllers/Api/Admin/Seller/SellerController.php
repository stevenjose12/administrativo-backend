<?php

namespace App\Http\Controllers\Api\Admin\Seller;

use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\ConfigSeller;
use App\Models\EnterpriseUser;
use App\Models\Person;
use App\Models\RoleSubuser;
use App\User;
use Illuminate\Http\Request;
use Image;
use Validator;

class SellerController extends Controller
{
    public function sellers_enterprise(Request $request)
    {
        $users = User::with('person')->where([
            ['level', config('constants.user.levels.SUB_USER')],
        ])
            ->whereHas('roles', function ($query) {
                $query->where('role_id', config('constants.user.levels.CLIENT'));
            })
            ->when($request->has('delivery'), function ($query) {
                $query->whereHas('seller', function($query2) {
                    $query2->where('type',config('constants.proccessed.types.DELIVERY_NOTE'));
                });
            })
            // ->whereHas('request')
            ->whereHas('enterprise_users', function ($query) use ($request) {
                $query->where('enterprise_id', $request->user_id);
            })->get();


        return $users;
    }

    public function sellersDeliveryNote(Request $request)
    {
        $users = User::with('person')
        ->whereHas('seller', function($query) {
            $query->where('type',config('constants.proccessed.types.DELIVERY_NOTE'));
        })
        ->whereHas('enterprise_users', function ($query) use ($request) {
            $query->where('enterprise_id', $request->user_id);
        })->get();

        $ownUser = User::with('person')
        ->whereHas('seller', function($query) {
            $query->where('type',config('constants.proccessed.types.DELIVERY_NOTE'));
        })
        ->where('id', $request->user_id)->get();

        $users = $users->merge($ownUser);


        return $users;
    }

    public function sellersRequests(Request $request)
    {
        $users = User::with('person')
        ->whereHas('requestSeller')
        ->whereHas('enterprise_users', function ($query) use ($request) {
            $query->where('enterprise_id', $request->user_id);
        })->get();

        $ownUser = User::with('person')
        ->whereHas('requestSeller')
        ->where('id', $request->user_id)->get();

        $users = $users->merge($ownUser);


        return $users;
    }

    public function sellers(Request $request)
    {        
        $users = User::with([
            'person',
            'modules_seller',
            'zones',
            'configuration_seller',
        ])
            ->whereHas('configuration_seller')
            ->whereHas('enterprise_users', function ($query) use ($request) {
                $query->where('enterprise_id', $request->id);
            })->whereHas('roles', function ($query) {
                $query->where('role_id',config('constants.user.roles.SELLER'));
            });

        if ($request->has('search')) {
            $users->where(function ($q) use ($request) {
                $q->where('email', 'like', '%' . $request->search . '%')
                    ->orWhere('name', 'like', '%' . $request->search . '%')
                    ->orWhereHas('person', function ($q) use ($request) {
                        $q->where('identity_document', 'like', '%' . $request->search . '%')
                            ->orWhere('first_name', 'like', '%' . $request->search . '%')
                            ->orWhere('last_name', 'like', '%' . $request->search . '%');
                    });
            });
        }

        if ($request->has('status')) {
            $users->where('status', $request->status);
        }

        $query = $users->orderBy('id', 'desc')->paginate(10);

        return $query;
    }

    public function create(Request $request)
    {
        $this->ValidatorSeller($request->all())->validate();

        if (count(json_decode($request->modules)) <= 0) {
            return response()->json([
                'message' => '¡Debe agregar al vendedor al menos un modulo!',
            ], 422);
        }

        if (count(json_decode($request->zones)) <= 0) {
            return response()->json([
                'message' => '¡Debe agregar al vendedor al menos una zona!',
            ], 422);
        }

        $me = User::with([
            'person' => function ($query) {
                $query->withTrashed();
            }
        ])->withTrashed()->findOrFail($request->creator_id);

        $name_validate = User::where([
            ['name', $me->person->code . '-' . studly_case($request->name)],
            ['level', 4],
        ])->whereNull('deleted_at')->count();

        if ($name_validate > 0) {
            return response()->json([
                'message' => '¡El nombre de usuario ya esta siendo usado por otra persona!',
            ], 422);
        }

        $document_validate = Person::where('identity_document', $request->document_type . '-' . $request->document)->whereHas('user', function ($query) {
            $query->where('level', 4);
        })->whereNull('deleted_at')->count();

        if ($document_validate > 0) {
            return response()->json([
                'message' => '¡La cédula ya esta siendo usada por otra persona!',
            ], 422);
        }

        $email_validate = User::where([
            ['email', $request->email],
            ['level', 4],
        ])->whereNull('deleted_at')->count();

        if ($email_validate > 0) {
            return response()->json([
                'message' => '¡La dirección de correo electrónico ya esta siendo usado por otra persona!',
            ], 422);
        }

        $user = new User;
        $user->name = $me->person->code . '-' . studly_case($request->name);
        $user->email = $request->email;
        $user->password = $request->password;
        $user->status = 1;
        $user->level = $request->level;
        $user->save();

        // $roleEnterprise = new RoleSubuser;
        // $roleEnterprise->role_enterprise_id = $request->role;
        // $roleEnterprise->enterprise_id = $me->id;
        // $roleEnterprise->user_id = $user->id;
        // $roleEnterprise->save();

        $enterpriseUser = new EnterpriseUser;
        $enterpriseUser->enterprise_id = $me->id;
        $enterpriseUser->user_id = $user->id;
        $enterpriseUser->save();

        $user->attachRole(5);

        $person = new Person;
        $person->first_name = $request->first_name;
        $person->phone = $request->phone;
        $person->identity_document = $request->document_type . '-' . $request->document;

        if ($request->hasFile('image')) {
            $name = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 40);
            Image::make($request->image)->save('img/users/' . $name . '.jpg');
            $person->avatar = 'img/users/' . $name . '.jpg';
        }

        $person->user_id = $user->id;
        $person->save();

        $config = new ConfigSeller;
        $config->seller_id = $user->id;
        $config->price_offer = $request->price_offer;
        $config->price_max = $request->price_max;
        $config->type_percentage = (int) $request->type_percentage;
        $config->percentage = $request->percentage;
        $config->save();

        if ($request->has('branch_id')) {

            $user->warehouses()->detach();

            $branch_id = (int) $request->branch_id;
            $warehouse_id = (int) $request->warehouse_id;

            $branches = Branch::with([
                'warehouses' => function ($query) use ($warehouse_id) {
                    $query->when($warehouse_id > 0, function ($query) use ($warehouse_id) {
                        return $query->where('id', $warehouse_id)->where('status', 1)->select('id', 'branch_id');
                    }, function ($query) {
                        return $query->where('status', 1)->select('id', 'branch_id');
                    });
                }
            ])
                ->when($branch_id > 0, function ($query) use ($branch_id) {
                    return $query->where('id', $branch_id);
                }, function ($query) use ($request) {
                    return $query->where('enterprise_id', $request->creator_id);
                })
                ->where('status', 1)
                ->get();

            foreach ($branches as $branch) {
                foreach ($branch->warehouses as $warehouse) {
                    $user->warehouses()->attach($user->id, [
                        'branch_id' => $branch->id,
                        'warehouse_id' => $warehouse->id,
                    ]);
                }
            }
        }

        if ($request->has('modules')) {
            $user->modules_seller()->sync(json_decode($request->modules));
        }

        if ($request->has('zones')) {

            $zones = json_decode($request->zones);

            foreach ($zones as $key => $zone) {
                $user->zones()->attach($zone->value, [
                    'creator_id' => $me->id,
                ]);
            }
        }

        return $user;
    }

    public function update(Request $request)
    {
        $this->ValidatorSellerUpdate($request->all())->validate();

        $me = User::with('person')->findOrFail($request->creator_id);

        $name_validate = User::where([
            ['id', '!=', $request->id],
            ['name', $me->person->code . '-' . studly_case($request->name)],
            ['level', 4],
        ])->whereNull('deleted_at')->count();

        if ($name_validate > 0) {
            return response()->json([
                'message' => '¡El nombre de usuario ya esta siendo usado por otra persona!',
            ], 422);
        }

        $document_validate = Person::where([
            ['user_id', '!=', $request->id],
            ['identity_document', $request->document_type . '-' . $request->document],
        ])->whereHas('user', function ($query) {
            $query->where('level', 4);
        })->whereNull('deleted_at')->count();

        if ($document_validate > 0) {
            return response()->json([
                'message' => '¡La cédula ya esta siendo usado por otra persona!',
            ]);
        }

        $email_validate = User::where([
            ['id', '!=', $request->id],
            ['email', $request->email],
            ['level', 4],
        ])->whereNull('deleted_at')->count();

        if ($email_validate > 0) {
            return response()->json([
                'message' => '¡La dirección de correo electrónico ya esta siendo usado por otra persona!',
            ]);
        }

        if ($request->password || $request->password_confimation) {
            $this->ValidatorPassword($request->all())->validate();
        }

        $user = User::findOrFail($request->id);
        $user->name = $me->person->code . '-' . studly_case($request->name);
        $user->email = $request->email;
        $user->level = $request->level;

        if (!empty($request->password)) {
            $user->password = $request->password;
        }

        $user->save();

        $person = Person::where('user_id', $request->id)->first();
        $person->first_name = $request->first_name;
        $person->identity_document = $request->document_type . '-' . $request->document;

        if ($request->hasFile('image')) {
            $name = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 40);
            Image::make($request->image)->save('img/users/' . $name . '.jpg');
            $person->avatar = 'img/users/' . $name . '.jpg';
        }

        $person->phone = $request->phone;
        $person->save();

        $configuration = ConfigSeller::where('seller_id', $request->id)->first();

        $config = $configuration ? $configuration : new ConfigSeller;
        $config->price_offer = $request->price_offer;
        $config->price_max = $request->price_max;
        $config->type_percentage = (int) $request->type_percentage;
        $config->percentage = $request->percentage;
        $config->save();

        if ($request->has('branch_id')) {

            $user->warehouses()->detach();

            $branch_id = (int) $request->branch_id;
            $warehouse_id = (int) $request->warehouse_id;

            $branches = Branch::with([
                'warehouses' => function ($query) use ($warehouse_id) {
                    $query->when($warehouse_id > 0, function ($query) use ($warehouse_id) {
                        return $query->where('id', $warehouse_id)->where('status', 1)->select('id', 'branch_id');
                    }, function ($query) {
                        return $query->where('status', 1)->select('id', 'branch_id');
                    });
                }
            ])
                ->when($branch_id > 0, function ($query) use ($branch_id) {
                    return $query->where('id', $branch_id);
                }, function ($query) use ($request) {
                    return $query->where('enterprise_id', $request->creator_id);
                })
                ->where('status', 1)
                ->get();

            foreach ($branches as $branch) {
                foreach ($branch->warehouses as $warehouse) {
                    $user->warehouses()->attach($user->id, [
                        'branch_id' => $branch->id,
                        'warehouse_id' => $warehouse->id,
                    ]);
                }
            }
        }

        if ($request->has('modules')) {
            $user->modules_seller()->detach();
            $user->modules_seller()->sync(json_decode($request->modules));
        }

        if ($request->has('zones')) {
            $user->zones()->detach();

            $zones = json_decode($request->zones);

            foreach ($zones as $key => $zone) {
                $user->zones()->attach($zone->value, [
                    'creator_id' => $me->id,
                ]);
            }
        }

        return $user;
    }

    protected function ValidatorPassword(array $data)
    {
        $attributes = [
            'password' => 'contraseña',
            'password_confirmation' => "confirmar contraseña",
        ];

        $validator = Validator::make($data, [
            'password' => ['string', 'min:6', 'required_with:password_confirmation', 'same:password_confirmation'],
            'password_confirmation' => ['required', 'string', 'min:6'],
        ], [
            "required_unless" => "El campo :attribute es obligatorio.",
            "required_if" => "El campo :attribute es obligatorio.",
            "confirmed" => 'Las contaseñas no coinciden',
            "min" => "El tamaño de :attribute debe ser de al menos :min.",
        ]);

        $validator->setAttributeNames($attributes);

        return $validator;
    }

    protected function ValidatorSeller(array $data)
    {
        $attributes = [
            'name' => 'nombre de usuario',
            'first_name' => 'nombre',
            'last_name' => 'apellido',
            'company' => 'empresa',
            'email' => 'correo electrónico',
            'phone' => 'teléfono',
            'document' => 'cédula',
            'password' => 'contraseña',
            'password_confirmation' => "confirmar contraseña",
            'level' => 'tipo de usuario',
            'percentage' => 'margen de descuento',
            'branch_id' => 'sucursales',
            'warehouse_id' => 'almacen',
        ];

        $validator = Validator::make($data, [
            'name' => ['required'],
            'document' => ['required', 'string', 'between:7,11'],
            'email' => ['required', 'email', 'unique:users,email,NULL,id,deleted_at,null'],
            'first_name' => ['required', 'string', 'max:50'],
            'password' => ['string', 'min:6', 'required_with:password_confirmation', 'same:password_confirmation'],
            'password_confirmation' => ['required', 'string', 'min:6'],
            'phone' => ['required', 'string', 'between:7,11'],
            'percentage' => ['nullable', 'numeric', 'between:0,100'],
            'branch_id' => ['required'],
            'warehouse_id' => ['required'],
        ], [
            "required_unless" => "El campo :attribute es obligatorio.",
            "required_if" => "El campo :attribute es obligatorio.",
            "confirmed" => 'Las contaseñas no coinciden',
            'between' => ':attribute tiene que estar entre :min - :max.',
        ]);

        $validator->setAttributeNames($attributes);

        return $validator;
    }

    protected function ValidatorSellerUpdate(array $data)
    {
        $attributes = [
            'name' => 'nombre de usuario',
            'first_name' => 'nombre',
            'last_name' => 'apellido',
            'company' => 'empresa',
            'email' => 'correo electrónico',
            'phone' => 'teléfono',
            'document' => 'cédula',
            'level' => 'tipo de usuario',
            'percentage' => 'margen de descuento',
        ];

        $validator = Validator::make($data, [
            'name' => ['required'],
            'document' => ['required', 'string', 'between:7,11'],
            'email' => ['required', 'email', 'unique:users,email,NULL,id,deleted_at,null'],
            'first_name' => ['required', 'string', 'max:50'],
            'phone' => ['required', 'string', 'between:7,11'],
            'percentage' => ['nullable', 'numeric', 'between:0,100'],
        ], [
            "required_unless" => "El campo :attribute es obligatorio.",
            "required_if" => "El campo :attribute es obligatorio.",
            "confirmed" => 'Las contaseñas no coinciden',
            'between' => ':attribute tiene que estar entre :min - :max.',
        ]);

        $validator->setAttributeNames($attributes);

        return $validator;
    }
}
