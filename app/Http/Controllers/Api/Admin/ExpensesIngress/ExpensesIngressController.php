<?php

namespace App\Http\Controllers\Api\Admin\ExpensesIngress;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ExpensesIngressRequest;
use App\Models\ExpensesIngress;
use App\Models\ExpensesIngressDetail;
use App\Models\ExpensesIngressPayment;
use App\Models\BankBalance;
use Carbon\Carbon;
use App\Http\Traits\Format;
use Illuminate\Support\Facades\DB;
use App\Support\Collection;
use App\User;

class ExpensesIngressController extends Controller
{
    use Format;

    public function generateId(Request $request)
    {
        $count = ExpensesIngress::where('enterprise_id', $request->enterprise_id)->count();

        $Id = str_pad(1, 9, 0, STR_PAD_LEFT);

        if ($count > 0) {
            $Id = str_pad($count + 1, 9, 0, STR_PAD_LEFT);
        }

        return $Id;
    }

    public function index(Request $request)
    {
        $expensesIngress = ExpensesIngress::with(['enterprise',
        'provider' => function ($query) {
            $query->with('person')->withTrashed();
        }, 'creator' => function ($query) {
            $query->with('person')->withTrashed();
        }, 'details',
         ])
        ->where('enterprise_id', $request->enterprise_id)
        ->when($request->has('provider') && $request->provider > 0, function ($query) use ($request) {
            $query->where('provider_id', $request->provider);
        })
        ->when($request->has('since') && $request->has('until'), function ($query) use ($request) {
            $query->whereBetween('date_emission', [$this->parseFormatDateTime($request->since, 'start'), $this->parseFormatDateTime($request->until, 'end')]);
        })
        ->when($request->has('since') && !$request->has('until'), function ($query) use ($request) {
            $query->whereDate('date_emission', '>=', $this->parseFormat($request->since));
        })
        ->when(!$request->has('since') && $request->has('until'), function ($query) use ($request) {
            $query->whereDate('date_emission', '<=', $this->parseFormat($request->until));
        })
        ->when($request->has('status'), function ($query) use ($request) {
            $query->where('status', $request->status);
        })
        ->when($request->has('payment_type') && $request->payment_type > 0, function ($query) use ($request) {
            $query->where('payment_type', $request->payment_type);
        })
        ->paginate(10);
        
        return $expensesIngress;
    }

    public function store(ExpensesIngressRequest $request)
    {
        // return $request->all();
        $checkControlNumber = DB::table('expenses_ingress')
            ->where('provider_id', (Int) $request->provider_id)
            ->where('control_number', $request->control_number)->exists();
        if ($checkControlNumber) {
            return response()->json([
                'msg' => "Ya existe este número de control para este proveedor"
            ], 422);
        }
        
        $check = ExpensesIngress::where('provider_id', $request->provider_id)
            ->where('bill_number', $request->bill_number)
            ->where('control_number', $request->control_number)
            ->exists();


        if ($check) {
            return response()->json([
                'msg' => 'Ya existe una factura con ese numero de factura para este proveedor!',
            ], 422);
        }

        $current_bank_balance = BankBalance::where('bank_account_id', $request->bank_account_id)->orderBy('id', 'desc');
        if ($current_bank_balance->exists()) {
            $current_bank_balance = $current_bank_balance->first()->total;
        } else {
            $current_bank_balance = 0;
        }

        if ($request->payment_type == config('constants.expenses_ingress.payment_types.CASH')) {
            if ($request->total > $current_bank_balance) {
                return response()->json([
                    'msg' => 'El monto de la factura no puede sobrepasar el saldo de la cuenta!',
                ], 422);
            }
        }

        $expensesIngress = new ExpensesIngress;
        $expensesIngress->enterprise_id = $request->enterprise_id;
        $expensesIngress->provider_id = $request->provider_id;
        $expensesIngress->date_emission = $request->date_emission_format;
        $expensesIngress->date_received = $request->date_received_format;
        $expensesIngress->date_expired = $request->date_expired_format;
        $expensesIngress->bill_number = $request->bill_number;
        $expensesIngress->control_number = $request->control_number;
        $expensesIngress->payment_type = $request->payment_type;
        $expensesIngress->description = $request->description;
        $expensesIngress->subtotal = $request->subtotal;
        if ($request->payment_type == config('constants.expenses_ingress.payment_types.CASH')) {
            $expensesIngress->status = config('constants.expenses_ingress.status.PROCESSED');
        }
        $expensesIngress->iva = $request->vat;
        $expensesIngress->total = $request->total;
        $expensesIngress->creator_id = $request->creator_id;
        $expensesIngress->save();

        foreach ($request->products as $key => $product) {
            $expensesIngressDetail = new ExpensesIngressDetail;
            $expensesIngressDetail->expenses_ingress_id = $expensesIngress->id;
            $expensesIngressDetail->description = $product['description'];
            $expensesIngressDetail->price = $product['price'];
            $expensesIngressDetail->amount = $product['amount'];
            $expensesIngressDetail->exempt = $product['exempt'];
            $expensesIngressDetail->subtotal = $product['subtotal'];
            $expensesIngressDetail->vat = $product['vat'];
            $expensesIngressDetail->total = $product['total'];
            $expensesIngressDetail->save();
        }

        if ($expensesIngress->payment_type == config('constants.expenses_ingress.payment_types.CASH')) {
            $expensesIngressPayment = new ExpensesIngressPayment;
            $expensesIngressPayment->expenses_ingress_id = $expensesIngress->id;
            if ($request->has('bank_account_id')) {
                $expensesIngressPayment->bank_account_id = $request->bank_account_id;
            }
            $expensesIngressPayment->payment_type = $request->method_type;
            $expensesIngressPayment->amount = $expensesIngress->total;
            $expensesIngressPayment->expenses_ingress_class = 2; //Ingreso de factura
                $expensesIngressPayment->expenses_ingress_type = 'App\Models\ExpensesIngress'; //Ingreso de factura
                $expensesIngressPayment->date = Carbon::now();
            $expensesIngressPayment->status = config('constants.expenses_ingress.status.PROCESSED');
            $expensesIngressPayment->creator_id = $request->creator_id;
            $expensesIngressPayment->save();

            $bank_balance = new BankBalance;
            $bank_balance->bank_id = $request->bank_id;
            $bank_balance->bank_account_id = $request->bank_account_id;
            $bank_balance->action_id = $expensesIngressPayment->id;
            $bank_balance->action_class = config()->get('constants.bank.type.EXPENSES_PAYMENT_EGRESSS');
            $bank_balance->action_type = config()->get('constants.bank.type.APP_EXPENSE_PAYMENT');
            $bank_balance->type = config()->get('constants.bank.type.OUTPUT');
            $bank_balance->amount = $expensesIngress->total;
            $bank_balance->current_amount = $current_bank_balance;
            $bank_balance->total = $current_bank_balance - $expensesIngress->total;
            $bank_balance->description = "Egreso por pago de factura";
            $bank_balance->creator_id = $request->creator_id;
            $bank_balance->operation_number = $request->reference_number;
            $bank_balance->save();
        }

        return $expensesIngress;
    }

    public function paymentReport(Request $request)
    {
        $expensesIngressIds = ExpensesIngress::where('enterprise_id', $request->enterprise_id)
        ->when($request->has('provider') && $request->provider > 0, function ($query) use ($request) {
            $query->where('provider_id', $request->provider);
        })
        ->when($request->has('role') != config('constants.user.roles.ENTERPRISE'), function ($query) use ($request) {
            $query-where('creator_id', $request->user_id);
        })->get()->pluck('id');
        
        $expensesIngressPayments = ExpensesIngressPayment::with(['expenses_ingress','creator'])
        ->whereIn('expenses_ingress_class', [1,2])
        ->when($request->has('since') && $request->has('until'), function ($query) use ($request) {
            $query->whereBetween('date', [$this->parseFormatDateTime($request->since, 'start'), $this->parseFormatDateTime($request->until, 'end')]);
        })
        ->when($request->has('since') && !$request->has('until'), function ($query) use ($request) {
            $query->whereDate('date', '>=', $this->parseFormat($request->since));
        })
        ->when(!$request->has('since') && $request->has('until'), function ($query) use ($request) {
            $query->whereDate('date', '<=', $this->parseFormat($request->until));
        })
        ->when($request->has('payment_type') && $request->payment_type > 0, function ($query) use ($request) {
            $query->where('payment_type', $request->payment_type);
        })
        ->orderBy('created_at', 'desc')->get();

        foreach ($expensesIngressPayments as $payment) {
            $provider = User::with('person')->find($payment->expenses_ingress['provider_id']);
            $payment->provider = $provider;
        }
        return (new Collection($expensesIngressPayments))->paginate(10);
    }
}
