<?php

namespace App\Http\Controllers\Api\Admin\ProductCompound;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductCompoundRequest;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductBrand;
use App\Models\ProductCategory;
use App\Models\ProductDetails;
use App\Models\ProductModel;
use App\Models\ProductSubCategory;
use App\Models\ProductWarehouse;
use App\Models\Warehouse;
use App\User;
use Illuminate\Http\Request;
use Image;
use App\Models\ProductSerial as Serial;

class ProductCompoundController extends Controller
{
    public function products(Request $request)
    {
        $products = Product::select(
            'id',
            'name',
            'code',
            'description',
            'percentage_earning',
            'percentage_commission',
            'exempt',
            'serialization',
            'avatar'
        )
            ->with([
                'product_subcategory' => function ($query) {
                    $query->select('id', 'subcategory_id', 'product_id')->with([
                        'subcategory' => function ($q) {
                            $q->select('id', 'category_id', 'name')->with([
                                'category' => function ($categ) {
                                    $categ->select('id', 'name');
                                },
                            ]);
                        },
                    ]);
                },
                'product_warehouse' => function ($query) {
                    $query->with(['warehouse','last_movement']);
                },
            ])
            ->with([
                'product_brand' => function ($product_brand) {
                    $product_brand->with('brand');
                }, 'product_model' => function ($product_model) {
                    $product_model->with('model');
                }, 'product_category' => function ($product_category) {
                    $product_category->with('category');
                }, 'product_details' => function ($product_details) {
                    $product_details->with(['product' => function ($product) {
                        $product->select('id', 'code', 'name', 'description', 'serialization')->withTrashed();
                    }]);
                },
            ])
            ->where('type', 2)
            ->whereNull('products.deleted_at')
            ->when(count($request->warehouse_id) > 1, function ($query) use ($request) {
                $query->whereHas('product_warehouse', function ($query2) use ($request) {
                    $query2->whereIn('warehouse_id', $request->warehouse_id);
                });
            }, function ($query) use ($request) {
                $query->where('user_id', $request->user_id);
            });

        if ($request->has('search')) {
            $products->where(function ($q) use ($request) {
                $q->where('products.name', 'like', '%' . $request->search . '%')
                    ->orWhere('products.code', 'like', '%' . $request->search . '%')
                    ->orWhere('products.description', 'like', '%' . $request->search . '%')
                    ->orWhereHas('product_category.category', function ($query) use ($request) {
                        $query->where('name', 'like', '%' . $request->search . '%');
                    })
                    ->orWhereHas('product_model.model.brand', function ($query) use ($request) {
                        $query->where('name', 'like', '%' . $request->search . '%');
                    });
            });
        }

        $response = $products->paginate(10);

        return response()->json([
            'products' => $response,
            'result' => true,
        ]);
    }

    public function productDetails(Request $request)
    {
        $productWare = ProductWarehouse::select('id', 'warehouse_id', 'stock_min', 'stock_max', 'price', 'location', '')->where('product_id', $request->id)->get();

        $productModel = ProductModel::select('id', 'model_id', 'product_id')
            ->with(['model' => function ($q) {
                $q->select('id', 'brand_id', 'name');
            }])
            ->where('product_id', $request->id)
            ->first();

        return response()->json([
            'product_warehouse' => $productWare,
            'product_model' => $productModel,
            'result' => true,
        ]);
    }

    public function getProductsFromPurchase(Request $request)
    {
        $products = Product::with([
            'product_provider',
            'product_warehouse' => function ($query) use ($request) {
                $query->select('product_id', 'warehouse_id', 'percentage_earning', 'percentage_max', 'percentage_offer')
                    ->when($request->has('warehouse_id'), function ($query) use ($request) {
                        $query->where('warehouse_id', $request->warehouse_id);
                    });
            },
        ])
            ->where([
                ['name', 'like', '%' . $request->search . '%'],
                ['user_id', $request->user_id],
                ['type', 1],
            ])
            ->get();

        return $products;
    }

    public function productSearch(Request $request)
    {
        $search_name = explode(' ', $request->search);

        $products = Product::with([
            'product_warehouse',
        ])->select('id', 'code', 'name', 'description', 'serialization')
            ->where(function ($query) use ($search_name) {
                foreach ($search_name as $search) {
                    $query->orWhere('name', 'like', '%' . $search . '%');
                }
            })->where([
            ['user_id', $request->user_id],
            ['type', 1],
        ])
        ->where('serialization', 0)
        ->whereHas('product_warehouse', function ($query) use ($request) {
            $query->whereIn('warehouse_id', $request->warehouses_id);
        })
        ->get();

        return $products;
    }

    public function productCategories(Request $request)
    {
        if ($request->role == 1) {
            $categories = Category::select('id', 'name');
        } elseif ($request->role == 2) {
            $categories = Category::select('id', 'name')->where('creator_id', $request->user_id);
        } else {
            $categories = Category::select('id', 'name')->where('user_id', $request->user_id);
        }

        $response = $categories->with('subcategories')->where('status', 1)->get();

        return response()->json([
            'categories' => $response,
            'result' => true,
        ]);
    }

    public function warehousesGet(Request $request)
    {
        $warehouses = Warehouse::select('id', 'name')
            ->where('user_id', $request->user_id)
            ->where('status', 1)
            ->get();

        return response()->json([
            'result' => true,
            'warehouses' => $warehouses,
        ]);
    }

    public function companiesGet(Request $request)
    {
        return response()->json([
            'companies' => User::where('level', 3)->where('status', 1)->get(),
            'result' => true,
        ]);
    }

    public function productBrands(Request $request)
    {
        if ($request->role == 1) {
            $brands = Brand::select('id', 'name');
        } elseif ($request->role == 2) {
            $brands = Brand::select('id', 'name')->where('creator_id', $request->user_id);
        } else {
            $brands = Brand::select('id', 'name')->where('user_id', $request->user_id);
        }

        $response = $brands->with('models')->where('status', 1)->get();

        return response()->json([
            'brands' => $response,
            'result' => true,
        ]);
    }

    protected function ValidateStockExistence($warehouses, $products)
    {
        foreach ($warehouses as $key => $warehouse) {
            foreach ($products as $product) {
                $product_warehouse = ProductWarehouse::where([
                    ['product_id', $product->id],
                    ['warehouse_id', $warehouse->id],
                ]);

                if (!$product_warehouse->first()) {
                    return [
                        'msg' => "¡El producto $product->name no se encuentra en $warehouse->name!",
                    ];
                }
            }
        }
    }

    protected function constAmount($warehouse, $products)
    {
        $costAmount = 0;

        foreach ($products as $product) {
            $product_warehouse = ProductWarehouse::where([
                ['product_id', $product->id],
                ['warehouse_id', $warehouse->id],
            ])->first();

            $costAmount += $product_warehouse->cost * $product->quanty;
        }

        $costAmount = $costAmount;

        return $costAmount;
    }

    public function productCreate(ProductCompoundRequest $request)
    {
        $ware = json_decode($request->ware);
        $prod = json_decode($request->prods);

        $productsSerialized = array_filter($prod, function ($product) {
            return $product->serialization == 1;
        });

        if ($validator = $this->ValidateStockExistence($ware, $prod)) {
            return response()->json($validator, 422);
        }

        if (count($prod) == 0) {
            return response()->json([
                'msg' => '¡Debe cargar al menos un producto para crear un producto compuesto o empaquetado!',
                'result' => false,
            ], 422);
        }

        for ($i = 0; $i < count($prod); $i++) {
            if (!is_numeric($prod[$i]->quanty)) {
                return response()->json([
                    'msg' => '¡El campo precio del producto compuesto debe ser numérico!',
                    'result' => false,
                ]);
            }
        }

        foreach ($ware as $w) {
            if ($w->stock_min != "" || $w->stock_min != null) {
                if (!is_numeric($w->stock_min)) {
                    return response()->json([
                        'msg' => '¡El campo stock mínimo ' . $w->name . ' debe ser numérico!',
                        'result' => false,
                    ]);
                }
                if (!is_numeric($w->stock_max)) {
                    return response()->json([
                        'msg' => '¡El campo stock máximo ' . $w->name . ' debe ser numérico!',
                        'result' => false,
                    ]);
                }
            }
        }

        $producto = Product::select('id')->where('user_id', $request->user_id)
            ->orderBy('id', 'DESC');

        $produ = $producto->first();

        if ($producto->count() == 0) {
            $add = "0001";
        } else {
            $id = ++$produ->id;
            $quantyDigit = strlen($produ->id);

            for ($i = $quantyDigit; $i < 4; $i++) {
                $id = "0" . $id;
            }

            $add = $id;
        }

        $code = $request->category . $request->subcategory;
        $code .= $request->brand . $request->model;
        $code .= $add;

        $product = new Product;
        $product->user_id = $request->user_id;
        $product->code = $code;
        $product->name = $request->name;
        $product->description = $request->description;

        if ($request->hasFile('image')) {
            $name = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 40);
            Image::make($request->image)->save('img/products/' . $name . '.jpg');
            $product->avatar = 'img/products/' . $name . '.jpg';
        }

        $product->percentage_earning = $request->percentage_earning && $request->percentage_earning != "null" ? $request->percentage_earning : 0;
        $product->percentage_commission = $request->percentage_commission && $request->percentage_commission != "null" ? $request->percentage_commission : 0;

        $product->exempt = $request->exempt == false ? 0 : 1;
        $product->serialization = $request->serialization == false ? 0 : 1;
        $product->creator_id = $request->user_id;
        $product->type = 2;
        $product->save();

        if ($request->has('subcategory')) {
            $productCategory = new ProductSubCategory;
            $productCategory->subcategory_id = $request->subcategory;
            $productCategory->product_id = $product->id;
            $productCategory->creator_id = $request->user_id;
            $productCategory->save();
        }

        if ($request->has('category')) {
            $productCategory = new ProductCategory;
            $productCategory->category_id = $request->category;
            $productCategory->product_id = $product->id;
            $productCategory->creator_id = $request->user_id;
            $productCategory->save();
        }

        if ($request->has('brand')) {
            $productBrand = new ProductBrand;
            $productBrand->brand_id = $request->brand;
            $productBrand->product_id = $product->id;
            $productBrand->creator_id = $request->user_id;
            $productBrand->save();
        }

        if ($request->has('model')) {
            $productCategory = new ProductModel;
            $productCategory->model_id = $request->model;
            $productCategory->product_id = $product->id;
            $productCategory->creator_id = $request->user_id;
            $productCategory->save();
        }

        for ($i = 0; $i < count($prod); $i++) {
            $productDetail = new ProductDetails;
            $productDetail->compound_id = $product->id;
            $productDetail->product_id = $prod[$i]->id;
            $productDetail->amount = $prod[$i]->quanty;
            $productDetail->serialization = $prod[$i]->serialization;
            $productDetail->save();
        }

        foreach ($ware as $w) {
            if ($w->stock_min != "" || $w->stock_max != "" || $w->percentage_earning != ""
                || $w->percentage_max != "" || $w->percentage_offer != "" || $w->location != "") {
                $costAmount = $this->constAmount($w, $prod);
                $productWare = new ProductWarehouse;
                $productWare->warehouse_id = $w->id;
                $productWare->product_id = $product->id;
                $productWare->percentage_earning = $w->percentage_earning ? $w->percentage_earning : 0;
                $productWare->percentage_max = $w->percentage_max ? $w->percentage_max : 0;
                $productWare->percentage_offer = $w->percentage_offer ? $w->percentage_offer : 0;
                $productWare->stock_min = $w->stock_min ? $w->stock_min : null;
                $productWare->stock_max = $w->stock_max ? $w->stock_max : null;
                $productWare->price = 0;
                $productWare->cost = $costAmount;
                $productWare->location = $w->location;
                $productWare->creator_id = $request->user_id;
                $productWare->save();
            }
        }

        return response()->json([
            'msg' => '¡Producto creado exitosamente!',
            'result' => true,
        ]);
    }

    public function productEdit(ProductCompoundRequest $request)
    {
        $ware = json_decode($request->ware);
        $prod = json_decode($request->prods);

        if (count($prod) == 0) {
            return response()->json([
                'msg' => '¡Debe cargar al menos un producto para crear un producto compuesto o empaquetado!',
                'result' => false,
            ], 422);
        }

        for ($i = 0; $i < count($prod); $i++) {
            if (!is_numeric($prod[$i]->quanty)) {
                return response()->json([
                    'msg' => '¡El campo precio del producto compuesto debe ser numérico!',
                    'result' => false,
                ]);
            }
        }

        foreach ($ware as $w) {
            if ($w->stock_min != "" || $w->stock_min != null) {
                if (!is_numeric($w->stock_min)) {
                    return response()->json([
                        'msg' => '¡El campo stock mínimo ' . $w->name . ' debe ser numérico!',
                        'result' => false,
                    ]);
                }

                if (!is_numeric($w->stock_max)) {
                    return response()->json([
                        'msg' => '¡El campo stock máximo ' . $w->name . ' debe ser numérico!',
                        'result' => false,
                    ]);
                }
            }
        }

        $product = Product::where('id', $request->id)->first();
        $product->name = $request->name;
        $product->description = $request->description;

        if ($request->hasFile('image')) {
            $name = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 40);
            Image::make($request->image)->save('img/products/' . $name . '.jpg');
            $product->avatar = 'img/products/' . $name . '.jpg';
        }

        $product->percentage_earning = $request->percentage_earning && $request->percentage_earning != "null" ? $request->percentage_earning : 0;
        $product->percentage_commission = $request->percentage_commission && $request->percentage_commission != "null" ? $request->percentage_commission : 0;

        $product->exempt = $request->exempt ? 1 : 0;
        $product->serialization = $request->serialization ? 1 : 0;
        $product->save();

        if ($request->has('subcategory')) {
            $productCateg = ProductSubCategory::where('product_id', $request->id);
            $productCategory = $productCateg->first();

            if ($productCateg->count() == 0) {
                $productCategory = new ProductSubCategory;
                $productCategory->product_id = $request->id;
                $productCategory->creator_id = $request->user_id;
            }

            $productCategory->subcategory_id = $request->subcategory;
            $productCategory->save();
        }

        if ($request->has('category')) {
            $productCateg = ProductCategory::where('product_id', $request->id);
            $productCategory = $productCateg->first();

            if ($productCateg->count() == 0) {
                $productCategory = new ProductCategory;
                $productCategory->product_id = $product->id;
                $productCategory->creator_id = $request->user_id;
            }

            $productCategory->category_id = $request->category;
            $productCategory->save();
        }

        if ($request->has('brand')) {
            $productBran = ProductBrand::where('product_id', $request->id);
            $productBrand = $productBran->first();

            if ($productBran->count() == 0) {
                $productBrand = new ProductBrand;
                $productBrand->product_id = $product->id;
                $productBrand->creator_id = $request->user_id;
            }

            $productBrand->brand_id = $request->brand;
            $productBrand->save();
        }

        if ($request->has('model')) {
            $productCateg = ProductModel::where('product_id', $request->id);
            $productCategory = $productCateg->first();

            if ($productCateg->count() == 0) {
                $productCategory = new ProductModel;
                $productCategory->product_id = $request->id;
                $productCategory->creator_id = $request->user_id;
            }

            $productCategory->model_id = $request->model;
            $productCategory->save();
        }

        ProductDetails::where('compound_id', $request->id)->delete();

        for ($i = 0; $i < count($prod); $i++) {
            $productDetail = new ProductDetails;
            $productDetail->compound_id = $request->id;
            $productDetail->product_id = $prod[$i]->id;
            $productDetail->amount = $prod[$i]->quanty;
            $productDetail->serialization = $prod[$i]->serialization;
            $productDetail->save();
        }

        foreach ($ware as $w) {
            if ($w->stock_min != "" || $w->stock_max != "" || $w->percentage_earning != ""
                || $w->percentage_max != "" || $w->percentage_offer != "" || $w->location != "") {
                $productQuery = ProductWarehouse::where('product_id', $request->id)
                    ->where('warehouse_id', $w->id);

                $productWare = $productQuery->first();

                if ($productQuery->count() == 0) {
                    $productWare = new ProductWarehouse;
                    $productWare->warehouse_id = $w->id;
                    $productWare->product_id = $request->id;
                    $productWare->creator_id = $request->user_id;
                    $productWare->price = 0;
                }

                $productWare->percentage_earning = $w->percentage_earning ? $w->percentage_earning : 0;
                $productWare->percentage_max = $w->percentage_max ? $w->percentage_max : 0;
                $productWare->percentage_offer = $w->percentage_offer ? $w->percentage_offer : 0;
                $productWare->stock_min = $w->stock_min ? $w->stock_min : null;
                $productWare->stock_max = $w->stock_max ? $w->stock_max : null;
                $productWare->location = $w->location ? $w->location : null;
                $productWare->save();
            }
        }

        return response()->json([
            'msg' => '¡Producto editado exitosamente!',
            'result' => true,
        ]);
    }

    public function productDelete(Request $request)
    {
        $productsWarehouse = ProductWarehouse::with('last_movement')
            ->where('product_id', $request->product_id)
            ->whereIn('warehouse_id', $request->warehouses)
            ->get()->toArray();

        $arrayStock = array_map(function ($product) {
            $current = $product['last_movement'] ? $product['last_movement'] : 0;
            if ($current) {
                $current = $current['type'] == 1 ? $current['current_stock'] + $current['amount'] : $current['current_stock'] - $current['amount'];
            }
            if (!$current) {
                return $product['id'];
            }
        }, $productsWarehouse);
        
        if (count($arrayStock) == 0) {
            return response()->json(['msg' => 'El producto compuesto posee stock, no puede eliminarlo'], 422);
        }
        
        ProductWarehouse::whereIn('id', $arrayStock)->delete();
        $countProductWarehouse = ProductWarehouse::where('product_id', $request->product_id)->count();
        if ($countProductWarehouse == 0) {
            ProductDetails::where('compound_id', $request->product_id)->delete();
            Product::where('id', $request->product_id)->delete();
        }

        return;
    }
}
