<?php

namespace App\Http\Controllers\Api\Admin\ProductProvider;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductProviderRequest;
use App\Models\ProductProvider;
use App\User;

class ProductProviderController extends Controller
{
    public function productProviders(Request $request)
    {
        $product_providers = ProductProvider::where('product_id', $request->product_id)
        ->with(['provide' => function($query) {
            $query->select('id', 'name', 'email')->where('status', 1);
        }]);

        if ($request->has('search')) {
			$product_providers->where(function($q) use ($request) {
				$q->whereHas('provide', function($query) use($request) {
                    $query->where('email','like','%'.$request->search.'%');
                })
                ->orWhere('price', 'like', '%'.$request->search.'%')
                ->orWhereHas('provide', function($query) use($request) {
                    $query->where('name','like','%'.$request->search.'%');
                });
			});
        }

        $product_providers = $product_providers->paginate(10);
        
        $providers = User::select('id', 'name', 'email')
            ->where('status', 1)
            ->where('level', 6)
            ->whereHas('enterprise_providers', function($enter) use($request) {
                $enter->where('enterprise_id', $request->user_id);
            })
            ->with(['product_provider' => function($query) use($request) {
                $query->select('id', 'price', 'provider_id', 'product_id')->where('product_id', $request->product_id);
            }])
            ->get();

        
        return response()->json([
            'providers' => $providers,
            'product_providers' => $product_providers,
            'result' => true
        ]);
    }

    public function productProvidersGet(Request $request)
    {
        $providers = User::select('id', 'name', 'email')
            ->where('status', 1)
            ->where('level', 6)
            ->whereHas('enterprise_providers', function($enter) use($request) {
                $enter->where('enterprise_id', $request->user_id);
            })
            ->with(['product_provider' => function($query) {
                $query->select('id', 'price', 'provider_id');
            }])
            // ->whereDoesntHave('product_provider', function($q) use($request) {
            //     $q->where('product_id', $request->product_id);
            // })
            ->get();
            
        return response()->json([
            'providers' => $providers,
            'result' => true
        ]);
    }

    public function productProvidersAssign(ProductProviderRequest $request)
    {
        $prov = new ProductProvider;
        $prov->provider_id = $request->provider_id;
        $prov->product_id = $request->product_id;
        $prov->creator_id = $request->creator_id;
        $prov->price = $request->price;
        $prov->save();

        return response()->json([
            'msg' => 'El proveedor ha sido asignado exitosamente',
            'result' => true
        ]);
    }

    public function productProvidersChange(ProductProviderRequest $request)
    {
        $prov = ProductProvider::find($request->id);
        $prov->provider_id = $request->provider_id;
        $prov->price = $request->price;
        $prov->save();

        return response()->json([
            'msg' => 'El proveedor ha sido cambiado exitosamente',
            'result' => true
        ]);
    }

    public function productProvidersInclude(Request $request)
    {
        $providers = User::select('id', 'name', 'email')
            ->where('status', 1)
            ->where('level', 6)
            ->whereHas('enterprise_providers')
            ->whereDoesntHave('product_provider', function($q) use($request) {
                $q->where('product_id', $request->product_id)
                ->where('provider_id', '!=', $request->provider_id);
            })
            ->get();
        
        return response()->json([
            'providers' => $providers,
            'result' => true
        ]);
    }

    public function productProvidersDelete(Request $request)
    {
        $prov = ProductProvider::where('id', $request->id);
        $prov->delete();

        return response()->json([
            'msg' => 'El proveedor ha sido eliminado exitosamente del producto.',
            'result' => true,
        ]);
    }

}
