<?php

namespace App\Http\Controllers\Api\Admin\CashCount;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\EnterpriseUser;
use App\Models\ProcessedOrder as ProcessedRequest;
use App\Models\ProcessedPayment;
use App\Models\BankBalance;
use App\Models\CashCount;
use App\Http\Traits\WarehouseFilter;

define('NOT_PROCESSED', 0);
define('PROCESSED', 1);
define('CASH_COUNT', 'App\Models\CashCount');
define('ENTRY', 1);

class CashCountController extends Controller
{
    use WarehouseFilter;

    public function index(Request $request)
    {
        $users_ids = [];
        
        if ($request->has('role') && $request->role == config('constants.user.roles.ENTERPRISE')) {
            $users_ids = EnterpriseUser::where('enterprise_id', $request->Id)->select('user_id')->get()->pluck('user_id')->toArray();
        }
        $users_ids[] = $request->enterprise_id;

        $warehouses_ids =  $this->warehouseFilter($request->enterprise_id, $request->user_id, $request->role, $request->branch_id, $request->warehouse_id);
       
        $requests = ProcessedRequest::with([
            'client' => function ($query) {
                $query->with('person')->withTrashed();
            },
            'method_payment' => function ($query) {
                $query->where('processed', NOT_PROCESSED);
            },
            'seller' => function ($query) {
                $query->with([
                    'person' => function ($query) {
                        $query->withTrashed();
                    },
                ])->withTrashed();
            },
        ])
        ->where('processed', NOT_PROCESSED)
        ->whereIn('warehouse_id', $warehouses_ids)
        ->whereIn('creator_id', $users_ids);

        $query = $requests->orderBy('code', 'DESC')->orderBy('id', 'DESC')->get();

        return $query;
    }

    public function process(Request $request) {

        $bankPayments = $request->bankPayments;
        $paymentsToProcess = $request->paymentsToProcess;

        foreach ($bankPayments as $key => $bankPayment) {

            $current_bank_balance = BankBalance::where('bank_account_id', $bankPayment['bank_account_id'])->orderBy('id', 'desc');
            if($current_bank_balance->exists()) {
                $current_bank_balance = $current_bank_balance->first()->total;
            } else {
                $current_bank_balance = 0;
            }

            $cashCount = new CashCount;
                $cashCount->bank_id = $bankPayment['bank_id'];
                $cashCount->bank_account_id = $bankPayment['bank_account_id'];
                $cashCount->amount = $bankPayment['amount'];
                $cashCount->creator_id = $request->userId;
            $cashCount->save();

            $bank_balance = new BankBalance;
                $bank_balance->bank_id = $bankPayment['bank_id'];
                $bank_balance->bank_account_id = $bankPayment['bank_account_id'];
                $bank_balance->action_id = $cashCount->id;
                $bank_balance->action_class = 3;
                $bank_balance->action_type = CASH_COUNT;
                $bank_balance->type = ENTRY;
                $bank_balance->amount = $bankPayment['amount'];
                $bank_balance->current_amount = $current_bank_balance;
                $bank_balance->total = $current_bank_balance + $bankPayment['amount'];
                $bank_balance->description = "Ingreso por pago de cierre de caja";
                $bank_balance->creator_id = $request->userId;
            $bank_balance->save();
        }

        foreach ($paymentsToProcess as $key => $paymentsToProcess) {

            $processedPayment = ProcessedPayment::where('id', $paymentsToProcess['id'])->first();
                $processedPayment->processed = PROCESSED;
            $processedPayment->save();

            $processedRequest = ProcessedRequest::where('id', $paymentsToProcess['processed_request_id'])->first();
                $processedRequest->processed = PROCESSED;
                $processedRequest->status = PROCESSED;
            $processedRequest->save();
        }

        $users_ids = [];
        
        if ($request->has('role') && $request->role == config('constants.user.roles.ENTERPRISE')) {
            $users_ids = EnterpriseUser::where('enterprise_id', $request->enterpriseId)->select('user_id')->get()->pluck('user_id')->toArray();
        }
        $users_ids[] = $request->enterpriseId;

        $warehouses_ids =  $this->warehouseFilter($request->enterpriseId, $request->userId, $request->role, $request->branch_id, $request->warehouse_id);
       
        $requests = ProcessedRequest::with([
            'client' => function ($query) {
                $query->with('person')->withTrashed();
            },
            'method_payment' => function ($query) {
                $query->where('processed', NOT_PROCESSED);
            },
            'seller' => function ($query) {
                $query->with([
                    'person' => function ($query) {
                        $query->withTrashed();
                    },
                ])->withTrashed();
            },
        ])
        ->when($request->has('seller') && $request->seller > 0, function ($query) use ($request) {
            $query->where('creator_id', $request->seller);
        })
        ->when($request->has('client') && $request->client > 0, function ($query) use ($request) {
            $query->where('client_id', $request->client);
        })
        ->where('processed', NOT_PROCESSED)
        ->whereIn('warehouse_id', $warehouses_ids)
        ->whereIn('creator_id', $users_ids);

        $query = $requests->orderBy('code', 'DESC')->orderBy('id', 'DESC')->get();

        return $query;
    }
}
