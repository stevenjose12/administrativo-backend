<?php

namespace App\Http\Controllers\Api\Admin\Purchase;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Models\Purchase;
use App\User;

class PurchaseController extends Controller
{
    
    public function purchases(Request $request){
        $purchases = Purchase::whereNull('deleted_at')
            ->with(['details' => function($q){
                $q->with(['product' => function($q){
                    $q->with([
                        'images' => function($q){
                            $q->whereNull('deleted_at');
                        },
                        'category'
                    ])
                    ->withTrashed();
                }]);
            }, 'method' => function($q){
                $q->withTrashed();
            }, 'user.person', 'client'])
            ->orderBy('id', 'DESC');
        $since = null;
        $until = null;
        if($request->has('since')){
            $since  = Carbon::parse($request->since)->format('Y-m-d 00:00 00');
            $purchases = $purchases->where('created_at', '>=', $since);
        }
        if($request->has('until')){
            $until  = Carbon::parse($request->until)->format('Y-m-d 23:59 59');
            $purchases = $purchases->where('created_at', '<=', $until);
        }
        if($request->has('seller')){
            $purchases = $purchases->where('user_id', $request->seller);
        }
        $response = $purchases->paginate(10);
        return response()->json([
            'result' => true,
            'purchases' => $response,
        ]);
        
    }

    public function getSellers(){
        $sellers = User::select(
            'users.id',
            'users.email'
        )
        ->join('purchases', 'purchases.user_id', '=', 'users.id')
        ->whereNull('users.deleted_at')
        ->where('users.level', 1)
        ->with(['person' => function($q){
            $q->withTrashed();
        }])
        ->groupBy('users.id')
        ->get();
        return response()->json([
            'sellers' => $sellers,
            'result' => true,
        ]);
    }
}
