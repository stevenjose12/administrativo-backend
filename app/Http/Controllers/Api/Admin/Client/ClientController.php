<?php

namespace App\Http\Controllers\Api\Admin\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\ClientRequest;
use App\Models\ConfigClient;
use App\Models\EnterpriseClient;
use App\Models\Person;
use App\Models\RetentionType;
use App\Models\RequestOrder;
use App\Models\ProcessedOrder;
use App\Models\UserRetentionType;
use App\Models\Zone;
use App\Models\ZoneUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class ClientController extends Controller
{

    public function getCustomersEnterprise(Request $request)
    {
        $customers = EnterpriseClient::with([
            'client' => function ($query) {
                $query->whereNotNull('name')->with([
                    'person' => function ($query) {
                        $query->whereNotNull('fiscal_identification')->orWhereNotNull('identity_document');
                    },
                ]);
            },
        ])->where('enterprise_id', $request->Id)->get();

        return $customers;
    }

    public function getCustomersSeller(Request $request)
    {
        $customers = EnterpriseClient::with([
            'client' => function ($query) {
                $query->whereNotNull('name')->with([
                    'person' => function ($query) {
                        $query->whereNotNull('fiscal_identification')->orWhereNotNull('identity_document');
                    },
                ]);
            },
        ])
            ->where('enterprise_id', $request->enterprise_id)
            ->whereHas('client.zones', function ($query) use ($request) {
                $query->whereIn('zones.id', $request->zones_id);
            })
            ->get();

        return $customers;
    }

    public function clients(Request $request)
    {

        if ($request->role == 1 || $request->role == 2) {
            $clients = User::whereNull('users.deleted_at')->where('users.level', 5);
        } else {
            $clients_ids = EnterpriseClient::where('enterprise_id', $request->user_id)->select('client_id')->get();
            $clients = User::whereIn('users.id', $clients_ids)->whereNull('users.deleted_at')->where('users.level', 5);
        }

        $clients->select(
            'users.id',
            'users.name',
            'users.email',
            'users.status',
            'persons.first_name',
            'persons.last_name',
            'persons.phone',
            'persons.direction',
            'persons.fiscal_identification',
            'persons.identity_document',
            'persons.person_type',
            'persons.deleted_at'
        )
            ->join('persons', 'persons.user_id', '=', 'users.id')
            ->orderBy('users.id', 'DESC')
            ->with([
                'configuration_client',
                'zone_user.zone',
                'user_retention_types.retention',
                'client_enterprise.enterprise' => function ($query) {
                    $query->select('id')->with(['person' => function ($query) {
                        $query->select('first_name', 'user_id')->withTrashed();
                    }])->withTrashed();
                }])->withTrashed();

        if ($request->has('search')) {
            $clients->where(function ($q) use ($request) {
                $q->where('users.email', 'like', '%' . $request->search . '%')
                    ->orWhere('persons.first_name', 'like', '%' . $request->search . '%')
                // ->orWhere('persons.last_name','like','%'.$request->search.'%')
                // ->orWhere(DB::raw("CONCAT(persons.first_name, ' ', persons.last_name)"), 'LIKE', "%".$request->search."%")
                    ->orWhere('persons.phone', 'like', '%' . $request->search . '%')
                    ->orWhere('persons.fiscal_identification', 'like', '%' . $request->search . '%')
                    ->orWhere('persons.identity_document', 'like', '%' . $request->search . '%');
            });
        }

        if ($request->has('status')) {
            $clients->where('users.status', intval($request->status));
        }

        if ($request->has('person_type')) {
            $clients->where('persons.person_type', intval($request->person_type));
        }

        if ($request->has('zone')) {
            $clients->whereHas('zone_user', function ($query) use ($request) {
                $query->where('zone_id', intval($request->zone));
            });
        }

        $response = $clients->orderBy('id', 'desc')->paginate(10);

        return response()->json([
            'clients' => $response,
            'result' => true,
        ]);
    }

    private function ValidClient(array $data, $Id)
    {
        $attributes = [
            'first_name' => 'nombre',
            'document_type' => 'tipo de documento',
            'identity_document' => 'documento de identidad',
            'email' => 'correo electrónico',
            'phone' => 'teléfono',
            'enterprise_id' => 'empresa',
            'zone_id' => 'zona',
            'direction' => 'dirección',
        ];

        $validator = Validator::make($data, [
            'first_name' => ['required'],
            'document_type' => ['required_without:fiscal_document_type', 'between:1,3','nullable'],
            'identity_document' => ['required_without:fiscal_identification', 'between:6,9','nullable'],
            'fiscal_document_type' => ['required_without:document_type', 'between:1,3','nullable'],
            'fiscal_identification' => ['required_without:identity_document', 'between:6,9','nullable'],
            'email' => [
                'required',
                'email',
                Rule::unique('users')->ignore($Id, 'id')->where(function ($query) {
                    $query->where('level', 5);
                }),
            ],
            'phone' => ['required', 'numeric', 'digits_between:7,11'],
            'zone_id' => ['required'],
            'enterprise_id' => ['required'],
            'direction' => ['required'],
        ], [
            "required" => "El campo :attribute es obligatorio.",
            "string" => "El campo :attribute debe ser una cadena de caracteres.",
            "numeric" => ":attribute debe ser numérico.",
            "required_without" => 'Debe ingresar una cédula o RIF'
        ]);

        $validator->setAttributeNames($attributes);

        return $validator;
    }

    public function clientCreateFromRequest(Request $request)
    {
        $this->ValidClient($request->all(), $Id = $request->create_id)->validate();

        $clients_enterprises_ids = EnterpriseClient::where('enterprise_id', $request->enterprise_id)->select('client_id')->get();

        if ($request->has('identity_document')) {
            $check_dni = Person::whereIn('user_id', $clients_enterprises_ids)
                ->where('identity_document', $request->document_type . '-' . $request->identity_document)
                ->count();

            if ($check_dni > 0) {
                return response()->json([
                    'message' => '¡La cédula ingresada ya se encuentra registrada¡',
                ], 422);
            }
        }

        $name = explode('@', $request->email);
        $client = new User;
        $client->name = $name[0];
        $client->email = $request->email;
        $client->password = $request->email;
        $client->status = 1;
        $client->level = 5;
        $client->save();

        $client_person = new Person;
        $client_person->user_id = $client->id;
        $client_person->first_name = $request->first_name;
        $client_person->phone = $request->phone;
        $client_person->direction = $request->direction;
        if ($request->has('identity_document')) {
            $client_person->identity_document = $request->document_type . '-' . $request->identity_document;
        }
        if ($request->has('fiscal_identification')) {
            $client_person->fiscal_identification = $request->fiscal_document_type . '-' . $request->fiscal_identification;
        }
        $client_person->person_type = $request->person_type;
        $client_person->save();

        if ($request->has('enterprise_id')) {
            $client_enterprise = new EnterpriseClient;
            $client_enterprise->enterprise_id = $request->enterprise_id;
            $client_enterprise->client_id = $client->id;
            $client_enterprise->creator_id = $request->creator_id;
            $client_enterprise->save();
        }

        if ($request->has('zone_id')) {
            $client_zone = new ZoneUser;
            $client_zone->zone_id = $request->zone_id;
            $client_zone->user_id = $client->id;
            $client_zone->creator_id = $request->creator_id;
            $client_zone->save();
        }

        if ($request->has('retention_id')) {
            $client_retention = new UserRetentionType;
            $client_retention->retention_type_id = $request->retention_id;
            $client_retention->user_id = $client->id;
            $client_retention->save();
        }

        return EnterpriseClient::with([
            'client' => function ($query) {
                $query->whereNotNull('name')->with([
                    'person' => function ($query) {
                        $query->whereNotNull('fiscal_identification')->orWhereNotNull('identity_document');
                    },
                ]);
            },
        ])->where('client_id', $client->id)->first();
    }

    public function clientCreate(ClientRequest $request)
    {

        $clients_enterprises_ids = EnterpriseClient::where('enterprise_id', $request->enterprise_id)->select('client_id')->get();

        $check_email = User::whereIn('id', $clients_enterprises_ids)
            ->where('email', $request->email)
            ->count();

        if ($check_email > 0) {
            return response()->json([
                'msg' => 'Ya esta registrado este correo electronico',
            ], 422);
        }

        if ($request->has('identity_document')) {
            $check_dni = Person::whereIn('user_id', $clients_enterprises_ids)
                ->where('identity_document', $request->document_type . '-' . $request->identity_document)
                ->count();

            if ($check_dni > 0) {
                return response()->json([
                    'msg' => 'Ya esta registrada esta cedula',
                ], 422);
            }
        }

        if ($request->has('fiscal_identification')) {
            $check_fiscal = Person::whereIn('user_id', $clients_enterprises_ids)
                ->where('fiscal_identification', $request->fiscal_document_type . '-' . $request->fiscal_identification)
                ->count();

            if ($check_fiscal > 0) {
                return response()->json([
                    'msg' => 'Ya esta registrado este RIF',
                ], 422);
            }
        }

        $name = explode('@', $request->email);
        $client = new User;
        $client->name = $name[0];
        $client->email = $request->email;
        $client->password = $request->email;
        $client->status = 1;
        $client->level = 5;
        $client->save();

        $client_person = new Person;
        $client_person->user_id = $client->id;
        $client_person->first_name = $request->first_name;
        // $client_person->last_name = $request->last_name;
        $client_person->phone = $request->phone;
        $client_person->direction = $request->direction;
        if ($request->has('fiscal_identification')) {
            $client_person->fiscal_identification = $request->fiscal_document_type . '-' . $request->fiscal_identification;
        }
        if ($request->has('identity_document')) {
            $client_person->identity_document = $request->document_type . '-' . $request->identity_document;
        }
        $client_person->person_type = $request->person_type;
        $client_person->save();

        if ($request->has('days_deadline')) {
            $config = new ConfigClient;
            $config->client_id = $client->id;
            $config->days_deadline = (int) $request->days_deadline;
            $config->creator_id = $request->creator_id;
            $config->save();
        }

        if ($request->has('enterprise_id')) {
            $client_enterprise = new EnterpriseClient;
            $client_enterprise->enterprise_id = $request->enterprise_id;
            $client_enterprise->client_id = $client->id;
            $client_enterprise->creator_id = $request->creator_id;
            $client_enterprise->save();
        }

        if ($request->has('zone_id')) {
            $client_zone = new ZoneUser;
            $client_zone->zone_id = $request->zone_id;
            $client_zone->user_id = $client->id;
            $client_zone->creator_id = $request->creator_id;
            $client_zone->save();
        }

        if ($request->has('retention_type_id')) {
            $client_retention = new UserRetentionType;
            $client_retention->retention_type_id = $request->retention_type_id;
            $client_retention->user_id = $client->id;
            $client_retention->save();
        }

        return response()->json([
            'msg' => 'Cliente registrado exitosamente',
            'result' => true,
        ]);
    }

    public function clientEdit(ClientRequest $request)
    {
        $clients_enterprises_ids = EnterpriseClient::where('enterprise_id', $request->enterprise_id)
            ->where('client_id', '!=', $request->id)->select('client_id')->get();

        $check_email = User::whereIn('id', $clients_enterprises_ids)
            ->where('email', $request->email)
            ->count();

        if ($check_email > 0) {
            return response()->json([
                'msg' => 'Ya esta registrado este correo electronico',
            ], 422);
        }

        if ($request->has('identity_document')) {
            $check_dni = Person::whereIn('user_id', $clients_enterprises_ids)
                ->where('identity_document', $request->document_type . '-' . $request->identity_document)
                ->count();

            if ($check_dni > 0) {
                return response()->json([
                    'msg' => 'Ya esta registrada esta cedula',
                ], 422);
            }
        }

        if ($request->has('fiscal_identification')) {
            $check_fiscal = Person::whereIn('user_id', $clients_enterprises_ids)
                ->where('fiscal_identification', $request->fiscal_document_type . '-' . $request->fiscal_identification)
                ->count();

            if ($check_fiscal > 0) {
                return response()->json([
                    'msg' => 'Ya esta registrado este RIF',
                ], 422);
            }
        }

        $name = explode('@', $request->email);
        $client = User::where('id', $request->id)->first();
        $client->name = $name[0];
        $client->email = $request->email;
        $client->password = $request->email;
        $client->save();

        $client_person = Person::where('user_id', $request->id)->first();
        $client_person->first_name = $request->first_name;
        // $client_person->last_name = $request->last_name;
        $client_person->phone = $request->phone;
        $client_person->direction = $request->direction;
        if ($request->has('fiscal_identification')) {
            $client_person->fiscal_identification = $request->fiscal_document_type . '-' . $request->fiscal_identification;
        }
        if ($request->has('identity_document')) {
            $client_person->identity_document = $request->document_type . '-' . $request->identity_document;
        }
        if ($request->has('person_type')) {
            $client_person->person_type = $request->person_type;
        }
        $client_person->save();

        if ($request->has('days_deadline')) {
            $configuration = ConfigClient::where('client_id', $request->id)->first();

            $config = $configuration ? $configuration : new ConfigClient;
            $config->client_id = $client->id;
            $config->days_deadline = (int) $request->days_deadline;
            $config->creator_id = $request->creator_id;
            $config->save();
        }

        if ($request->has('enterprise_id')) {
            $client_enterprise = EnterpriseClient::where('client_id', $request->id)->first();
            $client_enterprise->enterprise_id = $request->enterprise_id;
            $client_enterprise->save();
        }

        if ($request->has('zone_id')) {
            $client_zone = ZoneUser::where('user_id', $request->id)->first();
            $client_zone->zone_id = $request->zone_id;
            $client_zone->save();
        }

        if ($request->has('retention_type_id')) {
            $client_retention = UserRetentionType::where('user_id', $request->id)->first();
            if ($client_retention != null) {
                $client_retention->retention_type_id = $request->retention_type_id;
                $client_retention->save();
            } else {
                $client_retention = new UserRetentionType;
                $client_retention->retention_type_id = $request->retention_type_id;
                $client_retention->user_id = $client->id;
                $client_retention->save();
            }
        }

        return response()->json([
            'msg' => 'Cliente editado exitosamente',
            'result' => true,
        ]);
    }

    public function clientDelete(Request $request)
    {        


        if (isset($request->id)) {
            $request_order = RequestOrder::where('client_id', $request->id)
                ->where('status',config('constants.request.status.REQUEST_NOT_PROCESSED'))
                ->count();
            
            if($request_order > 0) {
                return response()->json([
                    'msg' => '¡El cliente tiene pedidos pendientes!',
                ], 422);
            }

            $request_order = ProcessedOrder::where('client_id', $request->id)
                ->where('status',config('constants.request.status.REQUEST_NOT_PROCESSED'))
                ->count();

            if($request_order > 0) {
                return response()->json([
                    'msg' => '¡El cliente tiene cuentas pendientes por cobrar!',
                ], 422);
            }

            EnterpriseClient::where('client_id', $request->id)->delete();
            ZoneUser::where('user_id', $request->id)->delete();
            Person::where('user_id', $request->id)->delete();
            User::where('id', $request->id)->delete();
            return response()->json([
                'msg' => 'Cliente eliminado exitosamente',
            ]);
        }
        return response()->json([
            'msg' => 'Ha ocurrido un error',
        ], 422);
    }

    public function clientSuspend(Request $request)
    {
        if (isset($request->id)) {
            $enterprise = User::where('id', $request->id)->first();
            $enterprise->status = $enterprise->status == 1 ? 2 : 1;
            $enterprise->save();
            return response()->json([
                'msg' => $enterprise->status == 2 ? 'Cliente suspendido exitosamente' : 'Cliente activado exitosamente',
            ]);
        } else {
            return response()->json([
                'msg' => 'Ha ocurrido un error',
            ], 422);
        }
    }

    public function clientVerify(Request $request)
    {
        if (isset($request->id)) {
            $enteprise = User::find($request->id);
            $enteprise->status = 1;
            $enteprise->save();
            return response()->json([
                'msg' => 'Cliente verificado exitosamente',
            ]);
        } else {
            return response()->json([
                'msg' => 'Ha ocurrido un error',
            ], 422);
        }
    }

    public function getZones()
    {
        $zones = Zone::where('status', 1)->get();
        return $zones;
    }

    public function getRetentions()
    {
        $retentions = RetentionType::where('status', 1)->get();
        return $retentions;
    }
}
