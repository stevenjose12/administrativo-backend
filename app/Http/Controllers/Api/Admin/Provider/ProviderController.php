<?php

namespace App\Http\Controllers\Api\Admin\Provider;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProviderRequest;
use App\Models\Person;
use App\Models\UserRetentionType;
use App\Models\EnterpriseProvider;
use App\Models\RetentionType;
use App\Models\ProductProvider;
use App\Models\ConfigProvider;
use App\User;
use App\Models\ExpensesIngress;
use App\Models\PurchaseOrder;
use Validator;

class ProviderController extends Controller
{
    public function providers(Request $request)
    {
        $providers = User::with('configuration_provider')->select(
            'users.id',
            'users.name',
            'users.email',
            'users.status',
            'persons.phone',
            'persons.first_name',
            'persons.last_name',
            'persons.direction',
            'persons.code',
            'persons.fiscal_identification',
            'persons.person_type',
            'persons.identity_document'
        )
            ->join('persons', 'persons.user_id', '=', 'users.id')
            ->where('level', 6);

        if ($request->role == 3 || $request->role == 4) {
            $providers->whereHas('enterprise_providers', function ($q) use ($request) {
                $q->where('enterprise_id', $request->user_id);
            });
        }

        $providers->with(['user_retention_types.retention']);

        if ($request->has('search')) {
            $providers->where(function ($q) use ($request) {
                $q->where('users.email', 'like', '%' . $request->search . '%')
                    ->orWhere('persons.first_name', 'like', '%' . $request->search . '%')
                    ->orWhere('persons.phone', 'like', '%' . $request->search . '%')
                    ->orWhere('persons.code', 'like', '%' . $request->search . '%')
                    ->orWhere('persons.fiscal_identification', 'like', '%' . $request->search . '%');
            });
        }

        if ($request->has('status')) {
            $providers->where('users.status', intval($request->status));
        }

        $providers = $providers->orderBy('users.id', 'desc')->paginate(10);

        $retentions = RetentionType::where('status', '1')->get();
        $enterprises = User::where('level', 3)->where('status', 1)->get();

        return response()->json([
            'result' => true,
            'providers' => $providers,
            'retentions' => $retentions,
            'enterprises' => $enterprises,
        ]);
    }

    public function getProvidersByEnterprise(Request $request)
    {
        $Id = $request->Id;

        $providers = User::with('configuration_provider')->where('level', 6)->whereHas('enterprise_providers', function ($query) use ($Id) {
            $query->where('enterprise_id', $Id);
        })->get();

        return $providers;
    }

    public function providerCreate(ProviderRequest $request)
    {
       
        $providers_ids = EnterpriseProvider::where('enterprise_id', $request->enterprise_id)->select('provider_id')->get();

        $check_name = User::whereIn('id', $providers_ids)
            ->where('name', $request->name)
            ->count();

        if ($check_name > 0) {
            return response()->json([
                'msg' => "¡El nombre " . $request->name . " ya se encuentra registrado!"
            ], 422);
        }

        $check_code = Person::whereIn('user_id', $providers_ids)
            ->where('code', $request->code)
            ->count();

        if ($check_code > 0) {
            return response()->json([
                'msg' => "¡El código " . $request->code . " ya se encuentra registrado!"
            ], 422);
        }

        $check_email = User::whereIn('id', $providers_ids)
            ->where('email', $request->email)
            ->count();

        if ($check_email > 0) {
            return response()->json([
                'msg' => "¡La dirección de correo electrónico " . $request->email . " ya se encuentra registrado!"
            ], 422);
        }

        if ($request->has('fiscal_identification')) {

            $rif = $request->fiscal_document_type . '-' . $request->fiscal_identification;

            $check_fiscal = Person::whereIn('user_id', $providers_ids)
                ->where('fiscal_identification', $rif)
                ->count();

            if ($check_fiscal > 0) {
                return response()->json([
                    'msg' => "¡El RIF " . $rif . " ya se encuentra registrado!"
                ], 422);
            }
        }

        $provider = new User;
        $provider->name = $request->name;
        $provider->email = $request->email;
        $provider->password = "123456";
        $provider->status = 1;
        $provider->level = 6;
        $provider->save();

        $person = new Person;
        $person->user_id = $provider->id;
        $person->first_name = $request->name;
        // $person->last_name = $request->name;
        $person->code = $request->code;
        $person->phone = $request->phone;
        $person->direction = $request->direction;
        if ($request->has('fiscal_identification')) {
            $person->fiscal_identification = $request->fiscal_document_type . '-' . $request->fiscal_identification;
        }
        $person->person_type = $request->person_type;
        $person->save();

        $config_providers = new ConfigProvider;
        $config_providers->provider_id = $provider->id;
        $request->time_limit == NULL ? $time_limit = 0 :$time_limit = $request->time_limit;
        $config_providers->days_deadline = $time_limit;
        $config_providers->creator_id = $request->user_id;
        $config_providers->save();

        if ($request->has('retention_type_id')) {
            $retention = new UserRetentionType;
            $retention->retention_type_id = $request->retention_type_id;
            $retention->user_id = $provider->id;
            $retention->save();
        }

        $enterpriseProvider = new EnterpriseProvider;
        $enterpriseProvider->enterprise_id = $request->enterprise_id;
        $enterpriseProvider->provider_id = $provider->id;
        $enterpriseProvider->creator_id = $request->user_id;
        $enterpriseProvider->save();

        return response()->json([
            'result' => true,
            'msg' => 'Proveedor Registrado con éxito.',
        ]);
    }

    private function ValidProvider(array $data, $Id)
    {
        $attributes = [
            'name'                  => 'nombre',
            'code'                  => 'código',
            'fiscal_identification' => 'RIF',
            'phone'                 => 'teléfono',
            'email'                 => 'correo electrónico',
            'enterprise_id'         => 'empresa',
            'time_limit'            => 'plazo de credito'
        ];

        $validator = Validator::make($data, [
            'name'                  => ['required', Rule::unique('users')->ignore($Id)],
            'code'                  => ['required', 'between:1,3'],
            'fiscal_identification' => ['required', 'between:6,9'],
            'phone'                 => ['required', 'numeric', 'digits_between:7,11'],
            'email'                 => ['required', 'email'],
            'enterprise_id'         => ['required'],
            'time_limit'            => ['required']
        ], [
            "required"  => "El campo :attribute es obligatorio.",
            "string"    => "El campo :attribute debe ser una cadena de caracteres.",
            "numeric"   => ":attribute debe ser numérico."
        ]);

        $validator->setAttributeNames($attributes);

        return $validator;
    }

    public function providerCreateFromOrder(Request $request)
    {
        $this->ValidProvider($request->all(), $Id = $request->enterprise_id)->validate();

        $providers_ids = EnterpriseProvider::where('enterprise_id', $request->enterprise_id)->select('provider_id')->get();

        $check_name = User::whereIn('id', $providers_ids)
            ->where('name', $request->name)
            ->count();

        if ($check_name > 0) {
            return response()->json([
                'msg' => 'El nombre ya se encuentra registrado'
            ], 422);
        }

        $check_code = Person::whereIn('user_id', $providers_ids)
            ->where('code', $request->code)
            ->count();

        if ($check_code > 0) {
            return response()->json([
                'code' => ['El código ya se encuentra registrado']
            ], 422);
        }

        if ($request->has('fiscal_identification')) {
            $check_fiscal = Person::whereIn('user_id', $providers_ids)
                ->where('fiscal_identification', $request->fiscal_document_type . '-' . $request->fiscal_identification)
                ->count();

            if ($check_fiscal > 0) {
                return response()->json([
                    'fiscal_identification' => ['El RIF ya se encuenta registrado']
                ], 422);
            }
        }

        $check_email = Person::join('users', 'users.id', '=', 'persons.user_id')
            ->whereIn('persons.user_id', $providers_ids)
            ->where('users.email', $request->email)
            ->count();

        if ($check_email > 0) {
            return response()->json([
                'email' => ['La dirección de correo electrónico ya existe']
            ], 422);
        }

        $provider = new User;
        $provider->name = $request->name;
        $provider->email = $request->email;
        $provider->password = "123456";
        $provider->status = 1;
        $provider->level = 6;
        $provider->save();

        $person = new Person;
        $person->user_id = $provider->id;
        $person->first_name = $request->name;
        $person->code = $request->code;
        $person->phone = $request->phone;
        $person->direction = $request->direction;
        if ($request->has('fiscal_identification')) {
            $person->fiscal_identification = $request->fiscal_document_type . '-' . $request->fiscal_identification;
        }
        $person->person_type = NULL;

        $person->save();

        if ($request->has('retention_type_id')) {
            $retention = new UserRetentionType;
            $retention->retention_type_id = 1;
            $retention->user_id = $provider->id;
            $retention->save();
        }

        if ($request->has('time_limit')) {
            $config_providers = new ConfigProvider;
            $config_providers->provider_id = $provider->id;
            $config_providers->days_deadline = $request->time_limit;
            $config_providers->creator_id = $request->user_id;
            $config_providers->save();
        }

        $enterpriseProvider = new EnterpriseProvider;
        $enterpriseProvider->enterprise_id = $request->enterprise_id;
        $enterpriseProvider->provider_id = $provider->id;
        $enterpriseProvider->creator_id = $request->user_id;
        $enterpriseProvider->save();

        return $provider;
    }

    public function providerEdit(ProviderRequest $request)
    {

        $providers_ids = EnterpriseProvider::where('enterprise_id', $request->enterprise_id)->where('provider_id', '!=', $request->id)->select('provider_id')->get();

        $check_name = User::whereIn('id', $providers_ids)
            ->where('name', $request->name)
            ->count();

        if ($check_name > 0) {
            return response()->json([
                'msg' => 'Ya esta registrado este Nombre'
            ], 422);
        }

        $check_code = Person::whereIn('user_id', $providers_ids)->where('code', $request->code)->count();
        if ($check_code > 0) {
            return response()->json([
                'msg' => 'Ya esta registrado este Código'
            ], 422);
        }

        $check_email = User::whereIn('id', $providers_ids)->where('email', $request->email)->count();
        if ($check_email > 0) {
            return response()->json([
                'msg' => 'Ya esta registrado este correo electronico'
            ], 422);
        }

        if ($request->has('fiscal_identification')) {
            $check_fiscal = Person::whereIn('user_id', $providers_ids)->where('fiscal_identification', $request->fiscal_document_type . '-' . $request->fiscal_identification)->count();
            if ($check_fiscal > 0) {
                return response()->json([
                    'msg' => 'Ya esta registrado este RIF'
                ], 422);
            }
        }

        $provider = User::find($request->id);
        $provider->name = $request->name;
        $provider->email = $request->email;
        $provider->save();

        $person = Person::where('user_id', $provider->id)->first();
        $person->first_name = $request->name;
        // $person->last_name = $request->name;
        $person->code = $request->code;
        $person->phone = $request->phone;
        $person->direction = $request->direction;
        if ($request->has('fiscal_identification')) {
            $person->fiscal_identification = $request->fiscal_document_type . '-' . $request->fiscal_identification;
        }
        $person->person_type = $request->person_type;
        $person->save();

        $config_providers = ConfigProvider::where('provider_id', $provider->id);
        if ($config_providers->exists()) {
            $config_providers = new ConfigProvider;
            $config_providers->provider_id = $provider->id;
            $config_providers->days_deadline = $request->time_limit;
            $config_providers->creator_id = $request->user_id;
        } else {
            $config_providers = $config_providers->first();
            $config_providers->days_deadline = $request->time_limit;
        }
        $config_providers->save();

        if ($request->has('retention_type_id')) {
            $retention = UserRetentionType::where('user_id', $provider->id);
            if ($retention->exists()) {
                $retention = $retention->first();
                $retention->retention_type_id = $request->retention_type_id;
                $retention->save();
            } else {
                $retention = new UserRetentionType;
                $retention->retention_type_id = $request->retention_type_id;
                $retention->user_id = $provider->id;
                $retention->save();
            }
        }

        if ($request->role == 1 || $request->role == 2) {
            $enterpriseProvider = EnterpriseProvider::where('provider_id', $request->provider_id)->first();
            $enterpriseProvider->enterprise_id = $request->enterprise_id;
            $enterpriseProvider->save();
        }

        return response()->json([
            'result' => true,
            'msg' => 'Proveedor Actualizado con éxito.',
        ]);
    }

    public function providerDelete(Request $request)
    {
        if($this->checkDebtsByProvider($request->id))
        {
            return response()->json(['msg' => 'No puede eliminar este proveedor'],422);
        }

        if (isset($request->id)) {
            $provider = User::where('id', $request->id);
            if ($provider->whereDoesntHave('product_providers')->exists()) {
                $provider->delete();
                Person::where('user_id', $request->id)->delete();
                UserRetentionType::where('user_id', $request->id)->delete();

                return response()->json([
                    'result' => true,
                    'msg' => 'Proveedor Eliminado con éxito.'
                ]);
            } else {
                return response()->json([
                    'msg' => 'No puede eliminar un Proveedor que tenga productos asignados'
                ], 422);
            }
        }
        return response()->json([
            'msg' => 'Ha ocurrido un error'
        ], 422);
    }

    public function providerActive(Request $request)
    {
        $provider = User::find($request->id);
        $provider->status = 1;
        $provider->save();

        return response()->json([
            'result' => true,
            'msg' => 'Proveedor activado con éxito.'
        ]);
    }

    public function providerSuspend(Request $request)
    {
        $provider = User::find($request->id);
        $provider->status = 2;
        $provider->save();

        return response()->json([
            'result' => true,
            'msg' => 'Proveedor suspendido con éxito.'
        ]);
    }

    private function checkDebtsByProvider($provider_id)
    {
        $purchaseOrders = PurchaseOrder::with([
            'provider' => function ($query) {
                $query->with('person')->withTrashed();
            }, 'details'])
            ->where('status', 1)
            ->where('payment_type', 2)
            ->doesntHave('expenses_ingress')
            ->where('provider_id', $provider_id)
            ->count();
        $expensesIngress = ExpensesIngress::where('provider_id',$provider_id)->where('status',0)->count();

        return $purchaseOrders + $expensesIngress;

    }
}
