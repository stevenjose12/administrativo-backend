<?php

namespace App\Http\Controllers\Api\Admin\Delivery;

use App\Http\Controllers\Controller;
use App\Http\Traits\Format;
use App\Http\Traits\WarehouseFilter;
use App\Http\Traits\ProductSerial;
use App\Models\EnterpriseUser;
use App\Models\ProcessedOrder as Delivery;
use App\Models\ProductWarehouse;
use App\Models\ProductWarehouseMovement;
use App\Models\UserBalance;
use App\Models\Warehouse;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
use App\Http\Requests\DeliveryRequest;


define('TYPE_NOTE', 1);
define('TYPE_BILL', 2);

define('ACTION_DELIVERY', 3);
define('PAYMENT_EXPENSES', 2);
define('WITHOUT_PAYING', 0);
define('PAYING', 1);
define('ENTRY',1);
define('APP', 'App\Models\ProcessedOrder');
define('APP_REQUEST', 'App\Models\ProcessedRequest');
define('PROCESSED', 1);

class DeliveryNotesController extends Controller
{
    use Format;
    use ProductSerial;
    use WarehouseFilter;

    public function getId(Request $request)
    {

        $users_ids = EnterpriseUser::where('enterprise_id', $request->Id)->select('user_id')->get()->pluck('user_id')->toArray();
        array_push($users_ids, $request->Id);

        $count = Delivery::where('type', TYPE_NOTE)->whereIn('creator_id', $users_ids)->count();

        $Id = str_pad(1, 9, 0, STR_PAD_LEFT);

        if ($count > 0) {
            $Id = str_pad($count + 1, 9, 0, STR_PAD_LEFT);
        }

        return $Id;
    }

    public function generateId(Request $request)
    {
        $users_ids = EnterpriseUser::where('enterprise_id', $request->Id)->select('user_id')->get()->pluck('user_id')->toArray();
        array_push($users_ids, $request->Id);

        $count = Delivery::where('type', TYPE_NOTE)->whereIn('creator_id', $users_ids)->count();

        $Id = str_pad(1, 9, 0, STR_PAD_LEFT);

        if ($count > 0) {
            $Id = str_pad($count + 1, 9, 0, STR_PAD_LEFT);
        }

        return $Id;
    }

    public function generateFromControllerId($Id)
    {

        $users_ids = EnterpriseUser::where('enterprise_id', $Id)->select('user_id')->get()->pluck('user_id')->toArray();
        array_push($users_ids, $Id);

        $count = Delivery::where('type', TYPE_NOTE)->whereIn('creator_id', $users_ids)->count();

        $Id = str_pad(1, 9, 0, STR_PAD_LEFT);

        if ($count > 0) {
            $Id = str_pad($count + 1, 9, 0, STR_PAD_LEFT);
        }

        return $Id;
    }

    public function deliveryNotes(Request $request)
    {
        $users_ids = [];

        if ($request->role == $this::ROLE_ENTERPRISE) {
            $users_ids = EnterpriseUser::where('enterprise_id', $request->Id)->select('user_id')->get()->pluck('user_id')->toArray();
        }

        array_push($users_ids, $request->Id);

        $warehouses_ids =  $this->warehouseFilter($request->Id, $request->user_id, $request->role, $request->branch_id, $request->warehouse_id);

        $deliveries = Delivery::with([
            'operation' => function ($query) {
                $query->select('operation_id', 'operation_type', 'product_warehouse_id', 'serial')
                    ->with([
                        'product_warehouse' => function ($query) {
                            $query->select('id', 'product_id');
                        },
                    ])->withTrashed();
            },
            'client' => function ($query) {
                $query->with([
                    'person' => function ($query) {
                        $query->withTrashed();
                    },
                ])->withTrashed();
            },
            'details' => function ($query) {
                $query->with([
                    'product_warehouse.warehouse' => function ($query) {
                        $query->select('id', 'branch_id', 'name', 'code', 'deleted_at')->withTrashed();
                    },
                    'product_warehouse.last_movement'
                ]);
            },
            'currency',
            'warehouse',
            'request_order',
            'seller' => function ($query) {
                $query->with([
                    'person' => function ($query) {
                        $query->withTrashed();
                    },
                ])->withTrashed();
            },
        ])->when($request->has('zone_id') && $request->zone_id > 0, function ($query) use ($request) {
            $query->where('zone_id', $request->zone_id);
        })
            ->when($request->has('seller') && $request->seller > 0, function ($query) use ($request) {
                $query->where('creator_id', $request->seller);
            })
            ->when($request->has('client') && $request->client > 0, function ($query) use ($request) {
                $query->where('client_id', $request->client);
            })
            ->when($request->has('since') && $request->has('until'), function ($query) use ($request) {
                $query->whereBetween('date_emission', [$this->parseFormatDateTime($request->since, 'start'), $this->parseFormatDateTime($request->until, 'end')]);
            })
            ->when($request->has('since') && !$request->has('until'), function ($query) use ($request) {
                $query->whereDate('date_emission', '>=', $this->parseFormat($request->since));
            })
            ->when(!$request->has('since') && $request->has('until'), function ($query) use ($request) {
                $query->whereDate('date_emission', '<=', $this->parseFormat($request->until));
            })
            ->when($request->has('type'), function ($query) use ($request) {
                $query->where('type', $request->type);
            })
            ->whereIn('warehouse_id', $warehouses_ids)
            ->when($request->role === 5, function ($query) use ($request) {
                $query->where('creator_id', $request->user_id);
            });

        return $deliveries->where('type', TYPE_NOTE)->orderBy('id', 'desc')->paginate(10);
    }

    private function ValidRequest(array $data)
    {
        $attributes = [
            'client_id' => 'cliente',
            'warehouse_id' => 'almacen',
            'code' => 'código',
            'currency_id' => 'divisa',
            'observations' => 'observaciones',
            'subtotal' => 'subtotal',
            'vat' => 'impuesto al valor agregado',
            'total' => 'total',
            'creator_id' => 'usuario',
        ];

        $validator = Validator::make($data, [
            'client_id' => ['required', 'numeric'],
            'warehouse_id' => ['required', 'numeric'],
            'code' => ['required', 'string'],
            'currency_id' => ['required', 'numeric'],
            'observations' => ['required'],
            'subtotal' => ['required'],
            'vat' => ['required'],
            'total' => ['required'],
            'creator_id' => ['required'],
        ], [
            "required" => "El campo :attribute es obligatorio.",
            "string" => "El campo :attribute debe ser una cadena de caracteres.",
            "numeric" => ":attribute debe ser numérico.",
        ]);

        $validator->setAttributeNames($attributes);

        return $validator;
    }

    private function ValidQuantityActually($products, $warehouse_id)
    {
        foreach ($products as $key => $product) {
            $check_pw = ProductWarehouse::where([
                ['warehouse_id', $warehouse_id],
                ['product_id', $product['id']],
            ])->first();

            $check_pwm = ProductWarehouseMovement::where('product_warehouse_id', $check_pw->id);

            if (!$check_pwm->exists()) {
                return [
                    'message' => "¡El producto " . $product['name'] . " no se encuentra registrado en el almacen!",
                ];
            }

            $current = $check_pwm->get()->last();

            // SETEANDO EL STOCK ACTUAL DEL PRODUCTO DE ACUERDO AL ULTIMO MOVIMIENTO DEL MISMO EN EL ALMACEN
            $stock = $current->type == $this::ENTRY ? $current->current_stock + $current->amount : $current->current_stock - $current->amount;
            // VALIDANDO SI EL STOCK ACTUAL DEL PRODUCTO ES SUFICIENTE PARA SU EXTRACCION
            $stock = $stock - $product['quantity'];

            if ($stock < 0) {
                return [
                    'message' => "¡El producto " . $product['name'] . " rebasa la cantidad en stock!",
                ];
            }
        }
    }

    public function createDeliveryNote(DeliveryRequest $request)
    {


        if ($validator = $this->ValidQuantityActually($products = $request->products, $warehouse_id = $request->warehouse_id)) {
            return response()->json($validator, 422);
        }

        $response = $this->checkExistSerial($request->products, $request->warehouse_id);

        if (!is_bool($response)) {
            return $response;
        }

        $client = User::with('zone_user')->find($request->client_id, ['id']);
        if(is_null($client)) {
            return response()->json([ 'message' => 'Este cliente ha sido eliminado!'], 422);
        }

        $delivery = new Delivery();
        $delivery->code = $this->generateFromControllerId($request->creator_id);
        $delivery->zone_id = $client->zone_user->zone_id;
        /**
         * Por refactorizar, agregar zona horaria adecuada
         */
        $delivery->date_emission = Carbon::now();
        /**
         * Fin refactorizar
         */
        $delivery->type = TYPE_NOTE;
        $delivery->status = WITHOUT_PAYING;
        $delivery->processed = PROCESSED;
        $delivery->fill($request->except(['zone_id', 'code', 'status', 'type', 'status']))->save();

        $user_balance = UserBalance::where([
            ['user_id', $delivery->client_id],
            ['type', PAYMENT_EXPENSES],
        ])->get()->last();

        $current_amount = 0;

        if (!empty($user_balance)) {
            $current_amount = $this->defineAmountBalance($user_balance);
        }

        $balance = new UserBalance;
        $balance->action_id = $delivery->id;
        $balance->action_class = $this::OUTPUT;
        $balance->action_type = APP_REQUEST;
        $balance->user_id = $delivery->client_id;
        $balance->type = $this::OUTPUT;
        $balance->current_amount = $current_amount;
        $balance->amount = $delivery->total;
        $balance->total = $current_amount + $balance->amount;
        $balance->creator_id = $request->creator_id;
        $balance->save();

        foreach ($products as $product) {

            $check_pw = ProductWarehouse::where([
                ['warehouse_id', $warehouse_id],
                ['product_id', $product['id']],
            ])->first();

            // Eliminando serialización anterior de los productos
            $this->deleteSerials($product, $check_pw->id);

            // Serializando la salida de los productos
            $this->createProductSerials($product, $check_pw->id, $delivery->id, APP, $this::OUTPUT);

            $check_pwm = ProductWarehouseMovement::where('product_warehouse_id', $check_pw->id)->get()->last();
            // SETEANDO EL STOCK ACTUAL DEL PRODUCTO DE ACUERDO AL ULTIMO MOVIMIENTO DEL MISMO EN EL ALMACEN
            $stock = $check_pwm->type == $this::ENTRY ? $check_pwm->current_stock + $check_pwm->amount : $check_pwm->current_stock - $check_pwm->amount;

            $movement = new ProductWarehouseMovement;
            $movement->action_id = $delivery->id;
            $movement->product_warehouse_id = $check_pw->id;
            $movement->product_id = $product['id'];
            $movement->action_class = ACTION_DELIVERY;
            $movement->action_type = APP;
            $movement->type = $this::OUTPUT;
            $movement->current_stock = $stock;
            $movement->amount = $product['quantity'];
            $movement->description = $request->description;
            $movement->status = 1;
            $movement->creator_id = $request->creator_id;
            $movement->save();

            $delivery->details()->attach($product['id'], [
                'quantity' => $product['quantity'],
                'subtotal' => $product['subtotal'],
                'vat' => $product['vat'],
                'total' => $product['total'],
            ]);

            // if ($request->forthright) {
            //     $processing->details()->attach($product['id'], [
            //         'quantity' => $product['quantity'],
            //         'subtotal' => $product['subtotal'],
            //         'vat' => $product['vat'],
            //         'total' => $product['total'],
            //     ]);
            // }
        }

        return $delivery;
    }

    protected function defineAmountBalance($user_balance)
    {
        return $user_balance->current_amount + $user_balance->amount;
    }
}
