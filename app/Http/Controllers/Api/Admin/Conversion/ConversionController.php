<?php

namespace App\Http\Controllers\Api\Admin\Conversion;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Currency;
use App\Models\CurrencyConversion;

class ConversionController extends Controller
{
    public function conversions(Request $request)
	{
		if($request->role == 1){
		$currencies = Currency::orderBy('id', 'ASC');
		} else {
		$currencies = Currency::where('status', 1)->orderBy('id', 'ASC');
        }
        
        $conversions = CurrencyConversion::where('enterprise_id', $request->enterprise_id)->with(['master', 'slave'])->get();

		return response()->json([
            'conversions' => $conversions,
            'currencies' => $currencies
		]);
	}
}
