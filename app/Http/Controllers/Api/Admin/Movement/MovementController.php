<?php

namespace App\Http\Controllers\Api\Admin\Movement;

use App\Http\Controllers\Controller;
use App\Http\Traits\Format;
use App\Http\Traits\WarehouseFilter;
use App\Models\ProductWarehouseMovement as Movement;
use App\Models\EnterpriseUser;
use Illuminate\Http\Request;

class MovementController extends Controller
{

    use Format;
    use WarehouseFilter;

    public function movements(Request $request)
    {
        $users_ids = [];

        if ($request->role == config('constants.user.roles.ENTERPRISE')) {
            $users_ids = EnterpriseUser::where('enterprise_id', $request->Id)->select('user_id')->get()->pluck('user_id')->toArray();
        }

        array_push($users_ids, $request->Id);

        $warehouses_ids =  $this->warehouseFilter($request->Id, $request->user_id, $request->role, $request->branch_id, $request->warehouse_id);

        $movements = Movement::with([
            'action',
            'creator' => function ($query) {
                $query->with(['person' => function ($query2) {
                    $query2->withTrashed();
                }])->withTrashed();
            },
            'product_warehouse' => function ($query) {
                $query->with(['warehouse' => function ($query2) {
                    $query2->with(['branch' => function ($query3) {
                        $query3->withTrashed();
                    }])->withTrashed();
                }]);
            },
            'product' => function ($query) {
                $query->withTrashed();
            },
        ])
            ->when($request->has('name'), function ($query) use ($request) {
                $query->whereHas('product', function ($query) use ($request) {
                    $query->where('name', 'LIKE', '%' . $request->name . '%');
                });
            })
            ->when($request->has('since') && $request->has('until'), function ($query) use ($request) {
                $query->whereBetween('created_at', [$this->parseFormatDateTime($request->since, 'start'), $this->parseFormatDateTime($request->until, 'end')]);
            })
            ->when($request->has('since') && !$request->has('until'), function ($query) use ($request) {
                $query->whereDate('created_at', '>=', $this->parseFormat($request->since));
            })
            ->when(!$request->has('since') && $request->has('until'), function ($query) use ($request) {
                $query->whereDate('created_at', '<=', $this->parseFormat($request->until));
            })
            ->when($request->has('action_class') && $request->action_class > 0, function ($query) use ($request) {
                $query->where('action_class', $request->action_class);
            })
            ->when($request->has('type') && $request->type > 0, function ($query) use ($request) {
                $query->where('type', $request->type);
            })
            ->when($request->has('status') && $request->status >= 0, function ($query) use ($request) {
                $query->where('status', $request->status);
            })
            ->when($request->has('type') && $request->type > 0, function ($query) use ($request) {
                $query->where('type', $request->type);
            })
            ->when($request->has('action_class') && $request->action_class > 0, function ($query) use ($request) {
                $query->where('action_class', $request->action_class);
            })
            ->whereHas('product_warehouse', function ($query2) use ($warehouses_ids) {
                $query2->whereIn('warehouse_id', $warehouses_ids);
            })
            ->when($request->role === 5, function ($query) use ($request) {
                $query->where('creator_id', $request->user_id);
            })
            ->orderBy('id', 'DESC');

        $query = $movements->paginate(10);

        return $query;
    }
}
