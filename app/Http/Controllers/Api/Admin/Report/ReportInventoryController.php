<?php

namespace App\Http\Controllers\Api\Admin\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Product;
use App\Models\Warehouse;
use App\Models\ProductWarehouse;
use App\Models\ProductWarehouseMovement;
use App\Models\Category;
use App\Models\ProductCategory;
use App\Models\ProductSubCategory;


use Carbon\Carbon;

class ReportInventoryController extends Controller
{
    public function reportInventory(Request $request){
		/*
		*	check category name and if that category_id has a relation with any products_id, and bring me those products
		*/

		$products_ids = Product::where('user_id', $request->user_id)->select('id')->get()->pluck('id');

		$products_warehouse = ProductWarehouse::select('id', 'product_id', 'warehouse_id', 'status','price','price_max','price_offer', 'cost')
		->whereIn('product_id', $products_ids)
		->whereHas('last_movement')
		->when($request->has('branch_id'), function ($query) use ($request) {
			return $query->whereHas('warehouse', function($rel) use ($request) {
				$rel->where('branch_id', $request->branch_id);
			});
		})
		->when($request->has('warehouse_id'), function ($query) use ($request) {
			return $query->whereHas('warehouse', function($rel) use ($request) {
				$rel->when(count($request->warehouse_id) > 1, function($query2) use ($request){
					$query2->whereIn('warehouse_id', $request->warehouse_id);
				}, function($query2) use ($request){
					$query2->where('warehouse_id', $request->warehouse_id);
				});
			});
		})
		->when($request->has('category_id'), function ($query) use ($request) {
			return $query->whereHas('product_category', function($rel) use ($request) {
				$rel->where('category_id', $request->category_id);
			});
		})
		->when($request->has('subcategory_id'), function ($query) use ($request) {
			return $query->whereHas('product_subcategory', function($rel) use ($request) {
				$rel->where('subcategory_id', $request->subcategory_id);
			});
		})
		->when($request->has('brand_id'), function ($query) use ($request) {
			return $query->whereHas('product_brand', function($rel) use ($request) {
				$rel->where('brand_id', $request->brand_id);
			});
		})
		->when($request->has('model_id'), function ($query) use ($request) {
			return $query->whereHas('product_model', function($rel) use ($request) {
				$rel->where('model_id', $request->model_id);
			});
		})
		->when($request->has('search'), function ($query) use ($request) {
			return $query->whereHas('product', function($q) use ($request) {
				$q->where('name','like','%'.$request->search.'%')
					->orWhere('code','like','%'.$request->search.'%')
					->orWhere('description','like','%'.$request->search.'%');
			});
		})
		->with([
			'product' => function($q) {
				$q->select('id', 'name', 'code');
			}, 
			'last_movement' => function($q) {
				$q->where('status', 1);
			},
			'committed_stock',
			'warehouse' => function ($query) {
                $query->with(['branch' => function ($query2) {
                    $query2->withTrashed();
                }])->withTrashed();
            }
		]);

		$response = $products_warehouse->paginate(10);
		
		return response()->json([
			'products_warehouse' => $response
		]);
	}
}
