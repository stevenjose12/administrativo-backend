<?php

namespace App\Http\Controllers\Api\Admin\Payment;

use App\Http\Controllers\Controller;
use App\Models\EnterpriseUser;
use App\Models\ProcessedPayment;
use App\Models\ProcessedRequest;
use App\Models\UserBalance;
use App\Models\UserPayment;
use App\Models\BankBalance;
use App\Models\RequestUserPayment;
use App\User;
use Illuminate\Http\Request;
use Image;
use Validator;
use App\Http\Traits\WarehouseFilter;
use App\Http\Traits\Format;

define('ENTRY', 1);
define('OUTPUT', 2);

define('PAY', 5);

define('TYPE_CHARGES', 6);
define('TYPE_ON_CREDIT', 5);
define('TRANSFER', 4);
define('ACTION_DELIVERY', 3);
define('PAYMENT_EXPENSES', 2);
define('PAYMENT_INCOME', 1);

define('WITHOUT_PAYING', 0);
define('PAYING', 1);

define('APP_PAYMENT', 'App\Models\UserPayment');
define('APP_REQUEST', 'App\Models\ProcessedRequest');

define('NOT_PROCESSED', 0);
define('PROCESSED', 1);

class PaymentController extends Controller
{
    use Format, WarehouseFilter;

    public function getCustomersWithDebts(Request $request)
    {
        $customers = User::with([
            'request' => function ($query) {
                $query->where('status', WITHOUT_PAYING);
            },
            'person',
        ])->whereHas('client_enterprise', function ($query) use ($request) {
            $query->where('enterprise_id', $request->Id);
        })->get();

        return $customers;
    }

    public function get(Request $request)
    {
        $warehouses_ids =  $this->warehouseFilter($request->Id, $request->user_id, $request->role, $request->branch_id, $request->warehouse_id);

        $customers = User::with([
            'configuration_client',
            'request' => function ($query) use ($request, $warehouses_ids) {
                $query->with([
                    'method_payment' => function ($query) {
                        $query->where('type', '!=', TYPE_ON_CREDIT);
                    }, 'seller' => function ($query) {
                        $query->with(['person' => function ($query2) {
                            $query2->withTrashed();
                        }])->withTrashed();
                    }
                ])->where('status', WITHOUT_PAYING)
                ->when($request->has('role')  && $request->role === config()->get('constants.user.roles.SELLER'), function ($query2) use ($request) {
                    $query2->where('creator_id', $request->user_id);
                })
                ->whereIn('warehouse_id', $warehouses_ids);
            },
            'accumulated_amount' => function ($query) {
                $query->select('user_id', 'action_class', 'type', 'current_amount', 'amount', 'status')->where([
                    ['action_class', PAYMENT_INCOME],
                ]);
            },
            'user_payments' => function ($query) {
                $query->where([
                    ['status', WITHOUT_PAYING],
                ])
                ->with(['creator' => function ($query) {
                    $query->with('person')->withTrashed();
                }]);
            },
            'person' => function ($query) {
                $query->withTrashed();
            },
        ])
            ->whereHas('request', function ($query) use ($warehouses_ids) {
                $query->whereIn('warehouse_id', $warehouses_ids);
            })
            ->when($request->has('client') && $request->client > 0, function ($query) use ($request) {
                $query->whereHas('request', function ($query) use ($request) {
                    $query->where('client_id', $request->client);
                });
            })
            ->when($request->has('zone_id') && $request->zone_id > 0, function ($query) use ($request) {
                $query->whereHas('request', function ($query) use ($request) {
                    $query->where('zone_id', $request->zone_id);
                });
            })
            ->when($request->has('seller') && $request->seller > 0, function ($query) use ($request) {
                $query->whereHas('request', function ($query) use ($request) {
                    $query->where('creator_id', $request->seller);
                });
            })
            ->when($request->has('role')  && $request->role === config()->get('constants.user.roles.SELLER'), function ($query) use ($request) {
                $query->whereHas('request', function ($query) use ($request) {
                    $query->where('creator_id', $request->user_id);
                });
            });


        $query = $customers->whereHas('client_enterprise', function ($query) use ($request) {
            $query->where('enterprise_id', $request->Id);
        })->paginate(10);

        return $query;
    }

    private function ValidPayment(array $data)
    {
        $attributes = [
            'client_id' => 'cliente',
            'bank_id' => 'banco',
            'bank_account_id' => 'cuenta bancaria',
            'method_payment' => 'método de pago',
            'amount' => 'monto',
            'voucher' => 'comprobante',
            'creator_id' => 'empresa',
        ];

        $validator = Validator::make($data, [
            'client_id' => ['required'],
            'bank_id' => ['required'],
            'bank_account_id' => ['required'],
            'method_payment' => ['required'],
            'amount' => ['required', 'numeric', 'min:0'],
            'creator_id' => ['required'],
        ], [
            "required" => "El campo :attribute es obligatorio.",
            "string" => "El campo :attribute debe ser una cadena de caracteres.",
            "numeric" => ":attribute debe ser numérico.",
            "min" => "El campo :attribute debe ser mayor de :min"
        ]);

        $validator->setAttributeNames($attributes);

        return $validator;
    }

    public function ValidFile(array $data)
    {
        $attributes = [
            'voucher' => 'comprobante de pago',
        ];

        $validator = Validator::make($data, [
            'voucher' => ['required'],
        ], [
            "required" => "El archivo :attribute es obligatorio.",
        ]);

        $validator->setAttributeNames($attributes);

        return $validator;
    }

    public function createPayment(Request $request)
    {
        $this->ValidPayment($request->all())->validate();

        if ((int) $request->method_payment === TRANSFER) {
            $this->ValidFile($request->all())->validate();
        }

        $payment = new UserPayment();
        $payment->user_id = $request->client_id;

        if ($request->hasFile('voucher')) {
            $name = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 40);

            Image::make($request->voucher)->save('img/payments/' . $name . '.jpg');
            $payment->image = 'img/payments/' . $name . '.jpg';
        }

        $payment->fill($request->except(['user_id', 'image','type']));
        // $payment->type = (Int) $request->method_payment;
        $payment->fill(['type' => $request->method_payment]);
        $payment->save();

        $current_bank_balance = BankBalance::where('bank_account_id', $request->bank_account_id)->orderBy('id', 'desc');
        if ($current_bank_balance->exists()) {
            $current_bank_balance = $current_bank_balance->first()->total;
        } else {
            $current_bank_balance = 0;
        }

        $bank_balance = new BankBalance;
        $bank_balance->bank_id = $request->bank_id;
        $bank_balance->bank_account_id = $request->bank_account_id;
        $bank_balance->action_id = $payment->id;
        $bank_balance->action_class = 1;
        $bank_balance->action_type = APP_PAYMENT;
        $bank_balance->type = ENTRY;
        $bank_balance->amount = $request->amount;
        $bank_balance->current_amount = $current_bank_balance;
        $bank_balance->total = $current_bank_balance + $request->amount;
        $bank_balance->description = "Ingreso por pago de cliente";
        $bank_balance->creator_id = $request->creator_id;
        $bank_balance->operation_number = $request->reference_number;
        $bank_balance->save();

        $user_balance = UserBalance::where([
            ['user_id', $payment->user_id],
            ['action_class', PAYMENT_INCOME],
        ])->get()->last();

        $current_amount = 0;

        if (!empty($user_balance)) {
            $current_amount = $this->defineEntryAmountBalance($user_balance);
        }

        $balance = new UserBalance;
        $balance->action_id = $payment->id;
        $balance->action_class = PAYMENT_INCOME;
        $balance->action_type = APP_PAYMENT;
        $balance->user_id = $payment->user_id;
        $balance->type = ENTRY;
        $balance->current_amount = $current_amount;
        $balance->amount = (float) $payment->amount;
        $balance->total = $current_amount + $payment->amount;
        $balance->creator_id = $request->creator_id;
        $balance->save();

        return $payment;
    }

    public function processing(Request $request)
    {
        $expenses = $request->expenses;

        if (count($expenses) <= 0) {
            return response()->json([
                'message' => 'No selecciono ninguna deuda al procesar la cuenta.',
            ], 422);
        } elseif ($request->amount_available <= 0) {
            return response()->json([
                'message' => '¡Disculpe, no tiene un monto a favor para procesar esta cuenta!',
            ], 422);
        }

        foreach ($expenses as $key => $row) {
            $processing = ProcessedRequest::findOrFail($row['id']);

            $processing->method_payment()->saveMany([
                new ProcessedPayment(['type' => TYPE_CHARGES, 'amount' => $row['toPay'], 'processed' => PROCESSED, 2]),
            ]);

            $processing->status = $row['payed'];
            $countNotProcessedPayments = ProcessedPayment::where('processed_request_id', $processing->id)
                ->where('processed', NOT_PROCESSED)->count();
            if ($countNotProcessedPayments == 0) {
                $processing->processed = $row['payed'];
            }

            $processing->save();
            
            foreach ($row['usedPayments'] as $usedPayment) {
                $currentPayment = UserPayment::where('id', $usedPayment['id'])->first();
                $currentPayment->current_amount = $usedPayment['available_amount'];
                $currentPayment->status = $usedPayment['status'];
                $currentPayment->save();

                $requestPayment = new RequestUserPayment;
                $requestPayment->processed_request_id = $processing->id;
                $requestPayment->user_payment_id = $currentPayment->id;
                $requestPayment->creator_id = $request->creator_id;
                $requestPayment->type = $usedPayment['full_payment'] ? 2 : 1;
                $requestPayment->amount = $usedPayment['used_amount'];
                $requestPayment->save();
            }

            $user_balance = UserBalance::where([
                ['user_id', $request->user_id],
                ['action_class', 1],
            ])->get()->last();

            $current_amount = 0;

            if (!empty($user_balance)) {
                $current_amount = $this->defineAmountBalance($user_balance);
                if ($current_amount >= $user_balance->total) {
                    $user_balance->status = 1;
                    $user_balance->save();
                }
            }

            $balance = new UserBalance;
            $balance->action_id = $processing->id;
            $balance->action_class = ENTRY;
            $balance->action_type = APP_REQUEST;
            $balance->user_id = $request->user_id;
            $balance->type = OUTPUT;
            $balance->current_amount = $current_amount;
            $balance->amount = $row['toPay'];
            $balance->total = $row['toPay'] - $current_amount;
            $balance->creator_id = $request->creator_id;
            $balance->save();
        }

        return;
    }

    public function report(Request $request)
    {
        $users_ids = [];

        if ($request->has('role') && $request->role != config('constants.user.roles.SELLER')) {
            $users_ids = EnterpriseUser::where('enterprise_id', $request->Id)->select('user_id')->get()->pluck('user_id')->toArray();
            $users_ids[] = $request->Id;
        } else {
            $users_ids[] = $request->user_id;
        }

        $payments = UserPayment::with(['user' => function ($query) {
            $query->with(['person' => function ($query2) {
                $query2->withTrashed();
            }])->withTrashed();
        },
            'expenses' => function ($query) {
                $query->with('processed_request');
            }
        ])
        ->when($request->has('status'), function ($query) use ($request) {
            $query->where('status', $request->status);
        })
        ->when($request->has('client') && $request->client > 0, function ($query) use ($request) {
            $query->where('user_id', $request->client);
        })
        ->when($request->has('since') && $request->has('until'), function ($query) use ($request) {
            $query->whereBetween('created_at', [$this->parseFormatDateTime($request->since, 'start'), $this->parseFormatDateTime($request->until, 'end')]);
        })
        ->when($request->has('since') && !$request->has('until'), function ($query) use ($request) {
            $query->whereDate('created_at', '>=', $this->parseFormat($request->since));
        })
        ->when(!$request->has('since') && $request->has('until'), function ($query) use ($request) {
            $query->whereDate('created_at', '<=', $this->parseFormat($request->until));
        })
        ->whereIn('creator_id', $users_ids)->orderBy('created_at', 'desc');

        return $payments->paginate(10);
    }

    protected function defineAmountBalance($user_balance)
    {
        return $user_balance->type == PAYMENT_EXPENSES ? $user_balance->current_amount - $user_balance->amount : $user_balance->current_amount + $user_balance->amount;
    }

    protected function defineEntryAmountBalance($user_balance)
    {
        return $user_balance->type == PAYMENT_EXPENSES ? $user_balance->current_amount - $user_balance->amount : $user_balance->current_amount + $user_balance->amount;
    }
}
