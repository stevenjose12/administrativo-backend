<?php

namespace App\Http\Controllers\Api\Admin\Sale;

use App\Http\Controllers\Controller;
use App\Http\Traits\ProductSerial;
use App\Models\ConfigEnterprise;
use App\Models\EnterpriseUser;
use App\Models\ProcessedPayment;
use App\Models\ProcessedRequest;
use App\Models\ProductWarehouse;
use App\Models\ProductWarehouseMovement;
use App\Models\ProductSerial as Serial;
use App\User;
use App\Models\Person;
use Carbon\Carbon;
use Illuminate\Http\Request;
use PDF;
use Validator;

define('ENTRY', 1);
define('OUTPUT', 2);
define('TYPE_NOTE', 1);
define('TYPE_BILL', 2);
define('ACTION_SALE', 5);
define('APP', 'App\Models\ProcessedRequest');
define('APP_ORDER', 'App\Models\ProcessedOrder');

define('TYPE_ON_CREDIT', 5);

define('WITHOUT_PAYING', 0);
define('PAID', 1);
class SaleController extends Controller
{
    use ProductSerial;
    

    public function generateId(Request $request)
    {
        $users_ids = EnterpriseUser::where('enterprise_id', $request->Id)->select('user_id')->get()->pluck('user_id')->toArray();
        array_push($users_ids, $request->Id);

        $count = ProcessedRequest::whereIn('creator_id', $users_ids)->where('type', $request->sale_type)->count();

        $Id = str_pad(1, 9, 0, STR_PAD_LEFT);

        if ($count > 0) {
            $Id = str_pad($count + 1, 9, 0, STR_PAD_LEFT);
        }

        return $Id;
    }

    private function ValidRequest(array $data)
    {
        $attributes = [
            'client_id' => 'cliente',
            'warehouse_id' => 'almacen',
            'code' => 'código',
        ];

        $validator = Validator::make($data, [
            'client_id' => ['required', 'numeric'],
            'warehouse_id' => ['required', 'numeric'],
            'code' => ['required', 'string'],
        ], [
            "required" => "El campo :attribute es obligatorio.",
            "string" => "El campo :attribute debe ser una cadena de caracteres.",
            "numeric" => ":attribute debe ser numérico.",
        ]);

        $validator->setAttributeNames($attributes);

        return $validator;
    }

    private function ValidQuantityActually($products, $warehouse_id)
    {
        foreach ($products as $key => $product) {
            $check_pw = $this->ProductWarehouse($product['id'], $warehouse_id);

            $check_pwm = $this->ProductWarehouseMovement($check_pw->id);

            if (!$check_pwm->exists()) {
                return [
                    'message' => "¡El producto " . $product['name'] . " no se encuentra registrado en el almacen!",
                ];
            }

            $current = $check_pwm->get()->last();

            // SETEANDO EL STOCK ACTUAL DEL PRODUCTO DE ACUERDO AL ULTIMO MOVIMIENTO DEL MISMO EN EL ALMACEN
            $stock = $current->type == ENTRY ? $current->current_stock + $current->amount : $current->current_stock - $current->amount;
            // VALIDANDO SI EL STOCK ACTUAL DEL PRODUCTO ES SUFICIENTE PARA SU EXTRACCION
            $stock = $stock - $product['quantity'];

            if ($stock < 0) {
                return [
                    'message' => "¡El producto " . $product['name'] . " rebasa la cantidad en stock!",
                ];
            }
        }
    }

    public function createSale(Request $request)
    {
        $this->ValidRequest($request->all())->validate();

        if ($validator = $this->ValidQuantityActually($products = $request->products, $warehouse_id = $request->warehouse_id)) {
            return response()->json($validator, 422);
        }

        $status = collect($request->type_payments)->first(function ($value, $key) {
            return (int) $value['id'] == TYPE_ON_CREDIT;
        });

        $response = $this->checkExistSerial($request->products, $request->warehouse_id);

        if (!is_bool($response)) {
            return $response;
        }


        $client = User::with('zone_user')->find($request->client_id, ['id']);
        if(is_null($client)) {
            return response()->json([ 'message' => 'Este cliente ha sido eliminado!'], 422);
        }
        
        $processing = new ProcessedRequest;
        $processing->warehouse_id = (int) $request->warehouse_id;
        $processing->zone_id = $client->zone_user->zone_id;
        $processing->client_id = (int) $request->client_id;
        $processing->code = $request->code;
        $processing->currency_id = 1;
        $processing->type = $request->sale_type;
        $processing->status = empty($status) ? PAID : WITHOUT_PAYING;
        $processing->date_emission = Carbon::now();
        $processing->discount_percentage = $request->discount;
        $processing->taxable = $request->taxable;
        $processing->exempt = $request->exempt;
        $processing->subtotal = $request->subtotal;
        $processing->vat = $request->vat;
        $processing->total = $request->total;
        $processing->creator_id = $request->creator_id;
        $processing->save();

        if ($request->has('type_payments')) {
            foreach ($request->type_payments as $key => $type_payments) {
                $processing->method_payment()->saveMany([
                    new ProcessedPayment(['type' => $type_payments['id'], 'amount' => $type_payments['amount'], 
                    'processed' => $type_payments['processed']]),
                ]);
            }
        }
        foreach ($request->products as $key => $product) {

            $check_pw = $this->ProductWarehouse($product['id'], $warehouse_id);

            // Eliminando serialización anterior de los productos
            $this->deleteSerials($product, $check_pw->id);

            // Serializando la salida de los productos
            $this->createProductSerials($product, $check_pw->id, $processing->id, APP_ORDER, OUTPUT);

            $current = $this->ProductWarehouseMovement($check_pw->id)->get()->last();
            // SETEANDO EL STOCK ACTUAL DEL PRODUCTO DE ACUERDO AL ULTIMO MOVIMIENTO DEL MISMO EN EL ALMACEN
            $stock = $current->type == ENTRY ? $current->current_stock + $current->amount : $current->current_stock - $current->amount;

            $movement = new ProductWarehouseMovement;
            $movement->action_id = $processing->id;
            $movement->product_warehouse_id = $check_pw->id;
            $movement->product_id = $product['id'];
            $movement->action_class = ACTION_SALE;
            $movement->action_type = APP;
            $movement->type = OUTPUT;
            $movement->current_stock = $stock;
            $movement->amount = $product['quantity'];
            $movement->description = 'Venta de producto(s)';
            $movement->status = 1;
            $movement->creator_id = $request->creator_id;
            $movement->save();

            $processing->details()->attach($product['id'], [
                'quantity' => $product['quantity'],
                'discount_percentage' => $product['discount_percentage'],
                'subtotal' => $product['subtotal'],
                'vat' => $product['vat'],
                'total' => $product['total'],
            ]);

            
        }

        return $processing;
    }

    public function getSaleType(Request $request)
    {
        $sale_type = ConfigEnterprise::where('enterprise_id', $request->Id)->select('sale_type')->first();

        return $sale_type;
    }

    public function printSale($enterprise_id, $process_id)
    {
        $enterprise_ = User::with([
            'person',
            'enterprise_users'
        ])->findOrFail($enterprise_id);
        
        $enterprise_name = "";

        if($enterprise_->enterprise_users)
        {
            $enterprise_name = Person::where('user_id',$enterprise_->enterprise_users->enterprise_id)->first()->first_name;
            $enterprise = Person::where('user_id',$enterprise_->enterprise_users->enterprise_id)->first();
        }
        else {

            $enterprise_name = Person::where('user_id',$enterprise_->id)->first()->first_name;
            $enterprise = Person::where('user_id',$enterprise_->id)->first();
        }
        $file = ProcessedRequest::with([
            'details',
            'client' => function ($query) {
                $query->with('person')->withTrashed();
            },
            'method_payment',
        ])->findOrFail($process_id);
        
        $productWarehouseIds = ProductWarehouse::where('warehouse_id', $file->warehouse_id)
            ->whereIn('product_id', $file->details->pluck('id'))->get()->pluck('id');
        
        $serials = [];
        for($i = 0; $i < count($productWarehouseIds); $i++)
        {
            $serials[$i] = Serial::where('product_warehouse_id',$productWarehouseIds[$i])
                ->where('operation_id',$process_id)
                ->get()
                ->pluck('serial')->toArray();
        }
        // $serials = Serial::whereIn('product_warehouse_id',$productWarehouseIds)
        //     ->where('operation_id', $process_id)
        //     // ->where('operation_type', 'App\Models\PurchaseOrder')
        //     ->get()
        //     ->pluck('serial');

        $pdf = PDF::loadView('files.delivery_notes', ['file' => $file, 'enterprise' => $enterprise, 'serials' => $serials, 'enterprise_name' => $enterprise_name]);
        
        return $pdf->stream();
    }

    protected function ProductWarehouse($Id, $warehouse_id)
    {
        return ProductWarehouse::where([
            ['warehouse_id', $warehouse_id],
            ['product_id', $Id],
        ])->first();
    }

    protected function ProductWarehouseMovement($Id)
    {
        return ProductWarehouseMovement::where('product_warehouse_id', $Id);
    }
}
