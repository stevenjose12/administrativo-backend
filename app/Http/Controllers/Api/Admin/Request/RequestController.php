<?php

namespace App\Http\Controllers\Api\Admin\Request;

use App\Http\Requests\Requests\CreateRequest;
use App\Http\Requests\Requests\UpdateRequest;
use App\Http\Controllers\Controller;
use App\Http\Traits\Format;
use App\Http\Traits\ProductSerial;
use App\Http\Traits\WarehouseFilter;
use App\Models\EnterpriseUser;
use App\Models\ProcessedOrder as ProcessedRequest;
use App\Models\ProductWarehouse;
use App\Models\ProductWarehouseMovement;
use App\Models\Request as Delivery;
use App\Models\Warehouse;
use Illuminate\Http\Request;
use Validator;
use App\User;

define('ENTRY', 1);
define('OUTPUT', 2);
define('TYPE_NOTE', 1);
define('TYPE_BILL', 2);
define('ACTION_DELIVERY', 3);
define('STATUS_PROCESSING', 1);
define('STATUS_REJECT', 2);
define('STATUS_NO_PROCESSING', 0);
define('WITHOUT_PAYING', 0);
define('PAYING', 1);
define('APP', 'App\Models\Request');
define('COMMITTED', 3);
define('PROCESSED', 1);

class RequestController extends Controller
{
    use Format;
    use ProductSerial;
    use WarehouseFilter;

    public function getId(Request $request)
    {

        $users_ids = EnterpriseUser::where('enterprise_id', $request->Id)->select('user_id')->get()->pluck('user_id')->toArray();
        array_push($users_ids, $request->Id);

        $count = Delivery::whereIn('creator_id', $users_ids)->count();

        $Id = str_pad(1, 9, 0, STR_PAD_LEFT);

        if ($count > 0) {
            $Id = str_pad($count + 1, 9, 0, STR_PAD_LEFT);
        }

        return $Id;
    }

    public function generateId(Request $request)
    {

        $users_ids = EnterpriseUser::where('enterprise_id', $request->Id)->select('user_id')->get()->pluck('user_id')->toArray();
        array_push($users_ids, $request->Id);

        $count = ProcessedRequest::where('type', TYPE_NOTE)->whereIn('creator_id', $users_ids)->count();

        $Id = str_pad(1, 9, 0, STR_PAD_LEFT);

        if ($count > 0) {
            $Id = str_pad($count + 1, 9, 0, STR_PAD_LEFT);
        }

        return $Id;
    }

    public function generateFromControllerId($Id)
    {

        $users_ids = EnterpriseUser::where('enterprise_id', $Id)->select('user_id')->get()->pluck('user_id')->toArray();
        array_push($users_ids, $Id);

        $count = ProcessedRequest::where('type', TYPE_NOTE)->whereIn('creator_id', $users_ids)->count();

        $Id = str_pad(1, 9, 0, STR_PAD_LEFT);

        if ($count > 0) {
            $Id = str_pad($count + 1, 9, 0, STR_PAD_LEFT);
        }

        return $Id;
    }

    public function generateFromControllerIdRequest($Id)
    {

        $users_ids = EnterpriseUser::where('enterprise_id', $Id)->select('user_id')->get()->pluck('user_id')->toArray();
        array_push($users_ids, $Id);

        $count = Delivery::whereIn('creator_id', $users_ids)->count();

        $Id = str_pad(1, 9, 0, STR_PAD_LEFT);

        if ($count > 0) {
            $Id = str_pad($count + 1, 9, 0, STR_PAD_LEFT);
        }

        return $Id;
    }

    public function processedAllStatus(Request $request)
    {
        $users_ids = [];
        
        if ($request->has('role') && $request->role == config('constants.user.roles.ENTERPRISE')) {
            $users_ids = EnterpriseUser::where('enterprise_id', $request->Id)->select('user_id')->get()->pluck('user_id')->toArray();
            $users_ids[] = $request->Id;
        }

        $warehouses_ids =  $this->warehouseFilter($request->Id, $request->user_id, $request->role, $request->branch_id, $request->warehouse_id);
       
        $requests = ProcessedRequest::with([
            'operation' => function ($query) {
                $query->withTrashed()->where('type', COMMITTED)->select('operation_id', 'operation_type', 'product_warehouse_id', 'serial')
                    ->with(['product_warehouse' => function ($query) {
                        $query->select('id', 'product_id');
                    }]);
            },
            'client' => function ($query) {
                $query->with('person')->withTrashed();
            },
            'details' => function ($query) {
                $query->with([
                    'product_warehouse.warehouse' => function ($query) {
                        $query->select('id', 'branch_id', 'name', 'code', 'deleted_at');
                    },
                    'product_warehouse.last_movement',
                ])->withTrashed();
            },
            'currency',
            'method_payment',
            'seller' => function ($query) {
                $query->with([
                    'person' => function ($query) {
                        $query->withTrashed();
                    },
                ])->withTrashed();
            },
        ])
            ->when($request->has('zone_id') && $request->zone_id > 0, function ($query) use ($request) {
                $query->where('zone_id', $request->zone_id);
            })
            ->when($request->has('seller') && $request->seller > 0, function ($query) use ($request) {
                $query->where('creator_id', $request->seller);
            })
            ->when($request->has('client') && $request->client > 0, function ($query) use ($request) {
                $query->where('client_id', $request->client);
            })
            ->when($request->has('since') && $request->has('until'), function ($query) use ($request) {
                $query->whereBetween('date_emission', [$this->parseFormatDateTime($request->since, 'start'), $this->parseFormatDateTime($request->until, 'end')]);
            })
            ->when($request->has('since') && !$request->has('until'), function ($query) use ($request) {
                $query->whereDate('date_emission', '>=', $this->parseFormat($request->since));
            })
            ->when(!$request->has('since') && $request->has('until'), function ($query) use ($request) {
                $query->whereDate('date_emission', '<=', $this->parseFormat($request->until));
            })
            ->when($request->has('code'), function ($query) use ($request) {
                $query->where('code', 'LIKE', '%' . $request->code . '%');
            })
            ->when(!$request->has('since') && !$request->has('until'), function ($query) {
                $query->whereDate('date_emission', \Carbon\Carbon::now()->format('Y-m-d'));
            })
            ->whereIn('warehouse_id', $warehouses_ids);

        if ($request->role == 3) {
            $requests->whereIn('creator_id', $users_ids);
        } elseif ($request->role == 5) {
            $requests->where('creator_id', $request->user_id);
        }

        $query = $requests->orderBy('code', 'DESC')->orderBy('id', 'DESC')->paginate(15);

        return $query;
    }

    public function requests(Request $request)
    {

        $warehouses_ids =  $this->warehouseFilter($request->Id, $request->user_id, $request->role, $request->branch_id, $request->warehouse_id);
        
        $requests = Delivery::with([
            'operation' => function ($query) {
                $query->withTrashed()->where('type', COMMITTED)->select('operation_id', 'operation_type', 'product_warehouse_id', 'serial')
                    ->with(['product_warehouse' => function ($query) {
                        $query->select('id', 'product_id');
                    }]);
            },
            'client' => function ($query) {
                $query->with('person')->withTrashed();
            },
            'details' => function ($query) {
                $query->with([
                    'product_warehouse.warehouse' => function ($query) {
                        $query->select('id', 'branch_id', 'name', 'code', 'deleted_at');
                    },
                    'product_warehouse.last_movement',
                ]);
            },
            'creator' => function ($query) {
                $query->with('person')->withTrashed();
            },
            'currency',
            'seller' => function ($query) {
                $query->with([
                    'person' => function ($query) {
                        $query->withTrashed();
                    },
                ])->withTrashed();
            },
        ])
            ->when($request->has('client') && $request->client > 0, function ($query) use ($request) {
                $query->where('client_id', $request->client);
            })
            ->when($request->has('since') && $request->has('until'), function ($query) use ($request) {
                $query->whereBetween('date_emission', [$this->parseFormatDateTime($request->since, 'start'), $this->parseFormatDateTime($request->until, 'end')]);
            })
            ->when($request->has('since') && !$request->has('until'), function ($query) use ($request) {
                $query->whereDate('date_emission', '>=', $this->parseFormat($request->since));
            })
            ->when(!$request->has('since') && $request->has('until'), function ($query) use ($request) {
                $query->whereDate('date_emission', '<=', $this->parseFormat($request->until));
            })
            ->when($request->has('code'), function ($query) use ($request) {
                $query->where('code', 'LIKE', '%' . $request->code . '%');
            })
            ->when($request->has('status') && $request->status >= 0, function ($query) use ($request) {
                $query->where('status', $request->status);
            })
            ->when($request->has('seller') && $request->seller > 0, function ($query) use ($request) {
                $query->where('creator_id', $request->seller);
            })
            ->when($request->has('role') && $request->role == config('constants.user.roles.SELLER'), function ($query) use ($request) {
                $query->where('creator_id', $request->user_id);
            })->whereIn('warehouse_id', $warehouses_ids)->orderBy('id', 'DESC')->paginate(10);

        return $requests;
    }

    private function ValidRequest(array $data)
    {
        $attributes = [
            'client_id' => 'cliente',
            'warehouse_id' => 'almacen',
            'code' => 'código',
            'currency_id' => 'divisa',
            'date_emission' => 'fecha de emisión',
            'observations' => 'observaciones',
            'subtotal' => 'subtotal',
            'vat' => 'impuesto al valor agregado',
            'total' => 'total',
            'creator_id' => 'usuario',
        ];

        $validator = Validator::make($data, [
            'client_id' => ['required', 'numeric'],
            'warehouse_id' => ['required', 'numeric'],
            'code' => ['required', 'string'],
            'currency_id' => ['required', 'numeric'],
            'date_emission' => ['required'],
            'observations' => ['required'],
            'subtotal' => ['required'],
            'vat' => ['required'],
            'total' => ['required'],
            'creator_id' => ['required'],
        ], [
            "required" => "El campo :attribute es obligatorio.",
            "string" => "El campo :attribute debe ser una cadena de caracteres.",
            "numeric" => ":attribute debe ser numérico.",
        ]);

        $validator->setAttributeNames($attributes);

        return $validator;
    }

    private function ValidQuantityActually($products, $warehouse_id)
    {
        foreach ($products as $key => $product) {
            $check_pw = ProductWarehouse::where([
                ['warehouse_id', $warehouse_id],
                ['product_id', $product['id']],
            ])->first();

            $check_pwm = ProductWarehouseMovement::where('product_warehouse_id', $check_pw->id);

            if (!$check_pwm->exists()) {
                return [
                    'message' => "¡El producto " . $product['name'] . " no se encuentra registrado en el almacen!",
                ];
            }

            $current = $check_pwm->get()->last();

            // SETEANDO EL STOCK ACTUAL DEL PRODUCTO DE ACUERDO AL ULTIMO MOVIMIENTO DEL MISMO EN EL ALMACEN
            $stock = $current->type == ENTRY ? $current->current_stock + $current->amount : $current->current_stock - $current->amount;
            // VALIDANDO SI EL STOCK ACTUAL DEL PRODUCTO ES SUFICIENTE PARA SU EXTRACCION
            $stock = $stock - $product['quantity'];

            if ($stock < 0) {
                return [
                    'message' => "¡El producto " . $product['name'] . " rebasa la cantidad en stock!",
                    'stock' => $stock
                ];
            }
        }
    }

    private function checkDeliveryDetail($delivery, $detail, $request)
    {
        foreach ($request->products as $product) {
            if ($detail->pivot->quantity != $product['quantity']) {
                $pw = ProductWarehouse::where(['product_id' => $product['id'], 'warehouse_id' => $product['warehouse_id']])->with('last_movement')->select('id')->first();

                $movement_type = $detail->pivot->quantity < $product['quantity'] ? OUTPUT : ENTRY;

                if ($movement_type == OUTPUT) {

                    $currentStock = $pw->last_movement->type == ENTRY ? $pw->last_movement->current_stock + $pw->last_movement->amount : $pw->last_movement->current_stock - $pw->last_movement->amount;
                    // VALIDANDO SI EL STOCK ACTUAL DEL PRODUCTO ES SUFICIENTE PARA SU EXTRACCION
                    $difference = abs(intval($product['quantity']) - intval($detail->pivot->quantity));
                    $stock = $currentStock - $difference;

                    if ($stock <= 0) {
                        return [
                            'msg' => "¡La cantidad a cargar supera el stock actual del producto: *" . $product['name'] . "*!",
                        ];
                    }
                }
            }
        }
    }

    private function processDeliveryDetail($delivery, $detail, $request)
    {
        foreach ($request->products as $product) {
            if ($product['id'] == $detail->id) {
                if ($detail->pivot->quantity != $product['quantity']) {
                    $pw = ProductWarehouse::where(['product_id' => $product['id'], 'warehouse_id' => $product['warehouse_id']])->with('last_movement')->select('id')->first();

                    $movement_type = $detail->pivot->quantity < $product['quantity'] ? OUTPUT : ENTRY;
                    $currentStock = $pw->last_movement->type == ENTRY ? $pw->last_movement->current_stock + $pw->last_movement->amount : $pw->last_movement->current_stock - $pw->last_movement->amount;
                    $difference = abs(intval($product['quantity']) - intval($detail->pivot->quantity));

                    $new_pwm = new ProductWarehouseMovement;
                    $new_pwm->action_id = $delivery->id;
                    $new_pwm->product_warehouse_id = $pw->id;
                    $new_pwm->product_id = $product['id'];
                    $new_pwm->action_class = ACTION_DELIVERY;
                    $new_pwm->action_type = APP;
                    $new_pwm->type = $movement_type;
                    $new_pwm->current_stock = $currentStock;
                    $new_pwm->amount = $difference;
                    $new_pwm->description = 'Ajuste por modificacion al pedido';
                    $new_pwm->status = STATUS_PROCESSING;
                    $new_pwm->creator_id = $request->creator_id;
                    $new_pwm->save();

                }
            }
        }
    }

    public function createRequest(CreateRequest $request)
    {

        if ($validator = $this->ValidQuantityActually($products = $request->products, $warehouse_id = $request->warehouse_id)) {
            return response()->json($validator, 422);
        }

        $response = $this->checkExistSerial($request->products, $request->warehouse_id);

        if (!is_bool($response)) {
            return $response;
        }

        $delivery = new Delivery();
        $delivery->code = $this->generateFromControllerIdRequest($request->enterprise_id);

        if ($request->forthright) {
            $delivery->status = STATUS_PROCESSING;
        } else {
            $delivery->status = STATUS_NO_PROCESSING;
        }
        
        /**
         * Inicio refactorizar
         * se le restan 4 horas a la fecha de emisión que viene de $request
         * se debe agregar un timezone para no tener que restar 4 horas a cualquier fecha que venga de
         * $request
         */
        $date_emission = \Carbon\Carbon::createFromTimeString($request->date_emission,"America/Curacao")
            ->subHours(4);
        $delivery->date_emission = $date_emission;
        /**
         * Fin refactorizar
         */

        $delivery->fill($request->except(['code', 'date_emission', 'status']))->save();

        if ($request->forthright) {
            $processing = $this->processingRequest($delivery, $request);
        }

        foreach ($products as $product) {

            $check_pw = ProductWarehouse::where([
                ['warehouse_id', $warehouse_id],
                ['product_id', $product['id']],
            ])->first();

            // Eliminando serialización anterior de los productos
            $this->deleteSerials($product, $check_pw->id);

            // Serializando la salida de los productos
            $this->createProductSerials($product, $check_pw->id, $delivery->id, APP, COMMITTED);

            $check_pwm = ProductWarehouseMovement::where('product_warehouse_id', $check_pw->id)->get()->last();
            // SETEANDO EL STOCK ACTUAL DEL PRODUCTO DE ACUERDO AL ULTIMO MOVIMIENTO DEL MISMO EN EL ALMACEN
            $stock = $check_pwm->type == ENTRY ? $check_pwm->current_stock + $check_pwm->amount : $check_pwm->current_stock - $check_pwm->amount;

            $movement = new ProductWarehouseMovement;
            $movement->action_id = $delivery->id;
            $movement->product_warehouse_id = $check_pw->id;
            $movement->product_id = $product['id'];
            $movement->action_class = ACTION_DELIVERY;
            $movement->action_type = APP;
            $movement->type = OUTPUT;
            $movement->current_stock = $stock;
            $movement->amount = $product['quantity'];
            $movement->description = $request->description;
            $movement->status = $request->forthright ? 1 : 0;
            $movement->creator_id = $request->creator_id;
            $movement->save();

            $delivery->details()->attach($product['id'], [
                'quantity' => $product['quantity'],
                'rate' => $product['rate'],
                'subtotal' => $product['subtotal'],
                'vat' => $product['vat'],
                'total' => $product['total'],
            ]);

            if ($request->forthright) {
                $processing->details()->attach($product['id'], [
                    'quantity' => $product['quantity'],
                    'subtotal' => $product['subtotal'],
                    'vat' => $product['vat'],
                    'total' => $product['total'],
                ]);
            }
        }

        return $delivery;
    }

    public function updateRequest(UpdateRequest $request)
    {
        $delivery = Delivery::with(['details'])->findOrFail($request->id);

        // if ($validator = $this->ValidQuantityActually($products = $request->products, $warehouse_id = $request->warehouse_id)) {
        //     return response()->json($validator, 422);
        // }

        $response = $this->checkExistSerial($request->products, $request->warehouse_id);

        if (!is_bool($response)) {
            return $response;
        }

        foreach ($delivery->details as $key => $detail) {
            if ($validator = $this->checkDeliveryDetail($delivery, $detail, $request)) {
                return response()->json($validator, 422);
            }
        }


        /**
         * Inicio refactorizar
         * se le restan 4 horas a la fecha de emisión que viene de $request
         * se debe agregar un timezone para no tener que restar 4 horas a cualquier fecha que venga de
         * $request
         */
        $date_emission = \Carbon\Carbon::createFromTimeString($request->date_emission,"America/Curacao")
            ->subHours(4);
        $delivery->date_emission = $date_emission;
        /**
         * Fin refactorizar
         */

        $delivery->observations = $request->observations;
        $delivery->status = STATUS_PROCESSING;
        $delivery->exempt = $request->exempt;
        $delivery->subtotal = $request->subtotal;
        $delivery->vat = $request->vat;
        $delivery->total = $request->total;
        $delivery->save();

        $delivery->details()->detach();

        $processing = $this->processingRequest($delivery, $request);

        foreach ($delivery->details as $key => $product) {
            $this->processDeliveryDetail($delivery, $product, $request);
        }

        foreach ($request->products as $key => $product) {

            
            $check_pw = ProductWarehouse::where([
                ['warehouse_id', $request->warehouse_id],
                ['product_id', $product['id']],
            ])->first();

            // Eliminando serialización anterior de los productos
            $this->deleteSerials($product, $check_pw->id);

            // Serializando la salida de los productos
            $this->createProductSerials($product, $check_pw->id, $delivery->id, APP, COMMITTED);
            

            $delivery->details()->attach($product['id'], [
                'quantity' => $product['quantity'],
                'rate' => $product['rate'],
                'subtotal' => $product['subtotal'],
                'vat' => $product['vat'],
                'total' => $product['total'],
            ]);

            $processing->details()->attach($product['id'], [
                'quantity' => $product['quantity'],
                'subtotal' => $product['subtotal'],
                'vat' => $product['vat'],
                'total' => $product['total'],
            ]);
        }

        return $delivery;
    }

    protected function processingRequest($delivery, $request)
    {

        $transfer_movements = ProductWarehouseMovement::where('action_id', $delivery->id)->where('action_class', ACTION_DELIVERY)->where('status', 0);
        $transfer_movements->update(['status' => 1]);

        $client = User::with('zone_user')->withTrashed()->findOrFail($delivery->client_id);

        $processing = new ProcessedRequest;
        $processing->request_order_id = $delivery->id;
        $processing->warehouse_id = $delivery->warehouse_id;
        $processing->client_id = $delivery->client_id;
        $processing->code = $this->generateFromControllerId($request->enterprise_id);
        $processing->currency_id = $delivery->currency_id;
        $processing->status = WITHOUT_PAYING;
        $processing->type = TYPE_NOTE;
        $processing->status = 0;
        $processing->processed = PROCESSED;
        $processing->date_emission = $delivery->date_emission;
        $processing->observations = $request->observations;
        $processing->taxable = $request->taxable;
        $processing->exempt = $request->exempt;
        $processing->subtotal = $request->subtotal;
        $processing->vat = $request->vat;
        $processing->total = $request->total;
        $processing->creator_id = $request->creator_id;
        $processing->zone_id = $client->zone_user->zone_id;
        $processing->save();

        // Eliminando serialización comprometida y generando serialización de salida
        $this->changeSerials($delivery->id, APP, true, $processing->id, 'App\Models\ProcessedOrder');

        return $processing;
    }

    public function rejectRequest(Request $request)
    {
        $delivery = Delivery::with(['details'])->findOrFail($request->Id);
        $delivery->status = STATUS_REJECT;
        $delivery->save();

        $transfer_movements = ProductWarehouseMovement::where('action_id', $request->Id)->where('action_class', ACTION_DELIVERY)->where('status', 0);
        $transfer_movements->update(['status' => 1]);

        foreach ($delivery->details as $product) {

            $check_pw = ProductWarehouse::where([
                ['warehouse_id', $delivery->warehouse_id],
                ['product_id', $product->id],
            ])->first();

            $check_pwm = ProductWarehouseMovement::where('product_warehouse_id', $check_pw->id)->get()->last();

            // SETEANDO EL STOCK ACTUAL DEL PRODUCTO DE ACUERDO AL ULTIMO MOVIMIENTO DEL MISMO EN EL ALMACEN
            $stock = $check_pwm->type == ENTRY ? $check_pwm->current_stock + $check_pwm->amount : $check_pwm->current_stock - $check_pwm->amount;

            $movement = new ProductWarehouseMovement;
            $movement->action_id = $delivery->id;
            $movement->product_warehouse_id = $check_pw->id;
            $movement->product_id = $product->id;
            $movement->action_class = ACTION_DELIVERY;
            $movement->action_type = APP;
            $movement->type = ENTRY;
            $movement->current_stock = $stock;
            $movement->amount = $product->pivot->quantity;
            $movement->description = 'Devolución de pedido';
            $movement->status = 1;
            $movement->creator_id = $delivery->creator_id;
            $movement->save();
        }

        // Eliminando serialización comprometida
        $this->changeSerials($request->Id, APP);

        return $delivery;
    }
}
