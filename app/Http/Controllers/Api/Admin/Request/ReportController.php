<?php

namespace App\Http\Controllers\Api\Admin\Request;

use App\Http\Controllers\Controller;
use App\Http\Traits\Format;
use App\Http\Traits\NumberFormat;
use App\Models\ProcessedRequest as Report;
use Excel;
use Illuminate\Http\Request;
use App\Models\EnterpriseUser;
use App\Models\Warehouse;

define('TYPE_CASH', 1);
define('TYPE_CREDIT', 2);
define('TYPE_DEBIT', 3);
define('TYPE_TRANSFER', 4);
define('TYPE_DEADLINES', 5);

class ReportController extends Controller
{
    use Format, NumberFormat;

    public function export_report(Request $request)
    {
        $users_ids = [];
        $warehouses_ids = [];

        if ($request->has('branch_id') && (int) $request->branch_id > 0 && $request->has('warehouse_id') && (int) $request->warehouse_id == 0) {
            $warehouses_ids = Warehouse::where('branch_id', $request->branch_id)->select('id')->get()->pluck('id')->toArray();
        }

        if ($request->has('role') && $request->role == 3) {
            $users_ids = EnterpriseUser::where('enterprise_id', $request->Id)->select('user_id')->get()->pluck('user_id')->toArray();
            $users_ids[] = $request->Id;
        }

        $query = Report::with([
            'client' => function ($query) {
                $query->with('person')->withTrashed();
            },
            'details' => function ($query) {
                $query->with([
                    'product_warehouse.warehouse' => function ($query) {
                        $query->select('id', 'branch_id', 'name', 'code', 'deleted_at');
                    },
                    'product_warehouse.last_movement',
                ])->withTrashed();
            },
            'currency',
            'method_payment',
            'seller' => function ($query) {
                $query->with('person')->withTrashed();
            },
        ])
            ->when($request->has('branch_id') && $request->branch_id > 0 && $request->has('warehouse_id') &&
                $request->warehouse_id == 0, function ($query) use ($warehouses_ids) {
                $query->whereIn('warehouse_id', $warehouses_ids);
            })
            ->when($request->has('seller') && $request->seller > 0, function ($query) use ($request) {
                $query->where('creator_id', $request->seller);
            })
            ->when($request->has('client') && $request->client > 0, function ($query) use ($request) {
                $query->where('client_id', $request->client);
            })
            ->when($request->has('since') && $request->has('until'), function ($query) use ($request) {
                $query->whereBetween('date_emission', [$this->parseFormatDateTime($request->since, 'start'), $this->parseFormatDateTime($request->until, 'end')]);
            })
            ->when($request->has('since') && !$request->has('until'), function ($query) use ($request) {
                $query->whereDate('date_emission', '>=', $this->parseFormat($request->since));
            })
            ->when(!$request->has('since') && $request->has('until'), function ($query) use ($request) {
                $query->whereDate('date_emission', '<=', $this->parseFormat($request->until));
            })
            ->when($request->has('code'), function ($query) use ($request) {
                $query->where('code', 'LIKE', '%' . $request->code . '%');
            })
            ->when(!$request->has('since') && !$request->has('until'), function ($query) {
                $query->whereDate('date_emission', \Carbon\Carbon::now()->format('yy-m-d'));
            });
        // ->when($request->role != 3, function ($query) use ($request) {
        //     $query->whereIn('warehouse_id', $request->warehouse_id);
        // }, function ($query) use ($request) {
        //     $query->whereHas('warehouse', function ($query2) use ($request) {
        //         $query2->where('user_id', $request->Id);
        //     });
        // })->get();

        if ($request->role == 3) {
            $query->whereIn('creator_id', $users_ids);
        } elseif ($request->role == 5) {
            $query->where('creator_id', $request->Id);
        }

        $reports = $query->orderBy('id', 'DESC')->get();

        $reports = $reports->map(function ($item, $key) {

            $item->type = $item->type == 1 ? "NE" : "FACT";
            $item->time_emission = \Carbon\Carbon::parse($item->date_emission)->format('H:i:s');
            $item->date_emission = \Carbon\Carbon::parse($item->date_emission)->format('d-m-Y');
            $item->total = $this->numberFormat(floatval($item->total), $item->currency_id);

            if (($item->method_payment->count() > 0)) {

                $cash_method = $item->method_payment->first(function ($value, $key) {
                    return $value->type == TYPE_CASH;
                });

                $credit_method = $item->method_payment->first(function ($value, $key) {
                    return $value->type == TYPE_CREDIT;
                });

                $debit_method = $item->method_payment->first(function ($value, $key) {
                    return $value->type == TYPE_DEBIT;
                });

                $transfer_method = $item->method_payment->first(function ($value, $key) {
                    return $value->type == TYPE_TRANSFER;
                });

                $deadlines_method = $item->method_payment->first(function ($value, $key) {
                    return $value->type == TYPE_DEADLINES;
                });
            }

            $item->cash = isset($cash_method) ? $this->numberFormat($cash_method->amount, $item->currency_id) : 0;
            $item->credit = isset($credit_method) ? $this->numberFormat($credit_method->amount, $item->currency_id) : 0;
            $item->debit = isset($debit_method) ? $this->numberFormat($debit_method->amount, $item->currency_id) : 0;
            $item->transfer = isset($transfer_method) ? $this->numberFormat($transfer_method->amount, $item->currency_id) : 0;
            $item->deadlines = isset($deadlines_method) ? $this->numberFormat($deadlines_method->amount, $item->currency_id) : 0;

            return $item;
        });

        $amount_cash = $reports->reduce(function ($carry, $item) {
            return $carry + floatval($item->cash);
        }, 0.00);

        $amount_credit = $reports->reduce(function ($carry, $item) {
            return $carry + floatval($item->credit);
        }, 0.00);

        $amount_debit = $reports->reduce(function ($carry, $item) {
            return $carry + floatval($item->debit);
        }, 0.00);

        $amount_transfer = $reports->reduce(function ($carry, $item) {
            return $carry + floatval($item->transfer);
        }, 0.00);

        $amount_deadlines = $reports->reduce(function ($carry, $item) {
            return $carry + floatval($item->deadlines);
        }, 0.00);

        $amount_total = $reports->reduce(function ($carry, $item) {
            return $carry + floatval($item->total);
        });

        $amounts = collect([
            'amount_cash',
            'amount_credit',
            'amount_debit',
            'amount_transfer',
            'amount_deadlines',
            'amount_total'
        ]);

        $amounts = $amounts->combine([
            $this->numberFormat($amount_cash),
            $this->numberFormat($amount_credit),
            $this->numberFormat($amount_debit),
            $this->numberFormat($amount_transfer),
            $this->numberFormat($amount_deadlines),
            $this->numberFormat($amount_total)
        ])->all();

        $file_name = 'REPORTE-' . \Carbon\Carbon::now()->format('d-m-Y') . strtoupper(str_random(10));

        Excel::create($file_name, function ($excel) use ($reports, $amounts) {
            $excel->setCreator('LimonByte')->setCompany('Adminstrativo');
            $excel->setDescription('Reporte de Ventas');
            $excel->sheet('Listado', function ($sheet) use ($reports, $amounts) {
                $sheet->loadView('reports.sales.sales')->with([
                    'reports' => $reports,
                    'amounts' => $amounts,
                ]);
            });
        })->store('xls', storage_path() . '/app/public/exports', true)['full'];

        return url('storage/exports/' . $file_name . '.xls');
    }
}
