<?php

namespace App\Http\Controllers\Api\Admin\Menu;

use App\Http\Controllers\Controller;
use App\Models\Module;
use App\User;
use Illuminate\Http\Request;

class ModuleController extends Controller
{
    public function getModules(Request $request)
    {

        if ($request->role == 1) {
            $modules = Module::with('menus')->where('user_only', 0)->orWhere('admin_only', 1)->where('status', 1)->get();
        } else if ($request->role == 2) {
            $modules = Module::with('menus')->where('admin_only', 1)->where('status', 1)->get();
        } else {
            $modules = Module::with('menus')->where('admin_only', 0)->orWhere('user_only', 1)->where('status', 1)->get();
        }

        return $modules;
    }

    public function getModuleByEnterprise(Request $request)
    {
        $modules = User::with([
            'enterprise_modules',
        ])->findOrFail($request->Id);

        return $modules->enterprise_modules;
    }

    public function getModuleBySeller(Request $request)
    {
        $modules = Module::where('seller_only', 1)->get();

        return $modules;
    }

    public function getModuleByUser(Request $request)
    {
        $modules = User::with([
            'roles',
            'enterprise_modules',
            'modules_seller',
            'role_enterprise_subuser' => function ($query) {
                $query->with('role_modules');
            },
        ])->findOrFail($request->Id);

        return $modules;
    }
}
