<?php

namespace App\Http\Controllers\Api\Admin\Role;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\RoleEnterprise;
use App\Models\RoleModule;
use App\Models\Module;
use App\User;
use Validator;
use Auth;

class RoleEnterpriseController extends Controller
{
    public function roles(Request $request)
    {
		$roles = RoleEnterprise::with([
			'user' => function($query) {
				return $query->select('id', 'name');
			},
			'user.enterprise_modules' => function ($query) {
				return $query->select('module_id as id');
			}
		])
		->select('role_enterprises.id', 'role_enterprises.enterprise_id', 'role_enterprises.name', 'role_enterprises.status', 'users.id as user_id')
		->join('users', 'users.id', '=', 'role_enterprises.enterprise_id');
	
		if ($request->role == 3) {
			$roles = RoleEnterprise::with([
				'user' => function($query) {
					$query->select('id', 'name');
				}
			])->where('enterprise_id', $request->user_id)->with(['role_modules.module'])
			->select('role_enterprises.id', 'role_enterprises.enterprise_id', 'role_enterprises.name', 'role_enterprises.status', 'users.id as user_id')
			->join('users', 'users.id', '=', 'role_enterprises.enterprise_id');
		}

      	if ($request->has('search')) {
			$roles->where(function($q) use ($request) {
				$q->where('users.name','like','%'.$request->search.'%')
				->orWhere('role_enterprises.name','like','%'.$request->search.'%');
			});
		}
        
		if ($request->has('status')) {
				$roles->where('role_enterprises.status', intval($request->status));
		}

      	$roles = $roles->paginate(10);

		return response()->json([
			'roles' => $roles,
			'result' => true,
		]);
	}
		
	public function rolesByEnterprise(Request $request)
	{
		$roles = User::with([
			'role_enterprise' => function ($query) {
				$query->where('status', 1);
			}
		])->findOrFail($request->Id);
		
		return $roles->role_enterprise;
	}

    public function enterprises()
    {
			$enterprises = User::whereHas('roles', function($q) {
				$q->where('name', 'enterprise');
			})->get();
			
			return response()->json([
				'enterprises' => $enterprises,
				'result' => true,
			]);
    }

    public function rolesCreate(Request $request)
    {
			$rules = [
				'enterprise_id' => 'required',
				'name'          => 'required',
				'modules' => [
					'required',
					'array',
					'min:1'
				],
			];

			$messages = [
				"required" => "El campo :attribute es obligatorio.",
				"min" => "Debe seleccionar al menos un :attribute"
			];

			$attributes = [
				'name' => 'Nombre',
				'enterprise_id' => 'Empresa',
				"modules" => "Modulo"
			];
        
			$validator = Validator::make($request->all(), $rules, $messages);
			$validator->setAttributeNames($attributes)->validate();

			$role = new RoleEnterprise;
			$role->enterprise_id = $request->enterprise_id;
			$role->name = $request->name;
			$role->status = 1;
			$role->save();
 
			$modulesId = $request->modules;

			$this->attachmentModules($modulesId, $role);

			return $role;
    }

    public function rolesEdit(Request $request)
    {
      $rules = [
				'name'          => ['required'],
				'enterprise_id' => ['required'],
				'modules'       => ['required', 'array', 'min:1']
			];
			$messages = [
				"required" => "El campo :attribute es obligatorio.",
			];
			$attributes = [
				'name'          => 'Nombre',
				'enterprise_id' => 'Empresa',
			];
			
			$validator = Validator::make($request->all(), $rules, $messages);
			$validator->setAttributeNames($attributes)->validate();

			$role = RoleEnterprise::where('id', $request->id)->first();
				$role->name = $request->name;
			$role->save();
			
			$modulesId = $request->modules;
			
			$this->attachmentModules($modulesId, $role);

			return $role;
    }

    public function rolesSuspend(Request $request)
    {
			$role = RoleEnterprise::findOrFail($request->Id);
			$role->status = 0;
			$role->save();
			
			return $role;
    }

    public function rolesActive(Request $request)
    {
			$role = RoleEnterprise::findOrFail($request->Id);
			$role->status = 1;
			$role->save();

			return $role;
    }

    public function rolesDelete(Request $request)
    {
			RoleModule::where('role_enterprise_id', $request->Id)->delete();
			RoleEnterprise::where('id', $request->Id)->delete();
			
			return $request->Id;
    }

    private function attachmentModules($modules, $role)
    {
			RoleModule::where('role_enterprise_id', $role->id)->delete();

			foreach ($modules as $moduleId) {
				$role_module = new RoleModule;
				$role_module->role_enterprise_id = $role->id;
				$role_module->module_id = $moduleId;
				$role_module->save();
			}
    }
}   
