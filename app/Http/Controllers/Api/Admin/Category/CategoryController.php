<?php

namespace App\Http\Controllers\Api\Admin\Category;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Validator;

class CategoryController extends Controller
{
    public function categories(Request $request)
    {
        if ($request->role == 1) {
            $categories = Category::whereNull('deleted_at')->with(['owner' => function ($query) {
                $query->with(['person' => function ($query) {
                    $query->withTrashed();
                }])->withTrashed();
            }]);
        } else if ($request->role == 2) {
            $categories = Category::where('creator_id', $request->user_id)->with(['owner' => function ($query) {
                $query->with(['person' => function ($query) {
                    $query->withTrashed();
                }])->withTrashed();
            }]);
        } else {
            $categories = Category::where('user_id', $request->user_id)->with(['owner' => function ($query) {
                $query->with(['person' => function ($query) {
                    $query->withTrashed();
                }])->withTrashed();
            }]);
        }

        if ($request->has('search')) {
            $categories->where(function ($q) use ($request) {
                $q->where('name', 'like', '%' . $request->search . '%')
                    ->orWhere('code', 'like', '%' . $request->search . '%');
            });
        }
        if ($request->has('status')) {
            $categories->where('status', $request->status);
        }

        $response = $categories->paginate(10);

        if ($request->has('select')) {
            $response = $categories->get();
        }

        return response()->json([
            'categories' => $response,
            'result' => true,
        ]);
    }

    private function ValidCategory(array $data)
    {
        $attributes = [
            'name' => 'nombre',
            'code' => 'código',
        ];

        $validator = Validator::make($data, [
            'name' => ['required', 'between:3,50'],
            'code' => ['required', 'between:1,3'],
        ], [
            'required' => 'El campo :attribute es obligatorio.',
            'between' => ':attribute tiene que estar entre :min - :max.',
        ]);

        $validator->setAttributeNames($attributes);

        return $validator;
    }

    public function categoryCreateFromCompound(Request $request)
    {
        $this->ValidCategory($request->all())->validate();

        $check_code = Category::where('user_id', $request->user_id)->where('code', $request->code)->count();
        $check_name = Category::where('user_id', $request->user_id)->where('name', $request->name)->count();

        if ($check_code > 0) {
            return response()->json([
                'message' => '¡El código de la categoría ya se encuentra registrado!',
            ], 422);
        }

        if ($check_name > 0) {
            return response()->json([
                'message' => '¡El nombre de la categoría ya se encuentra registrado!',
            ], 422);
        }

        $category = new Category();
            $category->user_id = $request->user_id;
            $category->code = $request->code;
            $category->name = $request->name;
            $category->creator_id = $request->user_id;
        $category->save();

        return $category->load('subcategories');
    }

    public function categoryCreate(Request $request)
    {
        $rules = [
            'name' => 'required|between:3,50',
            'code' => 'required|between:1,3',
        ];
        $messages = [
            "required_unless" => "El campo :attribute es obligatorio.",
            "required_if" => "El campo :attribute es obligatorio.",
            "confirmed" => 'Las contaseñas no coinciden',
        ];
        $attributes = [
            'name' => 'Nombre',
            'code' => 'Código',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        $validator->setAttributeNames($attributes);

        if ($validator->fails()) {
            return response()->json([
                'msg' => $validator->errors()->first(),
                'result' => false,
            ]);
        }

        $check_code = Category::where('user_id', $request->user_id)->where('code', $request->code)->count();
        $check_name = Category::where('user_id', $request->user_id)->where('name', $request->name)->count();

        if ($check_code > 0) {
            return response()->json([
                'msg' => 'El código de la categoría ya se encuentra registrado',
                'result' => false,
            ]);
        }

        if ($check_name > 0) {
            return response()->json([
                'msg' => 'El nombre de la categoría ya se encuentra registrado',
                'result' => false,
            ]);
        }
        /**
         * Category
         */
        $category = new Category();
        $category->user_id = $request->user_id;
        $category->name = $request->name;
        $category->code = $request->code;
        $category->creator_id = $request->creator_id;
        $category->save();

        return response()->json([
            'msg' => 'Categoría creada exitosamente',
            'result' => true,
        ]);
    }

    public function categoryEdit(Request $request)
    {
        $rules = [
            'name' => 'required|between:3,50',
            'code' => 'required|between:1,3',
        ];
        $messages = [
            "required_unless" => "El campo :attribute es obligatorio.",
            "required_if" => "El campo :attribute es obligatorio.",
            "confirmed" => 'Las contaseñas no coinciden',
        ];
        $attributes = [
            'name' => 'Nombre',
            'code' => 'Código',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        $validator->setAttributeNames($attributes);
        if ($validator->fails()) {
            return response()->json([
                'msg' => $validator->errors()->first(),
                'result' => false,
            ]);
        }

        $check_code = Category::where('id', '!=', $request->id)->where('user_id', $request->user_id)->where('code', $request->code)->count();
        if ($check_code > 0) {
            return response()->json([
                'msg' => 'El Código ya se encuentra registrado',
                'result' => false,
            ]);
        }

        $category = Category::where('id', $request->id)->first();
        $category->name = $request->name;
        $category->code = $request->code;
        $category->user_id = $request->user_id;
        $category->save();
        return response()->json([
            'msg' => 'Categoría editada exitosamente',
            'result' => true,
        ]);
    }

    public function categoryDelete(Request $request)
    {
        if (isset($request->id)) {
            $category = Category::where('id', $request->id);
            if ($category->whereDoesntHave('products')->exists()) {
                $category->delete();
                return response()->json([
                    'msg' => 'Categoría eliminada exitosamente',
                ]);
            } else {
                return response()->json([
                    'msg' => 'No puede eliminar una categoria que tenga productos asignados',
                ], 422);
            }
        }
        return response()->json([
            'msg' => 'Ha ocurrido un error',
        ], 422);
    }

    public function categorySuspend(Request $request)
    {
        if (isset($request->id)) {
            $category = Category::where('id', $request->id)->first();
            $category->status = $category->status == 1 ? 0 : 1;
            $category->save();
            return response()->json([
                'msg' => $category->status == 0 ? 'Categoría suspendida exitosamente' : 'Categoría activada exitosamente',
                'result' => true,
            ]);
        } else {
            return response()->json([
                'msg' => 'Ha ocurrido un error',
                'result' => false,
            ], 422);
        }
    }
}
