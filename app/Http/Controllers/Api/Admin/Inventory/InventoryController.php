<?php

namespace App\Http\Controllers\Api\Admin\Inventory;

use App\Http\Controllers\Controller;
use App\Models\EnterpriseUser;
use App\Models\Product;
use App\Models\ProductDetails;
use App\Models\ProductWarehouse;
use App\Models\ProductWarehouseMovement;
use App\Models\TransferWarehouse;
use App\Models\TransferWarehouseDetails;
use App\Models\Warehouse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
use App\Http\Traits\ProductSerial;
use Illuminate\Support\Collection;

define('ENTRY', 1);
define('OUTPUT', 2);
define('PRODUCT_COMPOUND', 6);
define('COMMITTED', 3);
define('TRANSLATE_MODEL', 'App\Models\TransferWarehouse');

class InventoryController extends Controller
{
    use ProductSerial;

    private static function getCode($enterprise_id)
    {
        $users_ids = EnterpriseUser::where('enterprise_id', $enterprise_id)->select('user_id')->get()->pluck('user_id')->toArray();

        array_push($users_ids, $enterprise_id);

        $count = TransferWarehouse::whereIn('enterprise_id', $users_ids)->count();

        $Id = str_pad(1, 9, 0, STR_PAD_LEFT);

        if ($count > 0) {
            $Id = str_pad($count + 1, 9, 0, STR_PAD_LEFT);
        }

        return $Id;
    }

    public function inventory(Request $request)
    {
        if (!$request->has('branch_id') && !$request->has('warehouse_id')) {
            $products_ids = Product::where('user_id', $request->user_id)->get()->pluck('id');
            $products_warehouse = ProductWarehouse::whereIn('product_id', $products_ids);
        }

        if ($request->has('branch_id') && !$request->has('warehouse_id')) {
            $warehouses_ids = Warehouse::where('branch_id', $request->branch_id)->get()->pluck('id');
            $products_warehouse = ProductWarehouse::whereIn('warehouse_id', $warehouses_ids);
        }

        if ($request->has('warehouse_id')) {
            $products_warehouse = ProductWarehouse::when(
                is_array($request->warehouse_id) && $request->warehouse_id > 1,
                function ($query) use ($request) {
                    $query->whereIn('warehouse_id', $request->warehouse_id);
                },
                function ($query) use ($request) {
                    $query->where('warehouse_id', $request->warehouse_id);
                }
            );
        }

        if ($request->has('search')) {
            $products_warehouse->whereHas('product', function ($query) use ($request) {
                $query->where('name', 'like', '%' . $request->search . '%');
            });
        }

        $products_warehouse->select('id', 'product_id', 'warehouse_id', 'status')->with([
            'serials' => function ($query) {
                $query->where('type', ENTRY);
            }, 'product' => function ($query) {
                $query->select('id', 'name', 'code');
            }, 'warehouse' => function ($query) {
                $query->select('id', 'branch_id', 'name', 'code', 'deleted_at')->with([
                    'branch' => function ($query) {
                        $query->select('id', 'name', 'code', 'deleted_at')->withTrashed();
                    }
                ])->withTrashed();
            },
            'last_movement'
        ]);

        $products = Product::where('user_id', $request->user_id)
            ->when(is_array($request->warehouse_id), function ($query) use ($request) {
                $query->whereHas('product_warehouse', function ($query2) use ($request) {
                    $query2->whereIn('warehouse_id', $request->warehouse_id);
                });
            }, function ($query) use ($request) {
                $query->where('user_id', $request->user_id);
            })->where('status', 1)->with(['product_details.product'])->get();

        $response = $products_warehouse->whereHas('product')->paginate(10);

        return response()->json([
            'products_warehouse' => $response,
            'products' => $products,
        ]);
    }

    public function checkExistence(Request $request)
    {
        $check_pw = ProductWarehouse::where('product_id', $request->product_id)->where('warehouse_id', $request->warehouse_id);

        $products_warehouse = collect();

        if ($check_pw->exists()) {
            $products_warehouse = $check_pw->select('id', 'product_id', 'warehouse_id', 'status')->with(['product' => function ($q) {
                $q->select('id', 'name', 'code');
            }, 'warehouse' => function ($q) {
                $q->select('id', 'branch_id', 'name', 'code')->with(['branch' => function ($q2) {
                    $q2->select('id', 'name', 'code')->withTrashed();
                }])->withTrashed();
            }, 'last_movement'])->first();
        }

        return $products_warehouse;
    }

    private function ValidInventory(array $data)
    {
        $attributes = [
            'branch_id' => 'sucursal',
            'branch_destination_id' => 'sucursal destino',
            'warehouse_id' => 'almacen',
            'warehouse_destination_id' => 'almacen destino',
            'type' => 'tipo de ajuste',
            'description' => 'descripción',
        ];

        $validator = Validator::make(
            $data,
            [
                'branch_id' => ['required', 'numeric'],
                'branch_destination_id' => ['required_if:type, 3'],
                'warehouse_id' => ['required', 'numeric'],
                'warehouse_destination_id' => 'required_if:type, 3',
                'type' => ['required', 'numeric'],
                'description' => ['required'],
            ],
            [
                "required" => "El campo :attribute es obligatorio.",
                "string" => "El campo :attribute debe ser una cadena de caracteres.",
                "numeric" => ":attribute debe ser numérico.",
            ]
        );

        $validator->setAttributeNames($attributes);

        return $validator;
    }

    private function checkProductDetails($product, $product_detail, $request, $quantity)
    {
        $check_pw = ProductWarehouse::where([
            ['warehouse_id', $request->warehouse_id],
            ['product_id', $product_detail->product_id],
        ])->first();

        $check_pwm = ProductWarehouseMovement::where('product_warehouse_id', $check_pw->id)->orderBy('id', 'desc');

        if (!$check_pwm->exists()) {
            return [
                'msg' => "Cantidad insuficiente de " . $product_detail->product['name'] . " para cargar al " . $product['name']
            ];
        }

        $current = $check_pwm->first();

        // SETEANDO EL STOCK ACTUAL DEL PRODUCTO DE ACUERDO AL ULTIMO MOVIMIENTO DEL MISMO EN EL ALMACEN
        $stock = $current->type == ENTRY ? $current->current_stock + $current->amount : $current->current_stock - $current->amount;
        // VALIDANDO SI EL STOCK ACTUAL DEL PRODUCTO ES SUFICIENTE PARA SU EXTRACCION
        $stock = $stock - ($product_detail->amount * $quantity);

        if ($stock <= 0) {
            return [
                'msg' => "¡La cantidad a cargar supera el stock actual del producto: *" . $product_detail->product['name'] . "*!",
            ];
        }
    }

    public function inventoryTranslate(Request $request)
    {
        $this->ValidInventory($request->all())->validate();

        // Verificando si los seriales existen en el almacen
        $response = $this->checkExistSerial($request->products, $request->warehouse_id);

        if (!is_bool($response)) {
            return $response;
        }

        foreach ($request->products as $product) {
            //Salida de Almacen Origen
            $warehouseOrigin = ProductWarehouse::where([
                ['warehouse_id', $request->warehouse_id],
                ['product_id', $product['id']],
            ])->first();

            if (!$warehouseOrigin) {
                return response()->json([
                    'msg' => "¡El producto: *" . $product['name'] . "* no se encuentra registrado en el almacén!",
                ], 422);
            }

            $warehouseMovement = ProductWarehouseMovement::where('product_warehouse_id', $warehouseOrigin->id)
                ->orderBy('id', 'desc');

            $stock = 0;

            if ($warehouseMovement->exists()) {

                $lastMovement = $warehouseMovement->first();
                // SETEANDO EL STOCK ACTUAL DEL PRODUCTO DE ACUERDO AL ULTIMO MOVIMIENTO DEL MISMO EN EL ALMACEN
                $stock = $lastMovement->type == ENTRY ? $lastMovement->current_stock + $lastMovement->amount : $lastMovement->current_stock - $lastMovement->amount;
                // VALIDANDO SI EL STOCK ACTUAL DEL PRODUCTO ES SUFICIENTE PARA SU EXTRACCION
                $stock = $stock - $product['quantity'];
            }

            if ($stock < 0) {
                return response()->json([
                    'msg' => "¡La cantidad a trasladar supera el stock actual del producto: *" . $product['name'] . "*!",
                    'product' => $product,
                ], 422);
            }
        }

        $transfer = new TransferWarehouse;
        $transfer->enterprise_id = $request->enterprise_id;
        $transfer->code = $this->getCode($request->enterprise_id);
        $transfer->origin_warehouse_id = $request->warehouse_id;
        $transfer->destiny_warehouse_id = $request->warehouse_destination_id;
        $transfer->date_sent = Carbon::now()->format('Y-m-d h:i:s');
        $transfer->date_recieved = $request->branch_id == $request->branch_destination_id ? Carbon::now()->format('Y-m-d h:i:s') : null;
        $transfer->status = $request->branch_id == $request->branch_destination_id ? 2 : 1;
        $transfer->description = $request->description;
        $transfer->creator_id = $request->creator_id;
        $transfer->save();

        foreach ($request->products as $product) {
            $check_pw = ProductWarehouse::where([
                ['warehouse_id', $request->warehouse_id],
                ['product_id', $product['id']],
            ])->first();

            if (!$check_pw) {
                return response()->json([
                    'msg' => "¡El producto: *" . $product['name'] . "* no se encuentra registrado en el almacén!",
                ], 422);
            }

            $check_pwm = ProductWarehouseMovement::where('product_warehouse_id', $check_pw->id)
                ->orderBy('id', 'desc');

            if ($check_pwm->exists()) {
                $current = $check_pwm->first();

                // SETEANDO EL STOCK ACTUAL DEL PRODUCTO DE ACUERDO AL ULTIMO MOVIMIENTO DEL MISMO EN EL ALMACEN
                $sameBranch = false;
                if ($request->branch_id == $request->branch_destination_id) {
                    $sameBranch = true;
                }
                $stockCurrent = $current->type == ENTRY ? $current->current_stock + $current->amount : $current->current_stock - $current->amount;

                $transferDetails = new TransferWarehouseDetails;
                $transferDetails->transfer_warehouse_id = $transfer->id;
                $transferDetails->product_id = $product['id'];
                $transferDetails->amount = $product['quantity'];
                $transferDetails->cost = $check_pw->cost;
                $transferDetails->save();

                $movement = new ProductWarehouseMovement;
                $movement->product_warehouse_id = $check_pw->id;
                $movement->action_id = $transfer->id;
                $movement->product_id = $product['id'];
                $movement->action_class = 4;
                $movement->action_type = 'App\Models\TransferWarehouse';
                $movement->type = 2;
                $movement->current_stock = $stockCurrent;
                $movement->amount = $product['quantity'];
                $movement->status = $sameBranch ? 1 : 0;
                $movement->description = $request->description;
                $movement->creator_id = $request->creator_id;
                $movement->save();

                // Eliminando serialización anterior de los productos
                $this->deleteSerials($product, $check_pw->id);

                if ($sameBranch) {

                    $check_dw = ProductWarehouse::where([
                        ['warehouse_id', $request->warehouse_destination_id],
                        ['product_id', $product['id']],
                    ])->first();

                    if (!$check_dw) {

                        $pwd = new ProductWarehouse;
                        $pwd->warehouse_id = $request->warehouse_destination_id;
                        $pwd->product_id = $product['id'];
                        $pwd->status = 1;
                        $pwd->percentage_earning = 0;
                        $pwd->percentage_max = 0;
                        $pwd->percentage_offer = 0;
                        $pwd->cost = $product['cost'];
                        $pwd->price = $product['cost'];
                        $pwd->price_max = $product['cost'];
                        $pwd->price_offer = $product['cost'];
                        $pwd->creator_id = $request->creator_id;
                        $pwd->save();

                        $movement_destiny = new ProductWarehouseMovement;
                        $movement_destiny->product_warehouse_id = $pwd->id;
                        $movement_destiny->product_id = $product['id'];
                        $movement_destiny->action_id = $transfer->id;
                        $movement_destiny->action_class = 4;
                        $movement_destiny->action_type = 'App\Models\TransferWarehouse';
                        $movement_destiny->type = 1;
                        $movement_destiny->current_stock = 0;
                        $movement_destiny->amount = $product['quantity'];
                        $movement_destiny->description = $request->description;
                        $movement_destiny->creator_id = $request->creator_id;
                        $movement_destiny->save();

                        $check_dw = $pwd;
                    } else {

                        $check_dw->cost = $product['cost'];
                        if ($product['change_price'] == true) {
                            $check_dw->price = ($product['cost'] * ($check_dw->percentage_earning / 100)) + $product['cost'];
                            $check_dw->price_offer = ($product['cost'] * ($check_dw->percentage_offer / 100)) + $product['cost'];
                            $check_dw->price_max = ($product['cost'] * ($check_dw->percentage_max / 100)) + $product['cost'];
                        }
                        $check_dw->save();

                        $check_dwm = ProductWarehouseMovement::where('product_warehouse_id', $check_dw->id)
                            ->orderBy('id', 'desc');

                        if ($check_dwm->exists()) {

                            $current = $check_dwm->first();

                            // SETEANDO EL STOCK ACTUAL DEL PRODUCTO DE ACUERDO AL ULTIMO MOVIMIENTO DEL MISMO EN EL ALMACEN
                            $stockCurrent = $current->type == ENTRY ? $current->current_stock + $current->amount : $current->current_stock - $current->amount;

                            $movement_destiny = new ProductWarehouseMovement;
                            $movement_destiny->product_warehouse_id = $check_dw->id;
                            $movement_destiny->product_id = $product['id'];
                            $movement->action_id = $transfer->id;
                            $movement_destiny->action_class = 4;
                            $movement_destiny->action_type = 'App\Models\TransferWarehouse';
                            $movement_destiny->type = 1;
                            $movement_destiny->current_stock = $stockCurrent;
                            $movement_destiny->amount = $product['quantity'];
                            $movement_destiny->description = $request->description;
                            $movement_destiny->creator_id = $request->creator_id;
                            $movement_destiny->save();
                        } else {

                            $movement_destiny = new ProductWarehouseMovement;
                            $movement_destiny->product_warehouse_id = $check_dw->id;
                            $movement_destiny->product_id = $product['id'];
                            $movement->action_id = $transfer->id;
                            $movement_destiny->action_class = 4;
                            $movement_destiny->action_type = 'App\Models\TransferWarehouse';
                            $movement_destiny->type = 1;
                            $movement_destiny->current_stock = 0;
                            $movement_destiny->amount = $product['quantity'];
                            $movement_destiny->description = $request->description;
                            $movement_destiny->creator_id = $request->creator_id;
                            $movement_destiny->save();
                        }
                    }

                    // Serializando los productos
                    $this->createProductSerials($product, $check_dw->id);
                } else {
                    // Serializando los productos comprometidos
                    $this->createProductSerials($product, $check_pw->id, $transfer->id, TRANSLATE_MODEL, COMMITTED);
                }
            } else {
                return response()->json([
                    'msg' => "¡El producto: *" . $product['name'] . "* no se encuentra registrado en el almacen!",
                ], 422);
            }
        }

        return;
    }

    public function inventoryCreate(Request $request)
    {

        $this->ValidInventory($request->all())->validate();

        if ($request->type == OUTPUT) {
            $response = $this->checkExistSerial($request->products, $request->warehouse_id);

            if (!is_bool($response)) {
                return $response;
            }
        }
        else if ($request->type == ENTRY) {
            $response = $this->checkEntrySerials($request->products,$request->warehouse_id);

            if (!is_bool($response)) {
                return $response;
            }
        }
        
        $products = $request->products;

        
        foreach ($products as $key => $product) {

            if ($request->type == ENTRY) {
                if (strlen($product['cost']) > 18) {
                    return response()->json([
                        'msg' => "¡El costo del producto *" . $product['name'] . "* no puede ser mayor a 18 dígitos!",
                    ], 422);
                }
            }

            if (strlen($product['quantity']) > 12) {
                return response()->json([
                    'msg' => "¡La cantidad a modificar del producto *" . $product['name'] . "* no puede ser mayor a 12 dígitos!",
                ], 422);
            }


            // if($this->checkIfAtLeastAProductIsSerialized($request->products) && $request->type == ENTRY) {
            //     $serialChecked = $this->getIfSerialsExistsInWarehouse($request->products, $request->warehouse_id);
        
            //     if($serialChecked)
            //     {
            //         return response()
            //                 ->json(['msg' => 'El serial '.$serialChecked->serial.' ya está asignado para este producto'],422);
            //     }
            // }


            $product_details = ProductDetails::with(['product'])->where('compound_id', $product['id']);

            if ($product['type'] == 2 && $request->type == ENTRY && ($product_details->count() > 0)) {
                foreach ($product_details->get() as $product_detail) {
                    if ($validator = $this->checkProductDetails($product, $product_detail, $request, $product['quantity'])) {
                        return response()->json($validator, 422);
                    }
                }
            }

            if ($request->type == OUTPUT) {
                $check_pw = ProductWarehouse::where([
                    ['warehouse_id', $request->warehouse_id],
                    ['product_id', $product['id']],
                ])->first();

                $check_pwm = ProductWarehouseMovement::where('product_warehouse_id', $check_pw->id)->orderBy('id', 'desc');

                if (!$check_pwm->exists()) {
                    if ($request->type == OUTPUT) {
                        return response()->json([
                            'msg' => "¡El producto: *" . $product['name'] . "* no se encuentra registrado en el almacen!",
                        ], 422);
                    }
                }

                $current = $check_pwm->first();

                // SETEANDO EL STOCK ACTUAL DEL PRODUCTO DE ACUERDO AL ULTIMO MOVIMIENTO DEL MISMO EN EL ALMACEN
                $stock = $current->type == ENTRY ? $current->current_stock + $current->amount : $current->current_stock - $current->amount;
                // VALIDANDO SI EL STOCK ACTUAL DEL PRODUCTO ES SUFICIENTE PARA SU EXTRACCION
                $stock = $stock - $product['quantity'];

                if ($stock < 0 && $request->type == OUTPUT) {
                    return response()->json([
                        'msg' => "¡La cantidad a descargar supera el stock actual del producto: *" . $product['name'] . "*!",
                        'product' => $product,
                    ], 422);
                }
            }
        }


        foreach ($request->products as $product) {

            $check_pw = ProductWarehouse::where([
                ['warehouse_id', $request->warehouse_id],
                ['product_id', $product['id']],
            ])->first();

            // Eliminando serialización anterior de los productos
            $this->deleteSerials($product, $check_pw->id);

            if ($request->type == ENTRY) {
                $check_pw->cost = $product['cost'];
                if ($product['change_price'] == true) {
                    $check_pw->price = ($product['cost'] * ($check_pw->percentage_earning / 100)) + $product['cost'];
                    $check_pw->price_offer = ($product['cost'] * ($check_pw->percentage_offer / 100)) + $product['cost'];
                    $check_pw->price_max = ($product['cost'] * ($check_pw->percentage_max / 100)) + $product['cost'];
                }
                $check_pw->save();

                // Serializando los productos
                $this->createProductSerials($product, $check_pw->id);
            } else if ($request->type == OUTPUT) {
                // Serializando la salida de los productos
                $this->createProductSerials($product, $check_pw->id, NULL, NULL, OUTPUT);
            }

            $check_pwm = ProductWarehouseMovement::where('product_warehouse_id', $check_pw->id)->orderBy('id', 'desc');

            if ($check_pwm->exists()) {
                $current = $check_pwm->first();
                // SETEANDO EL STOCK ACTUAL DEL PRODUCTO DE ACUERDO AL ULTIMO MOVIMIENTO DEL MISMO EN EL ALMACEN
                $stockCurrent = $current->type == ENTRY ? $current->current_stock + $current->amount : $current->current_stock - $current->amount;

                if ($stockCurrent < 0 && $request->type == OUTPUT) {
                    return response()->json([
                        'msg' => "¡La cantidad a descargar supera el stock actual del producto: *" . $product['name'] . "*!",
                        'product' => $product,
                    ], 422);
                }

                $movement = new ProductWarehouseMovement;
                $movement->product_warehouse_id = $check_pw->id;
                $movement->product_id = $product['id'];
                $movement->action_class = 1;
                $movement->type = $request->type;
                $movement->current_stock = $stockCurrent;
                $movement->amount = $product['quantity'];
                $movement->description = $request->description;
                $movement->creator_id = $request->creator_id;
                $movement->save();
            } else {

                $movement = new ProductWarehouseMovement;
                $movement->product_warehouse_id = $check_pw->id;
                $movement->product_id = $product['id'];
                $movement->action_class = 1;
                $movement->type = $request->type;
                $movement->current_stock = 0;
                $movement->amount = $product['quantity'];
                $movement->description = $request->description;
                $movement->creator_id = $request->creator_id;
                $movement->save();
            }

            $product_details = ProductDetails::with(['product'])->where('compound_id', $product['id']);

            if ($product['type'] == 2 && ($product_details->count() > 0)) {
                foreach ($product_details->get() as $product_detail) {

                    $check_pw = ProductWarehouse::where([
                        ['warehouse_id', $request->warehouse_id],
                        ['product_id', $product_detail->product_id],
                    ]);

                    if($check_pw->exists()) {
                        $check_pw = $check_pw->first();

                        $check_pwm = ProductWarehouseMovement::where('product_warehouse_id', $check_pw->id)->orderBy('id', 'desc');
    
                        $current = $check_pwm->first();
    
                        // SETEANDO EL STOCK ACTUAL DEL PRODUCTO DE ACUERDO AL ULTIMO MOVIMIENTO DEL MISMO EN EL ALMACEN
                        if($current){
                            $stock = $current->type == ENTRY ? $current->current_stock + $current->amount : $current->current_stock - $current->amount;
                        } else {
                            $stock = 0;
                        }
    
                        $movement = new ProductWarehouseMovement;
                        $movement->product_warehouse_id = $check_pw->id;
                        $movement->product_id = $product_detail->product_id;
                        $movement->action_class = PRODUCT_COMPOUND;
                        $movement->type = $request->type == ENTRY ? 2 : 1;
                        $movement->current_stock = $stock;
                        $movement->amount = ($product_detail->amount * $product['quantity']);
                        $movement->description = 'Movimiento por actualizacion de producto compuesto';
                        $movement->creator_id = $request->creator_id;
                        $movement->save();
                    } else {

                        $stock = 0;

                        $transferWarehouse = TransferWarehouse::where('destiny_warehouse_id', $request->warehouse_id)->first();

                        $originProductWarehouse = ProductWarehouse::where('warehouse_id', $transferWarehouse->origin_warehouse_id)->first();

                        $pwd = new ProductWarehouse;
                        $pwd->warehouse_id = $request->warehouse_id;
                        $pwd->product_id = $product_detail->product_id;
                        $pwd->status = 1;
                        $pwd->percentage_earning = 0;
                        $pwd->percentage_max = 0;
                        $pwd->percentage_offer = 0;
                        $pwd->cost = $originProductWarehouse->cost;
                        $pwd->price = $originProductWarehouse->price;
                        $pwd->price_max = $originProductWarehouse->price_max;
                        $pwd->price_offer = $originProductWarehouse->price_offer;
                        $pwd->creator_id = $request->creator_id;
                        $pwd->save();

                        $movement = new ProductWarehouseMovement;
                        $movement->product_warehouse_id = $pwd->id;
                        $movement->product_id = $product_detail->product_id;
                        $movement->action_class = PRODUCT_COMPOUND;
                        $movement->type = $request->type == ENTRY ? 2 : 1;
                        $movement->current_stock = $stock;
                        $movement->amount = ($product_detail->amount * $product['quantity']);
                        $movement->description = 'Movimiento por actualizacion de producto compuesto';
                        $movement->creator_id = $request->creator_id;
                        $movement->save();
                    }

                }
            }
        }

        return;
    }
}
