<?php

namespace App\Http\Controllers\Api\Admin\Branch;

use App\Http\Controllers\Controller;
use App\Http\Requests\BranchRequest;
use App\Models\Branch;
use App\Models\EnterpriseUser;
use App\Models\ProductWarehouseMovement;
use App\Models\Warehouse;
use DB;
use Illuminate\Http\Request;
use App\User;

class BranchController extends Controller
{
    public function getBranchesByEnterprise(Request $request)
    {
        $branches = Branch::where('enterprise_id', $request->Id)->get();

        return $branches;
    }

    /**
     *   @param $request->enterprise_id Cuando el que consulta es vendedor, este parametro posee el ID del vendedor, no de la empresa
     */
    public function branches(Request $request)
    {
        $query = Branch::where('enterprise_id', $request->enterprise_id)
            ->when($request->has('search'), function($query2) use ($request) {
                $query2->where(function ($query3) use ($request) {
                    $query3->where('name', 'like', '%' . $request->search . '%')
                    ->orWhere('code', 'like', '%' . $request->search . '%');
                });
            })
            ->when($request->has('status'), function($query2) use ($request) {
                $query2->when($request->status != 3, function($query3) use ($request) {
                    $query3->where('status', intval($request->status))->whereNull('deleted_at');
                }, function($query3) {
                    $query3->onlyTrashed();
                });
            })->with(['enterprise'])->withTrashed();

        if ($request->has('page')) {
            $branches = $query->paginate(10);
        } else {
            $branches = [
                'data' => $query->get()
            ];
        }

        return response()->json([
            'branches' => $branches,
            'result' => true,
        ]);
    }

    public function branchCreate(BranchRequest $request)
    {

        $branch = new Branch;
        $branch->enterprise_id = $request->enterprise_id;
        $branch->name = $request->name;
        $branch->code = $request->code;
        $branch->creator_id = $request->creator_id;
        $branch->save();

        $warehouse = new Warehouse;
        $warehouse->name = $request->name . ' - Almacen Principal';
        $warehouse->code = $request->code;
        $warehouse->description = $warehouse->name;
        $warehouse->user_id = $request->enterprise_id;
        $warehouse->creator_id = $request->creator_id;
        $warehouse->branch_id = $branch->id;
        $warehouse->save();

        return response()->json([
            'msg' => '¡Sucursal creada exitosamente!',
        ]);
    }

    public function branchEdit(BranchRequest $request)
    {

        $branch = Branch::find($request->id);
        $branch->name = $request->name;
        $branch->code = $request->code;
        $branch->save();

        return response()->json([
            'msg' => '¡Sucursal editada exitosamente!',
        ]);
    }

    public function branchSuspend(Request $request)
    {
        $branch = Branch::find($request->id);
        $branch->status = 2;
        $branch->save();

        return response()->json([
            'msg' => '¡Sucursal suspendida exitosamente!',
            'result' => true,
        ]);
    }

    public function branchActive(Request $request)
    {
        $branch = Branch::find($request->id);
        $branch->status = 1;
        $branch->save();

        return response()->json([
            'msg' => '¡Sucursal activada exitosamente!',
            'result' => true,
        ]);
    }

    public function branchDelete(Request $request)
    {
        if (!isset($request->id)) {
            return response()->json([
                'msg' => 'Ha ocurrido un error',
            ], 422);
        }

        $branch = Branch::where('id', $request->id);

        if (!$branch->firstOrFail()) {
            return response()->json([
                'msg' => 'La sucursal ya fue eliminada!',
            ], 422);
        }

        // Se evalua si todos los productos de los almacenes de la sucursal poseen un stock actual igual a 0, se utilizan los metodos whereHas,
        // para no usar un foreach y evaluar cada uno de los productos de los almacenes de la sucursal, ya que estos metodos nos traen todos
        // los productos asociados al almacen y el ultimo movimiento de los mismos. Se usa el MAX para poder agrupar por el ultimo registro.

        $pwm = ProductWarehouseMovement::select(DB::raw('MAX(id) as id'))->whereHas('product_warehouse', function ($query) use ($request) {
            $query->whereHas('warehouse', function ($query2) use ($request) {
                $query2->where('branch_id', $request->id);
            });
        })->orderBy('id', 'desc')->groupBy('product_id')->pluck('id');


        // Al obtener los ids de los productos, con SQL se determina el stock actual del producto y si cumple con la condicion
        // de que sea mayor a 0

        $check_pwm = ProductWarehouseMovement::whereIn('id', $pwm)
            ->whereRaw(DB::raw('IF(`type` = 1, `current_stock` + `amount`, `current_stock` - `amount`) > 0'))
            ->count();

        if ($check_pwm == 0) {
            $branch->delete();
            return response()->json([
                'msg' => 'Sucursal eliminada exitosamente!',
            ]);
        }

        return response()->json([
            'msg' => 'Los almacenes de la sucursal poseen productos en su stock! No se puede eliminar.',
        ], 422);
    }
}
