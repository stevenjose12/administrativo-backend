<?php

namespace App\Http\Controllers\Api\Admin\DebtsToPay;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ExpensesIngress;
use App\Models\ExpensesIngressPayment;
use Carbon\Carbon;
use App\Http\Requests\DebtsToPayRequest;
use App\Models\BankBalance;
use App\Http\Traits\Format;
use DB;
use App\Models\PurchaseOrder;
use App\Support\Collection;

class DebtsToPayController extends Controller
{
    use Format;
    public function index(Request $request)
    {
        $purchaseOrders = PurchaseOrder::with([
        'provider' => function ($query) {
            $query->with('person')->withTrashed();
        }, 'details'])
        ->where('status', 1)
        ->where('payment_type', 2)
        ->doesntHave('expenses_ingress')
        ->where('creator_id', $request->enterprise_id)
        ->select(
            'purchase_order.id',
            'purchase_order.creator_id as enterprise_id',
            'purchase_order.provider_id',
            'purchase_order.code',
            'purchase_order.bill_number',
            'purchase_order.control_number',
            'purchase_order.bill_date_emission as date_emission',
            'purchase_order.bill_date_emission as date_expired',
            'purchase_order.bill_date_reception as date_received',
            'purchase_order.observations as description',
            'purchase_order.subtotal_real as subtotal',
            'purchase_order.vat as iva',
            'purchase_order.total_real as total',
            'purchase_order.status',
            'purchase_order.creator_id',
            'purchase_order.created_at',
            'purchase_order.date_delivery_real as date'
        )
            ->when($request->has('provider') && $request->provider > 0, function ($query) use ($request) {
                $query->where('provider_id', $request->provider);
            })
            ->when($request->has('since') && $request->has('until'), function ($query) use ($request) {
                $query->whereBetween('bill_date_emission', [$this->parseFormatDateTime($request->since, 'start'), $this->parseFormatDateTime($request->until, 'end')]);
            })
            ->when($request->has('since') && !$request->has('until'), function ($query) use ($request) {
                $query->whereDate('bill_date_emission', '>=', $this->parseFormat($request->since));
            })
            ->when(!$request->has('since') && $request->has('until'), function ($query) use ($request) {
                $query->whereDate('bill_date_emission', '<=', $this->parseFormat($request->until));
            })
            ->when($request->has('status'), function ($query) use ($request) {
                $query->where('status', $request->status);
            })
        ->get();
        foreach ($purchaseOrders as $purchaseOrder) {
            $purchaseOrder->status = 0;
            $purchaseOrder->expenses_ingress_class = 1;

            foreach ($purchaseOrder->details as $detail) {
                $detail->expenses_ingress_class = 1;
                $detail->expenses_ingress_id = $purchaseOrder->id;
                $detail->description = $detail->name;
                $detail->amount = $detail->pivot->quantity;
                $detail->subtotal = $detail->pivot->subtotal;
                $detail->total = $detail->pivot->total;
                $detail->iva = $detail->pivot->vat;
                $detail->price = $detail->pivot->subtotal / $detail->pivot->quantity;
            }
        }
        $expensesIngress = ExpensesIngress::with(['enterprise','expenses_ingress' => function ($query) {
            $query->where('expenses_ingress_type');
        },
        'provider' => function ($query) {
            $query->with('person')->withTrashed();
        }, 'creator' => function ($query) {
            $query->with('person')->withTrashed();
        }, 'details'])
        ->where('enterprise_id', $request->enterprise_id)
        ->where('payment_type', config()->get('constants.expenses_ingress.payment_types.CREDIT'))
        ->where('status', config()->get('constants.expenses_ingress.status.PENDING'))
        ->when($request->has('provider') && $request->provider > 0, function ($query) use ($request) {
            $query->where('provider_id', $request->provider);
        })
        ->when($request->has('since') && $request->has('until'), function ($query) use ($request) {
            $query->whereBetween('date_emission', [$this->parseFormatDateTime($request->since, 'start'), $this->parseFormatDateTime($request->until, 'end')]);
        })
        ->when($request->has('since') && !$request->has('until'), function ($query) use ($request) {
            $query->whereDate('date_emission', '>=', $this->parseFormat($request->since));
        })
        ->when(!$request->has('since') && $request->has('until'), function ($query) use ($request) {
            $query->whereDate('date_emission', '<=', $this->parseFormat($request->until));
        })
        ->when($request->has('status'), function ($query) use ($request) {
            $query->where('status', $request->status);
        })
        ->when($request->has('payment_type') && $request->payment_type > 0, function ($query) use ($request) {
            $query->where('payment_type', $request->payment_type);
        })
        ->get();

        foreach ($expensesIngress as $expenseIngress) {
            $expenseIngress->expenses_ingress_class = 2;
        }

        $debtsToPay = $purchaseOrders->merge($expensesIngress)->all();
        
     
        return (new Collection($debtsToPay))->paginate(10);
    }
    public function pay(DebtsToPayRequest $request)
    {
        $current_bank_balance = BankBalance::where('bank_account_id', $request->bank_account_id)->orderBy('id', 'desc');
        if ($current_bank_balance->exists()) {
            $current_bank_balance = $current_bank_balance->first()->total;
        } else {
            $current_bank_balance = 0;
        }

        if ($request->amount > $current_bank_balance) {
            return response()->json([
                'msg' => 'El monto de la factura no puede sobrepasar el saldo de la cuenta!',
            ], 422);
        }

        $expensesIngressPayment = new ExpensesIngressPayment;
        $expensesIngressPayment->expenses_ingress_id = $request->expenses_ingress_id;
        $expensesIngressPayment->creator_id = $request->creator_id;
        if ($request->has('bank_account_id')) {
            $expensesIngressPayment->bank_account_id = $request->bank_account_id;
        }
        $expensesIngressPayment->payment_type = $request->method_type;
        $expensesIngressPayment->amount = $request->amount;
        $expensesIngressPayment->expenses_ingress_class = (Int) $request->expenses_ingress_class;
        
        $expenses_ingress_type="";
        if ((Int)$request->expenses_ingress_class == 2) {
            $expenses_ingress_type = "App\Models\ExpensesIngress";
        } else {
            $expenses_ingress_type = "App\Models\PurchaseOrder";
        }

        $expensesIngressPayment->expenses_ingress_type = $expenses_ingress_type;
        $expensesIngressPayment->date = Carbon::now();
        $expensesIngressPayment->status = config('constants.expenses_ingress.status.PROCESSED');
        $expensesIngressPayment->creator_id = $request->creator_id;
        $expensesIngressPayment->save();
        if ((Int)$request->expenses_ingress_class == 2) {
            ExpensesIngress::where('id', $request->expenses_ingress_id)->update([
                'status' => config('constants.expenses_ingress.status.PROCESSED')]);
        }
        
        $bank_balance = new BankBalance;
        $bank_balance->bank_id = $request->bank_id;
        $bank_balance->bank_account_id = $request->bank_account_id;
        $bank_balance->action_id = $expensesIngressPayment->id;
        $bank_balance->action_class = config()->get('constants.bank.type.EXPENSES_PAYMENT_EGRESSS');
        $bank_balance->action_type = config()->get('constants.bank.type.APP_EXPENSE_PAYMENT');
        $bank_balance->type = config()->get('constants.bank.type.OUTPUT');
        $bank_balance->amount = $request->amount;
        $bank_balance->current_amount = $current_bank_balance;
        $bank_balance->total = $current_bank_balance - $request->amount;
        $bank_balance->description = "Egreso por pago de factura";
        $bank_balance->creator_id = $request->creator_id;
        $bank_balance->operation_number = $request->reference_number;
        $bank_balance->save();

        return response()->json(['msg' => 'Pago realizado con exito']);
    }
}
