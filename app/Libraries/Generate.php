<?php

	namespace App\Libraries;

	class Generate {

		public static function get() {
			return strtoupper(substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0,6));
		} 
	}