<?php

	namespace App\Libraries;

	class Code {

		public static function get($value) {
			return str_pad($value, 5, '0', STR_PAD_LEFT);
		} 
	}