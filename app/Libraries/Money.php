<?php

	namespace App\Libraries;

	class Money {

		public static function get($value) {
			return 'Bs. '.number_format($value,2,',','.');
		} 
	}
