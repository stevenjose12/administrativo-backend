<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ConfigClient extends Model
{
    use SoftDeletes;

    protected $table = "config_clients";

    protected $fillable = [
        'client_id', 'days_deadline', 'creator_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'client_id');
    }
}
