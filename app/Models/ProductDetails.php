<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductDetails extends Model
{
    public function product()
    {
        return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }

    public function compound()
    {
        return $this->belongsTo('App\Models\Product', 'id', 'compound_id');
    }
}
