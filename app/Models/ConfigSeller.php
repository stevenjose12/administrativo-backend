<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConfigSeller extends Model
{
    protected $table = "config_sellers";

    protected $fillable = [
        'seller_id', 'price_offer', 'price_max', 'type_percentaje', 'percentage',
    ];

    public function seller()
    {
        return $this->belongsTo('App\User', 'seller_id');
    }
}
