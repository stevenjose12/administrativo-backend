<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserBalance extends Model
{
    protected $table = "user_balance";

    protected $fillable = [
        'action_id', 'action_class', 'action_type', 'user_id', 'type',
        'current_amount', 'amount', 'total', 'status', 'creator_id',
    ];

    public function action()
    {
        return $this->morphTo();
    }

    public function payment()
    {
        return $this->belongTo('App\Models\UserBalance', 'action_id', 'payment_id')->whereNotNull('payment_id');
    }

    public function expenses()
    {
        return $this->hasMany('App\Models\UserBalance', 'payment_id', 'action_id')->whereNotNull('payment_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function created_by()
    {
        return $this->belongsTo('App\User', 'creator_id');
    }
}
