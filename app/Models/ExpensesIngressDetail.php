<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExpensesIngressDetail extends Model
{
    use SoftDeletes;
    
    protected $table = "expenses_ingress_details";

    protected $fillable = [
        'expenses_ingress_id', 'description', 'price', 'amount', 'exempt',
        'subtotal', 'vat', 'total'
    ];

    public function expense_ingress()
    {
        return $this->belongsTo('App\Models\ExpensesIngress', 'expenses_ingress_id');
    }
}
