<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPayment extends Model
{
    protected $table = "user_payments";

    protected $fillable = [
        'user_id', 'type', 'amount', 'current_amount', 'image', 'creator_id',
    ];

    public function account()
    {
        return $this->hasOne('App\Models\UserBalance', 'action_id');
    }

    public function expenses()
    {
        return $this->hasMany('App\Models\RequestUserPayment', 'user_payment_id', 'id')->orderBy('user_payment_id', 'asc');
    }

    public function action()
    {
        return $this->morphMany('App\Models\BankBalance', 'action');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function creator()
    {
        return $this->belongsTo('App\User', 'creator_id');
    }
}
