<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CashCount extends Model
{
    protected $table = "cash_count";

    protected $fillable = [
        'bank_id', 'bank_account_id', 'amount', 'creator_id'
    ];

    public function bank_account()
    {
        return $this->belongsTo('App\Models\BankAccount', 'bank_account_id');
    }

    public function bank()
    {
        return $this->belongsTo('App\Models\Bank', 'bank_id');
    }

    public function creator()
    {
        return $this->belongsTo('App\User', 'creator_id');
    }
}
