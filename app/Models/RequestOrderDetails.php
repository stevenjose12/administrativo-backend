<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RequestOrderDetails extends Model
{
    protected $table = "request_order_details";
}
