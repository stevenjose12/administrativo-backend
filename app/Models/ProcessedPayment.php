<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProcessedPayment extends Model
{
    protected $table = "processed_request_payments";

    protected $fillable = [
		'processed_request_id', 'type', 'amount', 'processed'
    ];
    
    public function processedRequest()
    {
      return $this->belongsTo('App\Models\ProcessedOrder', 'processed_request_id');
    }

    public function action()
    {
        return $this->morphMany('App\Models\BankBalance', 'action');
    }
}   
