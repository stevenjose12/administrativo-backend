<?php

	namespace App\Models;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class Password extends Model {
	    protected $table = "password_resets";

	    use SoftDeletes;
	}
