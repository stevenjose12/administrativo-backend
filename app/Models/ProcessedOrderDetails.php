<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProcessedOrderDetails extends Model
{
    protected $table = "processed_request_details";
}
