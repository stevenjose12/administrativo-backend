<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Branch extends Model
{
    use SoftDeletes;

    protected $table = "branches";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'enterprise_id', 'name', 'code', 'status', 'creator_id',
    ];

    public function enterprise()
    {
        return $this->belongsTo('App\User', 'enterprise_id', 'id');
    }

    public function warehouses()
    {
        return $this->hasMany('App\Models\Warehouse', 'branch_id', 'id');
    }
}
