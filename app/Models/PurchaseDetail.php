<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PurchaseDetail extends Model
{
   	protected $table = "purchase_order_details";

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
        'id', 'purchase_order_id', 'product_id', 'quantity', 'quantity_real', 'subtotal', 'subtotal_real', 'vat', 
		'vat_real', 'total', 'total_real'
	];
}
