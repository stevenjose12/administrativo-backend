<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EnterpriseModule extends Model
{
    protected $table = "enterprise_modules";

    public function enteprise()
    {
        return $this->belongsTo('App\User', 'enterprise_id', 'id');
    }

    public function module()
    {
        return $this->belongsTo('App\Models\Module', 'module_id', 'id');
    }
}
