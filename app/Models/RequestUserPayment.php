<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RequestUserPayment extends Model
{
    protected $table = "requests_user_payments";
    
    protected $fillable = [
        'processed_request_id', 'user_payment_id', 'creator_id', 'type', 'amount'
    ];

    public function creator()
    {
        return $this->belongsTo('App\User', 'creator_id');
    }

    public function processed_request()
    {
      return $this->belongsTo('App\Models\ProcessedOrder', 'processed_request_id');
    }

    public function user_payment()
    {
        return $this->belongsTo('App\Models\UserPayment', 'user_payment_id')->orderBy('user_payment_id', 'asc');
    }
}
