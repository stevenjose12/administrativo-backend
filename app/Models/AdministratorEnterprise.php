<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdministratorEnterprise extends Model
{
    public function administrator()
    {
        return $this->belongsTo('App\User', 'administrator_id', 'id');
    }

    public function enterprise()
    {
        return $this->belongsTo('App\User', 'enterprise_id', 'id');
    }
}
