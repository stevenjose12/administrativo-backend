<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $table = "products";

    protected $fillable = [
        'user_id', 'code', 'name', 'description', 'avatar', 'status', 'percentage_earning', 'percentage_commission', 'exempt', 'serialization', 'creator_id', 'type',
    ];

    public function product_subcategory()
    {
        return $this->hasOne('App\Models\ProductSubCategory', 'product_id');
    }

    public function product_provider()
    {
        return $this->hasOne('App\Models\ProductProvider', 'product_id', 'id')->latest();
    }

    public function product_brand()
    {
        return $this->hasOne('App\Models\ProductBrand', 'product_id');
    }

    public function product_category()
    {
        return $this->hasOne('App\Models\ProductCategory', 'product_id');
    }

    public function subcategory()
    {
        return $this->belongsTo('App\Models\Subcategory', 'subcategory_id');
    }

    public function product_model()
    {
        return $this->hasOne('App\Models\ProductModel', 'product_id');
    }

    public function product_warehouse()
    {
        return $this->hasMany('App\Models\ProductWarehouse', 'product_id');
    }

    public function images()
    {
        return $this->hasMany('App\Models\ProductImage', 'product_id');
    }

    public function product_details()
    {
        return $this->hasMany('App\Models\ProductDetails', 'compound_id');
    }

    public function details_product()
    {
        return $this->belongsTo('App\Models\ProductDetails', 'id', 'product_id');
    }
}
