<?php

	namespace App\Models;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class ProductImage extends Model {
		use SoftDeletes;
		
		protected $table = "products_images";


		public function product() {
			return $this->belongsTo('App\Models\Product','product_id');
		}
	}
