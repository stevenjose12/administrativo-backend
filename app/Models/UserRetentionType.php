<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRetentionType extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function retention()
    {
        return $this->belongsTo('App\Models\RetentionType', 'retention_type_id');
    }
}
