<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductSerial extends Model
{
    protected $table = "product_serials";
    use SoftDeletes;

    public function product_warehouse() {
    	return $this->belongsTo('App\Models\ProductWarehouse','product_warehouse_id');
    }

    public function operation()
    {
        return $this->morphTo();
    }
}
