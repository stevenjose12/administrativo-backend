<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrder extends Model
{
    protected $table = "purchase_order";

    protected $fillable = [
        'provider_id', 'warehouse_id', 'code', 'bill_number', 'control_number', 'type', 'currency_id', 'date_purchase', 'date_delivery', 'date_delivery_real',
        'bill_date_reception', 'bill_date_emission', 'observations', 'taxable', 'exempt', 'subtotal', 'subtotal_real', 'vat', 'vat_real', 'total', 'total_real', 'creator_id',
    ];

    public function warehouse()
    {
        return $this->belongsTo('App\Models\Warehouse', 'warehouse_id');
    }

    public function details()
    {
        return $this->belongsToMany('App\Models\Product', 'purchase_order_details', 'purchase_order_id', 'product_id')->withPivot([
            'id', 'quantity', 'quantity_real', 'subtotal', 'subtotal_real', 'vat', 'vat_real', 'total', 'total_real',
        ])->withTimestamps()->withTrashed();
    }

    public function currency()
    {
        return $this->belongsTo('App\Models\Currency', 'currency_id');
    }

    public function provider()
    {
        return $this->belongsTo('App\User', 'provider_id');
    }

    public function action()
    {
        return $this->morphMany('App\Models\ProductWarehouseMovement', 'action');
    }

    public function operation()
    {
        return $this->morphMany('App\Models\ProductSerial', 'operation');
    }
    
    public function expenses_ingress()
    {
        return $this->morphMany('App\Models\ExpensesIngressPayment', 'expenses_ingress');
    }
}
