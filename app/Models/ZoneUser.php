<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ZoneUser extends Model
{
    use SoftDeletes;

    protected $table = 'zone_users';

    public function zone() {
        return $this->belongsTo('App\Models\Zone','zone_id');
    }

    public function user() {
        return $this->belongsTo('App\User','user_id');
    }
}
