<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CurrencyConversion extends Model
{
  protected $table = "currency_conversions";

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'master_id ', 'slave_id', 'conversion_rate'
  ];


  public function master() {
    return $this->belongsTo('App\Models\Currency', 'master_id');
  }
  
  public function slave() {
    return $this->belongsTo('App\Models\Currency', 'slave_id');
	}
}