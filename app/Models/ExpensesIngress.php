<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExpensesIngress extends Model
{
    use SoftDeletes;

    protected $table = "expenses_ingress";

    protected $fillable = [
        'enterprise_id', 'provider_id', 'payment_id', 'date_emission', 'date_received', 'date_expired', 'code', 'bill_number',
        'control_number', 'payment_type', 'description', 'subtotal', 'iva', 'total', 'status', 'creator_id'
    ];

    public function enterprise()
    {
        return $this->belongsTo('App\User', 'enterprise_id');
    }

    public function provider()
    {
        return $this->belongsTo('App\User', 'provider_id');
    }

    public function creator()
    {
        return $this->belongsTo('App\User', 'creator_id');
    }

    public function expenses_ingress()
    {
        return $this->morphMany('App\Models\ExpensesIngressPayment', 'expenses_ingress');
    }

    
    public function details()
    {
        return $this->hasMany('App\Models\ExpensesIngressDetail', 'expenses_ingress_id', 'id');
    }

}
