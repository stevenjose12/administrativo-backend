<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class EnterpriseUser extends Model
{
    
    public function enterprise()
    {
        return $this->belongsTo(User::class, 'enterprise_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
