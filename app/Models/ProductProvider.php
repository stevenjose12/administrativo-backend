<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductProvider extends Model
{
    protected $table = "product_providers";

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }

    public function provider()
    {
        return $this->belongsTo('App\User', 'provider_id');
    }

    public function provide()
    {
        return $this->hasOne('App\User', 'id', 'provider_id');
    }
}
