<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConfigEnterprise extends Model
{
    public function enterprise()
    {
        return $this->belongsTo('App\User', 'enterprise_id', 'id');
    }
}
