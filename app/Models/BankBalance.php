<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankBalance extends Model
{
    protected $table = "bank_balance";

    protected $fillable = [
        'bank_id', 'bank_action_id', 'action_id', 'action_class', 'action_type', 'operation_number', 'type', 'amount', 'current_amount',
        'total', 'description', 'creator_id',
    ];

    public function action()
    {
        return $this->morphTo();
    }

    public function bank_account()
    {
        return $this->belongsTo('App\Models\BankAccount', 'bank_account_id');
    }

    public function bank()
    {
        return $this->belongsTo('App\Models\Bank', 'bank_id');
    }

    public function creator()
    {
        return $this->belongsTo('App\User', 'creator_id');
    }
}
