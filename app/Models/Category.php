<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{

    use SoftDeletes;

    protected $table = "categories";

    protected $fillable = [
        'user_id', 'name', 'code', 'status', 'creator_id',
    ];

    public function subcategories()
    {
        return $this->hasMany('App\Models\Subcategory', 'category_id');
    }

    public function owner()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function products()
    {
        return $this->hasMany('App\Models\ProductCategory', 'category_id');
    }
}
