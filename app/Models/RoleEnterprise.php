<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class RoleEnterprise extends Model
{
    protected $table = "role_enterprises";

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'enterprise_id');
    }

    public function role_modules()
    {
        return $this->hasMany('App\Models\RoleModule', 'role_enterprise_id', 'id');
    }
}
