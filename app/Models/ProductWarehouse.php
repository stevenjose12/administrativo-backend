<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductWarehouse extends Model
{

    use SoftDeletes;

    protected $table = "product_warehouse";

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }

    public function warehouse()
    {
        return $this->hasOne('App\Models\Warehouse', 'id', 'warehouse_id');
    }

    public function product_brand()
    {
        return $this->hasOne('App\Models\ProductBrand', 'product_id', 'product_id');
    }

    public function product_model()
    {
        return $this->hasOne('App\Models\ProductModel', 'product_id', 'product_id');
    }

    public function product_category()
    {
        return $this->hasOne('App\Models\ProductCategory', 'product_id', 'product_id');
    }

    public function product_subcategory()
    {
        return $this->hasOne('App\Models\ProductSubCategory', 'product_id', 'product_id');
    }

    public function movements()
    {
        return $this->hasMany('App\Models\ProductWarehouseMovement', 'product_warehouse_id')->orderBy('id', 'desc');
    }

    public function last_movement()
    {
        return $this->hasOne('App\Models\ProductWarehouseMovement', 'product_warehouse_id')->latest('id');
    }

    public function committed_stock()
    {
        return $this->hasMany('App\Models\ProductWarehouseMovement', 'product_warehouse_id')->where('status', 0)->orderBy('id', 'desc');
    }

    public function purchase_orders()
    {
        return $this->belongsToMany('App\Models\PurchaseOrder', 'purchase_order_details', 'purchase_order_id', 'product_id')->withPivot([
            'id', 'product_id',
        ])->withTimestamps();
    }

    public function request_orders()
    {
        return $this->belongsToMany('App\Models\RequestOrder', 'request_order_details', 'request_order_id', 'product_id')->withPivot([
            'id', 'product_id',
        ])->withTimestamps();
    }

    public function transfer_warehouse()
    {
        return $this->belongsToMany('App\Models\TransferWarehouse', 'transfer_warehouse_details', 'transfer_warehouse_id', 'product_id')->withPivot([
            'id', 'product_id',
        ])->withTimestamps();
    }

    public function serials() {
        return $this->hasMany('App\Models\ProductSerial','product_warehouse_id');
    }
}
