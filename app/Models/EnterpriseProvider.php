<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\ConfigProvider;

class EnterpriseProvider extends Model
{
    public function enterprise()
    {
        return $this->belongsTo(User::class, 'enterprise_id');
    }
    public function configProviders()
    {
        return $this->belongsTo(ConfigProvider::class, 'provider_id');
    }
}
