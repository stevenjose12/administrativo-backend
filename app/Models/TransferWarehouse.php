<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransferWarehouse extends Model
{
    protected $table = "transfer_warehouse";
    
    public function details()
	{
		return $this->belongsToMany('App\Models\Product', 'transfer_warehouse_details', 'transfer_warehouse_id', 'product_id')->withPivot([
			'id', 'amount', 'cost'
		])->withTimestamps();
	}

    public function origin_warehouse()
    {
        return $this->hasOne('App\Models\Warehouse', 'id', 'origin_warehouse_id');
    }

    public function destiny_warehouse()
    {
        return $this->hasOne('App\Models\Warehouse', 'id', 'destiny_warehouse_id');
    }

    public function creator() 
    {
        return $this->belongsTo('App\User', 'creator_id');
    }

    public function action() 
	{
		return $this->morphMany('App\Models\ProductWarehouseMovement', 'action');
    }
    
    public function serial()
    {
        return $this->morphMany('App\Models\ProductSerial', 'operation');
    }

}
