<?php

	namespace App\Models;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class Person extends Model {
		protected $table = "persons";
	    
	    use SoftDeletes;

	    public function user() {
	        return $this->belongsTo('App\User','user_id');
	    }
	}
