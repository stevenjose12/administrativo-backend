<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
	protected $table = "currencies";

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name'
	];


	public function conversion_master() {
		return $this->hasMany('App\Models\CurrencyConversion', 'master_id', 'id');
	}
	
	public function conversion_slave() {
		return $this->hasMany('App\Models\CurrencyConversion', 'slave_id', 'id');
	}
}