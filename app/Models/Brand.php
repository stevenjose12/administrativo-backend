<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brand extends Model
{
    use SoftDeletes;

    protected $table = "brands";

	protected $fillable = [
		'user_id', 'name', 'code', 'status', 'creator_id'
	];

    public function models()
    {
        return $this->hasMany('App\Models\Modelo', 'brand_id', 'id');
    }

    public function owner() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function products() 
    {
		return $this->hasMany('App\Models\ProductBrand','brand_id');
	}
}
