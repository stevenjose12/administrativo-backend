<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExpensesIngressPayment extends Model
{
    use SoftDeletes;
    
    protected $table = "expenses_ingress_payments";

    protected $fillable = [
        'expenses_ingress_id', 'payment_type', 'amount', 'date', 'status'
    ];

    public function expenses_ingress()
    {
        return $this->morphTo();
    }

    public function action()
    {
        return $this->morphMany('App\Models\BankBalance', 'action');
    }

    public function creator()
    {
        return $this->belongsTo('App\User', 'creator_id');
    }
    
}
