<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bank extends Model
{
    use SoftDeletes;

    protected $table = "banks";

    protected $fillable = [
		'enterprise_id', 'name', 'status', 'creator_id'
	];

    public function accounts()
    {
        return $this->hasMany('App\Models\BankAccount', 'bank_id', 'id');
    }

    public function balance()
    {
        return $this->hasMany('App\Models\BankBalance', 'bank_id')->orderBy('id', 'desc');
    }

    public function last_balance()
    {
        return $this->hasOne('App\Models\BankBalance', 'bank_id')->latest('id');
    }

    public function enterprise() {
        return $this->belongsTo('App\User', 'enterprise_id', 'id');
    }

    public function creator() {
        return $this->belongsTo('App\User', 'creator_id', 'id');
    }
}
