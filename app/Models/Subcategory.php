<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subcategory extends Model
{
    use SoftDeletes;

    protected $table = "subcategories";

    protected $fillable = [
		'category_id', 'name', 'code', 'status', 'creator_id'
	];

    public function category() {
        return $this->belongsTo('App\Models\Category','category_id');
    }

    public function products() {
        return $this->hasMany('App\Models\ProductSubCategory','subcategory_id');
    }

    public function product_subcategory() {
        return $this->belongsTo('App\Models\ProductSubCategory', 'id');
    }
}
