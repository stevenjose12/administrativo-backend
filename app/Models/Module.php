<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Module extends Model
{

    use SoftDeletes;

    protected $table = "modules";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'menu_id', 'name', 'path', 'admin_only', 'enterprise_only', 'user_only', 'seller_only', 'required', 'status',
    ];

    public function enterprise_modules()
    {
        return $this->belongsToMany('App\User', 'enterprise_modules', 'module_id', 'enterprise_id')->withTimestamps();
    }

    public function permissions()
    {
        return $this->hasMany('App\Models\Permission', 'module_id', 'id');
    }

    public function menus()
    {
        return $this->belongsTo('App\Models\Menu', 'menu_id');
    }
}
