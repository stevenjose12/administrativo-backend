<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProcessedRequest extends Model
{

    protected $table = "processed_request";

    protected $fillable = [
        'request_order_id', 'warehouse_id', 'zone_id', 'client_id', 'code', 'bill_number', 'control_number', 'currency_id',
        'status', 'processed', 'type', 'date_emission', 'observations', 'taxable', 'exempt', 'subtotal', 'vat', 'total', 'creator_id',
    ];

    public function seller()
    {
        return $this->belongsTo('App\User', 'creator_id');
    }

    public function details()
    {
        return $this->belongsToMany('App\Models\Product', 'processed_request_details', 'processed_request_id', 'product_id')->withPivot([
            'id', 'quantity', 'discount_percentage', 'subtotal', 'vat', 'total',
        ])->withTimestamps();
    }

    public function currency()
    {
        return $this->belongsTo('App\Models\Currency', 'currency_id');
    }

    public function client()
    {
        return $this->belongsTo('App\User', 'client_id');
    }

    public function method_payment()
    {
        return $this->hasMany('App\Models\ProcessedPayment', 'processed_request_id', 'id');
    }

    public function action()
    {
        return $this->morphMany('App\Models\ProductWarehouseMovement', 'action');
    }

    public function warehouse()
    {
        return $this->belongsTo('App\Models\Warehouse', 'warehouse_id');
    }
}
