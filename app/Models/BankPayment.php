<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class BankPayment extends Model
{

    use SoftDeletes;

    protected $table = "bank_payments";
    
    protected $fillable = [
        'bank_action_id', 'amount', 'type', 'status', 'creator_id',
    ];

    public function bank_account()
    {
        return $this->belongsTo('App\Models\BankAccount', 'bank_account_id');
    }

    public function action()
    {
        return $this->morphMany('App\Models\BankBalance', 'action');
    }
}
