<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BankAccount extends Model
{
    use SoftDeletes;

    protected $table = "bank_accounts";

    public function bank()
    {
        return $this->belongsTo('App\Models\Bank', 'bank_id', 'id');
    }

    public function balance()
    {
        return $this->hasMany('App\Models\BankBalance', 'bank_account_id')->orderBy('id', 'desc');
    }

    public function last_balance()
    {
        return $this->hasOne('App\Models\BankBalance', 'bank_account_id')->latest('id');
    }

    public function creator() {
        return $this->belongsTo('App\User', 'creator_id', 'id');
    }
    
}
