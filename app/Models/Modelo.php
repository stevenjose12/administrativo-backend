<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Modelo extends Model
{
    protected $table = "models";

    protected $fillable = [
		'brand_id', 'name', 'code', 'status', 'creator_id'
	];
    
    public function brands()
    {
        return $this->belongsTo('App\Models\Brand', 'brand_id');
    }

    public function brand()
    {
        return $this->hasOne('App\Models\Brand', 'id', 'brand_id');
    }

    public function product_model()
    {
        return $this->belongsTo('App\Models\ProductModel', 'id');
    }

    public function products() 
    {
		return $this->hasMany('App\Models\ProductModel','model_id');
	}

    
}
