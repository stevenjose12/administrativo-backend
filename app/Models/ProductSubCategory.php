<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductSubCategory extends Model
{
    protected $table = "product_subcategory";
    
    public function subcategory()
    {
        return $this->hasOne('App\Models\Subcategory', 'id', 'subcategory_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }
}
