<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permission extends Model{
    
    //use SoftDeletes;

    protected $table = "permissions";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'display_name', 'description'
    ];

    public function permissions_by_enterprise()
    {
        return $this->belongsToMany('App\User', 'permission_user', 'permission_id', 'user_id');
    }

    public function roles() {
        return $this->belongsToMany('App\Models\Role', 'permission_role', 'permission_id', 'role_id');
    }  

    public function module() {
        return $this->belongsTo('App\Models\Module','module_id');
    }  

    public function user() {
        return $this->belongsTo('App\User','user_id');
    }
}
