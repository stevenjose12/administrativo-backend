<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Request extends Model
{
    protected $table = "request_order";

    protected $fillable = [
        'warehouse_id', 'client_id', 'code', 'currency_id', 'date_emission',
        'observations', 'taxable', 'exempt', 'subtotal', 'vat', 'total', 'creator_id',
    ];

    protected $dates = [
        'date_emission' 
    ];

    public function seller()
    {
        return $this->belongsTo('App\User', 'creator_id');
    }

    public function details()
    {
        return $this->belongsToMany('App\Models\Product', 'request_order_details', 'request_order_id', 'product_id')->withPivot([
            'id', 'quantity', 'subtotal', 'vat', 'total', 'rate',
        ])->withTimestamps();
    }

    public function client()
    {
        return $this->belongsTo('App\User', 'client_id');
    }

    public function creator()
    {
        return $this->belongsTo('App\User', 'creator_id');
    }

    public function warehouse()
    {
        return $this->belongsTo('App\Models\Warehouse', 'warehouse_id');
    }

    public function currency()
    {
        return $this->belongsTo('App\Models\Currency', 'currency_id');
    }

    public function action()
    {
        return $this->morphMany('App\Models\ProductWarehouseMovement', 'action');
    }

    public function operation()
    {
        return $this->morphMany('App\Models\ProductSerial', 'operation');
    }
}
