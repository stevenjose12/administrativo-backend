<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductModel extends Model
{
    protected $table = "product_model";

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }

    public function model()
    {
        return $this->hasOne('App\Models\Modelo', 'id', 'model_id');
    }
}
