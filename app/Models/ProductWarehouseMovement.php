<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductWarehouseMovement extends Model
{
    protected $table = "product_warehouse_movement";

    protected $fillable = [
        'action_id', 'product_warehouse_id', 'product_id', 'action_class', 'action_type', 'type', 'current_stock', 'amount',
        'creator_id',
    ];

    public function action()
    {
        return $this->morphTo();
    }

    public function product_warehouse()
    {
        return $this->belongsTo('App\Models\ProductWarehouse', 'product_warehouse_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }

    public function creator()
    {
        return $this->belongsTo('App\User', 'creator_id');
    }
}
