<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransferWarehouseDetails extends Model
{
    protected $table = "transfer_warehouse_details";
}
