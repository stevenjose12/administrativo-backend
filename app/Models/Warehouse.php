<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Warehouse extends Model
{
    use SoftDeletes;

    protected $table = 'warehouses';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function branch()
    {
        return $this->belongsTo('App\Models\Branch', 'branch_id');
    }

    public function product_warehouse()
    {
        return $this->hasMany('App\Models\ProductWarehouse', 'warehouse_id', 'id');
    }

    public function creator()
    {
        return $this->belongsTo('App\User', 'creator_id');
    }
}
