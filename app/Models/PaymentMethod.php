<?php

	namespace App\Models;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class PaymentMethod extends Model {
		protected $table = "payments_methods";

	    use SoftDeletes;
	}
