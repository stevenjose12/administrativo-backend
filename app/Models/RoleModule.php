<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleModule extends Model
{
    protected $table = "role_modules";

    public function role()
    {
        return $this->belongsTo('App\Models\RoleEnterprise', 'role_enterprise_id', 'id');
    }

    public function module()
    {
        return $this->belongsTo('App\Models\Module', 'module_id', 'id');
    }
}
