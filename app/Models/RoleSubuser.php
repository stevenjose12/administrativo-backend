<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleSubuser extends Model
{
    protected $table = "role_enterprise_subs";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_enterprise_id', 'enterprise_id', 'user_id',
    ];

    public function role_modules()
    {
        return $this->hasMany('App\Models\RoleModule', 'role_enterprise_id', 'role_enterprise_id');
    }

    public function role_enterprise()
    {
        return $this->belongsTo('App\Models\RoleEnterprise', 'role_enterprise_id');
    }
}
