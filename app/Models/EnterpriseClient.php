<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EnterpriseClient extends Model
{
    public function enterprise()
    {
        return $this->belongsTo('App\User', 'enterprise_id', 'id');
    }

    public function client()
    {
        return $this->belongsTo('App\User', 'client_id', 'id');
    }
}
